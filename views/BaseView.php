<?php

// базовий вид
// getnew() - статичний метод отримання нового екземеляра виду
// $name - назва виду
// $cssFiles - файли стилів, які потребує цей вид
// $jsFiles - файли скриптів, які потребує цей скрипт
// echooHtmlHeadSection() - вивід html для підключення css- i js- файлів
// $variables - масив змінних, які доступні у файлі скрипта виду (файл з малої букви)
// доступне зчитування і присвоєння елемента масиву як змінної з ім'ям що дорівнює ключу елемента
//    $this->variable['test'] те саме, що $this->test
// echoo - функція виводу
// 

class BaseView
{
	
	// створення нового екземпляру класу виду
	// якщо є клас з відповідним ім'ям, то повертається екземпляр цього класу (BaseView)
	// в протилежному випадку повертається екземпляр цього класу (VTest)
	// в будь-якому випадку виводиться відповідний скрипт виду (test.php)
	static public function getnew($view)
	{
		$viewN = 'V'.ucfirst($view);
		if (file_exists(VIEWS_DIR.'/'.$viewN.'.php')) {
			require_once(VIEWS_DIR.'/'.$viewN.'.php');
			return new $viewN($view);
		} else {
			return new BaseView($view);
		}
	}	
	
	protected $name;	
	protected $cssFiles = array('main2');
	protected $jsFiles = array();
	public $variables = array();
	
	public function __construct($name,$variables=array())
	{
		$this->name = $name;
		$this->variables = $variables;
	}
	
	// встановлення і видача змінної виду
	public function __get($propertyName)
	{
		if (!isset($this->variables[$propertyName]))
			$this->variables[$propertyName] = '';
		return $this->variables[$propertyName];	
	}
	public function __set($propertyName,$propertyValue)
	{
		$this->variables[$propertyName] = $propertyValue;	
	}

	// в head виводимо включення css i js файлів	
	public function echooHtmlHeadSection()
	{
		foreach ($this->cssFiles as $filename)
			echo '<link rel="stylesheet" href="/css/'.$filename.'.css" />';
		foreach ($this->jsFiles as $filename)
			echo '<script src="/js/'.$filename.'"></script>';
	}
	
	// функція виводу - витягуємо назовні масив $this->variables, підключаємо словник, включаємо файл виводу
	public function echoo()
	{
		extract($this->variables);
		$dict =& $GLOBALS['dict'];
		require_once(VIEWS_DIR.'/'.$this->name.'.tpl');
	}
	
	// додавання змінної у вид, коли магічний метод __get не працює - напр., для масивів
	public function addVariable($name,$value,$asNewArrayKey=false)
	{
		if ($asNewArrayKey) {
			$this->variables[$name][]=$value;
		} else {
			$this->variables[$name]=$value;
		}		
	}
}

?>