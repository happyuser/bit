$(document).ready(function(){
    
    $('#drop').click(function(){
        var id = $('[name=id]:checked');
        var table = $(this).attr('for');
        var str_id='';
        $.each(id, function(index, value){
            str_id=str_id+'id['+index+']='+value.value+'&';
        });
        
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=delete_by_id_basket&name_table='+table+'&'+str_id,
        dataType: "json",
        success: function(data)
        {
            if(data.ok=='true')location.reload();
        }
        });
    });

    $('.save_admin').click(function(){
        var form = $('form.admin');
        $.each(form, function(index, value){
            var str='';            
            var id_admin=$(value).attr('id_admin');
            
            var str = $("form.admin[id_admin="+id_admin+"]").serialize();
            console.log(str);
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/controllers/ajax.php",
                    data: 'name_func=edit_admin&'+str+'&id='+id_admin,
                    dataType: "json",
                    success: function(data)
                    {
                    }
                });
        });
                $('.status_message_ok').animate({'opacity':'1'},1000);
                $('.status_message_ok').animate({'opacity':'0'},500);
    });
    
    /*
    $('.save_admin').click(function(){
        var id=$(this).parent().parent().attr('id');
        var status=$('[name=status_'+id+']').val();
        console.log(status);
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=update_admin_status&status='+status+'&id='+id,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true'){
                    $('.status_message_ok').animate({'opacity':'1'},1000);
                    $('.status_message_ok').animate({'opacity':'0'},500);
                }
            }
        });
    });
    */
});