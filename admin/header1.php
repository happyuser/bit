<?
session_start();
if(empty($_SESSION['admin_id'])) header("Location: login.php");
ini_set('memory_limit', '2048M');
include ('../controllers/functions.php');
//Це потрібно щоб можно було нормально пости на стіну кидати із адмінки. Щоб система не сприймала адміна як юзера
    delCookie('id_user');
    delCookie('id_sponsor');

include ('../controllers/sessions.php');
include ('languages/english.php');
connect();
include ('../controllers/main.php');
include ('../controllers/taras/filter.php');
include ('../controllers/taras/html_dom.php');
include ('../controllers/taras/common.inc');
include ('../controllers/pages_table.php');
?>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>BIT admin</title>
<!-- css --->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/jquery-ui.css" rel="stylesheet" />
        <style>
        img.load_ajax {
          position: absolute;
          top: 160px;
          left: 400px;
          z-index: 999;
          display: none;
        }
        img.load_ajax2 {
          position: absolute;
          top: 40%;
          left: 45%;
          z-index: 999;
          display: none;
        }
        </style>
<!-- script --->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="js/jquery-2.0.0.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="js/datepicker-de.js" type="text/javascript"></script>
        <script src="js/bootstrap-carousel.js" type="text/javascript"></script>
        <script src="js/index.js" type="text/javascript"></script>
		<script src="js/tables.js" type="text/javascript"></script>
		<script src="js/filter.js" type="text/javascript"></script>
        <script src="js/jquery.textchange.min.js" type="text/javascript"></script>
        <script src="js/jquery.ui.timepicker.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="no_activ_body"></div>
    <div class="container-fluid container_fluid_registration">
        <div class="row-fluid menu" style="height: 40px;">
			<? $s = (preg_match('/admin\/(.*\.php)/', $_SERVER['REQUEST_URI'], $a) ? $a[1] : '');?>
            <ul class="menu_header navbar-fixed-top">
				<? if($_SESSION['status']!='hostess'){ ?>
                <li class="<?php print ($s == 'mitglieder_registered.php' or $s == 'mitglieder_kandidate.php' or $s == 'mitglieder_approve.php') ? active : ''; ?>"><a href="mitglieder_registered.php">Mitglieder</a></li>
                <li class="<?php print ($s == 'event_list.php') ? active : ''; ?>"><a href="./event_list.php">Veranstaltung</a></li>
                <li class="<?php print ($s == 'guest_list_eingeladene.php' or $s == 'guest_list_angemeldete.php' or $s == 'guest_list_bestatigte.php' or $s == 'guest_list_kandidaten.php') ? active : ''; ?>"><a href="./guest_list_eingeladene.php">Gästeliste</a></li>
                <li class="<?php print ($s == 'verteiler.php') ? active : ''; ?>"><a href="./verteiler.php">Verteiler</a></li>
                <li class="<?php print ($s == 'registration.php') ? active : ''; ?>"><a href="./registration.php">Einlass</a></li>
                <li class="<?php print ($s == 'sponsoren.php') ? active : ''; ?>"><a href="./sponsoren.php">Sponsoren</a></li>
                <li class="<?php print ($s == 'werbung.php') ? active : ''; ?>"><a href="./werbung.php">Werbung</a></li>
                <li class="<?php print ($s == 'wall.php') ? active : ''; ?>"><a href="./wall.php">Pinnwand</a></li>
				<? if($_SESSION['status']=='superadmin') {
					$header="<li class='";
					if($s=="administration.php"){$header.="active";} 
					$header.="'><a href='./administration.php'>Administration</a></li>"; 
					echo $header;} 
				?>
                <!--<li class="<?php print ($s == 'import.php') ? active : ''; ?>"><a href="./import.php">Import</a></li>-->
                <li class="archivieren_btn"><div><a href='/arch/index.php'>Archiv</a></div></li>
                <?}else{?>
					<li class="<?php print ($s == 'registration.php') ? active : ''; ?>"><a href="./registration.php">Einlass</a></li>
				<?}?>
				<li class="" style="float: right;"><a href="ses_des.php">Ausloggen</a></li>	
            </ul>
        </div>
<img class="load_ajax" src="../images/load.gif"/>
<img class="load_ajax2" src="../images/load2.gif" width="90px"/>