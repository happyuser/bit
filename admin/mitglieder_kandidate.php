<?
include ('header1.php');
include_once ('../controllers/func_creat_mitglieder_kandidaten.php');
?>

<script src="js/registration.js" type="text/javascript"></script>

<div class="row-fluid menu">
    <div class="input-append input_append_mitglieder_kandidate">
        <input type="text" class="search-query" value="<?=$_GET['search']?>"/>
        <button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
        <div class="search_clear"><i class="icon-remove"></i></div>
    </div>
    <div class="pagination pagination_mitglieder_kandidate">
        <ul>
            <?if(isset($data['pages_number'])) echo $data['pages_number'];?>
        </ul>
    </div>
	<ul class="sub_menu_header nav-tabs">
		<li class="<?php print ($s == 'mitglieder_registered.php') ? active : ''; ?>"><a href="./mitglieder_registered.php">Mitglieder</a></li>
		<li class="<?php print ($s == 'mitglieder_kandidate.php') ? active : ''; ?>"><a href="./mitglieder_kandidate.php">Bewerber</a></li>
		<?if($_SESSION['status']!='praktikanten'){?>
			<li class="<?php print ($s == 'mitglieder_approve.php') ? active : ''; ?>"><a href="./mitglieder_approve.php">Geänderte Mitglieder</a></li>
		<?}?>
	</ul>
</div>
<div class="pop_window" id="overflow" style="display: none">
	<button class="close_window pull-right" style="margin-right: -10px;margin-top: -10px">
		<i class="icon-remove icon-white"></i>
	</button>
	<h6 id="overtext" style="text-align:center "></h6>
</div>
<div class="container-fluid">
    <div class="row-fluid header">
        <div class="span4">
            <button class="btn" title="Bestätigen" id="approve"><i class="icon-thumbs-up"></i></button>
            <button class="btn" title="Ablehnen" id="decline"><i class="icon-thumbs-down"></i></button>          
            <button class="btn" title="Als Mitglied hinzufugen" id="new_user"><i class="icon-plus"></i></button>
            <button class="btn" id="without_filters" title="Filter an/ausschalten" style="margin: 2px; float: left;"><i class="icon-filter no_filters"></i></button>
        </div>

    </div>
   
     <?php  if(!isset($data['pages_number'])){echo $data['empty'];}
            else{    ?>

	<div class="row-fluid body view_table">
		<form class="table">
			<div class="no_sroll no-scroll-bewerber">
				<table class="table table-bordered">
					<thead>
						<? if(isset($data['header'])){echo $data['header'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table'])){echo $data['table'];}?>	
					</tbody>
				</table>
			</div>
			<div class="scroll scroll_bewerber">
				<table class="table table-bordered table_width_cell">
					<thead>
						<? if(isset($data['header_slider'])){echo $data['header_slider'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table_slider'])){echo $data['table_slider'];}?>	
					</tbody>
				</table>
			</div>
		</form>
	</div>
<?php } ?>
</div>
</body>
</html>