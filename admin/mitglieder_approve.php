<?
include_once ('header1.php');
include_once('../controllers/func_creat_mitglieder_approve.php');
?>

<div class="row-fluid menu">
    <div class="input-append input_append_mitglieder_approve">
        <input type="text" class="search-query" value="<? if(isset($_GET['search'])){echo $_GET['search'];}?>"/>
        <button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
        <div class="search_clear"><i class="icon-remove"></i></div>
    </div>
    <div class="pagination pagination_mitglieder_approve">
        <ul>
            <?if(isset($data['pages_number'])) echo $data['pages_number'];?>
        </ul>
    </div>
	<ul class="sub_menu_header nav-tabs">
		<li class="<?php print ($s == 'mitglieder_registered.php') ? active : ''; ?>"><a href="./mitglieder_registered.php">Mitglieder</a></li>
		<li class="<?php print ($s == 'mitglieder_kandidate.php') ? active : ''; ?>"><a href="./mitglieder_kandidate.php">Bewerber</a></li>
		<?if($_SESSION['status']!='praktikanten'){?>
			<li class="<?php print ($s == 'mitglieder_approve.php') ? active : ''; ?>"><a href="./mitglieder_approve.php">Geänderte Mitglieder</a></li>
		<?}?>
	</ul>
</div>
<div class="pop_window" id="overflow" style="display: none">
	<button class="close_window pull-right" style="margin-right: -10px;margin-top: -10px">
		<i class="icon-remove icon-white"></i>
	</button>
	<h6 id="overtext" style="text-align:center "></h6>
</div>
<div class="container-fluid">
    <div class="row-fluid header">
        <div class="span4">
            <button class="btn" id="edit_true" title="Bestätigen"><i class="icon-thumbs-up"></i></button>  
            <button class="btn" id="edit_false" title="Ablehnen"><i class="icon-thumbs-down"></i></button>
            <button class="btn" id="without_filters" title="Aktivieren den filter zu deaktivieren" ><i class="icon-filter no_filters"></i></button>        
        </div>
    </div>
   
     <?php  if(!isset($data['pages_number'])){echo $data['empty'];}
            else{    ?>

	<div class="row-fluid body view_table">
		<form class="table approve_table">
			<div class="no_sroll no_scroll_approve">
				<table class="table table-bordered table_mid">
					<thead>
						<? if(isset($data['header'])){echo $data['header'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table'])){echo $data['table'];}?>	
					</tbody>
				</table>
			</div>
			<div class="scroll scroll_geanderte_mitglieder scroll_approve">
				<table class="table table-bordered table_mid table_width_cell">
					<thead>
						<? if(isset($data['header_slider'])){echo $data['header_slider'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table_slider'])){echo $data['table_slider'];}?>	
					</tbody>
				</table>
			</div>
            <a class="btn btn-success approve_check_all">Check all</a>
		</form>
	</div>
<?php } ?>
</div>


<script>
    /* Check all checkbox */
    $(function(){
        $('.approve_check_all').click(function(){
            if($('.no_sroll.no_scroll_approve table tr td input').attr('checked') != 'checked'){
                $(".no_sroll.no_scroll_approve table tr td input").attr('checked', 'checked');
            }
            else{
                $('.no_sroll.no_scroll_approve table tr td input').removeAttr('checked');
            }
        });
    });
</script>