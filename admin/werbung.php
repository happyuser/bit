<?
include_once ('header1.php');
include_once('../controllers/func_creat_werbung.php');
?>
<script src="js/manadge_reclama.js"></script>
<script src="js/preload.js"></script>


<div class="row-fluid header"style="margin: 20px auto 0px;">
	<form class="table" style="" action="" method="post" enctype="multipart/form-data">
		<div id="ad" >
			<div class="row-fluid left_block">
				<div class="list">
					<table class="table_werbung">
						<tbody>
							<tr>
								<th></th>
								<th>Bild hochladen</th>
								<th></th>
								<th>Zeit</th>
								<th>Reihenfolge</th>
							</tr>
							<?if(isset($data['block1'])){echo $data['block1'];}
							else{?>
								<tr data-event="1">
									<th><label>Werbung oben 1:</label></th>
									<th>
										<div class="input_style">
											<div class="bg_input_file">Auswählen</div>
											<input class="file" type="file" name="file1[]" style='margin-bottom: 0px; height: 30px; width: 148px;'>
										</div>
									</th>
									<th><input type="text" name="link1[]" placeholder="LINK zur Seite" class="search-query" style="height: 30px; border-radius: 4px; width: 228px;"/></th>
									<th><input type="text" name="wait1[]" placeholder="Zeit" class="zeit" style="height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;"/></th>
									<th><input type="text" name="sort1[]" placeholder="Sort" class="zeit" style="height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;"/></th>
									<th><span class='add_ad' title='Hinzufügen'>+</span></th>
								</tr>
							<?}
							if(isset($data['block2'])){echo $data['block2'];}
							else{?>
								<tr data-event="2">
									<th><label>Werbung oben 2:</label></th>
									<th>
										<div class="input_style">
											<div class="bg_input_file">Auswählen</div>
											<input class="file" type="file" name="file2[]" style='margin-bottom: 0px; height: 30px; width: 148px;'>
										</div>
									</th>
									<th><input type="text" name="link2[]" placeholder="LINK zur Seite" class="search-query" style="height: 30px; border-radius: 4px; width: 228px;"/></th>
									<th><input type="text" name="wait2[]" placeholder="Zeit" class="zeit" style="height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;"/></th>
									<th><input type="text" name="sort2[]" placeholder="Sort" class="zeit" style="height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;"/></th>
									<th><span class='add_ad' title='Hinzufügen'>+</span></th>
								</tr>
							<?}
							if(isset($data['block3'])){echo $data['block3'];}
							else{?>
								<tr  data-event="3">
									<th><label>Werbung oben 3:</label></th>
									<th>
										<div class="input_style">
											<div class="bg_input_file">Auswählen</div>
											<input class="file" type="file" name="file3[]" style='margin-bottom: 0px; height: 30px; width: 148px;'>
										</div>
									</th>
									<th><input type="text" name="link3[]" placeholder="LINK zur Seite" class="search-query" style="z-index: 0; height: 30px; border-radius: 4px; width: 228px;"/></th>
									<th><input type="text" name="wait3[]" placeholder="Zeit" class="zeit" style="height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;"/></th>
									<th><input type="text" name="sort3[]" placeholder="Sort" class="zeit" style="height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;"/></th>
									<th><span class='add_ad' title='Hinzufügen'>+</span></th>
								</tr>
							<?}?>
							<tr class="werbung_links"></tr>
							<?if(isset($data['block4'])){echo $data['block4'];}
							else{?>
								<tr  data-event="4">
									<th><label>Werbung rechts:</label></th>
									<th>
										<div class="input_style">
											<div class="bg_input_file">Auswählen</div>
											<input class="file" type="file" name="file4[]" style='margin-bottom: 0px; height: 30px; width: 148px;'>
										</div>
									</th>
									<th><input type="text" name="link4[]" placeholder="LINK zur Seite" class="search-query" style="z-index: 0; height: 30px; border-radius: 4px; width: 228px;"/></th>
									<th><input type="text" name="wait4[]" placeholder="Zeit" class="zeit" style="height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;"/></th>
									<th><input type="text" name="sort4[]" placeholder="Sort" class="zeit" style="height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;"/></th>
									<th><span class='add_ad' title='Hinzufügen'>+</span></th>
								</tr>
							<?}
							if(isset($data['block5'])){echo $data['block5'];}
							else{?>
								<tr data-event="5">
									<th><label>Video werbung:</label></th>
									<th colspan="3"><input type="text" name="link51" placeholder="LINK zur Seite" class="search-query" style="z-index: 0; height: 30px; border-radius: 4px; width: 100%;"/></th>
								</tr>
							<?}?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="werbung_buttons">
            <input class="btn" title="Speichern" type="submit" name="save" value="Speichern">
			<input class="btn" title="Abbrechen" type="submit" name="clear" value="Abbrechen">
		</div>
	</form>
</div>