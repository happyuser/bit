<?include('header.php')?>
<div class="row-fluid body">
    <div class="span4 left_form">
        <div class="form_block_event">
            <div class="row-fluid">
                <label class="span5">Name des Events: </label>
                <input class="span7" type="text" placeholder="Advanced text input"/>
            </div>
            <div class="row-fluid">
                <label class="span5">Eventtermine: </label>
                <input class="span7" type="text" placeholder="Advanced text input"/>
            </div>
            <div class="row-fluid">
                <label class="span5">Ort des Events: </label>
                <input class="span7" type="text" placeholder="Advanced text input"/>
            </div>
            <div class="row-fluid">
                <label class="span5">Ort des Events: </label>
                <textarea class="span7" placeholder="Text area"></textarea>
            </div>
            <div class="row-fluid">
                <label class="span5">Ort des Events: </label>
                <textarea class="span7" placeholder="Text area"></textarea>
            </div>
            <div class="row-fluid">
                <label class="span5">Ort des Events: </label>
                <input class="span7" type="text" placeholder="Advanced text input"/>
            </div>
            <div class="row-fluid">
                <label class="span5">Ort des Events: </label>
                <select>
                    <option></option>
                </select>
            </div>
        </div>
    </div>
    <div class="span8 right_block_event">
        <div class="row-fluid header">
            <div class="span6">
                <label style="float: left;">die Gaste</label>
                <select style="margin-left: 10px;">
                    <option></option>
                </select>
            </div>
            <div class="span6">
                <div class="input-append">
                    <input type="text" class="search-query" />
                    <button type="submit" class="btn" id="appendedInputButtons">Search</button>
                </div>
            </div>
        </div>
        <div class="row-fluid body">
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
            <div class="event span6">
                <div class="row-fluid">
                    <div class="span3 img_event">
                        <img src="images/event_img.png"/>
                    </div>
                    <div class="span9 info_event">
                        <div class="row-fluid">
                            <span class="span7">Name Vorname, musician</span>
                            <span class="span5"><a href="">Full info >></a></span>
                        </div>
                        <span class="row-fluid">[Branche]</span>
                        <div class="row-fluid">
                            <span class="span5">[Arbeitgeber]</span>
                            <span class="span7">[Position im Unternehmen]</span>
                        </div>
                        <input type="checkbox"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>