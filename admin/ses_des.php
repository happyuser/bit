<?php
session_start();
session_destroy();

if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
	if(!empty($params)){
		setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
	);}
}

if(isset($_COOKIE['admin_id'])){
	setcookie('admin_id', '', time() - 42000);
}
if(isset($_COOKIE['PHPSESSID'])){
	setcookie('PHPSESSID', '', time() - 42000);
}
header("Location: /admin");
?>