<?php
session_start();
ini_set('display_errors', '1');
include ('../../controllers/taras/common.inc');
include ('../../controllers/functions.php');
connect();
if (!isset($_SESSION['admin_id'])) {
	header("Location: /admin");
	exit;
}
$success=select_DB('member',array('email_privat'=>$_POST['email_privat']));
if($success[0][id]==$_POST['id'])$success=select_DB('reg_sponsor',array('email'=>$_POST['email_privat']));
if($success){
    echo 'problem email';
    exit;
}
function image_upload(){
    if(!$_FILES[photo][size]){return '';exit;}
    $type=explode('/',$_FILES['photo']['type']);
                $chars = 'abcdefABCDEF';
                $numChars = strlen($chars);
                $string = '';
                for ($i = 0; $i < 3; $i++) {
                    $string .= substr($chars, rand(1, $numChars) - 1, 1);
                }
                $gen_key=$string;
    $name=time().$gen_key;
    $target_path = $name. "." . $type[1];
    
    if(@move_uploaded_file($_FILES['photo']['tmp_name'], '../../uploads/'.$target_path)) {
    
        $INFO=getimagesize('../../uploads/'.$target_path);
        if($INFO[0]>$INFO[1]){
            $new_width=$INFO[1]/4;
            $new_height=$INFO[1];
            $new_width=$new_width*3;
            $center_x=($INFO[0]-$new_width)/2;
        }else{
            $new_height=$INFO[0]/3;
            $new_width=$INFO[0];
            $new_height=$new_height*4;
            $center_y=($INFO[1]-$new_height)/2;
        }
        if($center_x<0)$center_x=0;
        if($center_y<0)$center_y=0;
        crop('../../uploads/'.$target_path,'../../uploads/'.$target_path,$center_x,$center_y,$new_width,$new_height);
    }
    return array('info_img'=>$INFO,'name'=>$name. "." . $type[1]);
}


$server_path = $_SERVER['SERVER_NAME'];
$str = $strVal = '';
$i = 0;
$j = 0;
$date = date('Y-m-d');
$chars = '8bm9e19th2lqqk6i41if083b94';
$numChars = strlen($chars);
$string = '';
for ($i = 0; $i < 10; $i++) $string .= substr($chars, rand(1, $numChars) - 1, 1);
$gen_key=$string;
$id_user = $_POST['id'];

$query = "SELECT * FROM member WHERE id=$id_user";
$result = mysql_query($query);
$rows = mysql_num_rows($result);
$row = mysql_fetch_assoc($result);
$arr_new_val=array();
foreach($_POST as $key=>$val){
    if ($key == "id") continue;
    if (strpos($key, "hefredakteure")) $key = "type_account";
    if ($val!=$row[$key]){
        $arr_new_val[$key]=$val;
        $insert = "INSERT INTO `logs_users_edit` (`id_user`,`type`,`last_val`,`new_val`,`date`, `gen_key`,`id_admin`) VALUES ($id_user,'$key','$row[$key]','$val','$date','$gen_key','".$_SESSION['admin_id']."')";
        $query_log_event = mysql_query($insert);       
    }
}

if ($arr_new_val) { //якщо зміни були то відзначимо в полі апрув
    $s = '';
    foreach ($arr_new_val as $key=>$val){
        if (($val !== false) && ($key != 'id')) $s .= "`" . $key . "`='" . $val . "'";
    }
	$s .= "`letzter`='".$_SESSION['admin_id']."'";
    $query = "UPDATE `member` SET " . str_replace("'`", "', `", $s) . " WHERE id=$id_user";
    if (!mysql_query($query)) {
		print "Error in:<br />$query";
		exit;
	}

    if ($_ACCESS != 'superadmin') {
        $query_approve = "UPDATE `member` SET `approve`='3' WHERE id=$id_user";
        $result_approve = mysql_query($query_approve);
    }
    $query_approve = "UPDATE `member` SET active=1 WHERE id=$id_user";
    $result_approve = mysql_query($query_approve);
}

if (count($_FILES) and ($s = image_upload())) {
	$query = "UPDATE `member` SET photo='$s[name]' WHERE id=$id_user";
	if (!mysql_query($query)) {
		print "Error in:<br />$query";
		exit;
	}
    
    crop_img_profile(array('id_user'=>$id_user, 'img'=>'../../uploads/'.$s[name],'img_crop'=>'../../uploads/crop_img/'.$s[name], 'img_h'=>$s[info_img][1],'img_w'=>$s[info_img][0],'x'=>0,'y'=>0,'w'=>$s[info_img][0],'h'=>$s[info_img][0]));
}
header("Location: ../mitglieder_registered.php");
