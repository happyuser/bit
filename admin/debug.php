<?php 
if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
	ini_set('display_errors','0');
	require_once 'exception/ESecurity.php';
	throw new ESecurity();
}

if (!defined('BIT_DEBUG')) {
	if (!empty($_SERVER["HTTP_X_REAL_IP"]) and $_SERVER["HTTP_X_REAL_IP"]=='194.44.136.110') {
		define('BIT_DEBUG',true);
		ini_set('display_errors','1');
	} else {
		define('BIT_DEBUG',false);
	}
}
?>
