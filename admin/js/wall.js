$('.add_post').click(function(){
    $('.img_box').html('');
    $('.no_activ_body').show();
    $('.pop_window.new_post').show();
});

$('.edit_post').click(function(){
    if($('[type="checkbox"]').is(':checked')){
        $('.img_box').html('');
        var id_post=$('[type="checkbox"]:checked:last').val();
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=get_info_post_comment&type=post&id='+id_post,
            dataType: "json",
            success: function(data)
            {
                data=data[0];
                $('.pop_window.change_post [name=type]').val(data.type);
                $('.pop_window.change_post textarea').val(data.text);
                if(data.attached_file)
                    $.each(data.attached_file, function(index,value){
                        var file=value.split('.');
                        if(file[1]!=='mpeg' && file[1]!=='mp4')
                            $('.pop_window.change_post .img_box').append('<div class="edit_img_for_post"><img id="'+value+'" src="../uploads/'+value+'" width="200"><div class="delete_img"><i class="icon-remove icon-white"></i></div></div>');
                        else
                            $('.pop_window.change_post .img_box').append('<div class="edit_img_for_post"><video id="'+value+'" src="../uploads/'+value+'" width="400" controls></video><div class="delete_img"><i class="icon-remove icon-white"></i></div></div>');
                    });
            }
        });
        $('.no_activ_body').show();
        $('.pop_window.change_post').show().attr('id_post',id_post);
    }
});

$('.btn_edit').click(function(){
    var id_post=$('[type="checkbox"]:checked:last').val();
    var images_str='',text='&text='+$('.pop_window.change_post:visible textarea').val();
    $.each($('.pop_window.change_post:visible .img_box img,.pop_window.change_post:visible .img_box video'), function(index,value){
        images_str+='&images[]='+$(value).attr('id');
    });
    var type_post=$('.pop_window:visible [name=type]').val();
    
    $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=edit_post&type=post&id='+id_post+'&'+images_str+'&text='+text+'&type_post='+type_post,
        dataType: "json",
        success: function(data)
        {
            location.reload();
        }
    });
})

$('[name="files_new"]').change(function(){
    var fd = new FormData();
    $.each($(this)[0].files, function(index,value){
        fd.append('img[]', value);
    });
    console.log(fd);
    $.ajax({
        type: 'POST',
        url: 'upload.php',
        data: fd,
        processData: false,
        contentType: false,
        dataType: "json",
        success: function(data){
            $.each(data.name_img,function(index,value){
                var file=value.split('.');
                if(file[1]!=='mpeg' && file[1]!=='mp4')
                    $('.img_box').append('<div class="edit_img_for_post"><img id="'+value+'" src="../uploads/'+value+'" width="200"><div class="delete_img"><i class="icon-remove icon-white"></i></div></div>');
                else
                    $('.img_box').append('<div class="edit_img_for_post"><video id="'+value+'" src="../uploads/'+value+'" width="400" controls></video><div class="delete_img"><i class="icon-remove icon-white"></i></div></div>');
            });
        }
    });
});

$('.btn_sender').click(function(){
    var files='',text='&text='+$('.pop_window:visible textarea').val();
    var type=$('.pop_window:visible [name=type]').val();
    $.each($('.pop_window:visible .img_box img,.pop_window:visible .img_box video'), function(index,value){
        files+='&files[]='+$(value).attr('id');
    });
    var str='&name_func=well_admin_post'+files+text+'&type='+type;
    $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: str,
        dataType: "json",
        success: function(data)
        {
            location.reload();
        }
    });
});

$('.pop_window').on('click','.delete_img',function(){
    $(this).parent().remove();
});