$(document).ready(function(e){
    $("select.filter").change(function(){
        var name=$(this).attr('name');
		var select=$(this).val();
        var tmp={};
		var GET={};
		var items=location.search;
		var pos=items.indexOf('?');
		if(pos==0){
			items=items.substr(1).split("&");
			for (var index=0; index<items.length; index++) {
				tmp=items[index].split("=");
				GET[tmp[0]]=decodeURIComponent(tmp[1]);
			}
		}
        var str='';
        var arr={};
        GET[name]=select;
        $.each(GET, function(index, value) {
            if(value=='*all')
                delete arr[index];
			else if(value=='')
				arr[index]='';
            else 
                arr[index]=value;
        }); 
		var i=0;		
        $.each(arr, function(index, value) {
			if(i==0){
				str=index+'='+value;
			}
			else{
				str=str+'&'+index+'='+value;
			}
			i++;
        });
        str.replace(/%20/g,"") // = "прошел еще прошел"       
        window.location.replace (window.location.pathname+'?'+str); 
    });
	
    $('#without_filters').click(function(){
        var GET=parseGetParams();
        var str='';
        var arr={};
        $.each(GET, function(index, value){
			if((index.search('filter')!= -1)){
				delete arr[index];
			}
			else{
				arr[index]=value;
			}
        });
		var i=0;
        $.each(arr, function(index, value) {
			if(i==0){
				str=index+'='+value;
			}
			else{
				str=str+'&'+index+'='+value;
			}
			i++;
        });
       
        window.location.replace (window.location.pathname+'?'+str);  
    });
});