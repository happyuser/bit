//загрузка фото для прелоада и загрузки данных в базу(переписать)!
    function load_photo(for_){
        var fd = new FormData();
        fd.append('img', $('#'+for_+' .file')[0].files[0]);
        fd.append('crop', 'true');
        response = $.ajax({
            type: 'POST',
			async: false,
            url: 'upload.php',
            data: fd,
            processData: false,
            contentType: false,
            dataType: "json"}).responseText;
		values = $.parseJSON(response);
		return values.name_img;
    }

$(document).ready(function(){
//сохранение данных в базу    
    $('form .btn.save').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        var for_=$(this).parent().parent().attr('class');
        var id=$('form.'+for_).attr('id');
		var photo=load_photo(for_);
        var str = $("form."+for_).serialize();
        if(for_=='new_sponsor')str+='&name_func=new_sponsor&photo='+photo; else str+='&name_func=change_sponsor&id='+id+'&photo='+photo;
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: str,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true'){
                    $('.load_ajax2').hide();
                    location.reload();
                }
                else if(data=='false'){
					$('.load_ajax2').hide();
					alert('Change email!');
				}
            }
        });
        return false;
    });
//открытие модального окна создания нового спонсора  
    $('.btn.new_sponsor').click(function(){
        $('.pop_window').hide();
        $('#new_sponsor.pop_window').slideDown(400);
        $('.no_activ_body').show();
    });
//открытие модального окна редактирования спонсора
    $('.btn.change_sponsor').click(function(){
        var id = $('.one_checkbox_active:checked').last().val();
        if (!id){
			alert("Nicht gewahlt Sponsor!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			$('#change_sponsor.pop_window').slideDown(100);
			$('.no_activ_body').show();
			$('form.change_sponsor').attr('id',id);
			$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: 'name_func=get_info_DB_id&name_table=reg_sponsor&id='+id,
				dataType: "json",
				success: function(data)
				{
					$.each(data.DB_member, function(index, value) {
						if(value){
							if(index=='photo' && value){		
								$('.change_sponsor').find('.bg_input_file').append('<img id="image" src="#" alt="" />');
								$('.change_sponsor').find('#image').attr('src', '../uploads/'+value);
							}
							else if(index!='land'){
								$('.change_sponsor [name='+index+']').val(value);
							}
							else{
								$('.change_sponsor #type_of_land [value="'+value+'"]').attr("selected", true);
							}
						}
					});
					$.ajax({
						type: "POST",
						url: "../controllers/ajax.php",
						data: 'name_func=sponsor_event_type_to_paste&id='+id,
						dataType: "json",
						success: function(data)
						{
							var i=0;
							$.each(data, function(index, value) {
								if(value){
									var event_list='#type_of_event [value="'+value.event_type+'"]';
									if(i>0){
										var clone=$('.change_sponsor').find('.type_to_copy').clone(true);
										$('.change_sponsor .type_to_copy').after(clone);
										var target=$('.change_sponsor .type_to_copy').nextAll('.control-group:first');
										$(target).children('.controls').append('<div class="close" style="margin-bottom: 10px">&times;</div>');
										var select_val=$(target).find('#type_of_event').val();
										$(target).find('#type_of_event [value="'+select_val+'"]').attr("selected", false);
										$(target).find(event_list).attr("selected", "selected");
										$(target).attr('class','control-group');	
									}
									else{
										$('.change_sponsor .type_to_copy '+event_list).attr("selected", "selected");
									}
									i++;
								}
							});
						}
					});
				}
			});
            $('.load_ajax2').hide();
			return false;
		}
    });
//открытие модального окна привязки спонсоров к событиям
    $('#sponsor_event').click(function(){      
        var id = $('.one_checkbox_active:checked').last().val();
        if (!id){
			alert("Nicht gewahlt Sponsor!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: '&name_func=sponsor_event_type&id='+id,
				dataType: "json",
				success: function(data)
				{
					$('.load_ajax2').hide();      
					$('#history_event tbody').html('<tr class="start_history"></tr>');
					var str='';
					$.each(data, function(index, value) {
						str=str+"<tr>";
						$.each(value, function(ind, val) {
							str=str+"<td>"+val+"</td>";
						});	
						str=str+"</tr>";
					});	
					$('.start_history').after(str);
				}
			});
			$('#history_event.pop_window').slideDown(100);
			$('.no_activ_body').show();
		}
    });
//открытие модального окна привязки спонсора к типам событий
    $('#sponsor_event_type').click(function(){      
        var id = $('.one_checkbox_active:checked').last().val();
        if (!id){
			alert("Nicht gewahlt Sponsor!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: '&name_func=get_event_type_sponsor&id='+id,
				dataType: "json",
				success: function(data)
				{
					$('.load_ajax2').hide();      
					$('#event_type tbody').html('<tr class="start_history"></tr>');
					var str='';
					$.each(data, function(index, value) {
						str=str+"<tr>";
						$.each(value, function(ind, val) {
							str=str+"<td>"+val+"</td>";
						});	
						str=str+"</tr>";
					});	
					$('.start_history').after(str);
				}
			});
			$('#event_type.pop_window').slideDown(100);
			$('.no_activ_body').show();
		}
    });
//копирование типов событий в модальных окнах создания и редактирования спонсора
	$(".add_new_type_point").click(function(){
		var clone=$(this).closest('.control-group').prevAll( '.type_to_copy' ).clone(true);
		$(this).closest('.control-group').before(clone);
		var target=$(this).closest('.control-group').prevAll( '.control-group:first' );
		$(target).children('.controls').append('<div class="close close_sponsoren" style="margin-bottom: 10px">&times;</div>');
		var select_val=$(target).find('#type_of_event').val();
		$(target).find('#type_of_event [value="'+select_val+'"]').attr("selected", false);
		$(target).attr('class','.control-group');
		$(target).attr('class','control_group_sponsoren');
	});
//удаление скопированного типа событий в модальных окнах создания и редактирования спонсора
	$('.pop_window').on('click','.close',function(){
        $(this).parent().parent().remove();
    });
//удаление спонсора   
    $('#drop_sponsor').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();

        var str_id=$('.one_checkbox_active:checked').serialize();
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=delete_by_sponsor&'+str_id,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true'){
                    $('.load_ajax2').hide();
                    location.reload();
                }
				else alert('Error BD!');
            }
        });
    })
});