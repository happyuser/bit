//функция подготовки GET строки поиска данных
function search(val){
	var GET=parseGetParams();
	var str='';
	var arr={};
	$.each(GET, function(index, value) {
		arr[index]=value;
	});
	arr['search']=val;
	$.each(arr, function(index, value) {
		val=str.length;
		if(val>0){
			str=str+'&'+index+'='+value;
		}
		else{
			str=str+index+'='+value;
		}
	});
	str.replace(/%20/g,"") // = "прошел еще прошел"
	window.location.replace (window.location.pathname+'?'+str);	
}

$(document).ready(function(){
    $('#excel_import').change(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
        $(this).attr('disabled',true);
        $('[for=excel_import] i').removeClass('icon-share').addClass('icon-time').parent().attr('disabled',true);
        var fd = new FormData();
        fd.append('file', $($(this))[0].files[0]);
        $.ajax({
          type: 'POST',
          url: 'import_xls_file.php',
          data: fd,
          processData: false,
          contentType: false,
          dataType: "html",
          success: function(data) {
            location.reload();
          },
          error: function(data) {
            console.log(data);
          }
        });
        return false; 
    });
//подтверждение заявки гостя
    $('.btn#bestatigte').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
		var str_id=$('.one_checkbox_active:checked').serialize();
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=bestatigte&'+str_id,
            dataType: "json",
            success: function(data)
            {
                if(data==true)location.reload();
            }
        }); 
    });

    $('select[name=salutation]').change(function(){
        if($(this).children('option:first').is(':selected'))
            $('select[name=before_title]').find('option:first').prop('selected',true);
        else
            $('select[name=before_title]').find('option:last').prop('selected',true);
    });
    
    $('select[name=before_title]').change(function(){
        if($(this).children('option:first').is(':selected'))
            $('select[name=salutation]').find('option:first').prop('selected',true);
        else
            $('select[name=salutation]').find('option:last').prop('selected',true);
    });

    $('.search_clear').click(function(){
        var val=$(this).prev().val();
        var GET=parseGetParams();

        var str='';
        var arr={};
        
        //arr['search']='';
        $.each(GET, function(index, value) {
            arr[index]=value;
        });
        delete arr.search;
        if(arr)
        $.each(arr, function(index, value) {
            val=str.length;
			if(val>0){
				str=str+'&'+index+'='+value;
			}
			else{
				str=str+index+'='+value;
			}
        });
        str.replace(/%20/g,"") // = "прошел еще прошел"
        window.location.replace (window.location.pathname+'?'+str);
    });
    
    $('#approve').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
		var check=$('.one_checkbox_active:checked').serialize();
        if (!check){
			$('.no_activ_body').hide();
			$('.load_ajax2').hide();
			alert("Nicht gewahlt Bewerber!");
		}
        else{
            $('.no_activ_body').show();
			$('.load_ajax2').show();
			$.ajax({
                type: "POST",
                url: "../controllers/ajax.php",
                data: check+'&name_func=kandidaten_decision&status=true',
                dataType: "json",
                success: function(data)
                {
                    if(data.ok=='true')location.reload();
    				else alert('Fehler beim Schreiben in Datenbank!');
                }
            });
           	$('.load_ajax2').hide();
			$('.no_activ_body').hide();		
        }
    });
//отмена заявки поданной пользователем на событие
    $('#decline').click(function(){   
        var check=$('.one_checkbox_active:checked').serialize();
		if (!check){
			alert("Nicht gewahlt Bewerber!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: check+'&name_func=kandidaten_decision&status=false',
				dataType: "json",
				success: function(data)
				{
					if(data.ok=='true'){
						$('.load_ajax2').hide();
						$('.no_activ_body').hide();
						location.reload();
					}
					else{
						$('.load_ajax2').hide();
						$('.no_activ_body').hide();
						alert('Fehler beim Schreiben in Datenbank!');
					}
				}
			});
		}
    });
//создание нового пользователя Mitglieder Bewerber
    $('#new_user').click(function(){
		var str = $("form.table [name='id[]']").serialize();
		if (!str){
			alert("Nicht gewahlt Bewerber!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			console.log(str);
			$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: str+'&name_func=guest_create_user',
				dataType: "json",
				success: function(data)
				{
					if(data==true)location.reload();
				}
			});
		}
    });
    $('#delete').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
        var table = $(this).attr('for');
		var str_id=$('.one_checkbox_active:checked').serialize();
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=delete_by_id&name_table='+table+'&'+str_id,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true')location.reload();
            }
        });
    });

    $('#edit_true').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
		var str_id=$('.one_checkbox_active:checked').serialize();
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=edit_member_approve&type=true&'+str_id,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true')location.reload();
            }
        });
    });
//отмена админом изменений в данных пользователей
	$('#edit_false').click(function(){
		var str_id=$('.one_checkbox_active:checked').serialize();
		if (!str_id){
			alert("Nicht gewahlt Bewerber!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
        	$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: 'name_func=edit_member_approve&type=false&'+str_id,
				dataType: "json",
				success: function(data)
				{
					if(data.ok=='true'){
						$('.load_ajax2').hide();
						$('.no_activ_body').hide();
						location.reload();
					}
					else{
						$('.load_ajax2').hide();
						$('.no_activ_body').hide();
						alert('Fehler beim Schreiben in Datenbank!');
					}
				}
				
			});    
		}
    });
    function msg_change(str){
        $('#msg_change').slideToggle(100);
        
        $('#btn_msg_change').click(function(){
            $('.no_activ_body').show();
            $('.load_ajax2').show();
        
            var text = $(this).prev().val();
            $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=edit_member_approve&type=false&'+str+'&text='+text,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true')location.reload();
            }
            });
        });
    }
    
    $('.btn#drop_user').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
		var str_id=$('.one_checkbox_active:checked').serialize();
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=delete_user&'+str_id,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true')location.reload();
            }
        });
    });
    
    $('#ablehnen').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
        var str_id=$('.one_checkbox_active:checked').serialize();
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=ablehnen&'+str_id,
            dataType: "json",
            success: function(data)
            {
                if(data==true)location.reload();
            }
        });
    });
    
//Отказ гостю в посещении события
    $('.btn#drop').click(function(){
		var str_id=$('.one_checkbox_active:checked').serialize();
		if (!str_id){
			alert("Nicht gewahlt Gast!");
            return false;
		}
		$('.no_activ_body').show();
		$('.load_ajax2').show();
		var table = $(this).attr('for');
		$.ajax({
			type: "POST",
			url: "../controllers/ajax.php",
			data: 'name_func=delete_by_id_basket&name_table='+table+'&'+str_id,
			dataType: "json",
			success: function(data)
			{
				if(data.ok=='true')location.reload();
			}
		});
    });

    $('.delete_member').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
        var id = $(this).attr('id');
        console.log(id);
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=delete_by_id&name_table=member&id_delete='+id,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true')location.reload();
            }
        });
        return false;
    });
    $('.delete_event').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
        var id = $(this).attr('id');
        console.log(id);
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=delete_by_id&name_table=event&id_delete='+id,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true')location.reload();
            }
        });
        return false;
    });

    $('.btn_search').click(function(){
		var val=$(this).prev().val();
		search(val);
    });
	$(".search-query").keypress(function(e){
		if(e.keyCode==13){
			var val=$(this).val();
			search(val);
		}
	});

    $( ".sort" ).click(function(){
        var name= $(this).attr('id');
        var GET=parseGetParams();

        var str='';
        var arr={};
        $.each(GET, function(index, value) {
            arr[index]=value;
        });
		var val=name.search('~asc');
		if(val!=-1){
			name=name.replace('~asc','~desc');
		}
		else{
			name=name.replace('~desc','~asc');
		}
        arr['sort']=name;
        $.each(arr, function(index, value){
			val=str.length;
			if(val>0){
				str=str+'&'+index+'='+value;
			}
			else{
				str=str+index+'='+value;
			}
        });
        str.replace(/%20/g,"") // = "прошел еще прошел"
        window.location.replace (window.location.pathname+'?'+str);
    });

    $('#new').click(function(){
        var _for=$(this).attr('for');
        $('#new_member.pop_window[for='+_for+']').slideDown(100);
        $('.no_activ_body').show();
    });
    
    $('.no_activ_body, #close_pop_window,.close_window').click(function(){
        if($('.pop_window').is(':visible')){
            if(confirm('Sind Sie sicher, dass Sie alle Änderungen verwerfen wollen?')){
				if($('#change_sponsor').is(':visible')){
					location.reload();
				}
				else if($('#sponsor_event_type').is(':visible')){
					location.reload();
				}
				
					$('.no_activ_body').hide();
					$('.pop_window').slideUp(100);
				
            }
        }
		return false;
    });

    $('.change_member').click(function(){
        var id = $('.one_checkbox_active:checked').last().val();
		if (!id){
			alert("Nicht gewahlt Mitglieder!");
            return false;
		}        
		$('.no_activ_body').show();
        $('.load_ajax2').show();
        $('#change_member.pop_window').slideDown(100);
        $('.no_activ_body').show();
        $('.form_change_member [name=id]').val(id);
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: 'name_func=get_info_DB_id& name_table=member& id='+id,
            dataType: "json",
            success: function(data)
            {
                $('.load_ajax2').hide();     
                console.log(data);
                $.each(data.DB_member, function(index, value) {
					if(index=='last_activ' || index=='letzter'){
                        $('#change_member [name='+index+']').text(value);					
					}
                    else if(index!='photo')
                    {
                        if(value)
                            $('#change_member [name='+index+']').val(value);
                        else
                            $('#change_member [name='+index+'] option[value=NEIN]').prop('selected',true);//це для селектів, щоб перші вибирались по замовчуванню
                    }
                });
                $('#btn_change_member').attr('id_member',data.DB_member.id);
            }
            //if(data.ok=='true')location.reload();

        });
        return false;
    });

//функція не підключена, добавлення нового юзера зараз працює без аяксу
    $('#btn_new_member').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        
        var _for=$(this).attr('for');
        var str = $("[for="+_for+"]form.new_member").serialize();
        $.ajax({
            async:false,
            type: "POST",
            url: "../controllers/ajax.php",
            data: str+'&name_func=new_member&name_table='+_for,
            dataType: "json",
            success: function(data)
            {
                if(data.ok=='true')location.reload();
				else alert('Fehler beim Schreiben in Datenbank!');
            }
        });
        return false;
    });
    
    $('.user_edit_icon').hover(function(){
        $(this).next().show();
    },function(){
        $(this).next().hide();
    });

    $('input.one_checkbox_active').on('change', function() {
        //$('input.one_checkbox_active').not(this).prop('checked', false);//нащо цього?? (Valera) 
        var id=$(this).val();
        $('.id_td_it [type=checkbox][name=id]').each(function(){
            if($(this).is(':checked')){
                $(this).parent().parent().css('background','rgba(9, 202, 19, 0.34)');
                $(this).parent().parent().find('.last_name_td, .first_name_td').css('background','rgba(9, 202, 19, 0.34)');
            }else{
                $(this).parent().parent().css('background','#fff');
                $(this).parent().parent().find('.last_name_td, .first_name_td').css('background','#fff');
            }
        });

    });


    $("body").on("click","#remove_photo",function(){
        var id=$(this).parent().attr('id');
        $(this).parent().remove();
        $('.photos').find('[id="'+id+'"]').remove();
    });

    $('#exportxls').click(function(){
        $('.exportxls').show();
        $('.link_excel_file').html('');        
    });
//экспорт данных в xls
    $('.exportxls').click(function(){
		var id=$('.one_checkbox_active:checked').serialize();
		if (!id){
			alert("Nicht gewahlt Member!");
			return false;
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			var type=$(this).attr('href');
			$.ajax({
				type: "POST",
				url: "../controllers/export_to_excel.php",
				data: 'table=member&'+id+'&type='+type,
				dataType: "json",
				success: function(data)
				{
					$('.no_activ_body').hide();
					$('.load_ajax2').hide();
					$('.link_excel_file').html('<a href="/controllers/'+data.name+'">Download: '+data.name+'</a>')
					$('.exportxls').hide();
				}
			});
			return false;
		}
    });
	
/*CREATE BY VOVA*/

$('body').on('click','td:not(.points,.images) > p', function(e) {
    if ($(this)[0].scrollWidth >  $(this).innerWidth()) {

        $("#overtext").text($(this).text());
        $('#overflow').slideDown(100);
        $('.no_activ_body').show();

    }
});

$("body").on('click','.photo_td', function(){
    var src = $(this).children().children().attr("rel");
    $('#pop_foto').attr("src",src);
    $('#photo_window').slideDown(100);
    $('.no_activ_body').show();
});

$("body").on('click','.professional_vita_td', function(){
    $('#vita').slideDown(100);
    $('.no_activ_body').show();
});

$("body").on('click','.points', function(){

    $("#program").children().remove();
    var newValue = $(this).text().replace('[', '');
    var newValue1 = newValue.replace(']', '');
    var newValue2 = newValue1.replace(/"/g, "");
    var array = newValue2.split(",");
    $('#programpoints').slideDown(100);
    $('.no_activ_body').show();

    for(var i in  array){

        var array1 = array[i].split(";")
        $("#program").append("<tr><td>"+array1[0]+"</td><td>"+array1[1]+"</td></tr>");
    }

});

$("body").on('click','.images', function(){

    $(".carousel-inner").children().remove();
    $('#myCarousel').carousel();
    $('#carousel').slideDown(400);
    $('.no_activ_body').show();0

    var newValue = $(this).text().replace('[', '');
    var newValue1 = newValue.replace(']', '');
    var newValue2 = newValue1.replace(/"/g, "");
    var array = newValue2.split(",");

    for (var i in array){
        var src = "http://fc.happyuser.info/uploads/" + array[i];
        $(".carousel-inner").append("<div class='item'>"+
        "<img style='width: 100%; max-height: 500px' src='"+src+"'>"+
        "</div>");
    }

   $(".carousel-inner > .item:first-child").addClass("active")

});



$("body").on('click','.professional_vita_td', function(){

    $("#vita_table_body").children().remove();

    $(".berufsvita span").each(function(){

    var time_end = $(this).children().children( "#time_end").text();
        if(time_end == ''){
            time_end = 'bis jeztz'
        }


    $("#vita_table_body").append("<tr><td>" + $(this).children().children("#time_start").text() +
        "-/" + time_end +"</td><td>"+$(this).children().children("#firmenname").text()+"</td>" +
        "<td>"+$(this).children().children("#position").text()+"</td>" +
        "<td>"+$(this).children().children("#aufgaben").text()+"</td></tr>");

    })
    $('#vita').slideDown(400);
    $('.no_activ_body').show();
});

});
$(document).on("mouseenter", ".draggable", function() {

$(".draggable").draggable({ cursor: "crosshair", revert: "invalid"});
$("#drop").droppable({ accept: ".draggable",
    drop: function(event, ui) {
        console.log("drop");
        $(this).removeClass("border").removeClass("over");
        var dropped = ui.draggable;
        var droppedOn = $(this);
        $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);


    },
    over: function(event, elem) {
        $(this).addClass("over");
        console.log("over");
    }
    ,
    out: function(event, elem) {
        $(this).removeClass("over");
    }
});
$("#drop").sortable();

$("#origin").droppable({ accept: ".draggable", drop: function(event, ui) {
    console.log("drop");
    $(this).removeClass("border").removeClass("over");
    var dropped = ui.draggable;
    var droppedOn = $(this);
    $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);
    $('.photos').html('');
    $.each($('.photo_part .draggable'), function(index,value){
        console.log(value);
        $('.photos').append('<input type="hidden" name="images[]" value="'+$(value).attr('id')+'" id="'+$(value).attr('id')+'"/>');
    });
}});

});

$(function(){
    $('.one_checkbox_active').click(function(){
        if($(this).prop('checked')){
            lastCheck = $(this);
        }else{
            lastCheck = null;
        }
    });
    $('.change_member_mitglied').click(function(){
        $('.one_checkbox_active').removeAttr('checked');
        if(lastCheck){
            lastCheck.prop('checked', 'true');
        }
    });
});