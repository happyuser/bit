$(document).ready(function(){
    $('[name^="status_"]').change(function(){
        var id=$(this).parent().parent().addClass('change');
    });
//удаление администратора    
    $('#drop').click(function(){
        var id = $('[name=id]:checked');
        var table = $(this).attr('for');
        var str_id='';
        $.each(id, function(index, value){
            str_id=str_id+'id['+index+']='+value.value+'&';
        });
        
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=delete_by_id_basket&name_table='+table+'&'+str_id,
        dataType: "json",
        success: function(data)
        {
            if(data.ok=='true')location.reload();
        }
        });
    });
//изменение уровня админов
    $('.save_admin').click(function(){
        var str='';
        $.each($('tr.change'), function(index, value){
            var id_admin=$(value).attr('id');
            str+= id_admin+'='+$(value).find(':radio:checked').val()+'&';
        });
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=edit_admin&'+str,
            dataType: "json",
            success: function(data)
            {
                $('.status_message_ok').animate({'opacity':'1'},1000).animate({'opacity':'0'},500);
            }
        });
    });
//открытие модального окна редактирования админов
    $('.btn.change_administration').click(function(){
        var id = $('.one_checkbox_active:checked').last().val();
        if (!id){
			alert("Nicht gewahlt Administrator!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			$('#change_admin.pop_window').slideDown(100);
			$('.no_activ_body').show();
			$('form.change_admin').attr('id',id);
			$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: 'name_func=get_info_DB_id&name_table=editor&id='+id,
				dataType: "json",
				success: function(data)
				{
					$.each(data.DB_member, function(index, value) {
						if(value){
							if(index!='land'){
								$('.change_admin [name='+index+']').val(value);
							}
							else{
								$('.change_admin .select_status [value="'+value+'"]').attr("selected", true);
							}
						}
					});
					$('.load_ajax2').hide();
				}
			});
		}
		return false;
	});
//сохранение данных в базу    
    $('form #btn_change_member').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();
        var id=$("form.change_admin").attr('id');
        var str = $("form.change_admin").serialize();
        str=str+'&name_func=change_admin&id='+id;
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: str,
            dataType: "json",
            success: function(data)
            {
                if(data==true){
                    $('.load_ajax2').hide();
                    location.reload();
                }
                else if(data=='mail'){
					$('.load_ajax2').hide();
					alert('Change email!');
				}
				else{
					$('.load_ajax2').hide();
					alert('Fehler beim Schreiben in Datenbank!');
				}
            }
        });
        return false;
    });
});