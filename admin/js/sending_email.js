$(document).ready(function(){
    function insertTextAtCursor(el, text, offset) {
        console.info($(el).html());
        var val = $(el).html(), endIndex, range, doc = el.ownerDocument;
        
        el.selectionStart=parseInt($(el).caret());
        el.selectionEnd=parseInt($(el).caret());
        
        if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
            endIndex = el.selectionEnd;
            val=val.slice(0, endIndex) + text + val.slice(endIndex);
            $(el).html(val);
            $(el).caret(endIndex);
        } else if (doc.selection != "undefined" && doc.selection.createRange) {
            el.focus();
            range = doc.selection.createRange();
            range.collapse(false);
            range.text = text;
            range.select();
        }
    }
    $(document).on('click','[data-edit="appeal"]',function(){
        insertTextAtCursor(document.getElementById('editor'),'Voranrede Anrede Name');
    });
    
    $('.get_text').click(function(){
        if($('#table_group [name=id]').is(':checked')){
            var id = $('[name=id]:checked:last').attr('id');
            var _for = $('[name=events] option:selected').val();
            var type_user =$('.user_types li.active').find('a').attr('href');
    
            $.ajax({
                type: "POST",
                url: "/controllers/ajax.php",
                data: 'name_func=message_type_account&for='+_for+'&id='+id+'&type='+type_user,
                dataType: "json",
                success: function(data){
                    $('#editor').html(data.message);
                }
            });
            $('tr').removeClass('grey_td');
            $('[name=id]').removeClass('check');
            $('[name=id]:checked').addClass('check').parent().parent().addClass('grey_td');
            //$('[name=id]').removeAttr('checked');
        }
    });
    
    $('#btn_save_message').click(function(){
        var html=$('#editor').html();
        var _for = $('[name=events] option:selected').val();
        var type_user =$('.user_types li.active').find('a').attr('href');
        var str='';
        $.each($('[name=id].check'),function(index,value){
            str=str+'id['+index+']='+value.id+'&';
        });
            $.ajax({
                type: "POST",
                url: "/controllers/ajax.php",
                data: 'name_func=save_emails&html='+html+'&for='+_for+'&type='+type_user+'&'+str,
                dataType: "json",
                success: function(data)
                {
                if(data.ok=='true'){
                    $('.status_message_ok').animate({'opacity':'1'},1000);
                    $('.status_message_ok').animate({'opacity':'0'},500);
                    $('[name=id].check').parent().parent().removeClass('grey_td').addClass('red_td');
                }
                }
            });
    });
    
    $('#btn_send_email').click(function(){
        var html=$('#editor').html().replace(/&nbsp;/gi,'');
        var _for = $('[name=events] option:selected').val();
        var type_user =$('.user_types li.active').find('a').attr('href');
        var str='';
        $.each($('[name=id].check'),function(index,value){
            str=str+'id['+index+']='+$(value).attr('id')+'&';
        });
            $('.no_activ_body').show();
            $('.load_ajax2').show();
        
            $.ajax({
                type: "POST",
                url: "/controllers/ajax.php",
                data: 'name_func=sending_emails&'+str+'&html='+html+'&type='+type_user+'&for='+_for,
                dataType: "json",
                success: function(data)
                {
                    
                    if(data==1){
                        $('.status_message_ok').animate({'opacity':'1'},1000);
                        $('.status_message_ok').animate({'opacity':'0'},500);
                        $('[name=id].check').parent().parent().removeClass('grey_td red_td').addClass('green_td');
                        
                        $('.no_activ_body').hide();
                        $('.load_ajax2').hide();
                    }
                }
            });
    });
    
    $('[name=events]').change(function(){
        var param={};
        param.id_event=$(this).val();
        gen_get_str(param);
    });
    $('.user_types .nav-tabs li a').click(function(){
        var param={};
        param.type=$(this).attr('href');
        gen_get_str(param);
        return false;
    });
    
    function gen_get_str(param){
        var GET=parseGetParams();
        var str='';
        var arr={};
        $.each(GET, function(index, value) {
            arr[index]=value;
        });
        for(var index in param){
            arr[index]=param[index];
        }

        if(arr)
        $.each(arr, function(index, value) {
            str=str+index+'='+value+'&';
        });
        str.replace(/%20/g,"") // = "прошел еще прошел"
        window.location.replace (window.location.pathname+'?'+str);
    }
});