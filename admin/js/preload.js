$(document).ready(function(){
	$('body').on('change','.file', function(){
		var input=$(this);
		if (input[0].files && input[0].files[0]) {
			var reader = new FileReader();
			$(input).parent().find('.bg_input_file').append('<img id="image" src="#" alt="" />');
			reader.onload = function (e) {
				$(input).parent().find('#image').attr('src', e.target.result);
			};
			reader.readAsDataURL(input[0].files[0]);
		}
	});
});