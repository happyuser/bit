
function build_form(event,type){
	var li_html="<tr data-event='"+event+"'><th><label>";
		if(event<5){
			if(event<4){
				li_html=li_html+"Werbung oben "+event;
			}
			else{
				li_html=li_html+"Werbung rechts";
			}
			li_html=li_html+":</label></th><th><div class='input_style'><div class='bg_input_file'>Auswählen</div><input class='file' ";
			li_html=li_html+"name='file"+event+"[]' ";
			li_html=li_html+"type='file' style='margin-bottom: 0px; height: 30px; width: 148px;'></div></th><th><input type='text'";
			li_html=li_html+"name='link"+event+"[]' ";
			li_html=li_html+"placeholder='LINK zur Seite' class='search-query' style='height: 30px; border-radius: 4px; width: 228px;'/></th><th><input type='text'";
			li_html=li_html+"name='wait"+event+"[]' ";
			li_html=li_html+"placeholder='Zeit' class='zeit' style='height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;'/></th><th><input type='text'";
			li_html=li_html+"placeholder='Sort' name='sort"+event+"[]' class='zeit' style='height: 30px; border-radius: 4px; margin-bottom: 0px; width: 50px;'/></th>"
			if(type=='yes'){
				li_html=li_html+"<th><span data-isset='no' class='clear_ad' title='Löschen'>-</span></th>";
			}
			li_html=li_html+"<th><span class='add_ad' title='Hinzufügen'>+</span>";
		}
		if(event==5){
			li_html=li_html+"Video werbung:</label></th>";
			li_html=li_html+"<th colspan='3'><input type='text' name='link51' placeholder='LINK zur Seite' class='search-query' style='z-index: 0; height: 30px; border-radius: 4px; width: 100%;'/>";
		}
		li_html=li_html+"</th></tr>";
	return li_html;
}


$(document).ready(function(e){
    $('body').on('click','.clear_ad',function(){
		var object=$(this);	
		var isset=$(object).data('isset');
		var event=$(object).closest('tr').data('event');
		if(isset=='yes'){
			if(event<5){
				var id=$(object).closest('tr').find('.id').val();
			}
			else{
				var id=$(object).closest('tr').find('.id').text();
			}
			$.ajax({
				'url': "../controllers/ajax.php",
				'type': "POST",
				'data': {param1: id, name_func:'delete_werbung'},
			});
		}
		if(event<5){
			var dat=$('[data-event='+event+']').length;
			if(dat==1){
				var li_html=build_form(event,'no');
				$(object).closest('tr').after(li_html);	
			}
			else if(dat==2){
				$(object).closest('tr').prev('tr').append("<th><span class='add_ad'>+</span>");
			}
			else{
				var find_add=$(object).closest('th').next('th').children('span').attr('class');
				if(find_add=='add_ad'){
					$(object).closest('tr').prev('tr').append("<th><span class='add_ad'>+</span>");
				}
			}
		}
		else{
			var li_html=build_form(event,'no');
			$(object).closest('tr').after(li_html);			
		}
		$(object).closest('tr').remove();
		
    });
	$('body').on('click','.add_ad',function(){
		var object=$(this);
		var event=$(object).closest('tr').data('event');
		var li_html=build_form(event,'yes');
		$(object).closest('tr').after(li_html);
		$(object).closest('th').remove();
    });

});