//загрузка имен спонсоров в селекты
function reload_sponsor_name(id,data){
	var count=$('#'+id).find('#rest_sponsor').length;
	if(count>1){
		var clon=$('#'+id).find('#rest_sponsor').clone();
		$('#'+id).find('#rest_sponsor').remove();
		$('#'+id).find('#sub_sposor').after(clon);
	}
	$('#'+id).find('#main_sposor').empty();
	$('#'+id).find('#main_sposor').append(data[0].main_sponsor);
	$('#'+id).find('#sub_sposor').empty();
	$('#'+id).find('#sub_sposor').append(data[1].sub_sponsor);
	$('#'+id).find('#rest_sponsor').empty();
	$('#'+id).find('#rest_sponsor').append(data[2].rest_sponsor);
	if(data.length>3){						
		for($i=3;$i<data.length;$i++){
			var clon=$('#'+id).find('#rest_sponsor').clone();
			$('#'+id).find('#rest_sponsor').after(clon);					
			$('#'+id).find('#rest_sponsor').empty();
			$('#'+id).find('#rest_sponsor').append(data[$i].rest_sponsor);
		}
	}	
}

$(document).ready(function(){
//удаление добавленного списка выбора
    $(document).on("click",".point .close",function(){
        $(this).parent().parent().remove();
        return false;
    });
//добавление элемента с выбором спонсора(переписать)
    $(document).on("click","#add_new_sponsor",function(){
        $(this).parent().before($("#new_sponsor").clone());
        $( "#label_rest_sponsor" ).css('margin-bottom','+=40px');
        return false;
    });
//добавление элемента с выбором спонсора в окне редактирования(переписать)
    $(document).on("click","#add_new_sponsor1",function(){
        $(this).parent().before($("#new_sponsor").clone());
        $( "#label_rest_sponsor1" ).css('margin-bottom','+=40px');
        return false;
    });
//удаление добавленного списка выбора(переписать)
    $(document).on("click","#remove_spansor",function(){
        $(this).parent().remove();
    
    });
//добавление елемента управления в создании события(переписать)
$(".add_new_event_point_first").click(function(){
    var form=$(this).attr('id');
    $("form."+form+" .point:last").parent().after('<div><label class="control-label space-right" id="label_event_points" style="" for="event_points">Programmpunkte mit Uhrzeiten</label>'+
							'<div class="controls point" id="points_time">'+
                            '<input type="text" class="span4 point_txt" name="points[txt][]" style="height: 30px" id="description_event_points" placeholder="" value=""/>'+
                            '<input type="time" class="point_time_plus" name="points[time][]" style="height: 30px;width: 85px" placeholder="" value=""/>'+
                            '<button class="close" id="remove_event_point" data-dismiss="alert" style="margin-bottom: 10px">&times;</button></div></div>');
    return false;
});


    $('.point_time_plus').timepicker({
        showPeriodLabels: false, // показывать обозначения am и pm
        hourText: 'Часы', // название заголовка часов
        minuteText: 'Минуты', // название заголовка минут
        showMinutes: true, // показывать блок с минутами
        rows: 4, // сколько рядов часов и минут выводить
        timeSeparator: ':', // разделитель часов и минут
        hours: {
            starts: 0, // настройка часов
            ends: 23 // от - до
        },
        minutes: {
            starts: 0, // настройка минут
            ends: 55, // от - до
            interval: 5 // интервал между минутами
        }
    });

//открытие и заполнение окна редактирование события
    $('.change_event').click(function(){
        var id = $('.one_checkbox_active:checked').last().val();
        if (!id){
			alert("Nicht gewahlt Sponsor!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			$('#change_event.pop_window').slideDown(400);
			$('.no_activ_body').show();
			$('#change_event .point, #change_event #origin, #change_event ul.sortable').html('');                                            
			$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: 'name_func=get_event&id='+id,
				dataType: "json",
				success: function(data)
				{
					$('.load_ajax2').hide();
					$('#btn_change_event').attr('id_event',data.id);
					$.each(data, function(index, value) {
						if(index!='points' && index!='images' && index!='background'){
							if(index=='type_of_event'){
								var datas='name_func=event_change_event&select='+value+'&id_event='+id;
								$.ajax({
									type: "POST",
									url: "../controllers/ajax.php",
									data: datas,
									dataType: "json",
									success: function(data)
									{
										reload_sponsor_name('change_event',data);
									}							
								});
							}
							$('#change_event [name='+index+']').val(value);
						}
						else if(index=='points'){
							if(value)
							$.each(value, function(index, val) {
								$('#change_event .start_point').before(
									'<div class="controls point" id="points_time">'+
									'<input type="text" class="span4 point_txt" name="points[txt]['+index+']" style="height: 30px" id="description_event_points" placeholder="" value="'+val.txt+'"/>' +
									'<input type="time" class="point_time" name="points[time]['+index+']" style="height: 30px;width: 85px" id="event_points" placeholder="" value="'+val.time+'"/>'+
									'<button class="close" id="remove_event_point" data-dismiss="alert" style="float: none;margin-bottom: 10px">&times;</button></div>');
							});
						}
						else if(index=='images'){
							if(value)
							$.each(value, function(index, value) {
								var photo=$('.imgs_row .photo_event:last');
								var id_photo=$(photo).attr('id');
								id_photo++;
								$("form.form_change_event .sortable").append("<li class='ui-state-default' name='images[]' value='"+value+"'>" +
								"<img  title='two' style='width: 70px; height: 70px; margin: 5px; position: relative;' src='/uploads/"+value+"'/>" +
								"<button id='remove_photo' style='display:none; position: absolute; background: rgba(255, 8, 8, 0.77);border: none;outline: none;width: 21px;padding: 0;'>"+
								"<i class='icon-remove icon-white'></i></button>"+
								"</li>");
							});
						}else if(index=='background'){
							$('#change_event [name='+index+']').jqteVal(value);
						}
						if(index=='paragraph'){
							$('#change_event .count').text(value.length);
						}
					});
				},
				error: function(error){
					$.each(error, function(index, value) {
						console.log('error: index-'+index+' val= '+value);
					});
				}
			});
			return false;
		}
    });
//удаление фото   
    $('.pop_window').on('click','.remove_photo_event',function(){
        console.log($(this).parent());
        $(this).parent().parent().parent().remove();
    });
//сохранение нового события    
    $('form.new_event #btn_new_event').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();

        var str = $("form.new_event").serialize();
        $.each($('form.new_event .sortable li'), function(index, value) {
            str=str+'&images[]='+$(value).attr('value');
        });
        $.ajax({
        async:false,
        type: "POST",
        url: "../controllers/ajax.php",
        data: str+'&name_func=new_event_list',
        dataType: "json",
        success: function(data)
        {
            if(data.ok=='true'){
                $('.load_ajax2').hide();
                $('.status_message_ok').css('opacity',1);
                setTimeout("location.reload()",100);
            }
        }
        });
        return false;
    });
//сохранение редактирования события   
    $('#btn_change_event').click(function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();

        var str = $("form.form_change_event").serialize();
        var id=$(this).attr('id_event');
        $.each($('form.form_change_event .sortable li'), function(index, value) {
            str=str+'&images[]='+$(value).attr('value');
        });
        $.ajax({
        type: "POST",
        url: "../controllers/ajax.php",
        data: str+'&name_func=update_event&id='+id,
        dataType: "json",
        success: function(data)
        {
            if(data.ok=='true'){
                $('.load_ajax2').hide();              
                $('.status_message_ok').css('opacity',1);
                setTimeout("location.reload()",100);
            }
        }
        });
        return false;
    });
//оформление textarea
    $("textarea[name=background]").jqte({
        formats: [
        ["p","normal"],
        ["h1","groß"],
        ["h2","klein"]
        ]
	});
//работа textarea
    $('[name=paragraph]').on('textchange',function(){
        var s = $(this).val();
        if(s.length<=300){
            $('.counter').removeClass('error_count');
            $(this).addClass('good').removeClass('bad');
        } else {
            $('.counter').addClass('error_count');
            $(this).addClass('bad').removeClass('good');
            $(this).val($(this).val().substr(0,$(this).val().length-1));
        }
        $('.count').text(s.length);    
    });
//добавление нового фото в событие????   
    $('.pop_window').on('change', '#new_event_add_new_photo',function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();

        var this_=$(this);
        var fd = new FormData();
        fd.append('id', '123');
        fd.append('type', 'one');
        fd.append('no_update','true');
        $.each($('#new_event_add_new_photo')[0].files, function(index,value){
            fd.append('img[]', value);
        });
        console.log(fd);
        $.ajax({
            type: 'POST',
            url: 'upload.php',
            data: fd,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function(data) {
                if (data.name_img) {
                    $('.load_ajax2').hide();

                    $.each(data.name_img, function(index, value){
                        var src = "/uploads/" + value;
                        $("#new_member .sortable").append("<li class='ui-state-default' name='images[]' value='"+value+"'>" +
                        "<img  title='two' style='width: 70px; height: 70px; margin: 5px; position: relative;' src='"+src+"'/>" +
                        "<button id='remove_photo' style='display:none; position: absolute; background: rgba(255, 8, 8, 0.77);border: none;outline: none;width: 21px;padding: 0;'>"+
                        "<i class='icon-remove icon-white'></i></button>"+
                        "</li>");
                    });
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
        return false;
    });
//добавление нового фото    
    $('.pop_window').on('change', '#add_new_photo',function(){
        $('.no_activ_body').show();
        $('.load_ajax2').show();

        var this_=$(this);
        var fd = new FormData();
        fd.append('id', '123');
        fd.append('type', 'one');
        fd.append('no_update','true');
        $.each($('#add_new_photo')[0].files, function(index,value){
            fd.append('img[]', value);
        });
        $.ajax({
            type: 'POST',
            url: 'upload.php',
            data: fd,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function(data) {
                if (data.name_img) {
                    $('.load_ajax2').hide();

                    $.each(data.name_img, function(index, value){
                    var src = "/uploads/" + value;

                    $(".form_change_event .sortable").append("<li class='ui-state-default' name='images[]' value='"+value+"'>" +
                        "<img  title='two' style='width: 70px; height: 70px; margin: 5px; position: relative;' src='"+src+"'/>" +
                        "<button id='remove_photo' style='display:none; position: absolute; background: rgba(255, 8, 8, 0.77);border: none;outline: none;width: 21px;padding: 0;'>"+
                        "<i class='icon-remove icon-white'></i></button>"+
                        "</li>");
                    });
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
        return false;
    });
//работа кнопки удаления фото    
    $('.pop_window').on({
    mouseenter: function () {
        $(this).find('#remove_photo').show(100);
    },
    mouseleave: function () {
        $(this).find('#remove_photo').hide(100);
    }
    },'.sortable li');
//предложение вариантов заполнения города   
    $('[name=city]').on('textchange',function(){
        var input=$(this);
        $(input).next().html('');
        var city_name=$(this).val();
        if(!city_name)return false;
        $('.no_activ_body').show();
        $('.load_ajax2').show();

        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: '&name_func=select_cities&city_name='+city_name,
            dataType: "json",
            success: function(data){
                $('.load_ajax2').hide();

                $.each(data, function(index,value){
                    $(input).next().append('<div class="city_name">'+value.name+'</div>');
                });
            }
        });
    });
//заполнение города предложденным вариантом    
    $('.pop_window').on('click','.city_name',function(){
        $('[name=city]').val($(this).text());
        $('.list_cities').html('');
    });
//подключение календаря
	$('.myCalendar').click(function(){
		var target=$(this);
		$(target).datepicker({
			changeMonth:true,
			changeYear:true,
			onSelect: function(dateText){ 
				$(target).children().val(dateText);
				$('.ui-datepicker').remove();
				$(target).attr('id','');
				$(target).removeClass('hasDatepicker');
			}
 		});
	});
//открытие модального окна привязки спонсоров к событиям
    $('#events_sponsor').click(function(){      
        var id = $('.one_checkbox_active:checked').last().val();
        if (!id){
			alert("Nicht gewahlt Veranstaltung!");
		}
		else{
			$('.no_activ_body').show();
			$('.load_ajax2').show();
			$.ajax({
				type: "POST",
				url: "../controllers/ajax.php",
				data: '&name_func=event_sponsors&id='+id,
				dataType: "json",
				success: function(data)
				{
					$('.load_ajax2').hide();      
					$('#event_sponsors tbody').html('<tr class="start_history"></tr>');
					var str='';
					$.each(data, function(index, value) {
						str=str+"<tr>";
						$.each(value, function(ind, val) {
							str=str+"<td>"+val+"</td>";
						});	
						str=str+"</tr>";
					});	
					$('.start_history').after(str);
				}
			});
			$('#event_sponsors.pop_window').slideDown(100);
			$('.no_activ_body').show();
		}
    });
//заполнение селектов спонсоров
	$('.pop_window').on('change','#type_of_event',function(){
		$('.load_ajax2').show();
		var target=$(this);
		var id=$(target).closest('.pop_window').attr('id');
		var func_name='event_'+id;
		var select=$(target).val();
		var datas = 'name_func='+func_name+'&select='+select;
		if(id=='change_event'){
			var id_event=$(target).closest('.pop_window').find('#btn_change_event').attr('id_event');
			datas=datas+'&id_event='+id_event;
		}
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: datas,
            dataType: "json",
            success: function(data){
				if(id=='change_event'){
					reload_sponsor_name(id,data);
				}
				else{
					$('#'+id).find('#main_sponsor').empty();
					$('#'+id).find('#main_sponsor').append(data);
					$('#'+id).find('#sub_sponsor').empty();
					$('#'+id).find('#sub_sponsor').append(data);
					$('#'+id).find('#rest_sponsor').empty();
					$('#'+id).find('#rest_sponsor').append(data);
				}
				$('.load_ajax2').hide();
			}
        });
	});
});

$(function() {
    $( ".sortable" ).sortable({
      revert: true
    });
    $( ".sortable" ).disableSelection();
});

// Time Picker
$(function () {
    $('.point_time').timepicker({
        showPeriodLabels: false, // показывать обозначения am и pm
        hourText: 'Часы', // название заголовка часов
        minuteText: 'Минуты', // название заголовка минут
        showMinutes: true, // показывать блок с минутами
        rows: 4, // сколько рядов часов и минут выводить
        timeSeparator: ':', // разделитель часов и минут
        hours: {
            starts: 0, // настройка часов
            ends: 23 // от - до
        },
        minutes: {
            starts: 0, // настройка минут
            ends: 55, // от - до
            interval: 5 // интервал между минутами
        }
    });
    $('.add_new_event_point_first').click(function(){
        $('.point_time_plus').timepicker({
            showPeriodLabels: false, // показывать обозначения am и pm
            hourText: 'Часы', // название заголовка часов
            minuteText: 'Минуты', // название заголовка минут
            showMinutes: true, // показывать блок с минутами
            rows: 4, // сколько рядов часов и минут выводить
            timeSeparator: ':', // разделитель часов и минут
            hours: {
                starts: 0, // настройка часов
                ends: 23 // от - до
            },
            minutes: {
                starts: 0, // настройка минут
                ends: 55, // от - до
                interval: 5 // интервал между минутами
            }
        });
    });
});