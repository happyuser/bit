<?
include ('header1.php');
include_once('../controllers/func_creat_sponsoren.php');
?>
<script src="js/sponsoren.js" type="text/javascript"></script>
<script src="js/preload.js"></script>
<div class="pop_window" id="overflow" style="display: none">
	<button class="close_window pull-right" style="margin-right: -10px;margin-top: -10px">
		<i class="icon-remove icon-white"></i>
	</button>
	<h6 id="overtext" style="text-align:center "></h6>
</div>
<div class="pop_window" id="change_sponsor" style="display: none;">
	<form class="change_sponsor" action="/" method="post">
		<div class="row-fluid" style="overflow: auto;height: 400px;">
			<div class="control-group">
				<label class="control-label space-right">Sponsorenname</label>
				<div class="controls">
					<input type="text" name="firmenname" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Land</label>
				<div class="controls">
					<select id="type_of_land" class="span5" name="land">
						<? if(isset($data['select_country'])){echo $data['select_country'];}?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Stadt</label>
				<div class="controls">
					<input type="text" name="stadt" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">PLZ</label>
				<div class="controls">
					<input type="text" name="plz" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Email</label>
				<div class="controls">
					<input type="text" name="email" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Telefon</label>
				<div class="controls">
					<input type="text" name="telefon" class="span5" style="height: 30px;"/>
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label space-right" for="photos">Fotos</label>
				<div class="controls">
					<div class="input_style input_style_change_sponsoren">
						<div class="bg_input_file">Auswählen</div>
						<input class="file" type="file" name="file[]" style='margin-bottom: 0px; height: 30px; width: 148px;'>
					</div>
				</div>
			</div>
			<div class="control-group control-group-internetseite">
				<label class="control-label space-right">Internetseite</label>
				<div class="controls">
					<input type="text" name="site_url" class="span5" style="height: 30px;"/>
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label space-right">Geschäftsführer</label>
				<div class="controls">
					<input type="text" name="first_person" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Beschreibung</label>
				<div class="controls">
					<textarea name="bitte" class="span5"></textarea>
				</div>
			</div>
			<div class="control-group type_to_copy">
				<label class="control-label space-right ">Type</label>
				<div class="controls">
					<select id="type_of_event" class="type_of_event_change" name="event_type[]">
						<? if(isset($data['select_options'])){echo $data['select_options'];}?>
					</select>
				</div>
			</div>
			<div class="control-group control-group-weitere-type">
				<label class="control-label space-right">Weitere Type:</label>
				<div class="controls">
					<div class="btn btn-primary add_new_type_point"><i class="icon-plus icon-white"></i> Hinzufugen</div>
				</div>
			</div>
<!---			<div class="control-group">
				<label class="control-label space-right">Adresse</label>
				<div class="controls">
					<input type="text" name="strabe" class="span5" style="height: 30px;"/>
				</div>
			</div>			-->

		</div>
        <div class="row-fluid sponsoren_submit_box">
			<div class="btn" id="close_pop_window">Abbrechen</div>
			<div class="btn save">Speichern</div>
        </div>
    </form>
</div>

<div class="pop_window pop_window_sponsoren" id="new_sponsor">
    <form class="new_sponsor" action="/" method="post">
        <div class="row-fluid row_fluid_window_modal_sponsoren" style="overflow: auto;height: 400px;">
			<div class="control-group">
				<label class="control-label space-right">Sponsorenname</label>
				<div class="controls">
					<input type="text" name="firmenname" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Land</label>
				<div class="controls">
					<select id="type_of_land" class="span5" name="land">
						<? if(isset($data['select_country'])){echo $data['select_country'];}?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Stadt</label>
				<div class="controls">
					<input type="text" name="stadt" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">PLZ</label>
				<div class="controls">
					<input type="text" name="plz" class="span5" style="height: 30px;"/>
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label space-right">Email</label>
				<div class="controls">
					<input type="text" name="email" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Telefon</label>
				<div class="controls">
					<input type="text" name="telefon" class="span5" style="height: 30px;"/>
				</div>
			</div>	
			<div class="control-group control-group-fotos">
				<label class="control-label space-right" for="photos">Fotos</label>
				<div class="controls">
					<div class="input_style">
						<div class="bg_input_file">Auswählen</div>
						<input class="file" type="file" name="file[]" style='margin-bottom: 0px; height: 30px; width: 148px;'>
					</div>
				</div>
			</div>
			<div class="control-group control-group-internetseite">
				<label class="control-label space-right">Internetseite</label>
				<div class="controls">
					<input type="text" name="site_url" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Geschäftsführer</label>
				<div class="controls">
					<input type="text" name="first_person" class="span5" style="height: 30px;"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label space-right">Beschreibung</label>
				<div class="controls">
					<textarea name="bitte" class="span5"></textarea>
				</div>
			</div>
			<div class="control-group type_to_copy">
				<label class="control-label space-right ">Type</label>
				<div class="controls">
					<select id="type_of_event" class="type_of_event_change" name="event_type[]">
						<? if(isset($data['select_options'])){echo $data['select_options'];}?>
					</select>
				</div>
			</div>
			<div class="control-group control-group-weitere-type">
				<label class="control-label space-right">Weitere Type:</label>
				<div class="controls">
					<div class="btn btn-primary add_new_type_point"><i class="icon-plus icon-white"></i> Hinzufugen</div>
				</div>
			</div>
<!--			<div class="control-group">
				<label class="control-label space-right">Adresse</label>
				<div class="controls">
					<input type="text" name="strabe" class="span5" style="height: 30px;"/>
				</div>
			</div>			-->
        </div>
        <div class="row-fluid sponsoren_submit_box">
			<div class="btn" id="close_pop_window">Abbrechen</div>
			<div class="btn save">Speichern</div>
		</div>
    </form>
</div>

<div class="pop_window" id="history_event" style="display: none;">
	<table class="table table-bordered table_bordered_history_user">
		<thead>
			<th>
                ID
                <div></div>
            </th>
			<th>
                Sponsoren
                <div></div>
            </th>
			<th>
                Veranstaltung
                <div></div>
            </th>
			<th>
                Sponsor Typ
                <div></div>
            </th>
		</thead>
		<tbody>
			<tr class="start_history"></tr>
		</tbody>
	</table>
	<div class="row-fluid">
			<input class="btn" id="close_pop_window" type="reset" value="Abbrechen"/>
	</div>
</div>

<div class="pop_window" id="event_type" style="display: none;">
	<table class="table table-bordered table_bordered_history_user">
		<thead>
			<th>
                ID
                <div></div>
            </th>
			<th>
                Sponsoren
                <div></div>
            </th>
			<th>
                Veranstaltung
                <div></div>
            </th>
		</thead>
		<tbody>
			<tr class="start_history"></tr>
		</tbody>
	</table>
	<div class="row-fluid">
			<input class="btn" id="close_pop_window" type="reset" value="Abbrechen"/>
	</div>
</div>

<div class="container-fluid">
    <div class="sponsore_btn_box">
        <button class="btn new_sponsor" title="Neuen Sponsor anlegen" style="margin: 2px; float: left;"><i class="icon-file"></i></button>
        <button class="btn change_sponsor" title="Sponsoren bearbeiten" style="margin: 2px; float: left;"><i class="icon-pencil"></i></button>
        <?if($_SESSION['status']=='superadmin'){?>
            <button class="btn" title="Sponsoren löschen" id="drop_sponsor" style="margin: 2px; float: left;"><i class="icon-remove"></i></button>
        <?}?>
        <button class="btn" id="sponsor_event_type" title="Arten von Veranstaltungen zu sponsern" style="margin: 2px; float: left;"><i class="icon-list-alt"></i></button>
        <button class="btn" id="sponsor_event" title="In Veranstaltungen gelehrt" style="margin: 2px; float: left;"><i class="icon-list-alt"></i></button>
        <button class="btn" id="without_filters" title="Filter an/ausschalten" style="margin: 2px; float: left;"><i class="icon-filter no_filters"></i></button>
    </div>
    <div class="input-append input_append_sponsoren">
        <input type="text" class="search-query" value="<? if(isset($_GET['search'])){echo $_GET['search'];}?>"/>
        <button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
        <div class="search_clear"><i class="icon-remove"></i></div>
    </div>
    <div class="pagination pagination_admin_sponsoren">
        <ul>
            <?if(isset($data['pages_number'])) echo $data['pages_number'];?>
        </ul>
    </div>
	<div class="row-fluid header" style="margin-top: 20px">
	</div>
	<div class="row-fluid body view_table" style="margin-top: 0;">
		<div class="tab-content">
	        <div class="tab-pane active" id="tab1">
				<div class="row-fluid body view_table">
					<form class="table">
						<div class="no_sroll no_sroll_sponsoren">
							<table class="table table-bordered">
								<thead>
									<? if(isset($data['header'])){echo $data['header'];}?>		
								</thead>
								<tbody>
									<? if(isset($data['table'])){echo $data['table'];}?>	
								</tbody>
							</table>
						</div>
						<div class="scroll scroll_sponsoren">
							<table class="table table-bordered">
								<thead>
									<? if(isset($data['header_slider'])){echo $data['header_slider'];}?>		
								</thead>
								<tbody>
									<? if(isset($data['table_slider'])){echo $data['table_slider'];}?>	
								</tbody>
							</table>
						</div>
					</form>
				</div>
	        </div>
	    </div>
	</div>
</div>