<?php

trait TAccessor
{
	
	public function __get($propertyName)
	{
		$method = 'get'.ucfirst($propertyName);
		if (method_exists($this,$method)) {
			return $this->$method();
		} else {
			return $this->__get2($propertyName);
		}
	}

	public function __set($propertyName,$propertyValue)
	{
		$method = 'set'.ucfirst($propertyName);
		if (method_exists($this,$method)) {
			return $this->$method($propertyValue);
		} else {
			return $this->__set2($propertyName,$propertyValue);
		}
	}

}

?>