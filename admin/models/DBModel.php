<?php

//require_once(dirname(__FILE__).'/TAccessor.php');
require_once dirname(__FILE__).'/../conf.php' ;

abstract class DBModel
{
	static protected $dbConn;
	protected $name;
	protected $whereIds = array();
	protected $where = '';
	protected $limit = '';
	protected $properties = array();
	
	//use TAccessor;
	
	static public function getDbConn()
	{
		if (!empty(self::$dbConn)) return self::$dbConn;
		self::$dbConn = new PDO($GLOBALS['pdo_conf']['addr'], $GLOBALS['pdo_conf']['user'], $GLOBALS['pdo_conf']['pass']);
		self::$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if (!empty(self::$dbConn)) return self::$dbConn;
	}
	
	public function filter($ids=array())
	{
		if (!is_array($ids) and !is_int($ids) and !preg_match('/\d+(,\d+)+/',$ids)) {
			throw new Exception("Incorrect id`s for DBModel::filter method");		
		}
		if (is_int($ids)) $ids = array('id'=>$ids);
		if (is_string($ids) and preg_match('/\d+(,\d+)+/',$ids)) {
			$ids_tmp = preg_split('/,/',$ids);
			$where_add = '(id=' . implode(' OR id=',$ids_tmp) .')';
			if (empty($this->where)) {
				$this->where = 'WHERE '.$where_add;
			} else {
				$this->where .= ' AND '.$where_add;
			}
			$ids = array();
		}
		foreach ($ids as $key=>$value) {
			if (empty($this->where)) {
				$this->where = 'WHERE ';
			} else {
				$this->where .= ' AND ';
			}
			$this->where .= '`'.$key.'`="'.addslashes($value).'"';		
		}
		$this->whereIds += $ids;
	}
	
	private $_paginationCount = null;
	private $_pagesCount = null;	
	public function getPaginationCount()
	{
		return $this->_paginationCount;
	}
	public function setPaginationCount($value)
	{
		if (!is_int($value) || $value<=0)
			throw new Exception('Incorrect pagination value: '.$value);
		$this->_paginationCount = $value;
		$this->_pagesCount = ceil($this->getCount() / $this->_paginationCount);
	}
	public function getPagesCount()
	{
		return $this->_pagesCount;	
	}	
	
	private $_pageNumber = null;
	public function getPageNumber()
	{
		return $this->_pageNumber;
	}
	public function setPageNumber($value)
	{
		if (!is_int($value) || $value<=0)
			throw new Exception('Incorrect pagination value: '.$value);
		$this->_pageNumber = $value;
		$this->limit = ' LIMIT '.($value-1)*$this->_paginationCount.','.$this->_paginationCount;
	}
	
	public function getCount()
	{
		$dbConn = self::getDbConn();
		$row = $dbConn->query('SELECT COUNT(*) as count FROM `'.$this->name.'` '.$this->where)->fetch();
		return (int)$row['count'];
	}


	public function __get($propertyName)
	{
		$method = 'get'.ucfirst($propertyName);
		if (method_exists($this,$method)) {
			return $this->$method();
		} else {
			return $this->__get2($propertyName);
		}
	}

	public function __set($propertyName,$propertyValue)
	{
		$method = 'set'.ucfirst($propertyName);
		if (method_exists($this,$method)) {
			return $this->$method($propertyValue);
		} else {
			return $this->__set2($propertyName,$propertyValue);
		}
	}
	
	public function __get2($propertyName)
	{
		if (!isset($this->properties[$propertyName])) {		
			$dbConn = self::getDbConn();
			$row = $dbConn->query('SELECT `'.$propertyName.'` FROM `'.$this->name.'` '.$this->where)->fetch();
			$this->properties[$propertyName] = $row[$propertyName];
		}
		return $this->properties[$propertyName];
	}
	
	public function __set2($propertyName,$propertyValue)
	{
		$this->properties[$propertyName] = $propertyValue;
	}
	
	public function getAll()
	{
		$dbConn = self::getDbConn();
		echo 'SELECT * FROM `'.$this->name.'` '.$this->where.$this->limit;
		return $dbConn->query('SELECT * FROM `'.$this->name.'` '.$this->where.$this->limit)->fetchAll();
	}
	
	public function insert()
	{
		$sql = '';		
		foreach ($this->properties as $key=>$value) {
			if (!empty($sql)) $sql .= ', ';
			$sql .= '`'.$key.'`="'.addslashes($value).'"';		
		}		
		$sql = 'INSERT INTO `'.$this->name.'` SET '.$sql;
		$dbConn = self::getDbConn();
		if ($dbConn->query($sql)==false)
			return false;
		$properties = $dbConn->query('SELECT MAX(id) as id FROM `'.$this->name.'`')->fetch();
		$this->properties['id'] = $properties['id'];
		$this->whereIds = array('id'=>$this->properties['id']);
		$this->where = 'id='.$this->properties['id'];
		return true;
	}
	
	public function update()
	{
		$sql = '';		
		foreach ($this->properties as $key=>$value) {
			if (!empty($sql)) $sql .= ', ';
			$sql .= '`'.$key.'`="'.addslashes($value).'"';		
		}		
		$sql = 'UPDATE `'.$this->name.'` SET '.$sql.' '.$this->where;
		$dbConn = self::getDbConn();
		if ($dbConn->query($sql)==false)
			return false;
	}

}

?>