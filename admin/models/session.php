<?php 

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
	ini_set('display_errors','0');
	require_once '../exception/ESecurity.php';
	throw new ESecurity();
}

define('MAX_SESSION_LIFETIME',10000);

require_once dirname(__FILE__).'/DBModel.php';



function sess_close()
{
	// To realize	
}
	
function sess_destroy($session_id)
{
	$dbConn = DBModel::getDbConn();
	$pdoQ = $dbConn->prepare('DELETE FROM session_editor WHERE session_id=:session_id');
	$pdoQ->execute(array('session_id'=>$id));
}
	
function sess_gc($maxlifetime)
{
	$dbConn = DBModel::getDbConn();
	$pdoQ = $dbConn->prepare('DELETE FROM session_editor WHERE ADDTIME(time_last,SEC_TO_TIME('.MAX_SESSION_LIFETIME.'))<NOW()');
	$pdoQ->execute(array());
}
	
function sess_open($save_path, $name)
{
	// To realize
}
	
function sess_read($id)
{
	$dbConn = DBModel::getDbConn();
	$pdoQ = $dbConn->prepare('SELECT * FROM session_editor WHERE session_id=:session_id
	AND ip_address=:ip_address AND user_agent_hash=SHA1(:user_agent)');
	$pdoQ->execute(array(
			'session_id'=>$id,
			':ip_address'=>!empty($_SERVER['HTTP_X_REAL_IP']) 
				? $_SERVER['HTTP_X_REAL_IP']
				: $_SERVER['REMOTE_ADDR'],
			'user_agent'=>$_SERVER['HTTP_USER_AGENT']
		));
	$row = $pdoQ->fetch();
	if (empty($row)) {
		
		$GLOBALS['session_to_regenerate'] = true;
		return '';	
	}
	$_SESSION['editor_id'] = $row['editor_id'];
	return $row['data'];
}	
	
function sess_write($id,$data)
{
	if (!empty($_SESSION['editor_id'])) {
		$editor_id = $_SESSION['editor_id'];
		unset($_SESSION['editor_id']);
	} else {
		$editor_id = NULL;
	}
	unset($_SESSION['editor']);
	$data = serialize($_SESSION);
		
	$dbConn = DBModel::getDbConn();
		
	$pdoQ = $dbConn->prepare('SELECT COUNT(*) as count FROM session_editor WHERE session_id=:session_id
	AND ip_address=:ip_address AND user_agent_hash=SHA1(:user_agent)');
	$pdoQ->execute(array(
			':session_id'=>$id,
			':ip_address'=>!empty($_SERVER['HTTP_X_REAL_IP']) 
				? $_SERVER['HTTP_X_REAL_IP']
				: $_SERVER['REMOTE_ADDR'],
			':user_agent'=>$_SERVER['HTTP_USER_AGENT']
		));
	$count = $pdoQ->fetchColumn(0);
	if ($count==1) {
		$pdoQ = $dbConn->prepare('UPDATE session_editor SET session_id=:session_id, editor_id=:editor_id, data=:data, time_last=NOW() WHERE session_id=:session_id');

		$res = $pdoQ->execute(array(':session_id'=>$id,':data'=>$data,':editor_id'=>$editor_id));
	} elseif ($count==0) {
		$pdoQ = $dbConn->prepare(
				'INSERT INTO session_editor SET 
				session_id=:session_id,
				ip_address=:ip_address,
				user_agent=:user_agent,
				user_agent_hash=SHA1(:user_agent),
				editor_id=:editor_id,
				data=:data,
				time_last = NOW(),
				time_first = NOW(); 
				'
				);
		$pdoQ->execute(array(
				':session_id'=>$id,
				':ip_address'=>!empty($_SERVER['HTTP_X_REAL_IP']) 
					? $_SERVER['HTTP_X_REAL_IP']
					: $_SERVER['REMOTE_ADDR'],
				':user_agent'=>$_SERVER['HTTP_USER_AGENT'],
				':data'=>$data,
				':editor_id'=>$editor_id
		));
	} else {
		throw new Exception("Session getting error");

	}
}	

session_set_save_handler('sess_open', 'sess_close', 'sess_read', 'sess_write', 'sess_destroy', 'sess_gc');


?>