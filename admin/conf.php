<?php
$pdo_conf = array(
	'addr'=>'mysql:host=localhost;dbname=bit',
	'user'=>'root',
	'pass'=>'3jku4ps'
);


$url_configuration = array(
	'/'=>array(
		'controller'=>'login',
		'views'=>'login,message',
		'models'=>'editor'
	),
	'/logout/'=>array(
		//'models'=>'user',
		'controller'=>'logout',
		//'views'=>'header,login'
	),
	'/passwort-vergessen/'=>array(
		'controller'=>'forgotpassword',
		'views'=>'forgotpassword,message,newpass_by_code',
		'models'=>'editor'
	),
	'.default'=>array(
		'controller'=>'retrieve',
	)
);
?>
