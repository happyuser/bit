<?

include ('header1.php');

@$_DB_member=_DB_views('events');
@$_DB_event_filter = DB_events_filter();

@$_DB_member=_DB_views('guest_list');
    if(!$_GET[id_event]){
        $_GET[id_event]=last_id('events');
    }
@$_DB_guest_filter = DB_guest_filter($_GET[id_event]);

@$_DB_member_name=_DB_views('member_column_names');
//print_r($_DB_guest_filter);
/*
include ('../controllers/filters.php');
include ('../controllers/search.php');
include ('../controllers/sort.php');
*/
?>
<script src="js/registration.js" type="text/javascript"></script>
<div class="row-fluid" style="margin-top:15px;">
    <div class="span4">
        <label class="registration_label_veranstaltung">Veranstaltung</label>
        <select class="event" name="events" style="margin-left: 10px;">
            <?
            foreach($_DB_event_filter as $key=>$val){?>
            <option value="<?=$val[id]?>" <?if($_GET[id_event]==$val[id]){?>selected<?}?>> <?=$val[name]?></option>
            <?}?>
        </select>
    </div>
</div>
<div class="span5 list_events list_events_registration">
    <table class="table table-hover table_hover_registration">
        <thead>
            <th>ID</th>
            <th>Vorname</th>
            <th>Name</th>
            <th>Funktion</th>
        </thead>
        <tbody>
        <?
        if($_DB_guest_filter)
        foreach($_DB_guest_filter as $key=>$val){?>
            <tr class='guest<?if($val[info_guest][status]=='was')echo" was";?>' 
                type="<?=$val[info_guest][type_user]?>" id="<?=$val[info_guest][id_user]?>" limit="<?=$val[info_user][limit]?>" 
                accompany="<?if($val[info_guest]['type_user']=='users')echo $val[info_guest][accompany]; else echo "kein";?>" 
                angemeldet="<?if($val[info_guest][time_at_event])echo print_date($val[info_guest][time_at_event],'hour')?>" 
                firma="<?=$val[info_user][firma]?>" photo="<?if($val[info_guest]['type_user']=='users')echo $val[info_user][photo]; else echo $val[info_user][foto];?>">
                <td><?=$val[info_guest][id_user]?></td>

                <td class="name_search name" id="<?=$val[info_guest][id_user]?>"><?=$val[info_user][first_name]?></td>
                <td class="name_search vorname" id="<?=$val[info_guest][id_user]?>"><?=$val[info_user][last_name]?></td>
                <td class="name_search functions" id="<?=$val[info_guest][id_user]?>"><?if($val[info_guest]['type_user']=='users')echo $val[info_user]['functions']; else echo $val[info_user]['funktion_bus'];?></td>
            </tr>
        <?}?>
        </tbody>
    </table>
</div>
<div class="full_info_event" id="">
    <div class="input-append input_append_registration">
        <input type="text" class="search-query"/>
        <button type="submit" class="btn search_einlass" id="appendedInputButtons">Suche</button>
        <div class="search_clear_einlass"><i class="icon-remove"></i></div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <img src="../uploads/useremptylogo.png"/>
        </div>
        <div class="span8">
            <div class="name_vorname_box">
                <div class="name_vorname">Vorname Name</div>
                <div class="">
                   <!-- <div class="row-fluid">
                        <div class="span5">Titel:</div><div class="span7 titel"></div>
                    </div>-->
                    <div class="row-fluid">
                        <span>Firma: </span><span class="firma"></span>
                    </div>
                    <div class="row-fluid">
                        <span>Funktion: </span><span class="function"></span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="span12" style="margin: 10px 0 0 0;">
            <div class="row-fluid">
                <span class="full_info_event_label">Uhrzeit eingeladen:</span><span class="time_invited"></span>
            </div>
            <div class="row-fluid">
                <span class="full_info_event_label">Uhrzeit angemeldet:</span><span class="time_at_event"></span>
            </div>
            <div class="row-fluid">
                <div class="full_info_event_label">Angemeldete Begleitung:</div>
            </div>
            <div class="row-fluid" id="with_guest">
                <div class="span8"><textarea name="accompany"></textarea></div>
            </div>
            <div class="row-fluid">
                <button class="btn" title="Anmelden" id="save_registration" style="height: 40px;">Anmelden</button>
            </div>
        </div>
                
    </div>
</div>