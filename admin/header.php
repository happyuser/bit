<?
include ('../controllers/functions.php');
include ('../controllers/sessions.php');
include ('languages/english.php');
connect();
/*
 * @author Dmytro
 * @todo Перевіряє чи користувач авторизований. Так - витягується с його права. Ні - редирект.
 */
if (!isset($_COOKIE['admin_id'])) {
	header("Location: login.php");
	exit;
}
$id = $_COOKIE['admin_id'];
$query = mysql_query("SELECT * FROM editor WHERE id='$id'");
$row = mysql_fetch_assoc($query);
$_ACCESS = $row['status'];
/******************************************/ 

include ('../controllers/main.php');
include ('../controllers/taras/filter.php');
include ('../controllers/taras/html_dom.php');
include ('../controllers/taras/common.inc');
include ('../controllers/pages_table.php');

global $_DB_member;
    if($_ACCESS=='registration'){
        if($_SERVER['SCRIPT_NAME']!='/admin/registration.php'){header("Location: /admin");die();}
    }
    if($_ACCESS=='team'){
        if($_SERVER['SCRIPT_NAME']=='/admin/mitglieder_approve.php'){header("Location: /admin");die();}
    }
?>
<html lang="de">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>BIT admin</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/bootstrap-table.css">
        <script src="js/jquery-2.0.0.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/index.js" type="text/javascript"></script>        
        <script src="js/bootstrap-table.js"></script>
    </head>
    <body>
    <div class="no_activ_body"></div>
    <div class="container-fluid">
        <div class="row-fluid menu">
			<? $s = (preg_match('/admin\/(.*\.php)/', $_SERVER['REQUEST_URI'], $a) ? $a[1] : ''); ?>
            <ul class="menu_header">
                <li class="<?php print ($s == 'mitglieder_registered.php') ? active : ''; ?>"><a href="mitglieder_registered.php">Mitglieder</a></li>
                <li class="<?php print ($s == 'event_list.php') ? active : ''; ?>"><a href="./event_list.php">Veranstaltung</a></li>
                <li class="<?php print ($s == 'guest_list_eingeladene.php') ? active : ''; ?>"><a href="./guest_list_eingeladene.php">Gästeliste</a></li>
                <li class="<?php print ($s == 'verteiler.php') ? active : ''; ?>"><a href="./verteiler.php">Verteiler</a></li>
                <li class="<?php print ($s == 'registration.php') ? active : ''; ?>"><a href="./registration.php">Einlass</a></li>
                <li class=""><a href="ses_des.php">Ausloggen</a></li>
            </ul>
            <ul class="sub_menu_header">
                <li class="<?php print ($s == 'mitglieder_registered.php') ? active : ''; ?>"><a href="./mitglieder_registered.php">Mitglieder</a></li>
                <li class="<?php print ($s == 'mitglieder_kandidate.php') ? active : ''; ?>"><a href="./mitglieder_kandidate.php">Bewerber</a></li>
				<li class="<?php print ($s == 'mitglieder_approve.php') ? active : ''; ?>"><a href="./mitglieder_approve.php">Geänderte Mitglieder</a></li>
                <li><button type="submit" style="height: 30px;border-radius: 4px;border: none;float: right;margin-left: 50px;">Archivieren</button></li>
            </ul>
        </div>
