<?
include ('header.php');
@$GLOBALS['_DB_member']=_DB_views('events');
$_DB_member_filter=DB_events_filter();
$_DB_member_name = _DB_views('event_column_names');

include ('controllers/filters.php');
include ('controllers/search.php');
include ('controllers/sort.php');
?>
<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>

<script src="js/tables.js" type="text/javascript"></script>
<script src="js/event_list.js" type="text/javascript"></script>
<link href="js/external/google-code-prettify/prettify.css" rel="stylesheet">
<script src="js/external/jquery.hotkeys.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
<script src="js/external/google-code-prettify/prettify.js"></script>
<link href="css/wysiwyg.css" rel="stylesheet">
<script src="js/bootstrap-wysiwyg.js"></script>

<div class="pop_window" id="change_event" style="display: none;overflow: auto; height: 350px;">
    <div class="form_change_event">

        <div id="alerts"></div>
        <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor,#editor1,#editor2">
            <div class="btn-group">
                <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
            </div>

            <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
        </div>

    </div>

    <script>
        $(function(){

            $('#editor,#editor1,#editor2').wysiwyg();

        });
    </script>


    <div class="control-group" style="overflow: hidden">
        <label class="control-label space-right"  for="editor">Names des Events</label>
        <div class="controls">
            <div id="editor" class="span4" style="height: 20px;margin: 0;width: 346px">
            </div>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" for="city">Sdatd</label>
        <div class="controls">
            <input type="text" class="span4" style="height: 30px" id="city" placeholder="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" for="date_event">Eventtermine</label>
        <div class="controls">
            <input type="text" class="span4" style="height: 30px" id="date_event" placeholder="">
        </div>
    </div>

    <div class="control-group" style="overflow: hidden">
        <label class="control-label space-right" for="description_event">Beschreibung des Events</label>
        <div class="controls">
            <div id="editor1" class="span4" style="min-height: 130px;margin: 0;width: 356px">
            </div>
        </div>
    </div>

    <div class="control-group" style="overflow: hidden">
        <label class="control-label space-right" for="paragraph">Paragraph</label>
        <div class="controls">
            <div id="editor2" class="span4" style="min-height: 110px;margin: 0;width: 356px">
            </div>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" id="label_event_points" for="event_points">Programmpunkte mit Uhrzeiten</label>

        <div class="controls" id="points_time">

            <input type="text" class="span4" style="height: 30px" id="description_event_points" placeholder="">
            <input type="time" style="height: 30px;width: 85px" id="event_points" placeholder="">
            <button class="close" id="remove_event_point" data-dismiss="alert" style="float: none;margin-bottom: 10px">&times;</button>

        </div>

        <div class="controls">
            <button class="btn btn-primary" id="add_new_event_point"><i class="icon-plus icon-white"></i> Hinzufugen</button>
        </div>

    </div>

    <div class="control-group" style="overflow: hidden">
        <label class="control-label space-right" for="photos">Fotos</label>
        <div class="controls">
            <div class="span4 photo_part" id="photos"style="width: 355px" >
                <div id="wrapper">
                    <div id="origin" class="fbox" style="min-height: 50px">
                        <div style="float: left">Hauptfoto</div>

                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/150x100" id="one" title="one" style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>

                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/160x100" id="one" title="one"  style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>
                    </div>

                    <hr>

                    <div id="drop" class="fbox"  style="min-height: 50px">
                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/150x100" id="one" title="one" style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>

                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/140x100" id="one" title="one"  style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>
                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/160x100" id="one" title="one"  style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>

                    </div>
                </div>
                <div class="controls" style="margin-top: 10px">
                    <label for="add_new_photo" class="btn btn-primary pull-right">
                        <i class="icon icon-plus"></i> Hinzfungen
                    </label>
                    <input id="add_new_photo" style="display: none" type="file"/>
                </div>
            </div>
        </div>
    </div>


    <div class="control-group">
        <label class="control-label space-right" for="youtube_link">Link zur Youtube Seite</label>
        <div class="controls">
            <input type="text" class="span4" style="height: 30px" id="youtube_link" placeholder="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" for="deadline">Anmeldeschluss</label>
        <div class="controls">
            <input type="time" class="span4" style="height: 30px" id="deadline" placeholder="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" style="margin-bottom: 30px" for="main_spaosor">Gastgeber</label>
        <div class="controls">
            <select class="span4" id="main_spaosor" style="height: 30px;width: 357px;">
                <option>Eventseite in System</option>
            </select>
            <input type="text" class="span4" style="height: 30px" id="main_spaosor_link" placeholder="Link im Internet">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" style="margin-bottom: 30px" for="sub_spaosor">Austatter-Partner</label>
        <div class="controls">
            <select class="span4" id="sub_spaosor" style="height: 30px;width: 357px;">
                <option>Eventseite in System</option>
            </select>
            <input type="text" class="span4" style="height: 30px" id="sub_spaosor_link" placeholder="Link im Internet">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" style="margin-bottom: 30px" id="label_rest_sponsor" for="rest_sponsor">Weitere Sponsoren</label>

        <div class="controls" id="new_sponsor">

            <select class="span3" id="rest_sponsor" style="height: 30px;width: 357px;">
                <option>Eventseite in System</option>
            </select>
            <button class="close" id="remove_spansor" data-dismiss="alert" style="float: none;margin-bottom: 10px">&times;</button>

        </div>

        <div class="controls">
            <button class="btn btn-primary" id="add_new_sponsor"><i class="icon-plus icon-white"></i> Hinzufugen</button>
        </div>

    </div>
    <div class="row-fluid" style="text-align: right">
        <div style="padding-bottom: 10px">
            <button class="btn" id="close_pop_window">Abbrechen</button>
            <button class="btn" id="btn_new_event" style="margin-right: 100px;">Speichern</button>
        </div>
    </div>
</div>

    <!--<form class="form_change_event">
            <div class="row-fluid" style="overflow: auto;height: 300px;">
            <?/*foreach($_DB_member_name as $key=>$val){
                if($val[english]!='active' && $val[english]!='approve' && $val[english]!='profession')
                {
                */?>
                <div class="row-fluid">
                    <?/*if($val[english]!='images'){*/?>
                        <div class="span4 offset2"><?/*=$val[german]*/?></div>
                    <?/*}else{*/?>
                    <div class="imgs_row">
                        <div class="span12" style="text-align: center;"><?/*=$val[german]*/?></div>
                        <i class="add_photo_event icon-plus"></i>
                    <?/*}
                    if($val[english]=='type_of_event'){*/?>
                    <select name="<?/*=$val[english]*/?>">
                        <option value="mmm">MMM</option>
                        <option value="mumm">MuMM</option>
                        <option value="sonstige">Sonstige</option>
                    </select>
                    <?/*}elseif($val[english]=='dates' || $val[english]=='deadline'){*/?>
                        <input type="date" name="<?/*=$val[english]*/?>" value=""/>
                    <?/*}elseif($val[english]=='points'){*/?>
                        <span class="start_point"></span>
                        <div class="point">
                            <input type="text" class="point_txt" name="points[txt][]" value=""/>
                            <input type="time" class="point_time" name="points[time][]" value=""/>
                        </div>
                    <?/*}elseif($val[english]=='images'){*/?>
                        <span class="start_images"></span>
                    </div>
                    <?/*}else{*/?>
                    <input type="text" name="<?/*=$val[english]*/?>" value=""/>
                    <?/*}*/?>
                </div>
                <?/*}
                }*/?>
            </div>
    </form>
        <div class="row-fluid">
            <button class="btn" id_event="" id="btn_change_event">Speichern</button>
            <button class="btn" id="close_pop_window">Abbrechen</button>
        </div>
    </div>-->

<div class="pop_window" id="new_member" for="events" style="overflow: auto; height: 350px; display: none;">
<div class="new_event">

    <div id="alerts"></div>
    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor,#editor1,#editor2">
        <div class="btn-group">
            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
        </div>
        <div class="btn-group">
            <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
        </div>

        <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
    </div>

	</div>

    <script>
        $(function(){

            $('#editor,#editor1,#editor2').wysiwyg();

        });
    </script>


         <div class="control-group" style="overflow: hidden">
            <label class="control-label space-right"  for="editor">Names des Events</label>
            <div class="controls">
                <div id="editor" class="span4" style="height: 20px;margin: 0;width: 346px">
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label space-right" for="city">Sdatd</label>
            <div class="controls">
                <input type="text" class="span4" style="height: 30px" id="city" placeholder="">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label space-right" for="date_event">Eventtermine</label>
            <div class="controls">
                <input type="text" class="span4" style="height: 30px" id="date_event" placeholder="">
            </div>
        </div>

        <div class="control-group" style="overflow: hidden">
            <label class="control-label space-right" for="description_event">Beschreibung des Events</label>
            <div class="controls">
                <div id="editor1" class="span4" style="min-height: 130px;margin: 0;width: 356px">
                </div>
            </div>
        </div>

        <div class="control-group" style="overflow: hidden">
            <label class="control-label space-right" for="paragraph">Paragraph</label>
            <div class="controls">
                <div id="editor2" class="span4" style="min-height: 110px;margin: 0;width: 356px">
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label space-right" id="label_event_points" for="event_points">Programmpunkte mit Uhrzeiten</label>

            <div class="controls" id="points_time">

                <input type="text" class="span4" style="height: 30px" id="description_event_points" placeholder="">
                <input type="time" style="height: 30px;width: 85px" id="event_points" placeholder="">
                <button class="close" id="remove_event_point" data-dismiss="alert" style="float: none;margin-bottom: 10px">&times;</button>

            </div>

            <div class="controls">
                <button class="btn btn-primary" id="add_new_event_point"><i class="icon-plus icon-white"></i> Hinzufugen</button>
            </div>

        </div>

    <div class="control-group" style="overflow: hidden">
        <label class="control-label space-right" for="photos">Fotos</label>
        <div class="controls">
            <div class="span4 photo_part" id="photos"style="width: 355px" >
                <div id="wrapper">
                    <div id="origin" class="fbox" style="min-height: 50px">
                        <div style="float: left">Hauptfoto</div>

                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/150x100" id="one" title="one" style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>

                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/160x100" id="one" title="one"  style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>
                    </div>

                    <hr>

                    <div id="drop" class="fbox"  style="min-height: 50px">
                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/150x100" id="one" title="one" style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>

                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/140x100" id="one" title="one"  style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>
                        <div style="display: inline-block" class="draggable">
                            <img src="http://placehold.it/160x100" id="one" title="one"  style="width: 70px;height: 70px;margin: 5px" />
                            <button id="remove_photo" style="position: absolute;top: 2px;right:0;background: none;border: none;outline: none">
                                <i class="icon-remove icon-white"></i></button>
                        </div>

                    </div>
                </div>
                <div class="controls" style="margin-top: 10px">
                    <label for="add_new_photo" class="btn btn-primary pull-right">
                        <i class="icon icon-plus"></i> Hinzfungen
                    </label>
                    <input id="add_new_photo" style="display: none" type="file"/>
                </div>
            </div>
        </div>
    </div>


    <div class="control-group">
        <label class="control-label space-right" for="youtube_link">Link zur Youtube Seite</label>
        <div class="controls">
            <input type="text" class="span4" style="height: 30px" id="youtube_link" placeholder="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" for="deadline">Anmeldeschluss</label>
        <div class="controls">
            <input type="time" class="span4" style="height: 30px" id="deadline" placeholder="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" style="margin-bottom: 30px" for="main_spaosor">Gastgeber</label>
        <div class="controls">
           <select class="span4" id="main_spaosor" style="height: 30px;width: 357px;">
               <option>Eventseite in System</option>
           </select>
            <input type="text" class="span4" style="height: 30px" id="main_spaosor_link" placeholder="Link im Internet">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" style="margin-bottom: 30px" for="sub_spaosor">Austatter-Partner</label>
        <div class="controls">
            <select class="span4" id="sub_spaosor" style="height: 30px;width: 357px;">
                <option>Eventseite in System</option>
            </select>
            <input type="text" class="span4" style="height: 30px" id="sub_spaosor_link" placeholder="Link im Internet">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label space-right" style="margin-bottom: 30px" id="label_rest_sponsor" for="rest_sponsor">Weitere Sponsoren</label>

        <div class="controls" id="new_sponsor">

            <select class="span3" id="rest_sponsor" style="height: 30px;width: 357px;">
                <option>Eventseite in System</option>
            </select>
            <button class="close" id="remove_spansor" data-dismiss="alert" style="float: none;margin-bottom: 10px">&times;</button>

        </div>

        <div class="controls">
            <button class="btn btn-primary" id="add_new_sponsor"><i class="icon-plus icon-white"></i> Hinzufugen</button>
        </div>

    </div>
		<div class="row-fluid" style="text-align: right">
			<div style="padding-bottom: 10px">
			<button class="btn" id="close_pop_window">Abbrechen</button>
			<button class="btn" id="btn_new_event" style="margin-right: 100px;">Speichern</button>
			</div>
		</div>
</div>


<div class="container-fluid">
        <div class="span8">
            <button class="btn" id="new" title="new" for="events" style="margin: 2px; float: left;"><i class="icon-file"></i></button>
            <button class="btn change_event" title="change" id="" style="margin: 2px; float: left;"><i class="icon-pencil"></i></button>
            <?if($_ACCESS=='superadmin'){?>
                <button class="btn" id="delete" title="delete" for="events"><i class="icon-remove"></i></button>
            <?}?>
            <div class="pagination" style="float: left;margin: 0 0 0 50px;">
              <ul>
                <li><a href="?<?=GET_str('page_start=1')?>" style="height: 25px;margin-top: -2px;"> << </a></li>
                <li><a href="?<?=GET_str('page_start='.($page_start-10))?>" style="height: 23px;margin: -1px;"> < </a></li>
                <li class="active"><a href="?<?=$_GET_STR?>&page_start=<?=$page_start?>"><?=ceil($page_start / 10)?></a></li>
                <?if (($page_start + 10) * 1 < count($_DB_member_filter)){?><li><a href="?<?=GET_str('page_start='.($page_start + 10) * 1)?>"><?=ceil($page_start / 10 + 1)?></a></li><?}?>
                <?if (($page_start + 10) * 2 < count($_DB_member_filter)){?><li><a href="?<?=GET_str('page_start='.($page_start + 10) * 2)?>"><?=ceil($page_start / 10 + 2)?></a></li><?}?>
                <?if (($page_start + 10) * 3 < count($_DB_member_filter)){?><li><a href="?<?=GET_str('page_start='.($page_start + 10) * 3)?>"><?=ceil($page_start / 10 + 3)?></a></li><?}?>
                <?if (($page_start + 10) * 4 < count($_DB_member_filter)){?><li><a href="?<?=GET_str('page_start='.($page_start + 10) * 4)?>"><?=ceil($page_start / 10 + 4)?></a></li><?}?>
                <li><a href="?<?=GET_str('page_start='.(floor(count($_DB_member_filter) / 10) * 10+1))?>"  style="height: 23px;margin: -1px;"> >> </a></li>
              </ul>
            </div>
        </div>
        <div class="span4">
            <div class="input-append">
                <input type="text" class="search-query" value="<?=$_GET[search]?>"/>
                <button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
            </div>
        </div>
    </div>

<div class="row-fluid body" style="overflow: auto;margin-left: 50px;">
    <form class="table">
        <table class="table table-bordered">
            <thead>
				<th class="id_th_check" style="margin-left: -64px; padding-top: 0">ID</th>
				<?php
				foreach($_DB_member_name as $names){
                ?>
                <th>
                    <?=$names[german]?>
                    <a class="sort" id="<?=$names[english]?>~<?if($_GET['sort']==($names[english].'~asc')){?>desc<?}else{?>asc<?}?>" style="cursor: pointer;"><i class="<?if($_GET['sort']==($names[english].'~asc')){?>icon-chevron-down<?}else{?>icon-chevron-up<?}?>"></i></a>
                    <!--<div><?get_filter_for_column("filter~$names[english]",'member');?></div>-->
                    </th>
                <?
				}
				?>
            </thead>
            <tbody>
                <?php
                $can = true;
                if($_DB_member_filter[$page_start-1])$page_start-=1;//це для тому що я люблю умови))  кароче сортування вертає массив починаючи з 0 а $page_start по замовчуванню 1. ось так ось
                for ($i = $page_start; $i < $page_start + 10; $i++){
                    if($i>count($_DB_member_filter))break;
                ?>
                <tr>
                    <td class="id_td_it" style="margin-left: -66px; height: 65px;"><?=$_DB_member_filter[$i][id];?></td>
                    <td class="id_td_check" style="margin-left: -91px;"><input type="checkbox" value="<?=$_DB_member_filter[$i][id];?>" name="id"/></td>
                    <?php
					//print_r($_DB_member_name); exit;

					foreach($_DB_member_name as $names)
					{
						if(in_array($names[english], array('active', 'approve') ))
							continue; //skip that fields

						print "<td>";
						print $_DB_member_filter[$i][$names[english]];
						print "</td>\n";
					}
					?>

                </tr>
                <?}?>
            </tbody>
        </table>
        </form>
    </div>
