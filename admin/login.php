<html>
<head>
<meta charset="utf=8" />
<title>BIT</title>
<style>
html, body {
	width: 100%;
	height: 100%;
}
html {
	display: table;
}
body {
	display: table-cell;
	text-align: center;
	vertical-align: middle;
}
body > * {
	display: inline-block;
}
form .error {
	color: red;
	display: block;
}
</style>
</head>
<?php
ini_set('display_errors', '1');
// include_once 'debug.php';
require_once 'models/Dictionary.php';
$dict = new Dictionary('de', array('en', 'ua'));
/*
$login_form = true;

if (!empty($_POST)) {
	require_once('model/Editor.php');
	$editor = new Editor();
	$editor->filter(array('email'=>$_POST['email'],'sha_password'=>sha1($_POST['password'])));

	switch ($editor->count) {
		case 1:
			//$editor->getFirst();
			echo 'Hello, '.$editor->first_name.' '.$editor->last_name.'<br /> Your status: '.$editor->status;
			$login_form = false;
			break;
		case 0:
			$error = $dict['incorrect_email_or_password'];
			break;
		default:
			throw new EDbConstructionErrror();
	}
}

if ($login_form) {
*/
?>
<center>
<p>Login for administrator</p>
<form method="post" action="index.php">
<?php if(!empty($error)) {?><span class="error"><?php echo $error; ?></span><?php } ?>
<label><?php echo $dict['email']; ?>:</label>
<input type="text" name="email" value="<?php if (!empty($_POST['email'])) echo htmlspecialchars(($_POST['email'])); ?>" />
<label><?php echo $dict['password']; ?>:</label>
<input type="password" name="password" />
<input type="submit" value="<?php echo $dict['submit']; ?>" />
</form>
<p><a href="passwort-vergessen"><?php echo $dict['password_forgotten']; ?></a></p>
</center>
</body>
</html>