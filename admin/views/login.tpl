<center>
<form action="/" method="post">
	<?php if(!empty($error)) {?><span class="error"><?php echo $error; ?></span><?php } ?>
	<label><?php echo $dict['email']; ?>:</label>
	<input type="text" name="email" value="<?php if (!empty($_POST['email'])) echo htmlspecialchars(($_POST['email'])); ?>" />	
	<label><?php echo $dict['password']; ?>:</label>
	<input type="password" name="password" />
	<input type="submit" value="<?php echo $dict['submit']; ?>" />
</form>
<p><a href="/passwort-vergessen/"><?php echo $dict['password_forgotten']; ?></a></p>
</center>