<?php

class BaseView
{
	
	static public function getnew($view)
	{
		$viewN = 'V'.ucfirst($view);
		if (file_exists(VIEWS_DIR.'/'.$viewN.'.php')) {
			require_once(VIEWS_DIR.'/'.$viewN.'.php');
			return new $viewN($view);
		} else {
			return new BaseView($view);
		}
	}	
	
	protected $name;	
	protected $cssFiles = array('main2');
	protected $jsFiles = array();
	public $variables = array();
	
	public function __construct($name,$variables=array())
	{
		$this->name = $name;
		$this->variables = $variables;
	}
	
	public function __get($propertyName)
	{
		if (!isset($this->variables[$propertyName]))
			$this->variables[$propertyName] = '';
		return $this->variables[$propertyName];	
	}
	public function __set($propertyName,$propertyValue)
	{
		$this->variables[$propertyName] = $propertyValue;	
	}

	public function echooHtmlHeadSection()
	{
		foreach ($this->cssFiles as $filename)
			echo '<link rel="stylesheet" href="/css/'.$filename.'.css" />';
		foreach ($this->jsFiles as $filename)
			echo '<script src="/js/'.$filename.'"></script>';
	}
	
	public function echoo()
	{
		extract($this->variables);
		$dict =& $GLOBALS['dict'];
		require_once(VIEWS_DIR.'/'.$this->name.'.tpl');
	}
	
	public function addVariable($name,$value,$asNewArrayKey=false)
	{
		if ($asNewArrayKey) {
			$this->variables[$name][]=$value;
		} else {
			$this->variables[$name]=$value;
		}		
	}
}

?>