<?php
set_time_limit(50);
ob_implicit_flush();
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/*error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

*/
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
require_once dirname(__FILE__) . '/lib/phpExcel/Classes/PHPExcel/IOFactory.php';
require_once '../controllers/functions.php';
connect();

    $chars = 'abcdefABCDEF';
    $numChars = strlen($chars);
    $string = '';
    for ($i = 0; $i < 3; $i++) {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
    }
    $gen_key=$string;
    $filename ='import_files/'.time().$gen_key.'.xls';
//print_r($_FILES['tmp_name']);
move_uploaded_file($_FILES['file']['tmp_name'], $filename);

// Check prerequisites
if (!file_exists($filename)) {
	exit("no file /admin/$filename.\n");
}

/*class MyReadFilter implements PHPExcel_Reader_IReadFilter
{
	public function readCell($column, $row, $worksheetName = '') {
		// Read title row and rows 20 - 30
		if ($row == 1 || ($row >= 20 && $row <= 30)) {
			return true;
		}

		return false;
	}
}
*/

$objPHPExcel = PHPExcel_IOFactory::load($filename);

//$objPHPExcel = $objReader->load($filename);
// get data from file 
$objWorksheet = $objPHPExcel->getActiveSheet(0);
//print_r($objWorksheet);

echo '<table>' . "\n";
$i=0;
$columns=$export=$arr=$select_column=array();
foreach ($objWorksheet->getRowIterator() as $row) {
  $cellIterator = $row->getCellIterator();
  $cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,
                                                     // even if it is not set.
                                                     // By default, only cells
                                                     // that are set will be
                                                     // iterated.
    if($i==0)
        echo '<tr style="background:#eee">'. "\n";        
    else
        echo '<tr>' . "\n";
    reset($columns);
    foreach ($cellIterator as $cell) {
        $val=$cell->getValue();
        if($i==0)
            $columns[]=$val;
        else{
            $arr[current($columns)]=$val;
            next($columns);
        }
            echo '<td>' . $val . '</td>' . "\n";
    }
    $export[]=$arr;
  $i++;
  echo '</tr>' . "\n";
}
echo '</table>' . "\n";
//end of get data 


//save data to DB

//echo date('H:i:s') , " Remove unnecessary rows" , EOL;
//$objPHPExcel->getActiveSheet()->removeRow(2, 18);

/*echo date('H:i:s') , " Write to Excel2007 format" , EOL;
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
*/

// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;
$select=select_DB('member_column_names');
foreach($select as $value){
    $select_column[trim(strtolower($value['german']))]=trim(strtolower($value['english']));
}

foreach(select_DB('countries') as $key=>$val){
    $countries[strtolower($val[country])]=$val;
}

$array=array(
'Firma'=>'privat_firma',
'Name'=>'first_name',
'Vorname'=>'last_name',
'Voranrede'=>'before_title',
'Anrede'=>'salutation',
'Funktion'=>'functions',
'Verteiler'=>'distribution',
'Straße'=>'adress_privat',
'PLZ & Ort'=>'plz_privat',
'Land'=>'country',
'Telefon'=>'telephone_privat',
'Fax'=>'fax_privat',
'Mobil'=>'phone_privat',
'E-Mail'=>'email_privat',
'Einladung per Mail oder per Post'=>'invitation',
'Wohnort'=>'private_wohnort',
'DC einladen'=>'dc',
'MumM einladen'=>'mumm',
'Sonstiges Einladen'=>'sonstige',
'E-Mail Newsletter schicken'=>'newsletter',
'Konkrete Angebote Mailen'=>'proposition',
'persönlich bekannt bei'=>'personal',
'per Du oder Sie'=>'du_or_sie',
'letzter Kontakt'=>'letzter'
);

foreach($export as $key=>$value){
    if(!$value)continue;
    unset($for_db);
    foreach($value as $k=>$v){
        if(!$v)continue;
        if(!isset($array[$k]))continue;
        if($k=='Verteiler')$v=strtolower($v);
        if($k=='Land')$v=$countries[strtolower($v)][id];
        
        $for_db[$array[$k]]=$v;
    }
    
    /*
    foreach($value as $k=>$v){
        echo $k.'=>'.$v."<br />";
    }
    */
    $for_db[name_table]='member';
    new_member($for_db);
    new_member(array('name_table'=>'access_users_info'));
    new_member(array('name_table'=>'friendes'));
    $pass=rand_string('qwertyuiop[]asdfghjkl;zxcvbnm,./!@#$%^&*()_+1234567890-=',10);
    new_member(array('name_table'=>'member_login','email'=>$for_db[email_privat],'password'=>md5($pass)));
    
    $reply_to = "fc@happyuser.info";
    $subject = "Willkommen bei FaceClub!";
    $message = "Password: $pass <br /> ";
    $headers  = "Content-type: text/html; charset=utf-8 \r\n";
    $headers .= "From: faceClub Admin <$reply_to>\r\n";
    
    //$headers .= "Bcc:vaylon@yandex.ru\r\n";
    mail($for_db[email_privat], $subject, $message, $headers);
}

    /*print_r($columns);
    print_r($export);
    print_r($select_column);*/




// Echo done
//echo date('H:i:s') , " Done writing file" , EOL;
//echo 'File has been created in ' , getcwd() , EOL;
