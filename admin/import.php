<?
include_once('header1.php');
?>
<input type="file" name="excel_import"/>
<button id="import">Import</button>
<div class="html_result" style="width: 80%; overflow: scroll; height: 80%;"></div>
<script type="text/javascript">
$(document).ready(function(){
    $('#import').click(function(){
        $(this).attr('disabled',true);
       var fd = new FormData();
        fd.append('file', $('[name=excel]')[0].files[0]);
        $.ajax({
          type: 'POST',
          url: 'import_xls_file.php',
          data: fd,
          processData: false,
          contentType: false,
          dataType: "html",
          success: function(data) {
            $('.html_result').html(data);
            $('#import').removeAttr('disabled');
          },
          error: function(data) {
            console.log(data);
          }
        });
        return false; 
    });
});
</script>
</body>
</html>