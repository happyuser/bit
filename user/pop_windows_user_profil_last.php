<div class="modal_edit_p change_profildaten">
    <div class="edit_profile">
        <div class="edit_profile_title">Profildaten</div>
        <a href="#">&nbsp;
            <?=if_access_info('profildaten')?></a>
        <form class="form_edit_p">
            <label>Titel</label>
            <input type="text" name="titel" value="<?=$_USER_INFO[member][titel]?>"/>
            <label>Funktion am Unternehmen</label>
            <input type="text" name="functions" value="<?=$_USER_INFO[member][functions]?>"/>
            <label>Firmenname</label>
            <input type="text" name="firma" value="<?=$_USER_INFO[member][firma]?>"/>
            <label>Geburtsdatum</label>
            <input type="date" name="birthdate" value="<?=$_USER_INFO[member][birthdate]?>"/><br />
            <label>Ich suche</label>
            <input type="text" name="i_search_for" value="<?=$_USER_INFO[member][i_search_for]?>"/>
            <label>Ich biete</label>
            <input type="text" name="i_offer_field" value="<?=$_USER_INFO[member][i_offer_field]?>"/>
            <button id="abbrechen">Abbrechen</button>
            <button id="speichern" after="change_close">Speichern</button>
        </form>
    </div>
</div>

<div class="pop_window change_textarea">
            <div class="close_window" style="margin-right: -28px; margin-top: -28px;">
                <i class="icon-remove icon-white"></i>
            </div>
            <?=if_access_info('textarea')?>
    <form class="form_change_member">
        <div class="row-fluid">
            <span class="title_window">Über mich</span>
            <div class="row-fluid" style="margin-top: 20px;">

                <textarea style="width: 600px;height: 100px" name="about_me"><?=$_USER_INFO[member][about_me]?></textarea>
                
            </div>
        </div>
        <div class="row-fluid">
            <div class="span7">
                <button class="btn btn-block btn-success save_user_btn" after="change_close">Speichern</button>
            </div>
            <div class="span5">
                <span class="label label-success status_message_ok">Speichern <i class="icon-ok icon-white"></i></span>
            </div>
        </div>
    </form>
</div>


<div class="pop_window change_contacts span7" style="display: none;">
            <div class="close_window" style="margin-right: -28px; margin-top: -28px;">
                <i class="icon-remove icon-white"></i>
            </div>
    <form class="form_change_member">
        <div class="row-fluid">
            <div class="span6" style="margin: 0;">
                <span class="title_window">Privatkontakten</span>
                
                <?=if_access_info('privatkontakten')?>
                
                <div class="row-fluid blocks" style="margin-top: 20px;">
                    <div class="span6" style="margin: 0;">
                        <label>Stadt</label>
                        <input type="text" name="postaddress" value="<?=$_USER_INFO[member][postaddress]?>"/>
                    </div>
                    <div class="span6">
                        <label>PLZ</label>
                        <input type="text" name="plz_privat" value="<?=$_USER_INFO[member][plz_privat]?>"/>
                    </div>
                    <label>Adresse</label>
                    <input type="text" name="adress_privat" value="<?=$_USER_INFO[member][adress_privat]?>"/>
                    <label>Telefonnummer</label>
                    <input type="text" name="telephone_privat" value="<?=$_USER_INFO[member][telephone_privat]?>"/>
                    <label>Handynummer</label>
                    <input type="text" name="phone_privat" value="<?=$_USER_INFO[member][phone_privat]?>"/>
                    <label>Faxnummer</label>
                    <input type="text" name="fax_privat" value="<?=$_USER_INFO[member][fax_privat]?>"/>
                    <label>Email</label>
                    <input type="text" name="email_privat" value="<?=$_USER_INFO[member][email_privat]?>"/>
                </div>
            </div>
            <div class="span6">
                <span class="title_window">Geschäftskontakten</span>
                
                <?=if_access_info('geschaftskontakten')?>
                
                <div class="row-fluid blocks" style="margin-top: 20px;">
                    <div class="span6" style="margin: 0;">
                        <label>Stadt</label>
                        <input type="text" name="postadress_bus" value="<?=$_USER_INFO[member][postadress_bus]?>"/>
                    </div>
                    <div class="span6">
                        <label>PLZ</label>
                        <input type="text" name="plz_bus" value="<?=$_USER_INFO[member][plz_bus]?>"/>
                    </div>
                    <label>Adresse</label>
                    <input type="text" name="adress_bus" value="<?=$_USER_INFO[member][adress_bus]?>"/>
                    <label>Telefonnummer</label>
                    <input type="text" name="telephone_bus" value="<?=$_USER_INFO[member][telephone_bus]?>"/>
                    <label>Handynummer</label>
                    <input type="text" name="phone_bus" value="<?=$_USER_INFO[member][phone_bus]?>"/>
                    <label>Faxnummer</label>
                    <input type="text" name="fax_bus" value="<?=$_USER_INFO[member][fax_bus]?>"/>
                    <label>Email</label>
                    <input type="email" name="email_bus" value="<?=$_USER_INFO[member][email_bus]?>"/>
                    <label>Internetadresse</label>
                    <input type="text" name="personal_homepage_url" value="<?=$_USER_INFO[member][personal_homepage_url]?>"/>

                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span7">
                <button class="btn btn-block btn-success save_user_btn" after="change_close">Speichern</button>
            </div>
            <div class="span5">
                <span class="label label-success status_message_ok">Speichern <i class="icon-ok icon-white"></i></span>
            </div>
        </div>
    </form>
</div>




<div class="pop_window change_netzprofilen">
            <div class="close_window" style="margin-right: -28px; margin-top: -28px;">
                <i class="icon-remove icon-white"></i>
            </div>
            
            <?=if_access_info('netzprofilen')?>
            
    <form class="form_change_member">
        <div class="row-fluid">
            <span class="title_window">Netzprofilen</span>
            <div class="row-fluid" style="margin-top: 20px;">
                <label>Facebook-Profil</label>
                <input type="text" name="facebook_url" value="<?=$_USER_INFO[member][facebook_url]?>" />
                <label>Linkedln-Profil</label>
                <input type="text" name="linked_in_url" value="<?=$_USER_INFO[member][linked_in_url]?>"/>
                <label>XING-Profil</label>
                <input type="text" name="xing_url" value="<?=$_USER_INFO[member][xing_url]?>"/>
                <label>Twitter-Profil</label>
                <input type="text" name="twitter_url" value="<?=$_USER_INFO[member][twitter_url]?>"/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span7">
                <button class="btn btn-block btn-success save_user_btn" after="change_close">Speichern</button>
            </div>
            <div class="span5">
                <span class="label label-success status_message_ok">Speichern <i class="icon-ok icon-white"></i></span>
            </div>
        </div>
    </form>
</div>

<div class="pop_window change_berufsvita span7" style="display: none;">
            <div class="close_window" style="margin-right: -28px; margin-top: -28px;">
                <i class="icon-remove icon-white"></i>
            </div>
            
            <?=if_access_info('berufsvita')?>
            
    <form class="form_change_member" id="">
        <div class="row-fluid">
            <span class="title_window">Berufsvita</span>
            <div class="row-fluid list_jobs blocks">
                                            
                <?php
                foreach($_USER_INFO[users_career] as $key=>$val){
                    if($val[time_start]>$val[time_end])
                    {
                       $_USER_INFO[users_career][$key][time_end] = $val[time_start];
                    }
                    
                    
                    $start=explode('-',$val[time_start]);
                    if($val[time_end]!='now' && $val[time_end]!='')$end=explode('-',$val[time_end]); else {$end[0]=date('Y',time()); $end[1]=date('M',time()); $val[time_end]='bis jetzt';}
                    $years=$end[0]-$start[0];
                    $months=$end[1]-$start[1];
                    if($months<0){$years-=1; $months=12+$months;}
                    if($years<0)
                    {
                        //$years = "0";    
                      if($months<1)
                      {
                        //$months = "0";
                        //$val[time_end] = $val[time_start];
                      }
                    }       
                          
                ?>
                <div class="job" id="<?=$val[id]?>">
                    <div class="span4 header_block">haben an dieser Stelle <?if($years){ echo $years;?> Jahre lang gearbeitet,  <?}if($months){ echo $months;?> Monate<?}?></div>
                    <div class="span6">
                        <div class="list_time_job"><span class="start_date"><?=date("M Y",strtotime($val[time_start]))?></span> - <span class="finish_date"><?=date("M Y",strtotime($val[time_end]))?></span></div>
                        <div class="list_firmenname"><?=$val[firmenname]?></div>
                        <div class="list_position"><?=$val[position]?></div>
                        <div class="list_aufgaben"><?=$val[aufgaben]?></div>
                    </div>
                    <div class="span2">
                        <div class="edit_job">
                            <i class="icon-pencil icon-white"></i>
                        </div>
                        <div class="drop_job">
                            <i class="icon-remove icon-white"></i>
                        </div>
                    </div>
                </div>
                <?}?>
            </div>
            <div class="row-fluid">
                <label>Unternehmen</label>
                <input type="text" name="where-firmenname" value="" />
                <div class="row-fluid">
                    <div class="span6">
                        <label>Seit</label>
                        <select class="mounth_start" style="float: left;width: 45%;">
                            <?php $mounths = '<option value="01">Januar</option>
                            <option value="02">Februar</option>
                            <option value="03">Marz</option>
                            <option value="04">April</option>
                            <option value="05">Mai</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Dezember</option>';
                            
                            echo $months; ?>
                        </select>
                        <select class="year_start" style="float: left;width: 45%;margin: 0 10px;">
                            <?for($i=2015;$i>1900;$i--){?>
                                <option <?=($i==2015?"selected":"")?> value="<?=$i?>"><?=$i?></option>
                            <?}?>
                        </select>
                        <input type="hidden" name="where-time_start" value=""/>
                    </div>
                    <div class="span6">
                        <label>Bis</label>
                        <select class="mounth_end" style="float: left;width: 45%;">
                            <?=$mounths;?>
                        </select>
                        <select class="year_end" style="float: left;width: 45%;margin: 0 10px;">
                            <?for($i=2015;$i>1900;$i--){?>
                                <option <?=($i==2015?"selected":"")?> value="<?=$i?>"><?=$i?></option>
                            <?}?>
                        </select>
                        <input type="hidden" name="where-time_end" value=""/>
                    </div>
                    <div class="offset6 span6">
                        <input type="checkbox" name="where-time_end" value="now" style="width: initial;"/> bis jetzt
                    </div>
                </div>
                <label>Position am Unternehmen</label>
                <input type="text" name="where-position" value=""/>
                <label>Geleistete Tätigkeiten</label>
                <input type="text" name="where-aufgaben" value=""/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <button class="btn btn-block btn-success save_user_career_btn">Hinzufügen</button>
                <button class="btn btn-block btn-success change_user_career_btn">Speichern</button>
            </div>
            <div class="span4">
                <span class="label label-success status_message_ok">Speichern <i class="icon-ok icon-white"></i></span>
            </div>
        </div>
    </form>
</div>