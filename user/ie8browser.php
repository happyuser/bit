<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8>
    <title>BIT user</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>BIT user</title>
    <link href="../css/jquery-ui.css" rel="stylesheet" />
    <link href="css/bootstrap-multiselect.css" rel="stylesheet" />
    <link href="css/glyphicons-extended.min.css" rel="stylesheet" />
    <script src="../js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/wall.css" rel="stylesheet" />
    <script src="../js/tabs.js" type="text/javascript"></script>
    <script src="js/jquery.textchange.min.js" type="text/javascript"></script>
    <script src="js/einstellungen.js" type="text/javascript"></script>
    <script src="js/bootstrap-dropdown.js" type="text/javascript"></script>
    <script src="js/bootstrap-select.js" type="text/javascript"></script>
    <script src="js/jquery.twbsPagination.min.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/jquery.dotdotdot.min.js" type="text/javascript"></script>
</head>
<body>
	<div id="container">
		<h2 class="whatnow">
			<b>Ihr Browser ist veraltet. Bitte laden sie einen dieser aktuellen, kostenlosen und exzellenten Browser herunter:</b>
		</h2>
		<table class="logos">
			<td class="b bf">
				<a class="l " href="https://www.mozilla.org/de/firefox/new/" target="_blank"><span class="bro">Firefox</span>
				<span class="vendor">Mozilla Foundation</span></a>
			</td>
			<td class="b bo">
				<a class="l" href="http://www.opera.com/de/computer" target="_blank"><span class="bro">Opera</span>
				<span class="vendor">Opera Software</span>  </a>
			</td>
			<td class="b bc">
				<a class="l" href="http://www.google.com.ua/intl/de/chrome/browser/desktop/index.html" target="_blank"><span class="bro">Chrome</span>
				<span class="vendor">Google</span>           </a>
			</td>
			<td class="b bi">
				<a class="l ie_last" href="http://windows.microsoft.com/de-DE/internet-explorer/downloads/ie" target="_blank"><span class="bro">Internet Explorer</span>
				<span class="vendor">Microsoft</span>        </a>
			</td>
			<td class="b safari_b">
				<a class="l safari_last" href="http://safari.de.softonic.com/" target="_blank"><span class="bro">Safari</span>
				<span class="vendor">Apple</span>        </a>
			</td>
		</table>
		<h2>
			Für mehr Sicherheit, Geschwindigkeit, Komfort und Spaß.
		</h2>
	<div>
</body>
</html>