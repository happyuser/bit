<div class="modal_edit_p pop_window guest_list" id_event="<?=get_get('id')?>">
    <div class="form_change_member">
        <div class="row-fluid">
        <div class="span7">
            <div class="title_window">GÄSTELISTE</div>
        </div>
        <div class="span5">
            <div class="input-append">
                <input type="text" class="guest_sushe search-query" />
                <i class="icon-remove search_clear"></i>
            </div>
        </div>
    </div>
        <div class="body_list_guest row-fluid" style="overflow: auto;height: 400px;">
        
        <?
        if($GUEST_LIST)
        foreach($GUEST_LIST as $key=>$val){
            if($val[type_user]=='user'){
                $functions=$val[functions];
                $name=$val[first_name];
                $vorname=$val[last_name];
            }else{
                $functions=$val[funktion_bus];
                $name=$val[name];
                $vorname=$val[vorname];
            }
        ?>
            <div class="li_guest row-fluid" id="<?=$val[id]?>">
                <div class="name_lastname span3" id="<?=$val[id]?>">
                    <?if($val[type_user]=='user'){?><a href="/user/?page=main_profil&id=<?=$val[id]?>" target="_blank"><?}?>
                        <?=$vorname.' '.$name?>
                    <?if($val[type_user]=='user'){?></a><?}?>
                </div>
                <div class="firma span4"><?=($val[firma])?$val[firma]:''?></div>
                <div class="contact span3"><?=$functions?></div>
                <div class="accompany span2"><?=$val[accompany]?></div>
            </div>
        <?}?>
        </div>
    </div>
</div>
<?
if($EVENT_IMAGES){
    $view=false;
    foreach($EVENT_IMAGES as $key=>$val){
        if(file_exists('../uploads/'.$val)){
            $view=true;
            break;
        }
    }
}
    if($view){
    ?>
<div class="modal_edit_p pop_window images_list">
    <div class="images_list_modal_edit_f">

        <div id="myCarousel" class="carousel slide" data-interval="false" data-ride="carousel">
            <div class="carousel-inner">
                <?foreach($EVENT_IMAGES as $key=>$val){?>
                    <div class="item"  id="<?=$key?>"><img style='width: 100%; max-height: 500px' src="../uploads/<?=$val?>"/></div>
                <?}?>
                <script>
                    $(document).ready(function(){
                        $(".carousel-inner > .item:first-child").addClass("active")
                    })
                </script>
            </div>
            <a class="left prev_photo" id="prev_photo" href="#myCarousel" data-slide="prev"></a>
            <a class="right next_photo" id="next_photo" href="#myCarousel" data-slide="next"></a>
        </div>
    </div>
</div>
<?
}

if(check_begleitung($EVENT)){?>
    <div class="modal_edit_p pop_window change_textarea window_begleitung begleitung">
        <div class="edit_profile">
            <div class="edit_profile_title uber-mich-header begleitung-header">Wollen Sie Begleitung mitnehmen?</div>
            <form class="form_change_member" style="margin: 0;">
                <div class="form_edit_p">
                    <label>
                        <input type="checkbox" checked name="not_begleitung" class="window-begleitung" /> ohne Begleitung
                    </label>
                    <label>
                        <input type="checkbox" name="has_begleitung" class="window-begleitung" /> mit <input type="text" name="accompany" class="window-begleitung-input-text" disabled="disabled" />
                    </label>
                    <div class="buttons_block">
                        <button id="speichern" id_event="<?=$EVENT[id]?>" class="begleitung-button anmelden" after="change_close">SPEICHERN</button>
                        <input class="btn" title="Abbrechen" id="close_pop_window" type="reset" value="Abbrechen"/>
                    </div>
                </div>
            </form>
        </div>
    </div>    
<?}?>
   <div class="modal_edit_p pop_window change_textarea window_begleitung message_event_ok">
        <div class="edit_profile window_begleitung-text">Ihre Anmeldung wird Bearbeitet.
            Sie bekommen die Antwort so schnell
            wie möglich.
        </div>
    </div>