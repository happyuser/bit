<div class="modal_edit_p pop_window change_photo">
    <div class="edit_profile">
        <form class="form_change_member" style="margin: 0;">
            <div class="photo_"></div>
            <div class="form_edit_p">
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <div class="buttons_block">
                    <button id="abbrechen">Abbrechen</button>
                    <button id="crop_img" after="change_close">SPEICHERN</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal_edit_p pop_window change_info" style="margin: 0 auto;width: 435px;padding: 30px;">
 <div class="edit_profile" style="padding: 14px 15px 10px 15px;">
  <div class="edit_profile_title" style="text-align: center; width: 100%;">INFORMATIONEN</div>
  <form class="form_change_info" style="margin: 0;">
      <div class="form_edit_p">
       <label>Uber</label>
       <textarea name="uber"><?=$_USER_INFO[uber]?></textarea>
       <div class="buttons_block">
            <button id="abbrechen">Abbrechen</button>
            <button id="speichern" after="change_close">SPEICHERN</button>
       </div>
      </div>
  </form>
 </div>
</div>

<div class="modal_edit_p pop_window change_kontakten" style="margin: 0 auto;width: 435px;padding: 30px;">
 <div class="edit_profile" style="padding: 14px 15px 10px 15px;">
  <div class="edit_profile_title" style="text-align: center; width: 100%;">KONTAKTEN</div>
  <form class="form_change_kontakten" style="margin: 0;">
      <div class="form_edit_p">
       <label>Firmenname</label>
       <input type="text" name="firmenname" value="<?=$_USER_INFO[firmenname]?>"/>
       <label>Strabe</label>
       <input type="text" name="strabe" value="<?=$_USER_INFO[strabe]?>"/>
       <label>Stadt</label>
       <input type="text" name="stadt" value="<?=$_USER_INFO[stadt]?>"/>
       <label>Land</label>
       <select name="land">
        <?foreach(select_DB('countries') as $value){?>
            <option value="<?=$value[id]?>" <?=($_USER_INFO[land]==$value[id])?'selected="selected"':''?>><?=$value[de]?></option>
        <?}?>
       </select>
       <label>Email</label>
       <input type="text" name="email" value="<?=$_USER_INFO[email]?>"/>
       <div class="buttons_block">
            <button id="abbrechen">Abbrechen</button>
            <button id="speichern" after="change_close">SPEICHERN</button>
       </div>
      </div>
  </form>
 </div>
</div>