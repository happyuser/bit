$(document).ready(function(){
    function search_kontakten(page){
        if(!page)page=1;
        page=parseInt(page);
        $('.pagination .page').remove();
        var str=$('.search_f').serialize();
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: str+'&name_func=friends_finden&page='+page,
            dataType: "json",
            success: function(data)
            {
                console.log(data);
                $('.kontakt_list').html('');
                var pages=Math.ceil(data.count/10);
                console.log(pages);
                if(page<=pages)
                    $('.prev a').attr('href',page);
                else
                    $('.prev a').attr('href',page-1);
                    
                if(page>=pages)
                    $('.next a').attr('href',page);
                else
                    $('.next a').attr('href',page+1);
                    
                $('.last a').attr('href',pages);
 
                if(page==1)plus=3; else plus=2; 
                for (var i=page-1;i<=pages;i++){
                    if(i>=(page+plus))break;
                    if(!i)continue;
                    var _class='page'
                    if(i==page)_class+=' active';
                    $('li.next').before('<li class="'+_class+'"><a href="'+i+'">'+i+'</a></li>');
                }
                $.each(data.list,function(index,value){
                    var str='<li class="user" id_user="'+value.id+'">'+
                                '<div class="row marg">'+
                                    '<div class="kontakt_image span5">'+
                                        '<a href="/user/?page=main_profil&id='+value.id+'">'+
                                            '<img src="../uploads/crop_img/'+value.photo+'"/>'+
                                        '</a>'+
                                    '</div>'+
    
                                    '<div class="kontakt_info span5">'+
                                        '<p class="kontakt_name"><a href="/user/?page=main_profil&id='+value.id+'">'+value.last_name+' '+value.first_name+'</a></p>'+
                                        '<p class="kontakt_function">'+value.functions+'</p>'+
                                        '<p class="kontakt_firm">'+value.firma+'</p>'+
                                    '</div>'+
                                    '<div class="kontakt_message span2">'+
                                        '<div>'+
                                            '<a href="/user/?page=nachrichtschreiben&id='+value.id+'"><button class="message_btn"></button></a>'+
                                            '<span class="dropdown-toggle caret" data-toggle="dropdown"></span>'+
                                            '<ul class="dropdown-menu message_list">'+
                                                '<li class="del_friend" id="'+value.id+'">'+
                                                    'Aus der Kontaktenliste entfernen'+
                                                '</li>'+
                                            '</ul>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</li>';
                $('.kontakt_list').append(str);
                });
            }
        });
    }
    function alert_friend(text){
        if(text){
            $('.alert_friend').text(text).show();
            setTimeout(alert_friend,3000);
        }else
            $('.alert_friend').hide(100);
    }
$('.kontakt_list').on('click','.del_friend',function(){
        var this_=$(this);
        var id=$(this).attr('id');
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=update_friend&id='+id,
        dataType: "json",
        success: function(data)
        {
            if(data.ok==true){
                $('.user[id_user='+id+']').remove();
                $('.alert_friend').text('Aus der kontaktenliste entfernt').show();
                alert_friend('Aus der kontaktenliste entfernt');
            }
        }
    });
    $(".new_kontakt_list li:last-child hr").remove();
    /*CHANGE NAVAGATION LI*/
    $(function(){
        $('.ul_kontakten_top a').filter(function(){

            return this.href==location.href}).parent().addClass('active').siblings().removeClass('active');

        $('.ul_kontakten_top a').click(function(){
            $(this).parent().addClass('active').siblings().removeClass('active')
        })
    });
    
    
    if($('#land').val()== 'all_land'){
        $('#city').attr('disabled', 'disabled');
    }
    $('#land').change(function(){
        $('#city').html('<option value="all_city">Alle Stadte</option>');
        if($(this).val()== 'all_land'){
            $('#city').attr('disabled', 'disabled');
            $('#city').find('option[value="all_city"]').attr("selected",true);
        }
        else{
            $('#city').attr('disabled',false );
            $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=view_cities_for_country&id='+$(this).val(),
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(index,value){
                    $('#city [value="all_city"]').after('<option value="'+value+'">'+value+'</option>');
                });
            }
            });
        }
    });    
});


    $('.pagination').on('click','.pagination li a',function(){
        var page=$(this).attr('href');
        search_kontakten(page);
        return false;
    });

    $('select,input').on('textchange',function(){
        search_kontakten();
    });
});