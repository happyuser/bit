function parseGetParams() { 
   
   var $_GET = {}; 
   var __GET = window.location.search.substring(1).split("&");
    
   for(var i=0; i<__GET.length; i++) { 
      var getVar = __GET[i].split("=");
       console.log(getVar);
      if(getVar[0]!=='' && getVar[1]!=='')
        $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1]; 
   } 
   console.log($_GET);
   return $_GET;
}
$(document).ready(function() {
    activateNavigationTab();
    $(document).keydown(function(e) {
        if( e.keyCode === 27 )
            $('#search[name=search]').next().remove();
    });

    $('.no_activ_body,.close_pop_window').click(function(){
        $('.no_activ_body').hide();
        $('.pop_window').hide();
        window.onscroll=function(){};
    });

    $('#search[name=search]').on('textchange',function(){
        $('#search[name=search]').next().remove();
        var val=$(this).val();
        if(val.length<3)return false;
        var str='';
        if(!val)return false;
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=search_user&text='+val,
            dataType: "json",
            success: function(data)
            {
                if(data == null)
                str='Ihre Suche hat kein Treffen ergeben';
                else
                $.each(data, function(index,value){
                    console.log(value);
                    if(value.type_search=='user'){
                        var link='/user/?page=main_profil&id=';
                        var photo='../uploads/crop_img/'+value.photo;
                        var text=value.first_name+' '+value.last_name;
                    }else if(value.type_search=='sponsor'){
                        var link='/user/?page=main_sponsor&id=';
                        var photo='../uploads/'+value.photo;
                        var text=value.firmenname;
                    }else if(value.type_search=='event'){
                        var link='/user/?page=event&id=';
                        if(value.folder_photo=='images'){
                            var photo=value.images[0];
                        }else{
                            var photo='../uploads/'+value.images[0];
                        }
                        var text=value.name;
                    }
                    
                    str+='<li><a href="'+link+value.id+'" class="row marg find_a"><div class="find_img span3"><img src="'+photo+'"/> </div><div class="find_info span9">'+text+'</div></a></li>';
                    
                });
                $('#search[name=search]').after('<div class="row-fluid"><div class="span4 offset4"><ul class="find_list">'+str+'</ul></div></div>');
            }
        });
    });
});

function activateNavigationTab() {
    var pageUrl = parseGetParams();
console.log(pageUrl);
    if (pageUrl.page === 'kontakten_finden') {
        $('#find-contacts').css('background', 'url("/user/images/people_active_30x30.png")');
    }  else if (pageUrl.page === 'wall') {
        $('#wall-all').css('background', 'url("/user/images/events_active_30x30.png")');
    } else if (pageUrl.page === 'main_profil') {
        $('#find-contacts').css('background', 'url("/user/images/people_active_30x30.png")');
    }else if (pageUrl.page === 'events') {
        $('#events').css('background', 'url("/user/images/pinnwand_active_30x30.png")');
    }else if (pageUrl.page === 'event') {
        $('#events').css('background', 'url("/user/images/pinnwand_active_30x30.png")');
    }
	else if (pageUrl.page === 'einstellungen') {
        $('#settings').css('background', 'url("/user/images/Settings_active_30x30.png")');
    } else if (pageUrl.page === 'main_profil') {
        $('.t_side_menu li:first-child a').addClass("t_side_menu_active");
    } else if (pageUrl.page === 'kontakten') {
        $('.t_side_menu li:nth-child(2) a').addClass("t_side_menu_active");
    }
    else if (pageUrl.page === 'nachrichten') {
        $('.t_side_menu li:nth-child(3) a').addClass("t_side_menu_active");
    }
    else if (pageUrl.page === 'veranstaltungen') {
        $('.t_side_menu li:nth-child(4) a').addClass("t_side_menu_active");
    }
    else if (pageUrl.page === 'einladungen') {
        $('.t_side_menu li:nth-child(5) a').addClass("t_side_menu_active");
    }
}


$(document).on("click",".t_header-logo" , function(){
    $(".t_body").show();
    $(".t_user_body").show();
    $(".t_wrapper").css({"min-height":"90%"});
    $(".form_login_header").show();
    $(".t_header_menu").show();
    $("#uber_fc").hide();
    $("#impressum").hide();
    $("#prev-page").hide();
    //$(".t_header").removeClass("span12").addClass("offset2").addClass("span8");
    //$(".t_header-logo").removeClass("span12").addClass("span5");
});

$(function(){
    var checkPage = $('#events').text();
    if(checkPage){
        $('.prev_page_veranstaltungen_before_login').hide();
    }
});


$.fn.alignCenterScreen = function() {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.outerHeight()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.outerWidth()) / 2 + $(window).scrollLeft() + "px");
    return this
};