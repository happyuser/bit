$(document).ready(function(){
    function check_input_access(){
        var inputs=$('[required]');
        $.each(inputs, function(index, value){
            if($(value).val()==''){
                $('.next').attr('disabled','disabled');
                return false;
            }
            $('.next').removeAttr('disabled');
        });
    }
    
    check_input_access();
    $('.form_page input,.form_page textarea,.form_page select').on('textchange', function(){
        check_input_access();
    });
    $('#imgFile').on('change', function(){
        var fd = new FormData();
        fd.append('new_ava', 'true');
        fd.append('img', $('#imgFile')[0].files[0]);
        
        $.ajax({
          type: 'POST',
          url: 'upload.php',
          data: fd,
          processData: false,
          contentType: false,
          dataType: "json",
          success: function(data) {
            if(data.name_img){
                $('.upload_image').attr('src','../uploads/'+data.name_img);
                $('[name=foto]').val(data.name_img);
                //$('.img_profil').css('height',$('.upload_image').css('height'));
                console.log($('.upload_image').css('height'));
            }
          },
          error: function(data) {
            console.log(data);
          }
        });
        return false;
    });
    $('.img_profil').hover(function(){
        $('.new_photo_upload').animate({'opacity':1},300);
    },function(){
        $('.new_photo_upload').animate({'opacity':0},100);
    });
    
    $('.back').click(function(){
        var id=parseFloat($('form.form_reg').attr('id'));
        id--;                
        document.location.href = "/user/?page=reg_sponsor&step="+id;
        return false;
    });
    $('.next').click(function(){
        var str=$('form.form_reg').serialize();
        var id=parseFloat($('form.form_reg').attr('id'));
        console.log(str);
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: str+'&name_func=save_sponsor_step',
        dataType: "json",
        success: function(data)
        {
            if(data.ok=='true'){
                console.log(data);
                id++;
                
                if(id<4)
                    document.location.href = "/user/?page=reg_sponsor&step="+id;
                else
                    document.location.href = "/user/?page=registration&step=5";
            }
        }
        });
        return false;
    });
});