$(document).ready(function(){
    $('[name=has_begleitung]').change(function(){
        if($(this).is(':checked')) {$('[name=accompany]').removeAttr('disabled'); $('[name=not_begleitung]').removeAttr('checked');} else {$('[name=accompany]').attr('disabled',true); $('[name=not_begleitung]').prop('checked',true);} 
    });
    $('[name=not_begleitung]').change(function(){
        if($(this).is(':checked')) {$('[name=accompany]').attr('disabled',true); $('[name=has_begleitung]').removeAttr('checked');} else {$('[name=accompany]').removeAttr('disabled'); $('[name=has_begleitung]').prop('checked',true);} 
    });
    
    $('.ok_solution').click(function(){
        var id=$(this).attr('id');
        var accompany=$('[name=accompany]').val();
        var str='name_func=bestatigte_for_event&id='+id+'&type=eingeladene';
        if(accompany)str=str+"&accompany="+accompany;
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: str,
            dataType: "json",
            success: function(data)
            {
                console.log(data);
                location.reload();
            }
        });
    });
    $('.no_solution').click(function(){
        var id=$(this).attr('id');
        var str='name_func=guest_list_solution_no&id='+id;
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: str,
            dataType: "json",
            success: function(data)
            {
                console.log(data);
                location.reload();
            }
        });
    });
});