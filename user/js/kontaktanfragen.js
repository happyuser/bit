$(document).ready(function () {
    $('.accept_request .accept').click(function () {
        var click = $(this);
        var id = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=update_friend&id=' + id,
            dataType: "json",
            success: function (data) {
                click.parent().parent().parent().parent().hide(200);
            }
        });
    });
    $('.accept_request .reject').click(function () {
        var click = $(this);
        var id = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=update_friend&id=' + id + '&status=del_incoming',
            dataType: "json",
            success: function (data) {
                click.parent().parent().parent().parent().hide(200);
            }
        });
    });
});