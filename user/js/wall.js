$(document).ready(function(){
    var activ_ajax=false;

/*$('input').click( function(){
 console.log($(this));   
});
*/

/*VIEW NEW POST OR COMMENT*/
var url=parseGetParams();
$('.view_crop').click(function(){
    $(this).hide().next().show();
});
$('.message_post').click(function(){
    $(this).find('.view_crop').hide().next().show();
});
setInterval(get_new, 1000);

    function get_new(start,new_page){
        /*
        console.log('get_new');
        console.log(start);
        */
        
    var str = '';
    if(!new_page){
        $('.media[name]').each(function(ind) {
            var key1 = $(this).attr('id');
            var value=$(this).find('[id_comment]:last').attr("id_comment");
            if(typeof value == 'undefined'){
                value = '';   
            }
            str = str + key1+"="+value+",";
        });
                
        str = str.substring(0, str.length - 1);
        var str1=  encodeURIComponent(str);
        var str_post='posts='+str1+'&name_func=get_new_posts_and_comments';
        if(typeof(post_id_user) != "undefined")str_post+='&post_id_user='+post_id_user;
        
        if(url.type)str_post+='&type='+url.type;
        if(start!=null)str_post=str_post+'&start='+$('[name="get_media"]').length;
        activ_ajax=true;
        
        if(start){
            var write_after=$('[name=get_media]:last').attr('id');
            write_after=$('#'+write_after+'[name=get_media]');
        }else{
            if($('#block_new_post').length)
                var write_after=$('#block_new_post');
            else 
                var write_after=$('.wall_navigation');
        }
    }else{
        var GET=parseGetParams();
        var str='';
        var arr={};
        if(GET)
            $.each(GET, function(index, value) {
                if(index!='type')
                    str=str+index+'='+value+'&';
            });
        str.replace(/%20/g,"");
        str+='type='+new_page;
        //console.log(str);
        window.history.pushState(null, null, window.location.pathname+'?'+str);
        url.type=new_page;
        var str_post='posts=0&name_func=get_new_posts_and_comments&type='+new_page;
        var write_after=$('#block_new_post');
    }
    
    $.ajax({
        async:false,
        type:'POST',
        url:'/controllers/ajax.php',
        data:str_post,
        dataType:'json',
        success:function(data) {
            for (var new_id in data.new_posts) {
                
                var current = data.new_posts[new_id];
                console.log(current);
                
                var j=i=image_str=youtube_str='';
                if(current['attached_file'] == null){
                    i=0;
                }
                else{
                    i=2;
                    $.each(current['attached_file'],function(index,value){
                        var arr_img=value.split('.');
                        image_str=image_str+'<div class="row-fluid" id="sub_photo"><div class="sub_attached_photo">'+(arr_img[1]=='mp4' ? '<video src="/uploads/'+value+'" controls style="width: 100%">':'<img src="/uploads/' + value + '"/>')+'</div></div>';
                    });
                }
                
                if (current['youtube'] == null){
                    j=0;
                }
                else{
                    j=2;
                    $.each(current['youtube'],function(index,value){
                        youtube_str=youtube_str+'<div class="youtube" id_youtube="' + value+ '"><iframe width="100%" height="200" src="https://www.youtube.com/embed/' + value +'"  frameborder="0" allowfullscreen></iframe></div>';
                    });
                }
                
                var count=0;
                if(current['liked_members']){
                    var liked_members='<ul class="dropdown-menu inline pull-right">';
                    $.each(current['liked_members'],function(index,value){
                        $.each(value,function(idx,val){
                            liked_members+='<li><img src="'+val+'" width="20" height="20"></li>';
                            count++;
                        });
                    });
                    liked_members+='</ul>';
                }else liked_members=' ';
                count='<div class="dropdown-toggle" data-toggle="dropdown" id="liked_member">'+count+'</div>';
                var temp = current['info_user'];
                var avatar = temp['photo']
                //console.log(start);
                                                                        
                    $(write_after).after("<div class='media row marg' style='overflow:hidden' name='get_media' id='" + new_id + "'>" +
                        "<a class='span2 link_post_img' href='#'>" +
                        "<img class='media-object' style='width: 65px;height: 65px' src='" + avatar + "' />" +
                        "</a>" +
                        "<div class='media-body span9'>" +
                        "<div class='comment'>" +
                        "<div class='row-fluid'>" +
                        "<h4 class='media-heading'>" +
                        "" + temp['first_name'] + " " + (temp['last_name']!=undefined ? temp['last_name']:'') + "" +
                        "<span class='pull-right' id='time_comment'>" +
                        "" + current['addiction_time'] + "" +
                        "</span>" +
                        "</h4>" +
                        "</div>" +
                        "<div class='row-fluid message_post'>" +
                        "<p>" + current['text'] + "</p>" +
                        "</div>" +
                        "<div class='attached_files'>" +
                        (i > 1 ? image_str : '')+
                        "</div>"+
                        "<div class='youtubes'>"+
                        (j > 1 ? youtube_str : '') +
                        "</div>"+
                        "<div class='row-fluid'><ul class='inline pull-right' id='like_part'>" +
                        (current.my_post ? "<li><div class='post_drop'></div></li>" : '')+
                        (current.my_post ? "<li><div class='edit_post' type='post' id='"+new_id+"'><i class='icon-pencil'></i></div></li>" : '')+
                        "<li>"+
                        "<div id='show_sub_comment1'></div>"+
                        "</li>"+
                        "<li>"+
                        "<div id='like_button'></div>"+
                        "</li>"+
                        "<li class='dropdown dropup' id='like_count'>" +
                        (count?count:'')+
                        (liked_members ? liked_members : '') +
                        "</li>"+
                        "</ul>"+
                        "</div>"+
                        "</div>"
                    )

            }
            for( var new_sub_id in data.new_comments){
                var carr_comment = data.new_comments[new_sub_id];
                for( var  i in carr_comment){
                    var temp = carr_comment[i];
                    var temp1 = temp['info_user'];
                    var avatar = temp1['photo']
                    var i,j;
                    var youtube_str=image_str='';
                    
                    if(temp['attached_file'] == null){
                        i=0;
                    }
                    else{
                        i=2;
                        $.each(temp['attached_file'],function(index,value){
                            var arr_img=value.split('.');
                            image_str=image_str+'<div class="row-fluid" id="sub_photo"><div class="sub_attached_photo">'+(arr_img[1]=='mp4'? '<video src="/uploads/'+value+'" controls style="width: 100%">':'<img src="/uploads/' + value + '"/>')+'</div></div>';
                        });
                    }
                    var youtube = temp['youtube'];
                    if (youtube==null){
                        j=0;
                    }
                    else{
                        $.each(youtube,function(index,value){
                            youtube_str=youtube_str+'<div class="youtube" id_youtube="' + value + '"><iframe width="100%" height="170" src="https://www.youtube.com/embed/' + value +'"  frameborder="0" allowfullscreen></iframe></div>';
                        });
                        j=2;
                    }
                    var count=0;
                    if(temp['liked_members']){
                        var liked_members='<ul class="dropdown-menu inline pull-right">';
                        $.each(temp['liked_members'],function(index,value){
                            $.each(value,function(idx,val){
                                liked_members+='<li><img src="'+val+'" width="20" height="20"></li>';
                                count++;
                            });
                        });
                        liked_members+='</ul>';
                    }else liked_members=' ';
                    
                    console.info(liked_members);
                    count='<div class="dropdown-toggle" data-toggle="dropdown" id="liked_member">'+count+'</div>';
                    
                        $('#'+new_sub_id+'.media').children('.media-body').append("<div class='media row marg' id='sub_comment'" +
                                "id_comment='"+temp['id']+"'"+"style='overflow:hidden'>"+
                                "<a class='span2' href='#' style='height: auto;width: auto;text-align: right'>"+
                                "<img class='media-object' style='width: 65px;height: 65px' src='"+avatar+"'/>"+
                                "</a>"+
                                "<div class='media-body pull-right span9'>"+
                                "<div class='comment'>"+
                                "<div class='row-fluid'>"+
                                "<h4 class='media-heading'>"+
                                "" + temp1['first_name'] + " " + (temp1['last_name']!=undefined ? temp1['last_name']:'') + "" +
                                "<span class='pull-right' id='time_comment'>" +
                                ""+temp['time']+""+
                                "</span>" +
                                "</h4>" +
                                "</div>" +
                                "<div class='row-fluid message_post'>" +
                                "<p>" +
                                "" +temp['text']+""+
                                "</p>" +
                                "</div>"+
                                "<div class='attached_files'>" +
                                (i > 1 ? image_str : '')+
                                "</div>"+
                                "<div class='youtubes'>"+
                                (j > 1 ? youtube_str : '') +
                                "</div>"+
                                "<div class='row-fluid'>" +
                                "<ul class='inline pull-right' id='like_part'>" +
                                (temp.my_comment ? "<li><div class='post_drop'></div></li>" : '')+
                                (temp.my_comment ? "<li><div class='edit_comment' id='"+temp['id']+"'><i class='icon-pencil'></i></div></li>" : '')+
                                "<li>" +
                                "<div id='like_button'></div>" +
                                "</li>" +
                                "<li class='dropdown dropup' id='like_count'>" +
                                (count?count:'')+
                                (liked_members ? liked_members : '') +
                                "</li>"+
                                "</ul>" +
                                "</div>" +
                                "</div>" +
                                "</div>" +
                                "</div>");
                    
                }
            }
            start=false;
        }
    });
    activ_ajax=false;
    delete str,str1;
    //console.log(start);
    }
    
    $(window).scroll(function() {
        //console.log(activ_ajax);
        if($(window).scrollTop() + $(window).height() >= $(document).height() - 200 && !activ_ajax){
            get_new(true);
         }
    });
    
    
    $('.wall_container').on('click','.sub_attached_photo img',function(){
        $('.no_activ_body').show();
        $('.view_photo').slideDown(200).find('.photo_').html('<img src="'+$(this).attr('src')+'" />');
    });
    
    
    $('.wall_container').on('click','li .edit_post, li .edit_comment', function(){
        //console.log('edit_c');
        $('.edit_post_block').remove();
        var id_post=$(this).attr('id');
        if($(this).hasClass('edit_comment'))
            var type='comment';
        else
            var type='post';
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=get_info_post_comment&type='+type+'&id='+id_post,
            dataType: "json",
            success: function(data)
            {
                data=data[0];
                //console.log(data);
                if(type=='post')
                    var selector='#'+id_post+'.media .media-body .comment:first';
                else
                    var selector='.media[id_comment='+id_post+'] .media-body .comment:first';
                $(selector).append("<div class='edit_post_block'><div class='type_block_container'> " +
                "<div class='row-fluid'>" +
                "<div class='span12'>" +
                "<textarea id='edit_post' placeholder='Hier Kommentar eintragen...' class='span12 contenteditable' onkeyup='autoGrow(this);' maxlength='2000'>" +
                data.text +
                "</textarea>" +
                "</div>" +
                "</div>" +
                "<div id='block_send_and_attached'>" +
                "<div class='row-fluid'>" +
                "<div class='span4'>" +
                "<label for='edit_attached_file' class='span12 edit_attached_file_wall'>" +
                "Anhängen" +
                "</label>" +
                "<input id='edit_attached_file' style='display: none' type='file' name='image[]' multiple='true'>" +
                "</div>" +
                "<div class='span4 pull-right'>" +
                "<input type='button' class='span12 new_post_btn_send edit_post_btn' value='Senden' type_post='"+type+"' id='"+id_post+"'/>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "<div class='videos_edit_post'></div><div class='photos_edit_post'></div>"+
                "</div></div>");
                var youtube_str=images_str=''
                
                if(data.youtube)
                    $.each(data.youtube, function(index,value){
                        youtube_str=youtube_str+'<div class="youtube" id_youtube="'+value+'">'+
                        '<div class="delete_youtube">DELETE</div>'+
                        '<iframe width="300" height="169" src="https://www.youtube.com/embed/'+value+'" frameborder="0" allowfullscreen=""></iframe>'+
                        '</div>';
                    });
                if(data.attached_file)
                    $.each(data.attached_file, function(index,value){
                        var arr_img=value.split('.');
                        images_str=images_str+(arr_img[1]=='mp4' ? '<div class="edit_img_for_post span12"><video id="'+value+'" src="/uploads/'+value+'" controls style="width: 100%"></video>':'<div class="edit_img_for_post span4"><img id="'+value+'" src="/uploads/'+value+'">')+'<div class="delete_img"><i class="icon-remove icon-white"></i></div></div>';                        
                        //images_str=images_str+'<div class="edit_img_for_post span4"><img id="'+value+'" src="/uploads/'+value+'"><div class="delete_img"><i class="icon-remove icon-white"></i></div></div>';
                    });
                
                if(images_str!='')$('.photos_edit_post').append('<div class="row-fluid" id="attached_photo">'+images_str+'</div>');
                if(youtube_str!='')$('.videos_edit_post').append(youtube_str);
            }
        });
    });
    
    $('.wall_container').on('click','.edit_post_btn', function(){
        var id_post=$(this).attr('id');
        var youtube_str='',images_str='';
        var text=$('#edit_post').val();
        var type=$(this).attr('type_post');
        var post=$(this).parents(':eq(5)');
        
        if($('.videos_edit_post .youtube').length)
            $.each($('.videos_edit_post .youtube'), function(index,value){
                youtube_str=youtube_str+'youtube[]='+$(value).attr('id_youtube')+'&';
            });
        if($('.photos_edit_post img, .photos_edit_post video').length)
            $.each($('.photos_edit_post img, .photos_edit_post video'), function(index,value){
                images_str=images_str+'images[]='+$(value).attr('id')+'&';
            });
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=edit_post&type='+type+'&id='+id_post+'&'+images_str+youtube_str+'&text='+text,
            dataType: "json",
            success: function(data)
            {
                console.info(data);
                if(data.ok=='true'){
                    $(post).find('.message_post').text(data.text);
                    $(post).find('.attached_files').html(data.files);
                    $(post).find('.youtubes').html(data.youtubes);
                    $('.edit_post_block').remove();
                }else if(data.problem=='empty_form'){
                    alert('Empty form!');
                }
            }
        });
    });
    
    $('.wall_container').on('click','.delete_img',function(){
        $(this).parent().remove();
        console.log(post_phooto);
        //post_phooto.push(value);//добавляємо фотку в глобальний масив з фотками
    });
    
    /*
    $(document).on('click',function(){
        console.log($('textarea'));
    });
    */
    
    $('.wall_container').on('keyup','#edit_post',function(){
        var comment=$(this).val();
        $.ajax({
            type:'POST',
            url:'/controllers/ajax.php',
            data:'comment='+comment+'&name_func=youtube_video',
            dataType:'json',
            success:function(data){
                $.each(data.ids, function(index,value){
                    if(!$('.videos_edit_post [id_youtube='+value+']').length)
                        $('.videos_edit_post').append('<div class="youtube" id_youtube="'+value+'">'+
                        '<div class="delete_youtube">DELETE</div>'+
                        '<iframe width="300" height="169" src="https://www.youtube.com/embed/'+value+'" frameborder="0" allowfullscreen=""></iframe>'+
                        '</div>');
                });
            }
        });
    });
    $('.wall_container').on('mouseenter','.attach_file',function(){
        $(this).find('.del_attach').animate({'opacity':1},100);
    });
    $('.wall_container').on('mouseleave','.attach_file',function(){
        $(this).find('.del_attach').css('opacity',0);
    });
    $('.wall_container').on('click','.del_attach',function(){
        var id_img=$(this).attr('id_img');
        $(this).parent().remove();
        //console.log(post_phooto.indexOf(id_img));
        post_phooto.splice(post_phooto.indexOf(id_img),1)
        //console.log(post_phooto);
    });
    
    $('.wall_navigation a').click(function(){
        $('.wall_navigation a div').removeClass('active');
        $(this).find('div').addClass('active');
        var type=$(this).attr('id');
        $.ajax({
            type:'POST',
            url:'/controllers/ajax.php',
            data:'posts=0&name_func=get_new_posts_and_comments&type='+type,
            dataType:'json',
            success:function(data){
                $('.wall_container [name=get_media]').remove()
                get_new(false,type);
            }
        });
        return false;
    });

});

/*CHANGE ACTIVE TAB IN WALL NAVIGATION*/

$(function(){

    changeActiveNavigationTab();

});
function changeActiveNavigationTab(){
    var activeTab = $('.wall_navigation a');

    var filteredObject = activeTab.filter(function(){
        return this.href==location.href
    });

    filteredObject.parent().addClass('active').siblings().removeClass('active');


    activeTab.click(function(){
        $(this).parent().addClass('active').siblings().removeClass('active');

    });
}

/*ALL ABOUT NEW POST*/

/*SEND NEW POST*/
$('#btn_send_comment').click(function(){
    var url=parseGetParams();
    var blockTypeNewPost = $('#block_type_new_post');
    var textOfNewPost = blockTypeNewPost.val();

    if( textOfNewPost == undefined){
        textOfNewPost='';
    }

    if(post_phooto === undefined){
        post_phooto = null;
    }

    var youtube= '';
    $.each($('#block_new_post #block_send_and_attached .youtube'), function(index,value){
        youtube=youtube+'&youtube['+index+']='+$(value).attr('id_youtube');
    });
    var images='';
if(post_phooto)
    $.each(post_phooto,function(index,value){
        images=images+'&images[]='+value;
    });
    
    $.ajax({
        type:'POST',
        url:'/controllers/ajax.php',
        //+'&type='+url.type+
        data:'text='+textOfNewPost+images+'&name_func=new_post_well'+youtube,
        dataType:'json',
        success:function(){
            $("#attached_photo").html('');
            $('#block_send_and_attached .youtube').remove();
        }
    });
    
        blockTypeNewPost.val('');
        blockTypeNewPost.animate({"height": "30px"}, "medium");
        $("#block_send_and_attached").hide();
        post_phooto=new Array();//тут ми масив з фотками і відео онуляємо
});

/*ATTACHED PHOTO TO POST*/
var post_phooto=new Array();//масив з фотками, глобальний
$(document).on('change',"#btn_attached_file, #download_photo1, #edit_attached_file", function(){
    var types= new Array('jpeg', 'jpg', 'png', 'mp4', 'mpeg');
    var message_type=false,message_size=false;
    var error_file= new Array();
    //console.log(post_phooto);
    var ID_click=$(this).attr('id');
    $('label[for='+ID_click+']').text('Warten Sie bitte...');
    
    var images=new Array();
    var fd = new FormData();
    var ajax=false;
    $.each($('#'+ID_click)[0].files, function(index,value){
        console.log(value);
        a = value.name.split('.'); 
        var ext = a[a.length-1];
        var upload=true;
        if(types.indexOf(ext)<0){
            message_type=true;
            error_file.push(value.name);
            upload=false;
        }
        if(value.size>800000000){
            message_size=true;
            upload=false;
        }
        if(upload){
            fd.append('img[]', value);
            ajax=true;
        }  
    });
    
if(ajax)
    $.ajax({
        type: 'POST',
        url:'upload.php',
        data: fd,
        processData: false,
        contentType: false,
        dataType: "json",
        success: function(data) {
            if(data.error_type)
                alert('Fehler: "'+data.error_type+'"!   Erlaubte Formaten: '+data.success_type);
            if (data.name_img){
                $('label[for='+ID_click+']').text('Anhängen');
                $.each(data.name_img, function(index, value){
                    var arr_img=value.split('.');
                    //console.log(arr_img);
                    var src="/uploads/" + value;
                    post_phooto.push(value);//добавляємо фотку в глобальний масив з фотками
                    //console.info(ID_click);
                    if(ID_click=='btn_attached_file')//залежить в якому полі юзер завантажує фото (новий пост, коментар, чи редагування)
                        if (arr_img[1]=='mp4') $("#attached_photo").append("<div class='attach_file'><div class='del_attach' id_img='"+value+"'><i class='icon-remove'></i></div><video src='"+src+"' controls style='width: 100%'></div>"); else $("#attached_photo").append("<div class='attach_file'><div class='del_attach' id_img='"+value+"'><i class='icon-remove'></i></div><img src='"+src+"'></div>");
                    else if(ID_click=='edit_attached_file'){
                        if (arr_img[1]=='mp4') $(".photos_edit_post").append("'<div class='edit_img_for_post span12'><video src='"+src+"' id='"+value+"' controls style='width: 100%'><div class='delete_img'><i class='icon-remove icon-white'></i></div></div>"); else $('.photos_edit_post').append('<div class="edit_img_for_post span4"><img id="'+value+'" src="/uploads/'+value+'"><div class="delete_img"><i class="icon-remove icon-white"></i></div></div>');
                    }else{
                        if (arr_img[1]=='mp4') $(".sub_attached_photo1").append("<div class='attach_file'><div class='del_attach' id_img='"+value+"'><i class='icon-remove'></i></div><video src='"+src+"' controls style='width: 100%'></div>"); else $(".sub_attached_photo1").append("<div class='attach_file'><div class='del_attach' id_img='"+value+"'><i class='icon-remove'></i></div><img src='"+src+"'></div>");
                    }
                })
            }
            $('label[for='+ID_click+']').text('Anhängen');
        },
        error:function(){
            alert('Diese Datei ist zu groß, maximal erlaubte Größe ist 8 mb.');
            $('label[for='+ID_click+']').text('Anhängen');
        }
    });

    if(message_type)
        alert('Fehler: "'+error_file.join(', ')+'"!    Erlaubte Formaten: '+types.join(', '));
    if(message_size)
        alert('Diese Datei ist zu groß, maximal erlaubte Größe ist 8 mb.');
    
    return false;
});

/*RESIZE TEXT MAIN TEXTFIELD*/
$("#block_type_new_post").focusin(function(e){
    //console.log(e);
    $(this).animate({"height": "80px"}, "medium");
    $(this).attr("placeholder", "Hier Nachricht eintragen...");
    e.stopPropagation();
    $('#block_send_and_attached').show(200).find('.hide').removeClass('hide');
});

/*FOCUSOUT MAIN TEXTFIELD*/
$("#block_type_new_post").on('textchange', function(){
    var comment=$('#block_type_new_post').val();
    $.ajax({
        type:'POST',
        url:'/controllers/ajax.php',
        data:'comment='+comment+'&name_func=youtube_video',
        dataType:'json',
        success:function(data){
            $.each(data.ids, function(index,value){
                if(!$('#post_input #block_send_and_attached [id_youtube='+value+']').length)
                    $('#start_video').after('<div class="youtube" id_youtube="'+value+'"><div class="delete_youtube">DELETE</div><iframe width="300" height="169" src="https://www.youtube.com/embed/'+value+'" frameborder="0" allowfullscreen></iframe></div>');
            });
        }
    });
});


/*ALL ABOUT COMMENT*/

/*BUTTON SEND SUB-POST*/
$(document).on('click','#send_comment1',function(){
    
    var youtube= '';
    $.each($('#sub1 #block_send_and_attached .youtube'), function(index,value){
        youtube=youtube+'&youtube['+index+']='+$(value).attr('id_youtube');
    });
    
    var images='';
    if(post_phooto)
    $.each(post_phooto,function(index,value){
        images=images+'&images[]='+value;
    })
    var str = $(this).parent().parent().parent().prev().children().children().val();
    

    var postId = $(this).parent().parent().parent().parent().parent().parent().attr("id");
    $.ajax({
        type:'POST',
        url:'/controllers/ajax.php',
        data:'&news_id='+postId+'&text='+str+'&name_func=new_comment_post&youtube='+youtube+images,
        dataType:'json',
        success:function(){
            post_phooto=new Array();
        }

    });
    
    $('#sub1').remove();
});

/*RESIZE TEXT SUB FIELD*/
$(document).on('focusin',"#sub_new_comment",function(){
    $(this).animate({"height": "80px"}, "medium");
});

$(document).on('keyup','#sub_new_comment', function(){
    //console.log('wefwfwe');
    var comment=$('#sub_new_comment').val();
    $.ajax({
        type:'POST',
        url:'/controllers/ajax.php',
        data:'comment='+comment+'&name_func=youtube_video',
        dataType:'json',
        success:function(data){
            $.each(data.ids, function(index,value){
                if(!$('#sub1 #block_send_and_attached [id_youtube='+value+']').length)
                    $('#start_video1').append('<div class="youtube" id_youtube="'+value+'"><div class="delete_youtube">DELETE</div><iframe width="300" height="169" src="https://www.youtube.com/embed/'+value+'" frameborder="0" allowfullscreen></iframe></div>');
            });
        }
    });
});

/*CREATE NEW SUB COMMENT*/
$(document).on("click", "#show_sub_comment1", function () {
    var open=false;
    if($('#sub1').length){
        var id_post_click=$(this).parents(':eq(5)').attr('id');
        var id_post_now=$('#sub1').parents(':eq(1)').attr('id');
        if(id_post_now != id_post_click) open=true;
        $('#sub1').remove();
        console.log('1');
    }else
        open=true;
        
        if(open){
            console.log('2');
            $(this).parents(':eq(4)').append(
            "<div class='type_block_container' id='sub1' > " +
            "<div class='row-fluid'>" +
            "<div class='span12'>" +
            "<textarea id='sub_new_comment' placeholder='Hier Kommentar eintragen...' class='span12 contenteditable' onkeyup='autoGrow(this);' maxlength='2000'></textarea>" +
            "</div>" +
            "</div>" +
            "<div id='block_send_and_attached'>" +
            "<div class='row-fluid'>" +
            "<div class='span4'>" +
            "<label for='download_photo1' class='span12 new_post_btn_attached_photo'>" +
            "Anhängen" +
            "</label>" +
            "<input id='download_photo1' style='display: none' type='file' name='image[]' multiple='true'>" +
            "</div>" +
            "<div class='span4 pull-right'>" +
            "<input type='button' class='span12 new_post_btn_send' value='Senden' id='send_comment1'/>" +
            "</div>" +
            "</div>" +
            "<div id='start_video1'></div>" +
            "<div class='row-fluid'>" +
            "<div class='span12 sub_attached_photo1'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }
});



/*ALL ABOUT POST AND COMMENT*/


/*DELETE VIDEO*/
$(document).on("click",".delete_youtube",function(){
    $(this).parent().remove();
    $("#block_type_new_post").val("");
    $('#sub_new_comment').val("");
});


/*DELETE POST OR COMMENT*/
$(document).on('click','.post_drop',function(){

    var type = "";
    var id;
    $(this).parents().eq(5).toggle( "highlight",function(){
        $(this).remove();
    });
    if($(this).parents().eq(5).attr("name") == 'get_media'){
        type = "post";
        id = $(this).parents().eq(5).attr("id");

    }
    if($(this).parents().eq(5).attr("id") == 'sub_comment'){
        type = "comment";
        id = $(this).parents().eq(5).attr("id_comment");
    }
    $.ajax({

        type:'POST',
        url:'/controllers/ajax.php',
        data:'&type='+type+'&id='+id+'&name_func=delete_comm_post',
        dataType:'json',
        success:function(data){

        }
    });

});


/*LIKE POST OR COMMENT*/
$(document).on('click','#like_button',function(){
    var temp1 = $("#my_ava").attr("src");

    var like = $(this).parent().next().text();
    var int_like = parseInt(like);
    var find_my_ava=$(this).parent().next().find('img#me');

        if(find_my_ava.length){
            $(find_my_ava).parent().remove();
            $(this).parent().next().find("#liked_member").text(int_like-1);
            if(!(int_like-1))
                $(this).parent().next().children("ul").remove();
        }else{
            $(this).parent().next().find("#liked_member").text(int_like+1);
            var ul=$(this).parent().next().children("ul");
            if(!ul.length)
                $(this).parent().next().append('<ul class="dropdown-menu inline pull-right"></ul>');
            $(this).parent().next().children("ul").append("<li><img src='"+temp1+"' id='me' width='20'/></li>");
        }
    
    var type = "";
    var id;
    if($(this).parents().eq(5).attr("name") == 'get_media'){
        type = "post";
        id = $(this).parents().eq(5).attr("id");

    }
    if($(this).parents().eq(5).attr("id") == 'sub_comment'){
        type = "comment";
        id = $(this).parents().eq(5).attr("id_comment");
    }


    $.ajax({
        type:'POST',
        url:'/controllers/ajax.php',
        data:'&type='+type+'&id='+id+'&name_func=like_comm_post',
        dataType:'json',
        success:function(data){

        }

    });
    delete like,int_like,temp1;
});

/*Textarea height auto resize*/
function autoGrow (oField) {
    if (oField.scrollHeight > oField.clientHeight) {
        oField.style.height = oField.scrollHeight + "px";
    }
}