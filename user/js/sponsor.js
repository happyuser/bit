$(document).ready(function(){
        //validation user information blocks
    function updateCoords(c)
    {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };
    
    function checkCoords()
    {
        if (parseInt($('#w').val())) return true;
        alert('Please select a crop region then press submit.');
        return false;
    };

$('.other_u_img img').click(function(){
    $('.view_photo').slideDown(200);
    $('.no_activ_body').show();
});  

$('.img_profil.my_img img').click(function(){
    $('.no_activ_body').show();
    $('.change_photo').slideDown().find('.photo_').html('<img src="'+$(this).attr('src')+'" class="photo_block_edit" id="cropbox" />');;
    $('#cropbox').Jcrop({
        onSelect: updateCoords,
        aspectRatio: 1
    });
    
});
$('#crop_img').click(function(){
    checkCoords();
    var img=$('.change_photo img').attr('data');
    var str=$(this).parent().parent().parent().serialize();
    if(img)
        str+='&img_user='+$('.change_photo img').attr('data');
    
    $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: str+'&name_func=crop_img_profile&img_w='+$('.photo_').width()+'&img_h='+$('.photo_').height(),
        dataType: "json",
        success: function(data)
        {
            $('.change_photo').slideUp();
            $('.no_activ_body').hide();
        }
    });
    return false;
});

$('#imgFile').on('change', function(){
    var fd = new FormData();
    fd.append('img', $('#imgFile')[0].files[0]);
    
    $.ajax({
      type: 'POST',
      url: 'upload.php',
      data: fd,
      processData: false,
      contentType: false,
      dataType: "json",
      success: function(data) {
        if(data.name_img){
            $('.change_photo').find('img').remove();
            $('.change_photo').slideDown().find('.photo_').html('<img src="../uploads/'+data.name_img+'" class="photo_block_edit" id="cropbox" data="'+data.name_img+'"/>');
            $('.img_profil img').attr('src','../uploads/'+data.name_img);
            $('[name=photo]').val(data.name_img);
            $('.photo_edit').show();
            
            
            $('#cropbox').Jcrop({
                onSelect: updateCoords,
                aspectRatio: 1
            });
        }
      },
      error: function(data) {
        console.log(data);
      }
    });
    return false;
});

    $('.edit_kontakten').click(function(){
        $('.pop_window.change_kontakten').slideDown(200);
        $('.no_activ_body').show();
    });
    $('.edit_info').click(function(){
        $('.pop_window.change_info').slideDown(200);
        $('.no_activ_body').show();
    });
    $('form #abbrechen').click(function(){
        $('.pop_window').slideUp(200);
        $('.no_activ_body').hide();
        return false;
    });
    $('form #speichern').click(function(){
        var str=$(this).parent().parent().parent().serialize();
        var after=$(this).attr('after');
        console.log(str);

        var inputs=$(this).parent().parent().find('input,textarea,select');
        console.log(inputs);

        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: str+'&name_func=update_sponsor_by_sponsor',
            dataType: "json",
            success: function(data)
            {
                if(data.ok === 'true'){
                    if(after === 'change_close'){
                        $.each(inputs,function(index,value){
                            console.log('2:'+index);
                            var class_=$(value).attr('name');
                            if(!$(value).is('select'))
                                var val_=$(value).val();
                            else
                                var val_=$(value).find('option:selected').text();
                            $('.'+class_).text(val_);
                            $('.no_activ_body').hide();
                            $('.pop_window').hide();
                            window.onscroll=function(){};
                        });
                    }

                    $('.status_message_ok').animate({'opacity':'1'},1000);
                    $('.status_message_ok').animate({'opacity':'0'},500);
                }
            }
        });
        return false;
    });
});