function check_input(pattern_ok, pattern_no,empty,val) {
    if(pattern_ok || pattern_no || empty)
        if(pattern_no && pattern_no.test(val)){//якщо є регулярка при якій стрічка вважається неправильною і вона "спрацювала", тоді вертаємо FALSE 
            return false;
            }
        else if(pattern_ok.test(val)){//якщо регулярка при якій стрічка вважається правильною пройшла тоді вертаємо успіх
            return true;
            }
        else if(empty && !val){//якщо поле в формі може бути пустим і воно пусте по факту, тоді вертаємо успіх 
            return true;
        }
        else{
            return false;
        }
}
    
$(document).ready(function(){
    function check_input_access(){
        var inputs=$('[required]');
        if(inputs.length)
            $.each(inputs, function(index, value){
                if($(value).val()==''){
                    $('.next').attr('disabled','disabled');
                    return false;
                }
                $('.next').removeAttr('disabled');
            });
        else
            $('.next').removeAttr('disabled');
    }
    
    check_input_access();
    $('.form_page input,.form_page textarea,.form_page select').on('textchange', function(){
        check_input_access();
    });
    
    $('#imgFile').on('change', function(){
        var fd = new FormData();
        fd.append('img', $('#imgFile')[0].files[0]);
        
        $.ajax({
          type: 'POST',
          url: 'upload.php',
          data: fd,
          processData: false,
          contentType: false,
          dataType: "json",
          success: function(data) {
            if(data.name_img){
                $('.upload_image').attr('src','../uploads/'+data.name_img);
                $('[name=foto]').val(data.name_img);
                //$('.img_profil').css('height',$('.upload_image').css('height'));
                console.log($('.upload_image').css('height'));
            }
          },
          error: function(data) {
            console.log(data);
          }
        });
        return false;
    });
    $('.img_profil').hover(function(){
        $('.new_photo_upload').animate({'opacity':1},300);
    },function(){
        $('.new_photo_upload').animate({'opacity':0},100);
    });
    
    $('.back_reg').click(function(){
        var id=parseFloat($('form.form_reg').attr('id'));
        id--;
        if(id)
            document.location.href = "/user/?page=reg_guest&step="+id;
        else
            document.location.href = "/user/?page=registration&step=0";
        return false;
    });
    
    $('.next').click(function(){
        var str=$('form.form_reg').serialize();
        var id=parseFloat($('form.form_reg').attr('id'));
        console.log(str);
        var wrong='';
        var pettern={//обєкт з регулярками, при яких стрічка вважатиметься правильною, неправильною і поля яким дозволено залишатись пустими
            ok:{//петтерни по яких дані визнаються як правильними
                email:/[0-9a-z_]+@[0-9a-z_]+\.[a-z]{2,5}/i,
                name:/[a-zA-Z]{2,20}/,
                vorname:/[a-zA-Z]{2,20}/,
                mobiltelefon:/[0-9]{2,20}/,
                telefon:/[0-9]{2,20}/
            },
            no:{//паттерн по якому відхиляються введені дані
                name:/[0-9а-я]\W/,
                vorname:/[0-9а-яА-Я]\W/,
                mobiltelefon:/[a-zA-Zа-яА-Я]{2,20}/,
                telefon:/[a-zA-Zа-яА-Я]{2,20}/
            },
            empty:{//може бути пустим
                mobiltelefon:1
            },
            name_de:{
                email:'Email',
                name:'Name',
                vorname:'Vorname',
                telefon:'Telefonummer',
                mobiltelefon:'Handynummer'
            }
        }
        $.each($('.form_reg_index input'), function(index,value){
            var name=$(value).attr('name');
            if(check_input(pettern.ok[name],pettern.no[name],pettern.empty[name],$(this).val())===false)
                wrong+='Falsche '+pettern.name_de[name]+"\n";
        });
        if(wrong!=''){
            alert(wrong);
            return false;
        }
        var page = $(document);
        var inactiveBackground = $('.inactive_background').css('height', 'calc(100% - 75px)').css('top', 75 + 'px').width('100%').css('position', 'fixed');
        inactiveBackground.fadeIn(200);
        
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: str+'&name_func=save_guest_step&id_step='+id,
        dataType: "json",
        success: function(data)
        {
            if(data.ok=='true'){
                console.log(data);
                id++;
                if(id<6)
                    document.location.href = "/user/?page=reg_guest&step="+id;
                else
                    document.location.href = "/user/?page=registration&step=5";
            }else if(!data){
                alert('Ihre Anmeldung ist bereits im Datenbank. Sie konnen sich nicht zum zweiten Mal bewerben.');
            }
        }
        });
        return false;
    });
    $('#event_input').on('textchange',function(){
        $('.search_events').remove();
        var val=$(this).val();
        var str='';
        if(!val){
            $('[name=event]').val('');
            check_input_access();
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=search_event&text='+val,
            dataType: "json",
            success: function(data)
            {
                if(data==null)
                {
                    $('.search_events').remove();
                    $('[name=event]').val('');
                    check_input_access();
                    return false;
                }
                else{
                    $.each(data, function(index,value){
                        console.log(value);
                        str+='<div class="li_event" id="'+value.id+'" style="cursor: pointer"><div class="img"><img src="../uploads/'+value.img+'"/></div><div class="title"><a href="/user/?page=event&id='+value.id+'" target="_blank"><span class="name_event">'+value.name+'</span> | <span class="city">'+value.city+'</span></a></div></div>';
                    });
                    $('#event_input').after('<div class="search_events">'+str+'</div>');
                }
            }
        });
    });
    $(document).on('click','.li_event', function(){
        var id = $(this).attr('id');
        var name = $(this).find('.name_event').text();
        $('#event_input').val(name);
        $('[name=event]').val(id);
        $('.search_events').remove();
        check_input_access();
    });

    /*$( "[name='unternehmen_tatig']" ).datepicker({
     onSelect:function() {check_input_access();}
     });

     $( "[name='branche_tatig']" ).datepicker({
     onSelect:function() {check_input_access();}
     });*/

    $(function(){
        //подключение календаря
        $('.unternehmen_tatig_box').click(function(){
            var target=$(this);
            $(target).datepicker({
                changeMonth:true,
                changeYear:true,
                yearRange: '1980:2025',
                onSelect: function(dateText){
                    $(target).children().val(dateText);
                    $('.ui-datepicker').remove();
                    $(target).attr('id','');
                    $(target).removeClass('hasDatepicker');
                    check_input_access();
                }
            });
            $('.ui-datepicker').css({
                'position': 'absolute',
                'top': '208px',
                'left': '119px'
            });
        });
    });

    $(function(){
        //подключение календаря
        $('.branche_tatig_seit_box').click(function(){
            var target=$(this);
            $(target).datepicker({
                changeMonth:true,
                changeYear:true,
                yearRange: '1980:2025',
                onSelect: function(dateText){
                    $(target).children().val(dateText);
                    $('.ui-datepicker').remove();
                    $(target).attr('id','');
                    $(target).removeClass('hasDatepicker');
                    check_input_access();
                }
            });
            $('.ui-datepicker').css({
                'position': 'absolute',
                'top': '272px',
                'left': '119px'
            });
        });
    });

    $(function(){
        //подключение календаря
        $('.birthdate_reg_box').click(function(){
            var target=$(this);
            $(target).datepicker({
                changeMonth:true,
                changeYear:true,
                yearRange: '1960:2000',
                onSelect: function(dateText){
                    $(target).children().val(dateText);
                    $('.ui-datepicker').remove();
                    $(target).attr('id','');
                    $(target).removeClass('hasDatepicker');
                    check_input_access();
                }
            });
            $('.ui-datepicker').css({
                'position': 'absolute',
                'top': '306px',
                'left': '145px'
            });
        });
    });
});