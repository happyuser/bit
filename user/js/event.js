$(document).ready(function(){
    $('.no_activ_body').click(function(){
        $('.no_activ_body').hide();
        $('.pop_window').hide();
        $("body").removeAttr("style");
    });

    /*
    $('#next_photo').click(function(){
        var photo_arr=$('.slide_image_event');
        var id = $('.slide_image_event:visible').attr('id');
        $('.slide_image_event:visible').hide(100);
        id++;
        if(id>=photo_arr.length)id=0;
        console.log(id);
        $('#'+id+'.slide_image_event').show(200);
    });
    */
    $('.close_window').click(function(){
        $('.pop_window').hide();
    });

    $('.search_clear').click(function(){
        $('.search-query').val('');
        search();
    });
    
    function search(){
        $('.body_list_guest').html('');
        var text=$('.search-query').val();
         var event=$('.pop_window.guest_list').attr('id_event');
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=search_guest_list&text='+text+'&event='+event,
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(index,value){
                    if(value.type_user=='user'){    
                        var functions=value.functions;
                        var last_name=value.last_name;
                        var first_name=value.first_name;
                    }else{
                        var functions=value.funktion_bus;
                        var last_name=value.vorname;
                        var first_name=value.name;
                    }
                    $('.body_list_guest').append('<div class="li_guest row-fluid" id="'+value.id+'">'+
                        '<div class="name_lastname span3" id="'+value.id+'">'+
                        (value.type_user=='user'?'<a href="/user/?page=main_profil&id='+value.id+'" target="_blank">':'')+
                        last_name+' '+first_name+
                        (value.type_user=='user'?'</a>':'')+
                        '</div>'+
                        '<div class="firma span4">'+functions+'</div>'+
                        (value.type_user=='user'?'<div class="contact span3">'+value.firma+'</div>':'')+
                        '<div class="accompany span2">'+value.accompany+'</div>'+
                    '</div>');
                });
            }
        });
    /*
        var text=$('.search-query').val();
        if(text==''){$('.li_guest').show(300); return false;}
        var name_list=$('.li_guest');//:visible забрати якщо пошук потрібно завше робити по всіх іменах
        var last_id=undefined;
        $.each(name_list, function(index, val){
            value=$(val).find('.name_lastname');
            value=value[0];
            if(((value.textContent).toLowerCase().indexOf(text.toLowerCase()))>=0 || last_id==value.id){
                last_id=value.id;
                $('#'+value.id+'.li_guest').show(300);
            }
            else
            {
                $('#'+value.id+'.li_guest').hide(300);
            }
        });
        return false;
    */
    }
    $('.search-query').on('textchange',function(){
        return search();
    });
    
    $('.btn_search').click(function(){
        return search();
    });
    $('li.anmelden').click(function(){
        var id=$(this).attr('id');
        if($('div').is('.begleitung'))
            $('.begleitung').slideDown(200);
        else
            $.ajax({
                type: "POST",
                url: "../controllers/ajax.php",
                data: 'name_func=application_for_event&id='+id,
                dataType: "json",
                success: function(data)
                {
                    $('.pop_window').hide();
                    if(data!=false)
                        $('.message_event_ok').slideDown(200);
                    else
                        alert("Sie sind bereits in der Liste der Eingeladenen. Überprüfen Sie bitte Ihre Einladungen, wenn es keine gibt, Sie bekommen bald eine.");
                }
            });
    });
    $('[name=has_begleitung]').change(function(){
        if($(this).is(':checked')) {$('[name=accompany]').removeAttr('disabled'); $('[name=not_begleitung]').removeAttr('checked');} else {$('[name=accompany]').attr('disabled',true); $('[name=not_begleitung]').prop('checked',true);} 
    });
    $('[name=not_begleitung]').change(function(){
        if($(this).is(':checked')) {$('[name=accompany]').attr('disabled',true); $('[name=has_begleitung]').removeAttr('checked');} else {$('[name=accompany]').removeAttr('disabled'); $('[name=has_begleitung]').prop('checked',true);} 
    });
    
    $('button.anmelden').click(function(){
        var str='name_func=application_for_event&id='+$(this).attr('id_event');
        if($('[name=has_begleitung]').is(':checked')){
            str=str+'&accompany='+$('[name=accompany]').val();
        }
        $.ajax({
            type: "POST",
            url: "../controllers/ajax.php",
            data: str,
            dataType: "json",
            success: function(data)
            {
                $('.pop_window').hide();
                if(data.ok=="true")$('.message_event_ok').slideDown(200);
                else if(data==false)alert("Sie sind bereits in der Liste der Eingeladenen. Überprüfen Sie bitte Ihre Einladungen, wenn es keine gibt, Sie bekommen bald eine.");
            }
        });
        return false;
    });
   $('.guest_list_btn').click(function(){
        $('.guest_list').show();
        $('.no_activ_body').show();
        var x=window.scrollX;
        var y=window.scrollY;
        window.onscroll=function(){window.scrollTo(x, y);};
    });

    $('.photos_list_btn').click(function(){
        if($('.images_list').length){
            $('.no_activ_body').show();
            $("body").css("overflow", "hidden");
            $('.images_list').toggle(0);
        }
    }); 
});