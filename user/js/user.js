$(document).ready(function(){
    //validation user information blocks
    isInfoBlockEmpty();
    function updateCoords(c)
    {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };
    
    function checkCoords()
    {
        if (parseInt($('#w').val())) return true;
        alert('Please select a crop region then press submit.');
        return false;
    };

$('.other_u_img img').click(function(){
    $('.view_photo').slideDown(200);
    $('.no_activ_body').show();
});  

$('.img_profil.my_img img').click(function(){
    $('.no_activ_body').show();
    $('.change_photo').slideDown().find('.photo_').html('<img src="'+$(this).attr('src')+'" class="photo_block_edit" id="cropbox" />');;
    $('#cropbox').Jcrop({
        onSelect: updateCoords,
        aspectRatio: 1
    });
    
});
$('#crop_img').click(function(){
    checkCoords();
    var str=$(this).parent().parent().parent().serialize();
    var img=$('.change_photo img').attr('data');
    if(img)
        str+='&img_user='+$('.change_photo img').attr('data');
        
    $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: str+'&name_func=crop_img_profile&img_w='+$('.photo_').width()+'&img_h='+$('.photo_').height(),
        dataType: "json",
        success: function(data)
        {
            $('.change_photo').slideUp();
            $('.no_activ_body').hide();
        }
    });
    return false;
});


    $('form .form_edit_p.user_profil #speichern').click(function(){
        var str=$(this).parent().parent().parent().serialize();
        var after=$(this).attr('after');

        var inputs=$(this).parent().parent().find('input,textarea');

        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: str+'&name_func=update_user_for_user&msg=Benutzerprofil bearbeiten',
            dataType: "json",
            success: function(data)
            {
                if(data.ok === 'true'){
                    if(after === 'change_close'){
                        $.each(inputs,function(index,value){
                            var class_=$(value).attr('name');
                            var val_=$(value).val();
                            $('.'+class_).text(val_);
                            $('.no_activ_body').hide();
                            $('.pop_window').hide();
                            window.onscroll=function(){};
                        });
                    }
                    $('.status_message_ok').animate({'opacity':'1'},1000);
                    $('.status_message_ok').animate({'opacity':'0'},500);
                }
            }
        });
        return false;
    });
    
    $('.edit_icon').click(function(){
        $('.pop_window').slideUp(0);
        var id=$(this).attr('id');
        $('.pop_window.change_'+id).slideDown(200);
        $('.no_activ_body').show();
        var x=window.scrollX;
        var y=window.scrollY;
        window.onscroll=function(){window.scrollTo(x, y);}
    });

    $('form #abbrechen').click(function(){
        $('.no_activ_body').hide();
        $('.pop_window').hide();
        window.onscroll=function(){};
        
        //дальше код який виконуватиметься на беруфсвіта
        
        if($(this).hasClass('abbrechen_berufsvita')){
            $('.year_end').show().css('opacity',1);
            $('.form_change_member').removeAttr('id');
            $('.change_berufsvita .form_change_member input[type=text],.change_berufsvita .form_change_member input[type=hidden]').val('');
            $('.mounth_start').find('option[value="01"]').attr("selected",true);
            $('.year_start').find('option[value="2015"]').attr("selected",true);
            $('.mounth_end').find('option[value="now"]').attr("selected",true);
            $('.year_end').find('option[value="2015"]').attr("selected",true);
            $('.change_user_career_btn').hide();
            $('.save_user_career_btn').show();
        }
        
        return false;
    });

    $('#imgFile').on('change', function(){
        var fd = new FormData();
        fd.append('img', $('#imgFile')[0].files[0]);
        
        $.ajax({
          type: 'POST',
          url: 'upload.php',
          data: fd,
          processData: false,
          contentType: false,
          dataType: "json",
          success: function(data) {
            if(data.name_img){
                $('.change_photo').find('img').remove();
                $('.change_photo').slideDown().find('.photo_').html('<img src="../uploads/'+data.name_img+'" class="photo_block_edit" id="cropbox" data="'+data.name_img+'"/>');
                $('.img_profil img').attr('src','../uploads/'+data.name_img);
                $('[name=photo]').val(data.name_img);
                $('.photo_edit').show();
                
                $('#cropbox').Jcrop({
                    onSelect: updateCoords,
                    aspectRatio: 1
                });
            }
          },
          error: function(data) {
            console.log(data);
          }
        });
        return false;
    });


    $('.photo_edit #true').click(function(){
        var img=$('[name=photo]').val();
        $.ajax({
        async: false,
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=update_user_for_user&photo_user='+img,
        dataType: "json",
        success: function(data)
        {
            if(data.ok === 'true'){
                $('.img_profil img').attr('src','../uploads/'+img);
                $('.photo_edit').hide();
            }
        }
        });
    });
    
    $('.photo_edit #false').click(function(){
        $('.photo_edit').hide();
    });
    
    $('.img_profil').hover(function(){
        $('.block_after_photo').animate({'opacity':1},300);
    },function(){
        $('.block_after_photo').animate({'opacity':0},100);
    });

    $('.ul_menu_buttom li').click(function(){
        var tab=$(this).attr('tabindex');
        $('.ul_menu_buttom li').removeClass('active');
        $(this).addClass('active');
        $('[id^="tab"].info_li').hide();
        $('#'+tab+'.info_li').show();
    });
    
    $('.save_user_career_btn').click(function(){
        $('[name=where-time_start]').val($('.year_start').val()+'-'+$('.mounth_start').val()+'-01');
        var str=$(this).parent().parent().parent().serialize();
        if(!$('[name=where-firmenname]').val()){
            alert('Bitte geben Sie den Namen des Unternehmens.');
            return false;
        }
        $(this).parent().parent().parent().find('input[type=text], input[type=date]').val('');
        
        $.ajax({
        async: false,
        type: "POST",
        url: "/controllers/ajax.php",
        data: str+'&name_func=new_job',
        dataType: "json",
        success: function(data)
        {
            console.log(data);
            var str='<ul class="job" id="'+data.id_job+'">';
			str=str+'<li><i class="work1 list_time_job">&nbsp;</i><b><span class="start_date">'+data.time_start_format+'</span> - <span class="finish_date">'+data.time_end_format+'</span></b></li>';
			str=str+'<li><p><b class="list_firmenname" style="display: block;">'+data.firmenname+'</b></p>';
			str=str+'<p class="list_position">'+data.position+'</p></li>';
			str=str+'<li class="list_aufgaben">'+data.aufgaben+'</li></ul>';
                
            var str2='<div class="job" id="'+data.id_job+'">';
                str2=str2+'<div class="span3 header_block">';
                str2=str2+'<div class="date_job">';
                str2=str2+'<span class="start_date" date="'+data.time_start+'">'+data.time_start_format+'</span> - <span class="finish_date" date="'+data.time_end+'">'+data.time_end_format+'</span>';
                str2=str2+'</div><div class="left_time_job">'+data.years+' Jahre '+ data.months+' Monate</div></div>';
                str2=str2+'<div class="span4" style="color: #000;">';
                str2=str2+'<div class="list_firmenname">'+data.firmenname+'</div>';
                str2=str2+'<div class="list_position">'+data.position+'</div></div>';
                str2=str2+'<div class="span3"><div class="list_aufgaben">'+data.aufgaben+'</div></div>';
                str2=str2+'<div class="span2"><div class="edit_job" style="float: left;"><i class="icon-pencil icon-white"></i></div><div class="drop_job">X</div></div></div>';
                
                $('.t_profile .berufsvita .job_start').after(str);
                $('.start_jobs').after(str2);
                $('.no_activ_body').hide();
                $('.pop_window').hide();
                window.onscroll=function(){};
        }
        });
        return false;
    });
    
    $('.list_jobs').on('click','.edit_job',function(){
        $('.change_user_career_btn').show(200);
        $('.save_user_career_btn').hide(200);
        
        var select_form=$(this).parent().parent();
        var id=$(this).parent().parent().attr('id');
        
        $('.form_change_member').attr('id',id);
        $('.form_change_member input[type=text], .form_change_member input[type=hidden]').val('');
        
        var start_date=$(select_form).find('.start_date').attr("date");
        console.log(start_date);
        $('[name=where-firmenname]').val($(select_form).find('.list_firmenname').text());
        start_date = start_date.split('-');
        console.log(start_date);
        $('.year_start').val(start_date[0]);
        $('.mounth_start').val(start_date[1]);
        $('[name=where-time_start]').val(start_date[0]+'-'+start_date[1]);
        
        
        var finish_date=$(select_form).find('.finish_date').attr("date");
        
        if(finish_date!='bis jetzt' && finish_date!='')
            {
                finish_date = finish_date.split('-');
                $('.year_end').val(finish_date[0]);
                $('.mounth_end').val(finish_date[1]);
                $('[name=where-time_end]').val(finish_date[0]+'-'+finish_date[1]);
                $('.year_end').show().css({'opacity':1});
            }
        else
            {
                $('.mounth_end [value=now]').attr('selected',true);
                $('.year_end').animate({'opacity':0},200).hide(0);
                $('[name=where-time_end]').val('now');
            }
            
        $('[name=where-position]').val($(select_form).find('.list_position').text());
        
        $('[name=where-aufgaben]').val($(select_form).find('.list_aufgaben').text());
    });
    
    
    $('.change_berufsvita select').change(function(){
        //якщо закінчення раніше початку роботи
        if($('.year_end').val()<$('.year_start').val())$('.year_end').val($('.year_start').val()).prop('selected',true);
        if($('.year_end').val()==$('.year_start').val()){//якщо одинакові роки то перевіряємо місяці
            if($('.mounth_end').val()<$('.mounth_start').val()){
                $('.mounth_end option:selected').prop('selected',false);
                $('.mounth_end option[value='+$('.mounth_start').val()+']').prop('selected','selected');
            }
        }
        var time_end=$('.year_end').val()+'-'+$('.mounth_end').val();
        
        if($(this).hasClass('mounth_end') || $(this).hasClass('year_end'))
            $('.year_end').show().animate({'opacity':1},200);

        
        var time_start=$('.year_start').val()+'-'+$('.mounth_start').val();
        $('[name=where-time_start]').val(time_start);
        if($('.mounth_end').val()=='now'){
            time_end='now';
            $('.year_end').animate({'opacity':0},200).hide(0);
        }
        
        $('[name=where-time_end]').val(time_end);
    });
    
    $('.change_user_career_btn').click(function(){
        if(!$('[name=where-firmenname]').val()){
            alert('Bitte geben Sie den Namen des Unternehmens.');
            return false;
        }
        var str=$(this).parent().parent().parent().serialize();
        var id=$(this).parent().parent().parent().attr('id');
        str=str+'&id='+id;
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: str+'&name_func=update_userJob_for_user',
        dataType: "json",
        success: function(data)
        {
            $('.change_berufsvita #'+data.id+'.job .start_date, .t_profile #'+data.id+'.job .start_date').text(data.time_start);
            $('.change_berufsvita #'+data.id+'.job .finish_date, .t_profile #'+data.id+'.job .finish_date').text(data.time_end);
            $('.change_berufsvita #'+data.id+'.job .list_firmenname, .t_profile #'+data.id+'.job .list_firmenname').text(data.firmenname);
            $('.change_berufsvita #'+data.id+'.job .list_position, .t_profile #'+data.id+'.job .list_position').text(data.position);
            $('.change_berufsvita #'+data.id+'.job .list_aufgaben, .t_profile #'+data.id+'.job .list_aufgaben').text(data.aufgaben);
            $('.change_berufsvita #'+data.id+'.job .left_time_job, .t_profile #'+data.id+'.job .left_time_job').text(data.years+' Jahre '+ data.months+' Monate');
            
            if($(this).attr('type')=='change_close')$('.change_berufsvita').slideToggle(200);
            $('.form_change_member input[type=text], .form_change_member input[type=date]').val('');
            $('.no_activ_body').hide();
            $('.pop_window').slideUp(400);
        }
        });
        return false;
    });
    
    $('.delete_photo').click(function(){
        if(confirm('delete?')){
            $.ajax({
                type: "POST",
                url: "/controllers/ajax.php",
                data: 'name_func=delete_my_photo',
                dataType: "json",
                success: function(data)
                {
                    if(data.ok=='true'){
                        location.reload();
                    }
                }
            });
        }
    })
    
    $('.list_jobs').on('click','.drop_job',function(){
        var id=$(this).parent().parent().attr('id');
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=drop_job&id='+id,
        dataType: "json",
        success: function(data)
        {
            if(data.ok=='true'){
                $('#'+id+'.job').hide(300);
            }
        }
        });        
    });
    
    $('.pop_window').on('click','.block_info',function(){
        var status=0;
        if($(this).hasClass('no_access'))status=1;
        
        var type=$(this).attr('for');
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=edit_access_users_info&type='+type+'&status='+status,
        dataType: "json",
        success: function(data)
        {
            if(data.ok=='true'){
                if(status===1){
                    $('.block_info[for='+type+']').removeClass('no_access');
                    $('.block_info[for='+type+'] i').addClass('icon-white');
                    $('.block_info[for='+type+']').attr('title','Für andere Mitglieder unsichtbar sichtbar machen');
                }else{
                    $('.block_info[for='+type+']').addClass('no_access');
                    $('.block_info[for='+type+'] i').removeClass('icon-white');
                    $('.block_info[for='+type+']').attr('title','Für andere Mitglieder sichtbar machen');
                }
            }
        }
        });
    });
    
    $('.block_info').hover(function(){
        $(this).find('.info_text_access').animate({'width':'100%'});
    },function(){
        $(this).find('.info_text_access').animate({'width':'0%'});
    });
    
    
    $('.add_friend').click(function(){
        var id=$(this).attr('id_user');
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=update_friend&id='+id,
        dataType: "json",
        success: function(data)
        {
           if(data.ok=='true')location.reload();
        }
        });
    });
    
    $('.block_info').hover(function(event) {
          // Hover over code
    var titleText = $(this).attr('title');
    $(this)
      .data('tipText', titleText)
      .removeAttr('title');
      
    $('<p class="tooltip"></p>')
      .text(titleText)
      .appendTo('body')
      .css('top', (event.pageY - 10) + 'px')
      .css('left', (event.pageX + 20) + 'px')
      .fadeIn('slow').animate({'opacity':1},300);
      }, function() {
        // Hover out code
        if(!$(this).attr('title'))$(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
      }).mousemove(function(event){
        // Mouse move code
        $('.tooltip')
          .css('top', (event.pageY - 10) + 'px')
          .css('left', (event.pageX + 20) + 'px');
    });
    
    $('.change_contacts .ul_kontakten_top li a').click(function(){
        var id=$(this).attr('href');
        $('.change_contacts .ul_kontakten_top li').removeClass('active');
        $(this).parent().addClass('active');
        $('#kontakten_block, #netzprofilen_block').hide(0);
        $('#'+id+'.form_change_member').show(100);
        return false;
    });

    // for empty info block's on main page, inserting default text
    function isInfoBlockEmpty() {
        var profileInfo = $('.t_profile_info');
        var ubermich = $('.ubermich');
        var kontaktdaten = $('.kontaktdaten');
        var berufsvita = $('.berufsvita');

        var str = 'Your profile is empty. Click the pensil to fill in the information.';

        if (!(profileInfo.children().length > 1)) {
            var text = $(document.createElement('p'));
            text.html(str).appendTo('.t_profile_info');
        }
        else if (!(ubermich.children().length > 1)) {
            var text2 = $(document.createElement('p'));
            text2.html(str).appendTo('.ubermich');
        }
        else if (!(kontaktdaten.children().length > 1)) {
            var text3 = $(document.createElement('p'));
            text3.html(str).appendTo('.kontaktdaten');
        }
        else if (!(berufsvita.children().length > 1)) {
            var text4 = $(document.createElement('p'));
            text4.html(str).appendTo('.berufsvita');
        }
    }
});