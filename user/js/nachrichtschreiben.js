$(document).ready(function(){
    
    $('#chose_file').click(function(){
        $('#chose_file_input').trigger('click');
       return false; 
    });
    
    $('#chose_file_input').change(function(){
        var fd = new FormData();
        $.each($(this)[0].files, function(index,value){
            fd.append('img[]', value);
        });
        $.ajax({
            type: 'POST',
            url:'upload.php',
            data: fd,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function(data) {
                $.each(data.name_img,function(index,value){
                    var file=value.split('.');
                    if(file[1]!=='mpeg' && file[1]!=='mp4')
                        $('.files').append('<img id="'+value+'" src="../uploads/'+value+'" width="200">');
                    else
                        $('.files').append('<video id="'+value+'" src="../uploads/'+value+'" width="400" controls></video>');
                });
            }
        });
    });
    $('.send_msg').click(function(){
        var to=$('[name=to]').val();
        if(!to)return false;
        var msg=$('[name=message]').val();
        var files='';
        $.each($('.files img,.files video'),function(index,value){
            files+='&files[]='+$(value).attr('id');
        });
        $.ajax({
            type:'POST',
            url:'/controllers/ajax.php',
            data:'&name_func=send_msg&to='+to+'&text='+msg+files,
            dataType:'json',
            success:function(data){
                if(data.id)
                    location.href='/user/?page=readmess&id_mess='+data.id;
            }
        });
    });
    $('#write_name').on('focusout',function(){
        setTimeout("$('.select').html('')",300);
    });
    $('#write_name').on('focus',function(){
        var text=$(this).val();
        if(text.length<=0){
            $('#write_name').trigger('textchange');
        }
    });
    
    
    $('#write_name').on('textchange',function(){
        var text=$(this).val();
        
        /*
        if(text.length<=0){
            $('.select').html('');
            return false;
        }
        */
        $.ajax({
            type:'POST',
            url:'/controllers/ajax.php',
            data:'&name_func=search_friends&text='+text,
            dataType:'json',
            success:function(data){
                $('.select').html('');
                $.each(data,function(index,value){
                    $('.select').append('<div class="option" id_user="'+index+'"><img src="../uploads/crop_img/'+value.photo+'" width="50"/> <span>'+value.first_name+' '+value.last_name+'</span></div>');
                });
            }
        });
    });
    $('.select').on('click','.option',function(){
        var name=$(this).text();
        var id_user=$(this).attr('id_user');
        $('#write_name').val(name);
        $('[name=to]').val(id_user);
        $('.select').html('');
    });
});