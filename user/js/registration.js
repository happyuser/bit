$(document).ready(function () {

    $('.send_mail_hash').click(function(){
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=send_hash_guest',
            dataType: "json",
            success: function(data)
            {
                if(data){
                    document.location.href = "/user/?page=registration&step=1";
                }
            }
        });
    });

    $('.btn_activ').click(function(){
        var hash=$('[name=hash]').val();
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'hash='+hash+'&name_func=check_hash_guest',
            dataType: "json",
            success: function(data)
            {
                if(data){
                    document.location.href = "/user/?page=reg_guest&step=1&";
                }
            }
        });
    });

    $('.btn_new_bewerber').click(function(){
        document.location.href = "/user/?page=reg_guest&step=1";
    });

    $('.btn_new_sponsoren').click(function(){
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=move_guest_to_sponsor',
        dataType: "json",
        success: function(data)
        {
            if(data){
                document.location.href = "/user/?page=reg_sponsor&step=1";
            }
        }
        });
    });

    $('.btn_new_email').click(function(){
        var new_email=$('[name=new_email]').val();
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=update_mail_guest&new_email='+new_email,
        dataType: "json",
        success: function(data)
        {
            if(data){
                document.location.href = "/user/?page=registration&step=1";
            }
        }
        });
    });
    
    
    function enableRegGuestBtn() {
        var vorname = $('[name=vorname]').val();
        var name = $('[name=name]').val();
        var email = $('[name=email]').val();

        if (vorname !== '' && name !== '' && email !== '') {
            $('#reg_guest').removeAttr('disabled');
            return true;
        } else {
            $('#reg_guest').attr('disabled', true);
            return false;
        }
    }

    $( ".form_reg_index input[type='text']" ).bind("change keyup paste", function() {
        enableRegGuestBtn();
    });
    $('[name="email"]').keydown(function(e) {
        if (e.keyCode === 13) {
            $('#reg_guest').trigger('click');
        }
    });
    
    $('#reg_guest').click(function() {
        var wrong='';
        var pettern={//обєкт з регулярками, при яких стрічка вважатиметься правильною, неправильною і поля яким дозволено залишатись пустими
            ok:{//петтерни по яких дані визнаються як правильними
                email:/[0-9a-z_]+@[0-9a-z_]+\.[a-z]{2,5}/i,
                name:/[a-zA-Z]{2,20}/,
                vorname:/[a-zA-Z]{2,20}/,
                mobil_bus:/[0-9]{2,20}/
            },
            no:{//паттерн по якому відхиляються введені дані
                name:/[0-9а-я]\W/,
                vorname:/[0-9а-яА-Я]\W/,
                mobil_bus:/[a-zA-Zа-яА-Я]{2,20}/
            },
            empty:{//може бути пустим
                //name:1
            },
            name_de:{
                email:'Email',
                name:'Name',
                vorname:'Vorname',
            }
        }
        $.each($('.form_reg_index input'), function(index,value){
            var name=$(value).attr('name');
            if(check_input(pettern.ok[name],pettern.no[name],pettern.empty[name],$(this).val())===false)
                wrong+='Falsche '+pettern.name_de[name]+"\n";
        });
        if(wrong!=''){
            alert(wrong);
            return false;
        }
        
        var page = $(document);
        var inactiveBackground = $('.inactive_background').css('height', 'calc(100% - 75px)').css('top', 75 + 'px').width('100%').css('position', 'fixed');
        inactiveBackground.fadeIn(200);
    });

    $("[name=check_true]").change(function() {
        if ($(this).is(':checked')) {
            $('#licenseAccepted').removeAttr('disabled');
        } else if (!($(this).is(':checked'))) {
            $('#licenseAccepted').attr('disabled', 'true');
        }
    });

    $('#licenseAccepted').click(function() {
        var vorname = $('[name=vorname]').val();
        var name = $('[name=name]').val();
        var email = $('[name=email]').val();
        var anrede = $('[name=anrede]').val();

        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'email='+email+'&name='+name+'&vorname='+vorname+'&name_func=new_guest&anrede='+anrede,
            dataType: "json",
            success: function(data) {
                if (data.ok === 'true') {
                    console.log(data);
                    document.location.href = "/user/?page=registration&step=1";
                }else if (data === false) {
                    document.location.href = "/user/?page=reg_guest&step=1";
                }
            }
        });
    });

    $('#unregister').click(function() {
        var inactiveBackground = $('.inactive_background');
            inactiveBackground.fadeOut(200);
    });
    

/*
функция для проверки даных по регуляркам
параметры:
регулярка при которой данные считаются правильными
регулярка при которой данные считаются НЕправильными
флаг, может ли поле быть пустым
значение для проверки по регуляркам
*/
    function check_input(pattern_ok, pattern_no,empty,val) {
        if(pattern_ok || pattern_no || empty)
            if(pattern_no && pattern_no.test(val)){//якщо є регулярка при якій стрічка вважається неправильною і вона "спрацювала", тоді вертаємо FALSE 
                return false;
                }
            else if(pattern_ok.test(val)){//якщо регулярка при якій стрічка вважається правильною пройшла тоді вертаємо успіх
                return true;
                }
            else if(empty && !val){//якщо поле в формі може бути пустим і воно пусте по факту, тоді вертаємо успіх 
                return true;
            }
            else{
                return false;
            }
    }
});

/*окно перед закрытием window befor close*/

$(document).on('click','a',function(){
    var a=$(this);
    if(!$(this).hasClass('valera') && !$(this).hasClass('ui-corner-all'))
        if(confirm('Sind Sie sicher , dass Sie die Seite verlassen wollen?  Ihre Angaben werden nicht gespeichert')){
            $.ajax({
                type: "POST",
                url: "/controllers/ajax.php",
                data: 'name_func=del_guest',
                dataType: "json",
                success: function(data) {
                    document.location.href = a.attr('href');
                }
            });
        }else return false;
});