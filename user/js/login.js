$(document).ready(function () {
    var currentUrl = window.location.search;
    var submitBtn = $('#submit');

    // submit button should be disabled during the registration process
    if (currentUrl.slice(0,18) === '?page=registration') {
        submitBtn.attr('disabled', 'true');
    }

	$('form.form_login_guest #submit').click(function() {
		var email = $('#email').val(),
			pass  = $('#password').val(),
            loginErrBox = $('#err').hide(),
            textError = $('#err .text-error').html("Überprüfen Sie Ihren Benutzernamen und das Passwort. Sie haben  falsche Zugangsdaten eingegeben.");

        var str='email=' + email + '&password=' + pass;

        if($('[name=save]').is(":checked"))str+='&save=true';

		if (email === "" || pass === "") {
            loginErrBox.fadeIn(200);
            $('#email').attr('disabled', true);
            $('#password').attr('disabled', true);
        } else {
    		$.ajax({
    			type: "POST",
    			url: '/controllers/ajax.php',
    			data: str + '&name_func=user_login',
                dataType: "json",
    			success: function(data) {
    			 console.log(data);
    				if (data.ok === 'true') {
    				    console.log(data.ok);
    				    document.location.href = "/user/?page=wall&type=alle";
    				} else if (data.ok !== 'true') {
                        loginErrBox.fadeIn(200);
                        $('#email').attr('disabled', true);
                        $('#password').attr('disabled', true);
                    }
    			}	
    		});
		}
		return false;
	});

    $('.form_login_guest #email, .form_login_guest #password').keyup(function(e) {
        console.log('wefwef');
        if (e.keyCode === 13) {
            var email = $('#email').val(),
                pass  = $('#password').val(),
                loginErrBox = $('#err').hide(),
                textError = $('#err .text-error').html("Überprüfen Sie Ihren Benutzernamen und das Passwort. Sie haben  falsche Zugangsdaten eingegeben.");

            if (email === "" || pass === "") {
                loginErrBox.fadeIn(200);
                $('#email').attr('disabled', true);
                $('#password').attr('disabled', true);
            } else {
                $.ajax({
                    type: "POST",
                    url: '/controllers/ajax.php',
                    data: 'email=' + email + '&password=' + pass + '&name_func=user_login',
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.ok === 'true') {
                            console.log(data.ok);
                            document.location.href = "/user/?page=wall&type=alle";
                        } else if (data.ok !== 'true') {
                            loginErrBox.fadeIn(200);
                            $('#email').attr('disabled', true);
                            $('#password').attr('disabled', true);
                        }
                    }
                });
            }
            return false;
        }
    });

    $('#close-err-window').click(function() {
        $('#err').fadeOut(200);
        $('#email').removeAttr('disabled');
        $('#password').removeAttr('disabled').val('');
    });

    
    $('.update_mail').click(function(){
        $('.pop_window').slideUp();
        $('.update_mail_window').slideDown();
    });
    
    $('#logout').click(function(){
        $.ajax({
        type: "POST",
        url: "/controllers/ajax.php",
        data: 'name_func=exit_site',
        dataType: "json",
        success: function(data){
            document.location.href = "/user/?page=index";
        }
        });
    });
    
    /*коли вводиш логін і пароль то зникають дані з полів форми реєстрації гостя
    $('.form_login_header input').on('textchange',function(){
        $('.form_reg_index input.index').val('');
    });
    $('.form_reg_index input.index').on('textchange',function(){
        $('.form_login_header').find('input[type=text],[type=password]').val('');
        $('.form_login_header [type=checkbox]').prop('checked',false);
    });
    */
    
    $('.send_pass').click(function(){
        if($('.pop_window.send_password').css('display') !== 'none') {
            $('.block_reg_index_einmalige_bewerburg').css('display','none');
        }
        else if($('.pop_window.send_password').is(':visible'))
            {$('.pop_window.send_password').hide();
            $('.no_activ_body').hide();}
        else
            {$('.pop_window.send_password').show(100);
            $('.no_activ_body').show();}
        return false;
    });



    $('.send_pass').click(function(){
        if($('.pop_window.send_password').css('display') !== 'none') {
            $('.block_reg_index_einmalige_bewerburg').css('display','none');
        }
    });

    $('.no_activ_body').click(function(){
        if($('.pop_window.send_password').css('display') === 'none') {
            $('.block_reg_index_einmalige_bewerburg').css('display','block');
        }
    });


    $('[name=email_by_pass]').keyup(function(event){
        console.log(event);
        if(event.keyCode==13){
          $('#send_password').trigger('click');
          return false;
        }
    })
    
    $('#send_password').click(function(){
        var email=$('[name=email_by_pass]').val();
        if(email)
            $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=send_my_pass&email='+email,
            dataType: "json",
            success: function(data){
                console.log(data);
                $('.error_new_mail').remove();
                if(data)
                    $('.form_edit_p .buttons_block').after('<span class="error_new_mail">Check-mail - You received letter</span>');
                else
                    $('.form_edit_p .buttons_block').after('<span class="error_new_mail">Es gibt keinen Face Club Mitglied mit solcher Email-Adresse</span>');
            }
            });
        return false;
    });
});