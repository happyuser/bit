$(document).ready(function () {
    $('.dropdown-toggle').dropdown();
    $('#example-selectAllValue').multiselect({
        numberDisplayed: 1,
        buttonWidth: '227px',
        maxHeight: 200,
        selectAllText: 'alle!',
        allSelectedText:'alle ausgewählt',
        includeSelectAllOption: true,
        selectAllValue: 'select-all-value',
        nonSelectedText: 'nicht ausgewählt',
        nSelectedText: 'ausgewählt'
    });

    $('#example-multiple-selected,#example-multiple-selected1').multiselect({
        buttonWidth: '228px',
        maxHeight: 200,
        allSelectedText:'alle ausgewählt',
        nonSelectedText: 'nicht ausgewählt',
        nSelectedText: 'ausgewählt'
    });

    $('#news_system').click(function(){
        if($('[name=privat]').is(':checked')) var type=1;
        if($('[name=geschaft]').is(':checked')) var type=2;
        if($('[name=privat]').is(':checked') && $('[name=geschaft]').is(':checked')) var type=3;
            $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=send_news&type='+type,
            dataType: "json",
            success: function(data)
            {
               if(data.ok=='true')$('#news_system_info').removeClass('alert-error').addClass('alert-success').
            text('Änderungen gespeichert').show().delay(3000).fadeOut();
            }
            });
    });
});

/*$('.btn .btn-primary .btn-large').click(function(){
    $('.modal-backdrop').css("z-index","-1");
});*/

    $('#basicModal').click(function(){
        $("#myModal").modal('show')
    })



    /*ClEAR OLD PASSWORD VALUE AND INFO SPAN*/

    $('#old_pass').focusin(function () {
        $(this).val('');
        $('#change_pass_btn').prop("disabled", false);
    });

    /*CLEAR NEW PASSWORD VALUE*/

    $('#new_pass').focusin(function () {

        $(this).val('');

    });

    /*CLEAR RE-NEW PASSWORD VALUE*/

    $('#old_pass').focusin(function () {

        $(this).val('');

    });


    $('#new_email').focusin(function(){

        $(this).val('')

    });


/*CHECKING OLD PASSWORD*/

    $('#old_pass').focusout(function () {
        var old_pass = $("#old_pass").val();
        $.ajax({
            type: 'POST',
            url: '/controllers/ajax.php',
            data: 'password=' + old_pass + '&name_func=check_pass',
            dataType: 'json',
            success: function (response) {
                if (response == "ERROR") {
                    $('#password_info').removeClass('alert-success').addClass('alert-error').
                        text('Aktuelles passwort nicht korrekt eingegeben. Versuchen Sie noch einmal.').show().delay(3000).fadeOut();
                    $('#change_pass_btn').prop("disabled", true);
                }
            }
        });

    });


    /*VALIDATION PASSWORD*/

    $('#change_pass_btn').click(function () {

        var str_new = $("#new_pass").val();
        var str_re_new = $('#re-new_pass').val();

        if (str_new != str_re_new) {
            $("#new_pass").val("");
            $('#re-new_pass').val("");
            $('#password_info').removeClass('alert-success').addClass('alert-error').
               text('Tragen Sie bitte den neunen Passwort noch einmal. Sie stimmen nicht überein.').show().delay(3000).fadeOut();
        }

        else if($("#old_pass").val()==""){
            $("#new_pass").val("");
            $('#re-new_pass').val("");
            $('#password_info').removeClass('alert-success').addClass('alert-error').
                text('Please enter old password').show().delay(3000).fadeOut();
        }


        /*SENDING NEW PASSWORD*/

        else {
            $.ajax({
                type: 'POST',
                url: '/controllers/ajax.php',
                data: '&new_password=' + str_new + '&name_func=change_user_password',
                dataType: 'json',
                success: function (response) {
                    if(response.ok=='true'){
                        $('#password_info').removeClass('alert-error').addClass('alert-success').
                        text('Änderungen gespeichert').show().delay(3000).fadeOut();
                    }else if(response.ok=='false'){
                        $('#password_info').removeClass('alert-success').addClass('alert-error').
                        text('Error').show().delay(3000).fadeOut();
                    }

                }
            });
        }

    });


    /*CHANGE EMAIL*/

    $('#change_email_btn').click(function () {
        var mail = $('#new_email').val();
        $('#email_icon').removeClass();

        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

            if (testEmail.test(mail)) {
                $.ajax({
                    type: 'POST',
                    url: '/controllers/ajax.php',
                    data: '&email='+mail + '&name_func=change_user_login_data',
                    dataType: 'json',
                    success: function (response) {

                    }
                });

                $('#email_info').removeClass('alert-error').addClass('alert-success').
                    text('Änderungen gespeichert').show().delay(3000).fadeOut();
                $('#email_icon').addClass('icon-ok');
            }else{

                $('#email_info').removeClass('alert-success').addClass('alert-error').
                    text('Es gibt keinen Face Club Mitglied mit solcher Email-Adresse.').show().delay(3000).fadeOut();

                $('#email_icon').addClass('icon-remove');
            }


    });

    /*CHANGE NAME AND SURNAME*/

    $('#change_name_btn').click(function () {
        var name = $('#name').val();
        if($('#msg').length){
            var msg = $('#msg').val();
            if(!msg){
                alert('Вкажіть причину!');
                return false;
            }
        }else var msg='';
        $.ajax({
            type: 'POST',
            url:'/controllers/ajax.php',
            data:'first_name='+name+'&name_func=change_user_data&msg='+msg,
            dataType: 'json',
            success: function (data) {
                $('.no_activ_body').show();
                $('.change_einstellungen').slideDown(200);
            }
        });
        

        $('#name_info').removeClass('alert-error').addClass('alert-success').
            text('Änderungen gespeichert').show().delay(3000).fadeOut();
    });


    /*CHOOSE EVENT*/

    $('#choose_category').click(function () {

        var arr = [];

        $('#example-selectAllValue option').each(function(){

            if ($(this).is(':selected')){
                var temp=$(this).val();
                arr.push(temp);
            }
        });

        $.ajax({
            type:'POST',
            url:'/controllers/ajax.php',
            data:'&events='+arr+'&name_func=change_user_events',
            dataType:'json'
        });


        $('#category_info').removeClass('alert-error').addClass('alert-success').
            text('Änderungen gespeichert').show().delay(3000).fadeOut();

    });

    /*SEND TO MAIL AND ADDRRES*/

    $('#send_address').click(function(){

        var newsletter_email_addresses = 0, newsletter_post_addresses = 0;

        if($("#enable_newsletter_by_email").is(':checked')){


            if ($("#example-multiple-selected").children('option:first-child').is(':selected')) {
                newsletter_email_addresses |= 1;
            }
            if($("#example-multiple-selected").children('option:first-child').next().is(':selected')){
                newsletter_email_addresses |= 2;
            }

            $('#example-multiple-selected').each(function() {
                if (this.selected){
                    newsletter_email_addresses |= 3;
                }
                else{
                    //do something
                }

            });

        }

        if($("#enable_newsletter_by_post").is(':checked')){


            if ($("#example-multiple-selected1").children('option:first-child').is(':selected')) {
                newsletter_post_addresses |= 1;
            }

            if($("#example-multiple-selected1").children('option:first-child').next().is(':selected')){
                newsletter_post_addresses |= 2;
            }

            $('#example-multiple-selected1').each(function() {
                if (this.selected){
                    newsletter_post_addresses |= 3;
                }
            });

        }

        $.ajax({
            type:'POST',
            url:'/controllers/ajax.php',
            data:'&newsletter_email_addresses='+newsletter_email_addresses+'&newsletter_post_addresses='
                +newsletter_post_addresses+'&name_func=change_user_login_data',
            dataType:'json',
            success:function(respone){

            }
        });

        $('#newsletter_info').removeClass('alert-error').addClass('alert-success').
            text('Änderungen gespeichert').show().delay(3000).fadeOut();

    });
    $(function(){
        $('button[title="nicht ausgewählt"]').focus(function(){
            $('button[title="nicht ausgewählt"]').next().css('position', 'relative');
        });
        $('button[title="Geschäft-Email"]').click(function(){
            $('#send_address').css('margin-top', '38px');
        });
        $('#send_address').click(function(){
            $('#send_address').css('margin-top', '0');
        });
        //background dropdown
        $('ul.dropdown-menu-einstellungen li a').click(function(){
            $(this).addClass('einstellungen-select-a');
        });
        $('ul.dropdown-menu-einstellungen a.multiselect-all').click(function(){
            $('li.active a').addClass('einstellungen-select-a');
        });

    });