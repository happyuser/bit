$(document).ready(function(){
    function alert_friend(text){
        if(text){
            $('.alert_friend').text(text).show();
            setTimeout(alert_friend,3000);
        }else
            $('.alert_friend').hide(100);
    }
    $('.add_new_friend').hover(function(event) {
        var titleText = $(this).attr('title');
        $(this)
          .data('tipText', titleText)
          .removeAttr('title');
          
        $('<p class="tooltip"></p>')
          .text(titleText)
          .appendTo('body')
          .css('top', (event.pageY - 10) + 'px')
          .css('left', (event.pageX + 20) + 'px')
          .fadeIn('slow').animate({'opacity':1},300);
          }, function() {
            // Hover out code
            if(!$(this).attr('title'))$(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
          }).mousemove(function(event){
            // Mouse move code
            $('.tooltip')
              .css('top', (event.pageY - 10) + 'px')
              .css('left', (event.pageX + 20) + 'px');
    });
    
    $('.kontakt_list').on('click','.add_new_friend',function(){
        var this_=$(this);
        var id=$(this).attr('id');
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=update_friend&id='+id,
            dataType: "json",
            success: function(data)
            {
                if(data.ok==true){
                    if(data.status=='add_initial'){
                        this_.css('background-image','url(images/del.png)').attr('title','Aus der Kontaktenliste entfernen');
                        $('.tooltip').text('Aus der Kontaktenliste entfernen');
                        alert_friend('Dieser mitglieder wird uber ihre Anmeldung benachrichtigt');
                    }else{
                        this_.css('background-image','url("images/add_30x30.png")').attr('title','Zur Kontaktenliste hinzufugen');
                        $('.tooltip').text('Zur Kontaktenliste hinzufugen');
                        alert_friend('Anmeldung zuruckgenommen');
                    }
                }
            }
        });
    });


    if($('#land').val()== 'all_land'){
        $('#city').attr('disabled', 'disabled');
    }
    $('#land').change(function(){
        $('#city').html('<option value="all_city">Alle Stadte</option>');
        if($(this).val()== 'all_land'){
            $('#city').attr('disabled', 'disabled');
            $('#city').find('option[value="all_city"]').attr("selected",true);
        }
        else{
            $('#city').attr('disabled',false );
            $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: 'name_func=view_cities_for_country&id='+$(this).val(),
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(index,value){
                    $('#city [value="all_city"]').after('<option value="'+value+'">'+value+'</option>');
                });
            }
            });
        }
    });
    
    function search_kontakten(page){
        if(!page)page=1;
        page=parseInt(page);
        $('.pagination .page').remove();
        var str=$('.search_f').serialize();
        $.ajax({
            type: "POST",
            url: "/controllers/ajax.php",
            data: str+'&name_func=kontakten_finden&page='+page,
            dataType: "json",
            success: function(data)
            {
                console.log(data);
                $('.kontakt_list').html('');
                var pages=Math.ceil(data.count/10);
                console.log(pages);
                if(page<=pages)
                    $('.prev a').attr('href',page);
                else
                    $('.prev a').attr('href',page-1);
                    
                if(page>=pages)
                    $('.next a').attr('href',page);
                else
                    $('.next a').attr('href',page+1);
                    
                $('.last a').attr('href',pages);
 
                if(page==1)plus=3; else plus=2; 
                for (var i=page-1;i<=pages;i++){
                    if(i>=(page+plus))break;
                    if(!i)continue;
                    var _class='page'
                    if(i==page)_class+=' active';
                    $('li.next').before('<li class="'+_class+'"><a href="'+i+'">'+i+'</a></li>');
                }
                $.each(data.list,function(index,value){
                    var str='<li class="user">'+
                        '<div class="row marg">'+
    
                            '<div class="kontakt_image span4">'+
                                '<img src="../uploads/crop_img/'+value.photo+'" />'+
                            '</div>'+
    
                            '<div class="kontakt_info span6">'+
                                '<p class="kontakt_name"><a style="text-decoration: none; color: #690F19;" href="/user/?page=main_profil&id='+value.id+'">'+value.last_name+' '+value.first_name+'</a></p>'+
                                '<p class="kontakt_function">'+value.functions+'</p>'+
                                '<p class="kontakt_firm">'+value.firma+'</p>'+
                            '</div>'+
                            (data.type_acc=='user' ? '<div class=" span1">'+
                                '<div class="add_new_friend" '+(value.initial ? 'style="background-image:url(images/del.png)"' : '')+' id="'+value.id+'"></div>'+
                                '<div class="kontakt_message"></div>'+
                            '</div>' : '')+
                        '</div>'+
                    '</li>';
                $('.kontakt_list').append(str);
                });
            }
        });
    }
    
    $('.pagination').on('click','.pagination li a',function(){
        var page=$(this).attr('href');
        search_kontakten(page);
        return false;
    });

    $('select,input').on('textchange',function(){
        search_kontakten();
    });
});