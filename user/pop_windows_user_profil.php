<div class="modal_edit_p pop_window change_photo">
    <div class="edit_profile">
        <form class="form_change_member" style="margin: 0;">
            <div class="photo_"></div>
            <div class="form_edit_p user_profil">
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <div class="buttons_block">
                    <button id="abbrechen">Abbrechen</button>
                    <button id="crop_img" after="change_close">SPEICHERN</button>
                </div>
            </div>
        </form>
    </div>
    <div class="delete_photo">Delete</div>
</div>

<div class="modal_edit_p pop_window change_profildaten">
    <div class="edit_profile">
        <div class="edit_profile_title">Profildaten</div>
        <?=if_access_info('profildaten')?>
        <form class="form_change_member" style="margin: 0;">
            <div class="form_edit_p user_profil">
                <label>Funktion am Unternehmen</label>
                <input type="text" name="functions" value="<?=$_USER_INFO[member][functions]?>"/>
                <label>Firmenname</label>
                <input type="text" name="firma" value="<?=$_USER_INFO[member][firma]?>"/>
                <label>Geburtsgatum</label>
                <input type="date" name="birthdate" value="<?=$_USER_INFO[member][birthdate]?>"/>
                <label>Ich suche</label>
                <input type="text" name="i_search_for" value="<?=$_USER_INFO[member][i_search_for]?>"/>
                <label>Ich biete</label>
                <input type="text" name="i_offer_field" value="<?=$_USER_INFO[member][i_offer_field]?>"/>
                <div class="buttons_block">
                    <button id="abbrechen">Abbrechen</button>
                    <button id="speichern" after="change_close">SPEICHERN</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div class="modal_edit_p pop_window change_textarea">
    <div class="edit_profile">
        <div class="edit_profile_title uber-mich-header">Über mich</div>
        <?=if_access_info('textarea')?>
        <form class="form_change_member" style="margin: 0;">
            <div class="form_edit_p user_profil">
                <textarea class="uber_mich_textarea" name="about_me" rows="5" placeholder="<?=$_USER_INFO[member][about_me]?>"><?=$_USER_INFO[member][about_me]?></textarea>
                <div class="buttons_block">
                    <button id="abbrechen">Abbrechen</button>
                    <button id="speichern" after="change_close">SPEICHERN</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div class="modal_edit_p pop_window change_netzprofilen">
 <div class="edit_profile">
  <div class="edit_profile_title">Netzprofilen</div>
  <?=if_access_info('netzprofilen')?>
  <form class="form_change_member" style="margin: 0;">
      <div class="form_edit_p user_profil">
        <label>Facebook-Profil</label>
        <input type="text" name="facebook_url" placeholder="" value="<?=$_USER_INFO[member][facebook_url]?>" />
        <label>Linkedln-Profil</label>
        <input type="text" name="linked_in_url" value="<?=$_USER_INFO[member][linked_in_url]?>"/>
        <label>XING-Profil</label>
        <input type="text" name="xing_url" value="<?=$_USER_INFO[member][xing_url]?>"/>
        <label>Twitter-Profil</label>
        <input type="text" name="twitter_url" value="<?=$_USER_INFO[member][twitter_url]?>"/>
       <div class="buttons_block">
            <button id="abbrechen">Abbrechen</button>
            <button id="speichern" after="change_close">SPEICHERN</button>
       </div>
      </div>
  </form>
 </div>
</div>


<div class="modal_edit_p pop_window change_contacts">

    <ul class="nav nav-pills row-fluid ul_kontakten_top" style="text-align: center;">
        <li class="active"><a href="kontakten_block">KONTAKDATEN</a></li>
        <li><a href="netzprofilen_block">NETZPROFILE</a></li>
    </ul>
        <form class="form_change_member" id="netzprofilen_block" style="margin: 0;">
            <div class="form_edit_p user_profil">
            <?=if_access_info('netzprofilen')?>
                <div class="row-fluid">
                    <div class="span6" style="margin: 0;">
                        <div class="row-fluid blocks" style="margin-top: 20px;">
                            <label>Facebook-Profil</label>
                            <input type="text" name="facebook_url" value="<?=$_USER_INFO[member][facebook_url]?>" />
                            <label>Linkedln-Profil</label>
                            <input type="text" name="linked_in_url" value="<?=$_USER_INFO[member][linked_in_url]?>"/>
                            <label>XING-Profil</label>
                            <input type="text" name="xing_url" value="<?=$_USER_INFO[member][xing_url]?>"/>
                            <label>Twitter-Profil</label>
                            <input type="text" name="twitter_url" value="<?=$_USER_INFO[member][twitter_url]?>"/>
                            <label>Instagram-Profil</label>
                            <input type="text" name="instagram" value="<?=$_USER_INFO[member][instagram]?>"/>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="row-fluid blocks" style="margin-top: 11px;">
                            <label>Youtube-Profil</label>
                            <input type="text" name="youtube" value="<?=$_USER_INFO[member][youtube]?>" />
                            <label>MyVideo-Profil</label>
                            <input type="text" name="myvideo" value="<?=$_USER_INFO[member][myvideo]?>"/>
                            <label>IMDB-Profil</label>
                            <input type="text" name="imdb" value="<?=$_USER_INFO[member][imdb]?>"/>
                            <label>Skype</label>
                            <input type="text" name="skype" value="<?=$_USER_INFO[member][skype]?>"/>
                            <label>Weitere Netzprofile</label>
                            <input type="text" name="other_url" value="<?=$_USER_INFO[member][other_url]?>"/>
                        </div>
                    </div>
                    <div class="buttons_block">
                        <button id="abbrechen">Abbrechen</button>
                        <button id="speichern" after="change_close">SPEICHERN</button>
                    </div>
                </div>
            </div>
        </form>
        
        <form class="form_change_member" id="kontakten_block">
            <div class="form_edit_p user_profil">
                <div class="row-fluid">
                    <div class="span6" style="margin: 0;">
                        <span class="title_window">Privatkontakten</span>
                        <?=if_access_info('privatkontakten')?>
                        <div class="row-fluid blocks" style="margin-top: 20px;">
                            <div class="span6" style="margin: 0;">
                                <label>Stadt</label>
                                <input type="text" name="postaddress" value="<?=$_USER_INFO[member][postaddress]?>"/>
                            </div>
                            <div class="span6">
                                <label>PLZ</label>
                                <input type="text" name="plz_privat" value="<?=$_USER_INFO[member][plz_privat]?>"/>
                            </div>
                            <label class="label-address">Adresse</label>
                            <input type="text" name="adress_privat" value="<?=$_USER_INFO[member][adress_privat]?>"/>
                            <label>Telefonnummer</label>
                            <input type="text" maxlength="17" name="telephone_privat" value="<?=$_USER_INFO[member][telephone_privat]?>"/>
                            <label>Handynummer</label>
                            <input type="text" maxlength="17" name="phone_privat" value="<?=$_USER_INFO[member][phone_privat]?>"/>
                            <label>Faxnummer</label>
                            <input type="text" maxlength="17" name="fax_privat" value="<?=$_USER_INFO[member][fax_privat]?>"/>
                            <label>E-Mailadresse</label>
                            <input type="text" maxlength="17" name="email_privat" value="<?=$_USER_INFO[member][email_privat]?>"/>
                        </div>
                    </div>
                    <div class="span6">
                        <span class="title_window">Geschäftskontakten</span>
                        
                        <?=if_access_info('geschaftskontakten')?>
                        
                        <div class="row-fluid blocks" style="margin-top: 20px;">
                            <div class="span6" style="margin: 0;">
                                <label>Stadt</label>
                                <input type="text" name="postadress_bus" value="<?=$_USER_INFO[member][postadress_bus]?>"/>
                            </div>
                            <div class="span6">
                                <label>PLZ</label>
                                <input type="text" name="plz_bus" value="<?=$_USER_INFO[member][plz_bus]?>"/>
                            </div>
                            <label class="label-address">Adresse</label>
                            <input type="text" name="adress_bus" value="<?=$_USER_INFO[member][adress_bus]?>"/>
                            <label>Telefonnummer</label>
                            <input type="text" maxlength="17" name="telephone_bus" value="<?=$_USER_INFO[member][telephone_bus]?>"/>
                            <label>Handynummer</label>
                            <input type="text" maxlength="17" name="phone_bus" value="<?=$_USER_INFO[member][phone_bus]?>"/>
                            <label>Faxnummer</label>
                            <input type="text" maxlength="17" name="fax_bus" value="<?=$_USER_INFO[member][fax_bus]?>"/>
                            <label>E-Mailadresse</label>
                            <input type="text" maxlength="17" name="email_bus" value="<?=$_USER_INFO[member][email_bus]?>"/>
                            <label>Internetadresse</label>
                            <input type="text" name="personal_homepage_url" value="<?=$_USER_INFO[member][personal_homepage_url]?>"/>
                        </div>
                    </div>
                </div>
                <div class="buttons_block">
                    <button id="abbrechen">Abbrechen</button>
                    <button id="speichern" after="change_close">SPEICHERN</button>
                </div>
            </div>
        </form>
</div>



<div class="modal_edit_p pop_window change_berufsvita">
<?=if_access_info('berufsvita')?>            
    <form class="form_edit_p user_profil form_change_member" id="">
        <div class="row-fluid">
            <span class="title_window">Berufsvita</span>
            <div class="row-fluid list_jobs blocks">
            <span class="start_jobs"></span>
                <?
                if($_USER_INFO[users_career])
                foreach($_USER_INFO[users_career] as $key=>$val){
                    $start=explode('-',$val[time_start]);
                    if($val[time_end]!='now' && $val[time_end]!='')$end=explode('-',$val[time_end]); else {$end[0]=date('Y',time()); $end[1]=date('m',time()); $val[time_end]='bis jetzt';}
                    $years=$end[0]-$start[0];
                    $months=$end[1]-$start[1];
                    if($months<0){$years-=1; $months=12+$months;}            
                ?>
                <div class="job" id="<?=$val[id]?>">
                    <div class="span3 header_block">
                        <div class="date_job">
                            <span class="start_date" date="<?=$start[0].'-'.$start[1]?>"><?=print_date($val[time_start])?></span> - <span class="finish_date" date="<?if($val[time_end]!='bis jetzt')echo $end[0].'-'.$end[1]; else echo $val[time_end];?>"><?if($val[time_end]!='bis jetzt') echo print_date($val[time_end]); else echo$val[time_end];?></span>
                        </div>
                        <div class="left_time_job">
                            <?if($years){ echo $years;?> Jahre <?}if($months){ echo $months;?> Monate<?}?>
                        </div>
                    </div>
                    <div class="span4" style="color: #000;">
                        <div class="list_firmenname"><?=$val[firmenname]?></div>
                        <div class="list_position"><?=$val[position]?></div>
                    </div>
                    <div class="span3">
                        <div class="list_aufgaben"><?=$val[aufgaben]?></div>
                    </div>
                    <div class="span2 berufsvita_edit_job_span2">
                        <div class="edit_job berufsvita_edit_job">
                            <i class="icon-pencil icon-white"></i>
                        </div>
                        <div class="drop_job">
                            X
                        </div>
                    </div>
                </div>
                <?}?>
            </div>
            <div class="row-fluid">
                <div class="row-fluid">
                    <div class="span6">
                        <label>Seit</label>
                        <select class="mounth_start" style="float: left;width: 48%;">
                            <option value="01">Januar</option>
                            <option value="02">Februar</option>
                            <option value="03">Marz</option>
                            <option value="04">April</option>
                            <option value="05">Mai</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Dezember</option>
                        </select>
                        <select class="year_start" style="float: left;width: 48%;margin: 0 0 0 10px;">
                            <?for($i=date('Y',time());$i>1970;$i--){?>
                                <option value="<?=$i?>"><?=$i?></option>
                            <?}?>
                        </select>
                        <input type="hidden" name="where-time_start" value=""/>
                    </div>
                    <div class="span6">
                        <label class="bis_label">Bis</label>
                        <select class="mounth_end">
                            <option value="now">bis jetzt</option>
                            <option value="01">Januar</option>
                            <option value="02">Februar</option>
                            <option value="03">Marz</option>
                            <option value="04">April</option>
                            <option value="05">Mai</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Dezember</option>
                        </select>
                        <select class="year_end" style="display: none;">
                            <?for($i=date('Y',time());$i>1970;$i--){?>
                                <option value="<?=$i?>"><?=$i?></option>
                            <?}?>
                        </select>
                        <input type="hidden" name="where-time_end" value="now"/>
                    </div>
                    <!--
                        <input type="checkbox" name="where-time_end" value="now" style="width: initial; padding-left: 20px;"/> bis jetzt
                    -->
                </div>
                <div class="span6 unternehmen_box_input" style="margin: 0 8px 0 0;">
                    <label>Unternehmen</label>
                    <input type="text" name="where-firmenname" value="" style="width: 100%;"/>
                </div>
                <div class="span6 position_unternehmen_box_input" style="margin: 0 8px 0 0;">
                    <label>Position am Unternehmen</label>
                    <input type="text" name="where-position" value="" style="width: 100%;"/>
                </div>
                <div class="span12 geleistete_box_input" style="margin: 0 8px 0 0;">
                    <label>Geleistete Tätigkeiten</label>
                    <input type="text" name="where-aufgaben" value="" style="width: 100%;"/>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="buttons_block">
                <button id="abbrechen" class="abbrechen_berufsvita">Abbrechen</button>
                <button class="change_user_career_btn" style="display: none;">Speichern</button>
                <button class="save_user_career_btn">Hinzufügen</button>
            </div>
            <span class="label label-success status_message_ok">Save <i class="icon-ok icon-white"></i></span>
        </div>
    </form>
</div>