<?php
include ('../controllers/functions.php');
connect();

session_start();
$CONTENT=get_get('page');
$ID_USER=get_cookie('id_user');
$ID_SPONSOR=get_cookie('id_sponsor');
$ID_GET=get_get('id');
if($ID_SPONSOR)define('TYPE_ACCOUNT','sponsor'); else define('TYPE_ACCOUNT','user');
if(TYPE_ACCOUNT=='sponsor')$id_cookie=$ID_SPONSOR; elseif(TYPE_ACCOUNT=='user')$id_cookie=$ID_USER;
if(!$ID_GET){
    $ID_GET=$id_cookie;
    define('MY_PAGE',true);
}

if(($CONTENT=='main_sponsor' || $CONTENT=='main_profil') && $id_cookie===$ID_GET)
    define('MY_PAGE',true); else define('MY_PAGE',false);
?>

<html lang="de">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--        <meta name="viewport" content="width=device-width, initial-scale=1.0" />-->
        <title>BIT user</title>
        <link href="../css/jquery-ui.css" rel="stylesheet" />
        <link href="css/bootstrap-multiselect.css" rel="stylesheet" />
        <link href="css/glyphicons-extended.min.css" rel="stylesheet" />
        <script src="../js/jquery-2.0.0.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/wall.css" rel="stylesheet" />
        <script src="../js/tabs.js" type="text/javascript"></script>
        <script src="js/jquery.textchange.min.js" type="text/javascript"></script>
        <script src="js/einstellungen.js" type="text/javascript"></script>
        <script src="js/bootstrap-dropdown.js" type="text/javascript"></script>
        <script src="js/bootstrap-select.js" type="text/javascript"></script>
        <script src="js/jquery.twbsPagination.min.js" type="text/javascript"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/jquery.dotdotdot.min.js" type="text/javascript"></script>

        <!-- STYLES FOR IE -->
        <!--[if lte IE 8]>
        <link href="css/ie8.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            function GoNah(){
                location="/user/ie8browser.php";
            }
            setTimeout( 'GoNah()', 3000 );
        </script>
        <![endif]-->
        <!-- STYLES FOR IE -->
        <!--[if lte IE 9]>
        <link href="css/ie9.css" rel="stylesheet" type="text/css" />
        <![endif]-->
<?
if(!get_cookie('timezone')){?>
    <script src="js/time_zone.js" type="text/javascript"></script>
<?}?>
    </head>
    <!--
<script type="text/javascript">
    function parseGetParams() {
       var $_GET = {}; 
       var __GET = window.location.search.substring(1).split("&");
        
       for(var i=0; i<__GET.length; i++) { 
          var getVar = __GET[i].split("=");
           //console.log(getVar);
          if(getVar[0]!=='' && getVar[1]!=='')
            $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1]; 
       } 
       //console.log($_GET);
       return $_GET;
    }

    function check_get(){
        var GET=parseGetParams();
        var str='';
        var arr={};
        $.each(GET, function(index, value) {
            if(index!='mail' && index!='user_password' && index!='submit')
            arr[index]=value;
        });
        //console.log(arr);
        if(arr)
        $.each(arr, function(index, value) {
            str=str+index+'='+value+'&';
        });
        str.replace(/%20/g,"") // = "������ ��� ������"
        //console.log(str);
        window.history.pushState(null, null, window.location.pathname+'?'+str);
    }
    check_get();
</script>
-->