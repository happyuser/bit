<?
if(TYPE_ACCOUNT=='sponsor' && !get_get('id')){include_once('main_sponsor.php');exit;}//можна редірект зробити замість цього, щоб урли не путати, та то буде замітна перезагрузка (хіба сюда ж ще добавити зміну УРЛ JS)

if(!$ID_GET)$ID_GET=$ID_USER;
$_USER_INFO[member]=select_DB('member', array('id'=>$ID_GET));
$_USER_INFO[access_users_info]=select_DB('access_users_info', array('id_user'=>$ID_GET));
$_USER_INFO[users_career]=select_DB('users_career', array('id_user'=>$ID_GET), array('time_start'=>'DESC'));
foreach($_USER_INFO[member][0] as $key=>$value){
    $_USER_INFO[member][$key]=$value;
}
if(!file_exists('../uploads/'.$_USER_INFO[member][photo])){
    if(MY_PAGE)update_DB_and('member',array('photo'=>'useremptylogo.png'),array('id'=>$ID_USER));
    $_USER_INFO[member][photo]='useremptylogo.png';
}

foreach($_USER_INFO[access_users_info][0] as $key=>$value){
    $_USER_INFO[access_users_info][$key]=$value;
}
 
if(MY_PAGE)include('pop_windows_user_profil.php'); else include_once('pop_windows_other_profil.php');
if($_USER_INFO[member][photo]=='useremptylogo.png') include_once('pop_windows_other_profil.php');
?>
<script src="js/user.js" type="text/javascript"></script>
<script src="js/jquery.Jcrop.min.js" type="text/javascript"></script>
<link href="css/jquery.Jcrop.min.css" rel="stylesheet" />

<div class="t_profile">
	<div class="row-fluid">
		<div class="span3 t_profile_foto img_profil <?if(MY_PAGE && $_USER_INFO[member][photo]!='useremptylogo.png')echo"my_img"; else echo"other_u_img";?>">
			<img src="../uploads/<?=$_USER_INFO[member][photo]?>" alt="User foto"/>
			<?if(MY_PAGE){?>
                <input type="hidden" name="photo" value="" />
                <input type="hidden" name="id" value="<?=$_USER_INFO[member][id]?>" />
                <input type="file" id="imgFile" />  
                <div class="new_photo_upload block_after_photo">Profilbild ändern</div>
            <?}elseif(!MY_PAGE && $id_cookie){?>
                <div class="add_friend block_after_photo" id_user="<?=$_USER_INFO[member][id]?>"><?if(check_friend(array('id'=>$_USER_INFO[member][id])))echo"Aus Kontaktliste entfernen"; else echo"Kontakt aufnehmen";?></div>
            <?}?>
		</div>
        <div class="span9">
        	<div class="t_profile_info">
        		<span class="t_heading">
                    <?
                    echo $_USER_INFO[member][last_name].' '.$_USER_INFO[member][first_name];
                    echo access_edit_profil('profildaten');
                    ?>
                </span>
                <div class="profile_info_block">
                <?
                if(access_info_block('profildaten')){
                    $profildaten_block=empty_block('profildaten',$_USER_INFO);
                if(!$profildaten_block[profildaten])
                    if(MY_PAGE)
                        echo"<p style='color: #b2b2b2;'>Geben Sie hier allgemeine Informationen über Sie, Ihre aktuelle Tätigkeit, fachliche Fähigkeiten und Kompetemzen an.</p>";
                    else
                        echo"Das Mitglied har noch keine Informationen eingegeben";
                else{
                ?>
                <p class="functions"><?=$_USER_INFO[member][functions]?></p>
        		<p><b class="firma"><?=$_USER_INFO[member][firma]?></b>
        		</p>
        		<p>&nbsp;</p>
        		<?if($_USER_INFO[member][birthdate]){?>
                    <p class="birthdate"><?echo print_date($_USER_INFO[member][birthdate],'strtotime');?></p>
                <?}?>
        		<p>&nbsp;</p>
        		<?if($_USER_INFO[member][i_search_for]){?>
                    <p><b>Ich suche: </b><span class="i_search_for"><?=$_USER_INFO[member][i_search_for]?></span></p>
                <?}
                if($_USER_INFO[member][i_offer_field]){?>
        		<p><b>Ich biete: </b><span class="i_offer_field"><?=$_USER_INFO[member][i_offer_field]?></span></p>
                <?}
                }
                }?>
                </div>
            </div>
        </div>
    </div>
    
<?if(access_info_block('textarea')){
    $view=true;
    if(empty_block('textarea',$_USER_INFO))
        if(MY_PAGE)
            $view=true;else$view=false;
        if($view){
?>
    <div class="row-fluid">
		<div class="span12 ubermich">
			<span class="t_heading">
				Über mich
                <?echo access_edit_profil('textarea');?>
			</span>
            <div class="ubermich_block">
                <p class="about_me">
                    <?if(empty_block('textarea',$_USER_INFO))
                        echo"<p style='color: #b2b2b2'>Erzählen Sie in ein Paar Sätzen, wer Sie sind.</p>";
                    else
                        echo$_USER_INFO[member][about_me]?>
                </p>
            </div>
		</div>
	</div>
    <?}
}

    $view=true;
    if(empty_block('contacts',$_USER_INFO))
        if(MY_PAGE)
            $view=true;else$view=false;
        if($view)
            if(access_info_block('privatkontakten') || access_info_block('geschaftskontakten') || access_info_block('netzprofilen'))
                $view=true;
    		else
                $view=false;
        if($view){
?>
<div class="row-fluid">
    <div class="span12 kontaktdaten">
    	<span class="t_heading">
    		Kontaktdaten
            <?echo access_edit_profil('contacts');?>
    	</span>
        <?if(empty_block('contacts',$_USER_INFO))          
            echo"<p style='color: #b2b2b2'>Geben Sie hier an, wie Sie privat, geschafts order online zu erreichen Sind.</p>";
        else{
        ?>
    	<ul>
    		<?if(access_info_block('privatkontakten')){?>
            <li>
    			<b>Privat</b>
    		</li>
            <?}if(access_info_block('geschaftskontakten')){?>
            <li>
    			<b>Geschäfts</b>
    		</li>
            <?}if(access_info_block('netzprofilen')){?>
    		<li>
    			<b>Netzprofile</b>
    		</li>
            <?}?>
    	</ul>
    	<hr />
    	<ul>
        <?if(access_info_block('privatkontakten')){?>
    		<li>
                <?if($_USER_INFO[member][adress_privat]){?><p class="adress_privat"><?=$_USER_INFO[member][adress_privat]?></p><?}?>
    			<?if($_USER_INFO[member][postaddress]){?><p class="postaddress"><?=$_USER_INFO[member][postaddress]?></p><?}?>
    			<?if($_USER_INFO[member][plz_privat]){?><p class="plz_privat"><?=$_USER_INFO[member][plz_privat]?></p><?}?>
    			<?if($_USER_INFO[member][telephone_privat]){?><p>tel. <span class="telephone_privat"><?=$_USER_INFO[member][telephone_privat]?></span></p><?}?>
    			<?if($_USER_INFO[member][fax_privat]){?><p>fax. <span class="fax_privat"><?=$_USER_INFO[member][fax_privat]?></span></p><?}?>
    			<?if($_USER_INFO[member][phone_privat]){?><p>mob. <span class="phone_privat"><?=$_USER_INFO[member][phone_privat]?></span></p><?}?>
    			<?if($_USER_INFO[member][email_privat]){?><a href="mailto:<?=$_USER_INFO[member][email_privat]?>" class="email_privat"><?=$_USER_INFO[member][email_privat]?></a><?}?>
    		</li>
        <?}if(access_info_block('geschaftskontakten')){?>
    		<li>
                <?if($_USER_INFO[member][adress_bus]){?><p class="adress_bus"><?=$_USER_INFO[member][adress_bus]?></p><?}?>
    			<?if($_USER_INFO[member][postadress_bus]){?><p class="postadress_bus"><?=$_USER_INFO[member][postadress_bus]?></p><?}?>
    			<?if($_USER_INFO[member][plz_bus]){?><p class="plz_bus"><?=$_USER_INFO[member][plz_bus]?></p><?}?>
    			<?if($_USER_INFO[member][telephone_bus]){?><p>tel. <span class="telephone_bus"><?=$_USER_INFO[member][telephone_bus]?></span></p><?}?>
    			<?if($_USER_INFO[member][fax_bus]){?><p>fax. <span class="fax_bus"><?=$_USER_INFO[member][fax_bus]?></span></p><?}?>
    			<?if($_USER_INFO[member][phone_bus]){?><p>mob. <span class="phone_bus"><?=$_USER_INFO[member][phone_bus]?></span></p><?}?>
                <?if($_USER_INFO[member][email_bus]){?><p><a href="mailto:<?=$_USER_INFO[member][email_bus]?>" class="email_bus"><?=$_USER_INFO[member][email_bus]?></a></p><?}?>
                <?if($_USER_INFO[member][personal_homepage_url]){?><p><a href="<?=$_USER_INFO[member][personal_homepage_url]?>" target="_blank" class="personal_homepage_url"><?=$_USER_INFO[member][personal_homepage_url]?></a></p><?}?>


    		</li>
        <?}if(access_info_block('netzprofilen')){?>
    		<li>
                <?if($_USER_INFO[member][facebook_url]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][facebook_url], 'http')===false){?>http://<? } else{?><?} echo$_USER_INFO[member][facebook_url]?>" target="_blank">Facebook</a>
    			</p>
                <?}if($_USER_INFO[member][xing_url]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][xing_url], 'http')===false){?>http://<?} else{?><?} echo$_USER_INFO[member][xing_url]?>" target="_blank">XING</a>
    			</p>
                <?}if($_USER_INFO[member][linked_in_url]){?>
    			<p>
                    <a href="<?if(strripos($_USER_INFO[member][linked_in_url], 'http')===false){?>http://<?} else{?><?} echo$_USER_INFO[member][linked_in_url]?>" target="_blank">Linkedln</a>
    			</p>
                <?}if($_USER_INFO[member][twitter_url]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][twitter_url], 'http' )===false){?>http://<?} else{?><?} echo$_USER_INFO[member][twitter_url]?>" target="_blank">Twitter</a>
    			</p>
                <?}if($_USER_INFO[member][instagram]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][instagram], 'http' )===false){?>http://<?} else{?><?} echo$_USER_INFO[member][instagram]?>" target="_blank">Instagram</a>
    			</p>
                <?}if($_USER_INFO[member][youtube]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][youtube], 'http')===false){?>http://<?} else{?><?} echo$_USER_INFO[member][youtube]?>" target="_blank">Youtube</a>
    			</p>
                <?}if($_USER_INFO[member][myvideo]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][myvideo], 'http')===false){?>http://<?} else{?><?} echo$_USER_INFO[member][myvideo]?>" target="_blank">MyVideo</a>
    			</p>
                <?}if($_USER_INFO[member][imdb]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][imdb], 'http')===false){?><?} else{?><?} echo$_USER_INFO[member][imdb]?>" target="_blank">IMDB</a>
    			</p>
                <?}if($_USER_INFO[member][skype]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][skype], 'http')===false){?><?}else{?><?} echo$_USER_INFO[member][skype]?>" target="_blank">Skype</a>
    			</p>
                <?}if($_USER_INFO[member][other_link]){?>
    			<p>
    				<a href="<?if(strripos($_USER_INFO[member][other_link], 'http')===false){?><?} else{?><?} echo$_USER_INFO[member][other_link]?>" target="_blank">Weitere Netzprofile</a>
    			</p>
                <?}?>
    		</li>
      <?}?>
    	</ul>
        <?}?>
    </div>
</div>
<?}

if(access_info_block('berufsvita')){
    $view=true;
    if(empty_block('berufsvita',$_USER_INFO))
        if(MY_PAGE)
            $view=true;else$view=false;
        if($view){
?>
<div class="row-fluid">
	<div class="span12 berufsvita">
		<span class="t_heading">
			Berufsvita
			<?echo access_edit_profil('berufsvita');?>
		</span>
        <?if(empty_block('berufsvita',$_USER_INFO))
            echo"<p style='color: #b2b2b2'>Geben Sie hier ihre Berufserfahrungen an, wo Sie vother gearbeitet haben</p>";
        else{
            ?>
            <span class="job_start"></span>
            <?
            $i=1;
            $JOBS=array();
            if($_USER_INFO[users_career])
            foreach($_USER_INFO[users_career] as $key=>$val){
                if($val[time_end]=='now')array_unshift($JOBS,$val);else array_push($JOBS,$val);
            }
            
            foreach($JOBS as $key=>$val){
            $start=explode('-',$val[time_start]);
            if($val[time_end]!='now' && $val[time_end]!=''){$end=explode('-',$val[time_end]); $val[time_end]=$end[0].'-'.$end[1];} else {$end[0]=date('Y',time()); $end[1]=date('M',time()); $val[time_end]='bis jetzt';}
            $years=$end[0]-$start[0];
            $months=$end[1]-$start[1];
            if($months<0){$years-=1; $months=12+$months;}
            ?>
            <ul class="job" id="<?=$val[id]?>">
    			<li>
    				<i class="work<?=$i?> list_time_job">&nbsp;</i><span class="start_date"><?=print_date($val[time_start])?></span> - <span class="finish_date"><?if($val[time_end]!='bis jetzt')echo print_date($val[time_end]); else echo $val[time_end];?></span>
    			</li>
    			<li>
    				<p>
    					<span class="list_firmenname" style="display: block;"><?=$val[firmenname]?></span>
    				</p>
    				<p class="list_position"><?=$val[position]?></p>
    			</li>
    			<li class="list_aufgaben">
    				<?=$val[aufgaben]?>
    			</li>
    		</ul>
            <?
            $i++;
            }
        }?>
    </div>
</div>
        <?}
    }?>
</div>