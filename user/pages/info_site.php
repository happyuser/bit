<div class="container">
    <div class="row-fluid">
<?if(get_get('part')=='uber_fc'){?>
        <div class="span<?
        if(!$ID_USER and !$ID_SPONSOR){?>12<?}
        else{?>9<?}
        ?> t_user_body home_public" id="uber_fc">
            <div class="span12 t_main_content t_main_content_uber_before_login">
            <?if(!$ID_USER){?>
                <a href="/user/?page=main_profil">
                    <p id="prev-page" class="prev_page_uber_before_login">Zurück</p>
                </a>
            <?}?>
                <div class="footer_content">
                    <h4 class="footer_h4_color">DIE WERTE DES FACECLUBES</h4>

                    <p>
                        Der Face Club ist eine exklusive Gemeinschaft,
                        die dem Austausch und Networking ebenso dient,
                        wie der umkomplizieren Anmeldung zu exklusiven
                        Veranstaltungen. Wir befürworten ausdrücklich,
                        dass sich unsere Mitglieder vernetzen, auch
                        außerhalb des Face Club. Dennoch legen wir dabei
                        auf bestimmte Dinge Wert:
                    </p>
                    
                    <h4 class="footer_h4_color">WIR WAHREN UMGANGSFORMEN</h4>

                    <p>
                        Der Face Club legt Wert auf einen höflichen, freundlichen
                        und respektvollen Umgang. Wir siezen daher unsere Mitglieder.
                        Diese Umgangsformen erwarten wir von jedem Mitglied des Face
                        Club. Einzige Ausnahme: die Mitglieder kennen sich bereits und
                        sind außerhalb des Face Club per Du.
                    </p>

                    <p>
                        Wenn wir untereinander Kontakt aufnehmen, achten wir darauf, unseren
                        Gegenüber nicht zu belästigen. Eine Kontaktaufnahme ist daher nur mit
                        einem kurzen Anschreiben möglich, warum man sich vernetzen möchte.
                        Fragen Sie sich stets, ob Ihr Konversationspartner einen Nutzen davon
                        hat, mit Ihnen Bekanntschaft zu machen. Sollte das nicht der Fall sein,
                        sehen Sie von einer Kontaktaufnahme ab. Denken Sie stets daran, wie Sie
                        sich im umgekehrten Fall fühlen würden.
                    </p>

                    <p>
                        Auch beim Kommentieren und Bewerten bleiben wir stets freundlich und sachlich,
                        selbst wenn wir Kritik äußern. Konstruktive Kritik ist jederzeit willkommen!
                        Fehlerhafte Inhalte oder Darstellungen sowie sonstige Bugs können gerne an
                        <a href="mailto:info@faceclub.de" target="_blank">info@faceclub.de</a> gemeldet werden.
                        gemeldet werden.
                    </p>

                    <p>
                        Die Administratoren behalten sich jedoch vor,
                        jede Form von Schmähkritik, Verleumdung oder
                        anderen unangemessenen Inhalten aus den Kommentaren zu entfernen.
                    </p>

                    <h4 class="footer_h4_color">WIR BLEIBEN SACHLICH</h4>

                    <p>
                        Jedes Mitglied, das durch die Äußerung von werbenden,
                        propagandistischen (z.B. politisch, religiös), beleidigenden,
                        belästigenden, gewalttätigen, sexistischen, pornografischen,
                        rassistischen, recht- oder linksextremen oder sonstigen moralisch
                        verwerflichen oder anstößigen Inhalten auffällt, wird unverzüglich
                        und unwiderruflich aus dem Face Club ausgeschlossen.
                    </p>

                    <p>
                        Wir bitten unsere Mitglieder, solche Störenfriede umgehend zu melden.
                        Schreiben Sie zu diesem Zweck eine E-Mail an
                        <a href="mailto:info@faceclub.de" target="_blank">info@faceclub.de</a>.

                    </p>

                    <h4 class="footer_h4_color">WIR BLEIBEN EHRLICH</h4>

                    <p>
                        Nicht jede Person erhält Zutritt zum Face Club. Jedem,
                        der dieses Privileg genießt, garantieren wir, dass seine
                        Daten im Face Club sicher sind und nicht an Dritte weitergegeben werden.
                        Unsere Mitglieder sind daher aufgefordert, ausschließlich wahre Angaben
                        zu ihrer Person und ihren geschäftlichen wie privaten Daten zu machen.
                        Niemand darf für eine andere Person ein Profil aktivieren oder pflegen.
                        Bei Zuwiderhandlung wird das Mitglied unverzüglich und unwiderruflich aus dem Face Club ausgeschlossen.
                    </p>

                    <p>
                        Sollten Sie aus irgendeinem Grund kein Mitglied des Face Club
                        mehr sein wollen, können Sie sich selbstverständlich jederzeit
                        abmelden. Eine E-Mail an info@faceclub.de genügt. Ihr Profil
                        wird im Anschluss innerhalb von 28 Tagen gelöscht.
                    </p>
                </div>
            </div>
        </div>
<?}else{?>
        <div class="span<?
        if(!$ID_USER and !$ID_SPONSOR){?>12<?}
        else{?>9<?}
        ?> t_footer_container impressum_user impressum_before_login" id="impressum">
        <?if(!$ID_USER){?>
            <a href="/user/?page=main_profil">
                <p id="prev-page" class="prev_page_uber_before_login">Zurück</p>
            </a>
        <?}?>
            <div class="footer_content span10">
                <h4 class="footer_h4_color">FaceClub</h4>
                <h4 class="footer_h4_color">Sören Bauer Events GmbH</h4>

                <span>Sören Bauer Events GmbH</span>

                <span>Postfach 13 01 69</span>

                <span>20101 Hamburg</span>

                <span>Telefon: 040 / 533 08 96-0</span>

                <span>Telefax: 040 / 533 08 96-23</span>

                <span>E-Mail: <a href="mailto:info@faceclub.de" target="_blank">info@soerenbauer.de</a></span>

                <h4 class="footer_h4_color">Verantwortlich für die Inhalte dieser Seite (Chefredaktion):</h4>

                <span>Sabrina Appelshäuser</span>

                <h4 class="footer_h4_color">Vertretungsberechtigter Geschäftsführer:</h4>

                <span>Sören Bauer</span>

                <span>Registergericht: Hamburg</span>

                <span>Registernummer: HR 83724</span>

                <span>Inhaltlich Verantwortlicher gemäß § 10 Absatz 3 MDStV:</span>

                <span>Sören Bauer (Anschrift wie oben)</span>

                <span>Haftungshinweis:</span>

                <p>Trotz  sorgfältiger  inhaltlicher Kontrolle  übernehmen  wir keine  Haftung für die  Inhalte  externer Links.
                    Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.
                </p>

                <h4 class="footer_h4_color"><a href="../user/uploads/AGB.pdf" target="_blank">AGB</a>
                </h4>

            </div>
        </div>
<?}?>
    </div>
</div>
<script>
    $(document).ready(function(){
        var elemImpressum = $("div").is(".impressum_before_login");
        var wHeight = $(document).height();
        if(elemImpressum){
            $('.t_body').css('height',(wHeight - 119) + 'px');
        }else{
            $('.t_body').css('height',(wHeight - 119) + 'px');
        }
    });
</script>