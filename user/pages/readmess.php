<div class="row-fluid kontakten_background">
    <div class="row marg">
        <ul class="nav nav-pills ul_kontakten_top">
            <li>
                <a class="marg" href="/user/?page=nachrichten">Nachrichten</a>
            </li>
            <li>
                <a class="marg" href="/user/?page=nachrichtschreiben">Nachricht Schreiben</a>
            </li>
        </ul>
    </div>
    <?php
    
    /**
     * Достаем сообщение. Помимо номера сообщения ориентируемся и на id пользователя
     * Это исключит возможность чтения чужого сообщения, методом подбора id сообщения
     */
    $query="select * from messages WHERE (u_to = $id_cookie OR u_from = $id_cookie) AND id = ".get_get('id_mess');
    $result = mysql_query($query);
    $res = mysql_fetch_assoc($result);
    /**
     * Установим флаг о прочтении сообщения
     */
    if($res['u_to']==$id_cookie)
        update_DB_and('messages',array('flag'=>1),array('u_to'=>$id_cookie,'id'=>get_get('id_mess')));
    
    $www=select_DB('member',array('id'=>$res['u_from']),null,false,null,array('first_name','last_name','photo'));
    $www=$www[0];
    /**
     * Выводим сообщение с датой отправки
     */
    if($res['id']!=''){?>
    <div class="row marg" style="padding: 10px;">
        <div class="span1">
            <img src="/uploads/<?=$www[photo]?>" />
        </div>
        <div class="span11">
            <div class="row-fluid">
                <div class="span8">
                    <?=$www[last_name].' '.$www[first_name]?>
                </div>
                <div class="span4">
                    <?=print_date($res["data"],'hour')?>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span11">
                    <?=$res["message"]?>
                </div>
            </div>
            <?if($files=json_decode($res["attached_file"],true)){?>
                <div class="row-fluid">
                <?foreach($files as $value){
                    $type=explode('.',$value);
                    if($type[1]=='mpeg' || $type[1]=='mp4'){?>
                        <video src='/uploads/<?=$value?>' controls="controls"></video>
                    <?}else{?>
                        <img src='/uploads/<?=$value?>'/>
                    <?}
                }?>
                </div>
            <?}?>
        </div>
        <div class="span11" id="send_mess">
            <p>Nachricht</p>
    
            <textarea name="message" placeholder="Schreiben Sie Ihren Nachricht..." class="span12 textfield"></textarea><br />
    
            <a id="chose_file" class="btn btn_style" href="#"><p>Anhängen</p></a>
            <span id="chose_file_text"></span>
            <input type="hidden" name="to" value="<?=$res['u_from']?>"/>
            <input id="chose_file_input" name="files" type="file" value="Anhängen" multiple="true" />
            <input type="submit"  value="Senden" class="btn btn_style pull-right send_msg" />
            <div class="row-fluid span10 files"></div>
        </div>
    </div>
    <?}else{
        echo 'Данного сообщения не существует или оно предназначено не вам.';
    }
    ?>
</div>
<script src="js/nachrichtschreiben.js"></script>