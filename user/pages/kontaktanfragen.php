<?
$INFO=select_DB('friendes', array('id'=>$ID_USER));
$INFO=$INFO[0];
$LIST=json_decode($INFO[incoming],true);
?>
<script src="js/kontaktanfragen.js" type="text/javascript"></script>
<div class="row-fluid kontakten_background">
    <div class="row marg">
        <ul class="nav nav-pills ul_kontakten_top">
            <li>
                <a class="marg" href="/user/?page=kontakten">Kontakten</a>
            </li>
            <li>
            <?
            $select=select_DB('friendes',array('id'=>$ID_USER));
            $select=json_decode($select[0][incoming],true);?>
                <a class="<?if($select){?>color_bordo <?}?>marg" href="/user/?page=kontaktanfragen">Kontaktanfragen</a>
            </li>

        </ul>
    </div>

    <div class="row marg kontakten_content">

       <ul class="new_kontakt_list marg span12">
           <li>
           <?
                foreach($LIST as $value){
                $user=select_DB('member',array('id'=>$value));
                $user=$user[0];
                if(!file_exists('../uploads/crop_img/'.$user[photo])){
                    $user[photo]='useremptylogo.png';
                }
           ?>
               <div class="row marg kontaktanfragen">
                   <div class="span6">
                       <div class="row marg">

                           <div class="kontakt_image span5">
                               <img src="../uploads/<?=$user[photo]?>" />
                           </div>
    
                           <div class="kontakt_info span6">
                             <p class="kontakt_name"><a href="/user/?page=main_profil&id=<?=$user[id]?>"><?=$user[first_name].' '.$user[last_name]?></a></p>
                             <p class="kontakt_function"><?=$user[functions]?></p>
                             <p class="kontakt_firm"><?=$user[firma]?></p>
                           </div>
                       </div>
                   </div>

                   <div class="span6">
                       <div class="row marg">
                           <div class="accept_request">
                               <p class="accept" id="<?=$user[id]?>">Kontaktanfrage bestätigen</p>
                               <p class="reject" id="<?=$user[id]?>">Kontatkanfrage ablehnen</p>
                               <p class="write_message">Nachricht schreiben</p>
                           </div>
                       </div>
                   </div>
               </div>
               <? } ?>
           </li>
       </ul>

    </div>
    <script>
        $(document).ready(function(){
            $(".new_kontakt_list li:last-child hr").remove();
            $(function(){
                $('.ul_kontakten_top a').filter(function(){

                    return this.href==location.href}).parent().addClass('active').siblings().removeClass('active');

                $('.ul_kontakten_top a').click(function(){
                    $(this).parent().addClass('active').siblings().removeClass('active')
                })
            });
        });
    </script>

</div>

