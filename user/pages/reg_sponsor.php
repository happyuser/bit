<?
if(!get_cookie('id_guest'))exit;
$select=select_DB('reg_sponsor', array('hash'=>get_cookie('hash'),'id'=>get_cookie('id_sponsor')),null);
$sponsor=$select[0];
?>
<script src="js/reg_sponsor.js" type="text/javascript"></script>
<script src="js/registration.js" type="text/javascript"></script>
<div class="row-fluid">
    <div class="form_page span7 offset2">
    <?if(get_get('step')=='1'){?>
    <form class="form_reg" id="1">
        <div class="block_reg_index form_reg form_reg_guest_sponsoren" id="1">
        <div class="form_reg_index">
            <div class="row-fluid">
                <div class="span5 img_profil" style="overflow: hidden;">
                <img class="upload_image" src="<?if($sponsor[photo])echo"/uploads/".$sponsor[photo]; else echo"http://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Icon_-_upload_photo_2.svg/512px-Icon_-_upload_photo_2.svg.png";?>" width="100%"/>
                <input type="hidden" name="photo" value="<?=$sponsor[photo]?>" />
                <input type="file" id="imgFile" />
                <div class="new_photo_upload">new photo</div>
            </div>
            <div class="span7">
                <div class="row-fluid">
                    <div class="span4 ">
                        Password
                    </div>
                    <div class="span8">
                        <input style="width:100%; margin-left: 17px;" type="password" name="password" value="<?=$sponsor[password]?>" required/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        Firmenname
                    </div>
                    <div class="span8">
                        <input style="width:100%; margin-left: 17px;" type="text" name="firmenname" value="<?=$sponsor[firmenname]?>" required/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        Geschlecht
                    </div>
                    <div class="span8">
                        <select style="width:100%; margin-left: 17px;" name="geschlecht" required>
                            <option value="weiblich" <?if($sponsor[geschlecht]=='weiblich')echo"selected";?>>weiblich</option>
                            <option value="mannlich" <?if($sponsor[geschlecht]=='mannlich')echo"selected";?>>mannlich</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid" style="margin-top: 10px;">
                    <button class="btn next" style="display: inline-block;" disabled>Weiter</button>
                </div>
            </div>
            </div>
        </div>
    </div>
</form>
    <!--
    <form class="form_reg" id="1">
        <div class="span5 img_profil" style="overflow: hidden;">
            <img class="upload_image" src="<?if($sponsor[foto])echo"/uploads/".$sponsor[foto]; else echo"http://monta.ru/upload/medialibrary/3d6/Shish.gif";?>" width="100%"/>
            <input type="hidden" name="foto" value="<?=$sponsor[foto]?>" />
            <input type="file" id="imgFile" />
            <div class="new_photo_upload">new photo</div>
        </div>
        <div class="span7">
            <div class="row-fluid">
                <div class="span4">
                    Password
                </div>
                <div class="span8">
                    <input type="password" name="password" value="<?=$sponsor[password]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Firmenname
                </div>
                <div class="span8">
                    <input type="text" name="firmenname" value="<?=$sponsor[firmenname]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Geschlecht
                </div>
                <div class="span8">
                    <select name="geschlecht" required>
                        <option value="weiblich" <?if($sponsor[geschlecht]=='weiblich')echo"selected";?>>weiblich</option>
                        <option value="mannlich" <?if($sponsor[geschlecht]=='mannlich')echo"selected";?>>mannlich</option>
                    </select>
                </div>
            </div>
            <div class="row-fluid">
                <button class="btn next" disabled>Weiter</button>
            </div>
        </div>
    </form>
    -->

</form>
    <?
    }elseif(get_get('step')=='2'){
    ?>
<form class="form_reg" id="2">
    <div class="block_reg_index form_reg" style="width:500px; padding-bottom: 15px;" id="2">
        <div class="form_reg_index">

            <div class="row-fluid">
                <div class="span4">
                    Stra?e/ Hausnummer
                </div>
                <div class="span8">
                    <input type="text" name="strabe" value="<?=$sponsor[strabe]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    PLZ
                </div>
                <div class="span8">
                    <input type="text" name="plz" value="<?=$sponsor[plz]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Stadt
                </div>
                <div class="span8">
                    <input type="text" name="stadt" value="<?=$sponsor[stadt]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Land
                </div>
                <div class="span8">
                    <input type="text" name="land" value="<?=$sponsor[land]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Telefon
                </div>
                <div class="span8">
                    <input type="text" name="telefon" value="<?=$sponsor[telefon]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Internetadresse
                </div>
                <div class="span8">
                    <input type="text" name="site_url" value="<?=$sponsor[site_url]?>"/>
                </div>
            </div>
            <div class="row-fluid" style="margin-top: 10px;">
                <button class="btn next" style="display: inline-block;" disabled>Weiter</button>
                <button class="btn back" style="display: inline-block;" >Back</button>
            </div>
        </div>
    </div>
</form>
    <!--
    <form class="form_reg" id="2">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    Stra?e/ Hausnummer
                </div>
                <div class="span8">
                    <input type="text" name="strabe" value="<?=$sponsor[strabe]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    PLZ
                </div>
                <div class="span8">
                    <input type="text" name="plz" value="<?=$sponsor[plz]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Stadt
                </div>
                <div class="span8">
                    <input type="text" name="stadt" value="<?=$sponsor[stadt]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Land
                </div>
                <div class="span8">
                    <input type="text" name="land" value="<?=$sponsor[land]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Telefon
                </div>
                <div class="span8">
                    <input type="text" name="telefon" value="<?=$sponsor[telefon]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Internetadresse
                </div>
                <div class="span8">
                    <input type="text" name="site_url" value="<?=$sponsor[site_url]?>"/>
                </div>
            </div>
            <div class="row-fluid">
                <button class="btn next" disabled>Weiter</button>
                <button class="btn back">Back</button>
            </div>
        </div>
    </form>
    -->
</form>
    <?
    }elseif(get_get('step')=='3'){
    ?>
<form class="form_reg" id="3">
    <div class="block_reg_index form_reg" style="width:540px; padding-bottom: 15px;" id="3">
        <div class="form_reg_index">

            <div class="row-fluid">
                <div class="span8">
                    Stellen Sie bitte ihre Firma vor?
                </div>
                <div class="span4">
                    <input type="text" name="bitte" value="<?=$sponsor[bitte]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span8">
                    Uber die Firma
                </div>
                <div class="span4">
                    <input type="text" name="uber" value="<?=$sponsor[uber]?>"/>
                </div>
            </div>        
            <div class="row-fluid">
                <div class="span8">
                    Zu welcher Veranstaltung bewerber Sie sich?
                </div>
                <div class="span4">
                    <select style="width:100%;" name="event_type" required>
                        <option value="mmm" <?if($sponsor[event_type]=='mmm')echo"selected";?>>Movie meets Media</option>
                        <option value="mumm" <?if($sponsor[event_type]=='mumm')echo"selected";?>>Movie meets Media</option>
                        <option value="director" <?if($sponsor[event_type]=='director')echo"selected";?>>Direktors Cut</option>
                    </select>
                </div>
            </div>
            <div class="row-fluid" style="margin-top: 10px;">
                <button class="btn next" style="display: inline-block;" disabled>Weiter</button>
                <button class="btn back" style="display: inline-block;" >Back</button>
            </div>
        </div>
    </div>
</form>
    <!--
    <form class="form_reg" id="3">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    Stellen Sie bitte ihre Firma vor?
                </div>
                <div class="span8">
                    <input type="text" name="bitte" value="<?=$sponsor[bitte]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Uber die Firma
                </div>
                <div class="span8">
                    <input type="text" name="uber" value="<?=$sponsor[uber]?>"/>
                </div>
            </div>        
            <div class="row-fluid">
                <div class="span4">
                    Zu welcher Veranstaltung bewerber Sie sich?
                </div>
                <div class="span8">
                    <select name="event_type" required>
                        <option value="mmm" <?if($sponsor[event_type]=='mmm')echo"selected";?>>Movie meets Media</option>
                        <option value="mumm" <?if($sponsor[event_type]=='mumm')echo"selected";?>>Movie meets Media</option>
                        <option value="director" <?if($sponsor[event_type]=='director')echo"selected";?>>Direktors Cut</option>
                    </select>
                </div>
            </div>
            
            <div class="row-fluid">
                <button class="btn next" disabled>Weiter</button>
                <button class="btn back">Back</button>
            </div>
        </div>
    </form>
    -->
    
<div class="pop_window activ_guest">
    <div class="row-fluid">
        <a href="/user/?page=events">Zur Eventsseite</a>
    </div>
</div>
<?}?>
</div>
</div>
<script type="text/javascript">
    $(".block_reg_index").alignCenterScreen();
</script>