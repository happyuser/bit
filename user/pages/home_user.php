<?include_once('reklams.php');?>
<body>
<div class="t_wrapper">
<div class="no_activ_body"></div>
    <div id="white_bcg"></div>
    <div class="container">
        <div class="row-fluid" id="main_header" class="test">
            <div class="span12 t_header t_header_user_home t_header_user_home_impressum" style="margin-top: 0px;">
                <div class="t_header-logo span5 logo-user">

                    <a href="/user/?page=main_profil" style="text-decoration: none;margin-left: 10px;">
                        <span class="t_logo">Face</span>
                        <span>Club</span>
                    </a>

                </div>

                <div class="t_header_menu span7 t_header_menu_user">
                    <div class="controls span5 search-block">
                        <input id="search" type="text" name="search" placeholder="Suche..."/>
                    </div>
                    <ul class="span9 top-user-menu pull-right">
                        <li>
                            <a href="/user/?page=wall&type=alle" id="wall-all" title="Neuigkeiten">&nbsp;</a>
                        </li>
                        <?if(TYPE_ACCOUNT=='user'){?>
                        <li>
                            <a href="/user/?page=kontakten_finden" id="find-contacts" title="Kontakte hinzufügen">&nbsp;</a>
                        </li>
                        <?}?>
                        <li>
                            <a href="/user/?page=events" id="events" title="Veranstaltungen">&nbsp;</a>
                        </li>

                        <li>
                            <a href="/user/?page=einstellungen" id="settings" title="Einstellungen">&nbsp;</a>
                        </li>
                        <li>
                            <a href="#" id="logout" title="Logout">&nbsp;</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row-fluid">
            <div class="span12 t_user_body t_user_body_home_user">
                <div class="span9 t_main_content t_main_content_user">
                    <div class="t_advertisement reklama">
                    <?echo
                    '<ul class="span4 block_reklams">'.$BLOCK_REKLAM[1].'</ul>'.
                    '<ul class="span4 block_reklams">'.$BLOCK_REKLAM[2].'</ul>'.
                    '<ul class="span4 block_reklams">'.$BLOCK_REKLAM[3].'</ul>';
                    ?>
                    </div>
                    <?
                        if(!$CONTENT)$CONTENT='main_profil';
                        include_once( 'pages/'.$CONTENT. '.php')
                    ?>
                </div>
                <div class="span3 t_side_menu_box">
                    <ul class="t_side_menu">
                        <li><a href="/user/?page=<?=($ID_USER)? 'main_profil&id='.$ID_USER : 'main_sponsor&id='.$ID_SPONSOR?>" class="ico_mein_profil">Mein Profil</a>
                        </li>
                        <?if(TYPE_ACCOUNT=='user'){?>
                        <li>
                            <?
                            $select=select_DB('friendes',array('id'=>$ID_USER));
                            $select=json_decode($select[0]['incoming'],true);?>
                            <a href="/user/?page=kontakten" class="<?if($select){?>ico_kontakten_point<?} else {?>ico_kontakten<?}?>">Kontakten</a>
                        </li>
                        <li>
                        <?$messages=select_DB('messages',array('u_to'=>$ID_USER,'flag'=>'0'));?>
                        <a href="/user/?page=nachrichten" class="<?if($messages){?>ico_nachrichten_point<?} else{?>ico_nachrichten<?}?>">Nachrichten</a>
                        </li>
                        <?}?>
                        <li><a href="/user/?page=veranstaltungen" class="ico_veranstaltungen">Meine Veranstaltungen</a>
                        </li>
                        <li>
                        <?$einladungen_select=select_DB('guest_list',array('id_user'=>$ID_USER,'status'=>'view'));
                        $einladungen_new=false;
                        if($einladungen_select)
                        foreach($einladungen_select as $value){
                            if($value[status]=='no')continue;
                            $info_event=select_DB('events',array('id'=>$value[id_event],'active'=>1));
                            if(!$info_event)continue;
                            $info_event=$info_event[0];
                            if(strtotime($info_event[deadline])>time()){
                                if($value[status]=='view')$type='eingeladene'; else $type='bestatigte';
                                if(select_DB('message_type_account',array('id_event'=>$value[id_event],'type_user'=>$type,'id'=>$_USER_INFO[member][distribution]))){
                                    $einladungen_new=true;
                                    break;
                                }
                            }
                        }?>
                        <a href="/user/?page=einladungen" class="<?if($einladungen_new){?>ico_einladungen_point<?} else {?>ico_einladungen<?}?>">Einladungen</a>
                        </li>
                    </ul>
                    <!--<iframe class="video-sidebar" style="margin: -1.5%;" width="93%" src="//www.youtube.com/embed/QBhAQbhY_nI" frameborder="0" allowfullscreen></iframe>-->
                    <div class="video-sidebar" style="width: 93%;">
                    <?
                        //echo '<ul class="block_reklams" style="padding: 0;margin: 0;width: initial;">'.$BLOCK_REKLAM[5].'</ul>';
                        echo '<iframe width="100%" height="150" src="https://www.youtube.com/embed/'.$BLOCK_REKLAM[5].'?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
                    ?>
                    </div>
                    <div class="vertical-banner-first" style="width:93%">
                    <?
                        echo '<ul class="block_reklams">'.$BLOCK_REKLAM[4].'</ul>';
                    ?>
                    </div>
                    <!--<img class="vertical-banner-first" src="/images/banner-first.jpg" width="93%" />-->
                </div>
            </div>
        </div>
    </div>
    <!--/div-->
</div>

<div class="t_footer">
    <div class="container" id="footer_container" style="text-align: center">
        <ul class="footer_text">
            <li>
                <a href="/user/?page=faq" class="no_active_func">Hilfebereich</a>
            </li>
            <li>
                <a href="/user/?page=info_site&part=uber_fc" id="btn_uber_fc" class="no_active_func">Über FaceClub.de</a>
            </li>
            <li>
                <a href="/user/?page=info_site&part=impressum" id="btn_impressum" class="no_active_func">Impressum</a>
            </li>
        </ul>
    </div>
</div>

</body>
</html>