<div class="row-fluid kontakten_background">
    <div class="row marg">
        <ul class="nav nav-pills ul_kontakten_top">
            <li class="span12" >
                <a style="border-top-right-radius: 2px;" class="marg mitglieder-header">Face Club Mitglieder</a>
            </li>

        </ul>
    </div>

    <div class="row marg kontakten_content">
        <div class="row-fluid">
        <form class="search_f">
        <div class="control-group span6 marg">
            <div class="search_part">
                <label class="control-label lab_disp" for="gender">Land</label>

                <select id="land" name="country" class="select_style kontakten_finder_land">
                    <option value="all_land">Alle Länder</option>
                <?
                    $COUNTRIES=countries_users();
                    foreach($COUNTRIES as $key=>$val){
                ?>
                    <option value="<?=$val[id]?>"><?=$val[name]?></option>
                <?}?>
                </select>
            </div>
            <div class="search_part">
                <label class="control-label lab_disp" for="city">Stadt</label>

                <select id="city" name="postaddress" class="select_style kontakten_finder_city">
                    <option value="all_city">Alle Städte</option>
                </select>
            </div>


        </div>

        <div class="control-group span6 marg">
            <div class="search_part">
                <label class="control-label lab_disp label-finder-geschlecht" for="gender">Geschlecht</label>

                <select id="gender" name="gender" class="select_style kontakten_finder_gender">
                    <option value="all_beide">beide</option>
                    <option value="weiblich">weiblich</option>
                    <option value="männlich">männlich</option>
                </select>
            </div>

            <div class="search_part">
                <input type="text" name="search_text" class="span12 search_kontakt_finder_mitglieder" id="search_kontakt" placeholder="Mitglieder suchen" />
            </div>
        </div>
        </form>
        </div>
        <div class="row-fluid marg">
            <hr style="margin:10px 0;height: 1px;color: #7f7f7f;background-color: #7f7f7f;border: none;"/>
        </div>
        <div class="alert_friend" style="display:none;color: red;"></div>
        <ul class="kontakt_list marg">
<?
if(TYPE_ACCOUNT=='user'){
    $FRIENDS=select_DB('friendes',array('id'=>$ID_USER));
    $INITIAL=json_decode($FRIENDS[0][initial],true);
    $FRIENDS=json_decode($FRIENDS[0][id_friends],true);
    if(!$INITIAL)$INITIAL=array();
    if(!$FRIENDS)$FRIENDS=array();
}
    $colums='`first_name`,`last_name`,`firma`,`functions`,`id`,`photo`,`salutation`';
    $query = "SELECT %colums% FROM member";
    if($FRIENDS){
        foreach($FRIENDS as $value){
            $str_fr.="`id`!='".$value."'";
        }
        $str_fr = '('.str_replace("'`", "' AND `", $str_fr).')';
    }
    $query.=" WHERE ";
    $query.=($str_fr)? $str_fr.' AND ':'';
    $query.=" `id`!=".$id_cookie;
    //echo str_replace("%colums%", 'COUNT(*)', $query);
    $query_count = mysql_fetch_array(mysql_query(str_replace("%colums%", 'COUNT(*)', $query)));
    $query_count=$query_count[0];
    
    $query = str_replace("%colums%", $colums, $query);
    //echo $query." limit $page,10";
    $result = mysql_query($query." limit 0,10"); 
        while(@$row = mysql_fetch_assoc($result)){
        if(!file_exists('../uploads/crop_img/'.$row[photo])){
            $row[photo]='useremptylogo.png';
        }
?>
                <li class="user">
                    <div class="row marg">

                        <div class="find_page kontakt_image span4">
                            <img src="../uploads/crop_img/<?=$row[photo]?>"/>
                        </div>

                        <div class="kontakt_info span6">
                            <p class="kontakt_name"><a href="/user/?page=main_profil&id=<?=$row[id]?>"><?=$row[last_name].' '.$row[first_name]?></a></p>
                            <p class="kontakt_function"><?=$row[functions]?></p>
                            <p class="kontakt_firm"><?=$row[firma]?></p>
                        </div>
                    <?if(TYPE_ACCOUNT=='user'){?>
                        <div class="span1">
                            <div class="add_new_friend" <?if(false!==array_search($row[id],$INITIAL))echo "style='background-image:url(images/del.png)' title='Zur Kontaktenliste hinzufügen'"; else echo"title='Aus der Kontaktenliste entfernen'";?> id="<?=$row[id]?>"></div>
                            <div class="kontakt_message"></div>
                        </div>
                    <?}?>
                    </div>
                </li>
<?}?>
        </ul>

        <div class="row-fluid">
            <div class="pagination pagination-centered span12 contact-margin-bottom">
                <ul id="pagination1" class="pagination">
                    <li class="first disabled"><a href="1">&lt;</a></li>
                    <li class="prev disabled"><a href="1">zurück</a></li>
                    <?
                    $i=1;
                    while(ceil($query_count/10)>=$i && $i<=3){
                        ?>
                        <li class="page"><a href="<?=$i?>"><?=$i?></a></li>
                        <?
                        $i++;
                    }?>
                    <li class="next disabled"><a href="<?=(ceil($query_count/10)<2)?'1':'2'?>">weiter</a></li>
                    <li class="last disabled"><a href="<?=ceil($query_count/10)?>">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>

<script src="js/kontakten_finden.js"></script>