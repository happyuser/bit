<?include_once'pop_windows_einstellungen.php';?>
<!--Privatinstellungen-->
<?
$info_user=select_DB('member',array('id'=>get_cookie('id_user')));
$info_user=$info_user[0];
?>

<!--FORM START-->
<div class="span12 form_style" style="margin-left: 0px">

<!--    <div class="row-fluid">

        <div class="span12 form_header">Einstellungen</div>

    </div>

-->
    <p class="text-center legend_color ein_header">EINSTELLUNGEN</p>
    <form class="form-horizontal rest_form">

        <!--PASSWORD-->
        <fieldset>

            <legend class="legend_color">Passwort ändern</legend>

            <!--OLD PASSWORD-->

            <div class="control-group" >
                <label class="control-label span3 space-right" style="padding: 0;" for="old_pass">Aktuelles Passwort:</label>

                <div class="span8" >
                    <input class="span8" style="min-height: 25px;" type="password" id="old_pass" placeholder="Aktuelles Passwort eintragen">
                </div>

            </div>

            <!--NEW PASSWORD-->

            <div class="control-group">

                <label class="control-label span3 space-right" style="padding: 0;" for="new_pass">Neues Passwort:</label>

                <div class="span8" >
                    <input class="span8" style="min-height: 25px;"  type="password" id="new_pass" placeholder="Neues Passwort eintragen">
                </div>

            </div>

            <!--RE-NEW PASSWORD-->

            <div class="control-group">

                <label class="control-label span3 space-right" style="line-height:15px; padding:0;" for="re-new_pass">Neues Passwort wiederholen:</label>

                <div class="span8">

                <input class="span8" style="min-height: 25px;" type="password" id="re-new_pass" placeholder="Neues Passwort wiederholen">
                    </div>

            </div>



            <!--CHANGE PASSWORD BUTTON-->

            <div class="control-group offset3">

              <input type="button"  id="change_pass_btn" class="btn btn_style" value="Speichern">

            </div>

        </fieldset>

        <div class="alert alert-success pagination-centered" id="password_info" style="display: none">

            <button type="button" class="close" data-hide="alert">&times;</button>

        </div>


        <!--EMAIL-->

        <fieldset>
            <legend class="legend_color">E-Mail adresse</legend>

            <!--OLD EMAIL-->

            <div class="control-group text_align">

                <label class="control-label span3 space-right" for="old_email">Aktuelle E-Mail adresse</label>

                <div class="span8">
                    <div id="old_email" class="span8" style="line-height: 30px;">
                        <?php
                        //$q = "SELECT email from member_login WHERE id=$id";
                        if(TYPE_ACCOUNT=='user')$table='member_login'; else $table='reg_sponsor';
                        $a=select_DB($table, array('id'=>$id_cookie),0,0,0,array('email'));
                        $a=$a[0];
                        echo$a[email];
                        ?>

                    </div>
                </div>

            </div>

            <!--NEW EMAIL-->

            <div class="control-group text_align">

                <label class="control-label span3 space-right" style="padding-top: 0;" for="new-email">Neue E-Mailadresse eintragen</label>

                <div class="span8">
                    <input type="email" style="min-height: 25px;" class="span8 " id="new_email" placeholder="Neue E-Mailadresse eintragen">
                    <i id="email_icon"></i>
                </div>

            </div>

            <!--CHANGE EMAIL BUTTON-->

            <div class="control-group offset3">

                <input type="button" style="min-height: 25px;" id="change_email_btn" class="btn btn_style" value="Senden" />

            </div>

        </fieldset>

        <div class="alert alert-success pagination-centered" id="email_info" style="display: none">

            <button type="button" class="close" data-hide="alert">&times;</button>

        </div>


        <!--ID-->

       <!-- <fieldset>

            <legend class="legend_color">Ihre ID-Nummer</legend>

            <div class="control-group text_align">
                <label class="control-label span3 space-right" style="text-align: right" for="id_number">Profilnummer:</label>

                <div class="span8">
                    <div class="span8" style="min-height: 25px; line-height: 30px" id="id_number"><?/*=$ID_USER*/?></div>
                </div>
            </div>

        </fieldset>-->


        <!--NAME-->

        <fieldset>

            <legend class="legend_color">Name ändern</legend>

            <!--SURNAME-->
<?if(TYPE_ACCOUNT=='user'){
    $b=select_DB('member',array('id'=>$id));
    $b=$b[0];
    if($b[approve]==3){
        $q = "SELECT gen_key from logs_users_edit WHERE id_user=$id order by id desc limit 0,1";
        $r = mysql_query($q);
        $a = mysql_fetch_assoc($r);
        
        $q = "SELECT * from logs_users_edit WHERE id_user=$id AND gen_key='$a[gen_key]'";
        $r = mysql_query($q);
        while($a = mysql_fetch_assoc($r)){
            if($a[type]=='first_name'){$access_type=true;break;}
        }
    }
}else{
    $info_user=select_DB('reg_sponsor',array('id'=>$id_cookie),0,0,0,array('first_person'));
    $info_user[first_name]=$info_user[0]['first_person'];
}
    if($access_type && $a[msg] && $b[approve]==3)echo"Ihre Anfrage wird bearbeitet";
    else{    
?>
            <div class="control-group">

                <label class="control-label span3 space-right" style="padding: 0;" for="name">Name:</label>

                <div class="span8">

                    <input type="text" style="min-height: 25px;" class="span8" id="name" placeholder="Name eintragen" value='<?=$info_user['first_name']; ?>' />

                </div>

            </div>
            <?if(TYPE_ACCOUNT=='user'){?>
            <div class="control-group">

                <label class="control-label span3 space-right" for="name">Grund:</label>

                <div class="span8">

                    <textarea style="min-height: 25px; resize: none;" class="span8" id="msg"></textarea>

                </div>

            </div>
            <?}?>
            <!--CHANGE NAME BUTTON-->

            <div class="control-group offset3">
                <input type="button" style="min-height: 25px;" id="change_name_btn" class="btn btn_style" value="Anfrage senden"/>
            </div>

        </fieldset>

        <div class="alert alert-success pagination-centered" id="name_info" style="display: none">

            <button type="button" class="close" data-hide="alert">&times;</button>

        </div>
<?}?>

    </form>


<?if(TYPE_ACCOUNT=='user'){?>
    <!--FORM START-->

    <form class="form-horizontal rest_form">

        <fieldset>

            <legend class="legend_color">Über welche Veranstaltungen wollen Sie informiert werden?</legend>

            <div class="control-group">

                <label class="control-label span3 space-right" for="example-selectAllValue">Veranstaltungen:</label>

            <!--CATEGORY-->
                <div class="span8">
                    <select id="example-selectAllValue" style="min-height: 25px;" multiple="multiple">


                        <?php
                        /*ТУТ ВИВОДЯТЬСЯ УСІ ПОДІЇ а не їх типи
                        $user_id = $ID_USER;
                        $q = "SELECT id, name from events";
                        if (($r1 = mysql_query($q)) !== FALSE) {
                            while (($a = mysql_fetch_array($r1)) !== FALSE) {
                                $q = "SELECT * from guest_list WHERE (id_event=${a['id']}) and (id_user=$user_id)";
                                if (($r2 = mysql_query($q)) !== FALSE) {
                                    print (mysql_num_rows($r2) ? "<option value='${a['id']}' selected>" :
                                            "<option value='${a['id']}'>") . "${a['name']}</option>";
                                    mysql_free_result($r2);
                                } else { print "Error in: $q<br />" . mysql_error(); break; }
                            }
                            mysql_free_result($r1);
                        } else print "Error in: $q<br />" . mysql_error();*/
                        $query = "SELECT mmm_b,mmm_muc,mmm_hh,mumm,dc,sport,sonstige FROM member WHERE id=$ID_USER";
                        $result = mysql_query($query);
                        $row = mysql_fetch_assoc($result);
                        ?>
                        
                        <option value="mmm_b" <?if($row[mmm_b]=='JA')echo 'selected';?>>MMM Berlin</option>
                        <option value="mmm_muc" <?if($row[mmm_muc]=='JA')echo 'selected';?>>MMM München</option>
                        <option value="mmm_hh" <?if($row[mmm_hh]=='JA')echo 'selected';?>>MMM Hamburg</option>
                        <option value="mumm" <?if($row[mumm]=='JA')echo 'selected';?>>MuMM</option>
                        <option value="dc" <?if($row[dc]=='JA')echo 'selected';?>>Directors</option>
                        <option value="sport" <?if($row[sport]=='JA')echo 'selected';?>>Sport</option>
                        <option value="sonstige" <?if($row[sonstige]=='JA')echo 'selected';?>>Sonstige</option>
                    </select>
                </div>
            </div>

            <!--CHOOSE CATEGORY BUTTON-->

            <div class="control-group offset3">

                <input type="button" id="choose_category" class="btn btn_style" value="Speichern">

            </div>

        </fieldset>

        <div class="alert alert-success pagination-centered" id="category_info" style="display: none">

            <button type="button" class="close" data-hide="alert">&times;</button>

        </div>
<?
$id = $ID_USER;
$newsletter_email_addresses = $newsletter_post_addresses = 0;
$q = "SELECT newsletter_email_addresses, newsletter_post_addresses, news from member_login WHERE id=$id";
if (($r = mysql_query($q)) !== FALSE) {
    if (($a = mysql_fetch_assoc($r)) !== FALSE) {
        $newsletter_email_addresses = $a['newsletter_email_addresses'];
        $newsletter_post_addresses = $a['newsletter_post_addresses'];
        $news = $a['news'];
    }
    mysql_free_result($r);
} else print "Error in: $q<br />";
?>
        
        <!--EMAIL SENDS-->
        <fieldset>
            <legend class="legend_color">Wie wollen Sie über kommende Veranstaltungen informiert werden?</legend>
            <div class="control-group form-inline">
                <div class="span9 offset3" style="margin-bottom: 15px; font-size: 14px;">
                    <?if($info_user[email_privat]){?><input type="checkbox" id="privat" style="margin: 2px 5px 5px" name="privat" <? if ($news==1 || $news==3) print 'checked'; ?> value="1"/>
                            Privat-Email<br /><?}?>
                    <?if($info_user[email_bus]){?><input type="checkbox" style="margin: 2px 5px 5px" name="geschaft" <? if ($news==2 || $news==3) print 'checked'; ?> value="2"/>
                            Geschäft-Email<?}?>
                </div>
                <br />
                <div class="control-group offset3">
                    <input type="button"  id="news_system" class="btn btn_style" value="Speichern"/>
                </div>
                <div class="alert alert-success pagination-centered" id="news_system_info" style="display: none">
                    <button type="button" class="close" data-hide="alert">&times;</button>
                </div>
            </div>
        </fieldset>
        <fieldset>

            <legend class="legend_color">Wie wollen Sie Einladungen zu kommenden Veranstaltungen erhalten?</legend>


            <!--EMAIL-->

            <div class="control-group form-inline">

                <div class="control-label span3 control-label-checkbox" style="padding-top: 0px;">
                    <input type="checkbox" style="margin: 0px 2px" id="enable_newsletter_by_email"<? if ($newsletter_email_addresses) print 'checked'; ?>/>
                    <label class="checkbox" style="padding: 0px; text-align: left" for="example-multiple-selected">per Email:</label>
                </div>

                <div class="span8">
                    <select multiple="multiple" id="example-multiple-selected">
                        <?if($info_user[email_privat]){?><option id="enable_newsletter_by_private_email"
                            <? if ($newsletter_email_addresses & 1) print 'selected'; ?>>Privat-Email</option><?}?>
                        <?if($info_user[email_bus]){?><option id="enable_newsletter_by_work_email"
                            <? if ($newsletter_email_addresses & 2) print 'selected'; ?>>Geschäft-Email</option><?}?>
                    </select>
                </div>

            </div>

             <!--ADDRESS-->

            <div class="control-group form-inline">

                <div class="control-label span3 control-label-checkbox" style="padding-top: 0px;">
                    <input type="checkbox"  style="margin: 0 2px" id="enable_newsletter_by_post"
                        <? if ($newsletter_post_addresses) print 'checked'; ?>>
                    <label class="checkbox" style="padding: 0;"  for="example-multiple-selected1">per Post:</label>
                </div>

                <div class="span8">
                    <select id="example-multiple-selected1" multiple="multiple">
                        <?if($info_user[adress_privat]){?><option id="enable_newsletter_by_private_post"
                            <? if ($newsletter_post_addresses & 1) print 'selected'; ?>>Privatadresse</option><?}?>
                        <?if($info_user[adress_bus]){?><option id="enable_newsletter_by_work_post"
                            <? if ($newsletter_post_addresses & 2) print 'selected'; ?>>Geschäftadresse</option><?}?>
                    </select>
                </div>

            </div>

            <!--SEND ADDRESS BUTTON-->

            <div class="control-group offset3">

                <input type="button"  id="send_address" class="btn btn_style" value="Speichern"/>

            </div>

        </fieldset>

        <div class="alert alert-success pagination-centered" id="newsletter_info" style="display: none">

            <button type="button" class="close" data-hide="alert">&times;</button>

        </div>


    </form>

<?}?>

    <!--<a href="#" class="btn btn-primary offset5"
       data-toggle="modal"
       data-target="#basicModal">FaceClub-Profil löschen</a>

    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Sind Sie sicher, dass Sie Ihren FaceClub-Profil löschen wollen?</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Nein</button>
                    <button type="button" class="btn btn-primary" id="delete">Ja</button>
                </div>
            </div>
        </div>
    </div>-->


</div>

<script src="js/einstellungen.js"></script>