<?
$FRIENDS=view_friends(array('id'=>$ID_USER));
?>

<div class="row-fluid kontakten_background">
    <div class="row marg">
        <ul class="nav nav-pills ul_kontakten_top">
            <li class="active">
                <a class="marg" href="/user/?page=kontakten">Kontakten</a>
            </li>
            <li>
            <?
            $select=select_DB('friendes',array('id'=>$ID_USER));
            $select=json_decode($select[0][incoming],true);?>
                <a class="<?if($select){?>color_bordo <?}?>marg" href="/user/?page=kontaktanfragen">Kontaktanfragen</a>
            </li>
        </ul>
    </div>

    <div class="row marg kontakten_content">
        <div class="row-fluid">
            <form class="search_f">
                <div class="control-group span6 marg">
                    <div class="search_part">
                        <label class="control-label lab_disp" for="gender">Land</label>

                        <select id="land" name="country" class="select_style">
                            <option value="all_land">Alle Länder</option>
                            <option value="deutschland">Deutschland</option>
                            <option value="frankreich">Frankreich</option>
                            <option value="grobritannien">Großbritannien</option>
                        </select>
                    </div>
                    <div class="search_part">
                        <label class="control-label lab_disp" for="city">Stadt</label>

                        <select id="city" name="postaddress" class="select_style">
                            <option value="all_city">Alle Städte</option>
                            <option value="berlin">Berlin</option>
                            <option value="koln">Koln</option>
                            <option value="trier">Trier</option>
                        </select>
                    </div>


                </div>

                <div class="control-group span6 marg">
                    <div class="search_part geschlecht_block">
                        <label class="control-label lab_disp" for="gender">Geschlecht</label>

                        <select id="gender" name="gender" class="select_style">
                            <option value="beide">beide</option>
                            <option value="weiblich">weiblich</option>
                            <option value="männlich">männlich</option>
                        </select>
                    </div>

                    <div class="search_part">
                        <input type="text" class="span12 kontakten_kontakt_suchen" name="search_text" id="search_kontakt" placeholder="Kontakt suchen" />
                    </div>
                </div>
            </form>
        </div>

        <div class="row marg">
            <hr />
        </div>

        <div class="row marg">
        <div class="alert_friend" style="display:none;color: red;"></div>
            <ul class="kontakt_list marg">
                <?
                $i=0;
                if($FRIENDS)
                    foreach($FRIENDS as $key=>$val){
                        if(!$val[id])continue;
                        if(++$i>10)break;
                        if(!file_exists('../uploads/crop_img/'.$val[photo])){
                            $val[photo]='useremptylogo.png';
                        }
                        ?>
                        <li class="user" id_user="<?=$val[id]?>">
                            <div class="row marg">
                                <div class="kontakt_image span5">
                                    <a href="/user/?page=main_profil&id=<?=$val[id]?>">
                                        <img src="../uploads/crop_img/<?=$val[photo]?>"/>
                                    </a>
                                </div>

                                <div class="kontakt_info span5">
                                    <p class="kontakt_name"><a href="/user/?page=main_profil&id=<?=$val[id]?>"><?=$val[last_name].' '.$val[first_name]?></a>
                                    </p>

                                    <p class="kontakt_function"><?=$val[functions]?></p>

                                    <p class="kontakt_firm"><?=$val[firma]?></p>
                                </div>
                                <div class="kontakt_message span2">
                                    <div>
                                        <a href="/user/?page=nachrichtschreiben&id=<?=$val[id]?>"><button class="message_btn"></button></a>
                                        <span class="dropdown-toggle caret" data-toggle="dropdown"></span>
                                        <ul class="dropdown-menu message_list">
                                            <li class="del_friend" id="<?=$val[id]?>">
                                                Aus der Kontaktenliste entfernen
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?}
					else echo"Sie haben noch keine Kontakten.";?>
            </ul>
        </div>

        <div class="row-fluid">
            <div class="pagination pagination-centered span12 contact-margin-bottom">
                <ul id="pagination1" class="pagination">
                    <li class="first disabled"><a href="1">&lt;</a></li>
                    <li class="prev disabled"><a href="1">zurück</a></li>
                    <?
                    $i=1;
                    while(ceil(count($FRIENDS)/10)>=$i && $i<=3){
                        ?>
                        <li class="page"><a href="<?=$i?>"><?=$i?></a></li>
                        <?
                        $i++;
                    }?>
                    <li class="next disabled"><a href="<?=(ceil(count($FRIENDS)/10)<2)?'1':'2'?>">weiter</a></li>
                    <li class="last disabled"><a href="<?=ceil(count($FRIENDS)/10)?>">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
<script src="js/contacts.js"></script>