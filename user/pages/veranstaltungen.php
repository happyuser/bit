<?
$type=1;
if(get_get('type')=='close'){
    $type=0;
}

if(TYPE_ACCOUNT=='user'){$table='bestatigte';$id='id_user';} else {$table='sponsor_in_event';$id='id_sponsor';}
$MY_EVENTS=select_DB($table, array($id=>$id_cookie),0,0,0,array('id_event'));
if($MY_EVENTS)
foreach($MY_EVENTS as $key=>$val){
    $select=select_DB('events', array('id'=>$val[id_event],'active'=>$type));
    $select=$select[0];
    if($select)
        $EVENTS[]=$select;
}
?>

<div class="row-fluid event_conteiner">
    <?if(!$ID_USER){?><span class="prev-page-guest prev-page-guest-events" title="Zur Startseite"></span><?}?>
    <ul class="nav nav-pills row-fluid ul_kontakten_top" style="text-align: center;">
        <li class="active"><a href="/user/?page=veranstaltungen&type=open">Kommende Veranstaltungen</a></li>
        <li><a href="/user/?page=veranstaltungen&type=close">Vergangene Veranstaltungen</a></li>
    </ul>
    <div class="list_events">
    <?if($EVENTS)
        foreach($EVENTS as $key=>$val){
            $val[images]=json_decode($val[images]);
            ?>
        <div class="li_event row-fluid">
            <?if($val[images][0]){?>
                <div class="span4"><img src="../uploads/<?=$val[images][0]?>"/></div>
            <?}else{?>
                <div class="span4"><img src="images/noFotoEvent_min.jpg" /></div>
            <?}?>
            <div class="info_event span8">
                <div class="title_event"><a href="/user/?page=event&id=<?=$val[id]?>"><?=$val[name]?></a></div>
                <div class="date_event"><?=print_date($val[dates]).' '.$val[city]?></div>
                <div class="text_event"><p><?=$val[paragraph]?></p></div>
            </div>
        </div>
        <?}elseif($type=='0')echo"Sie haben noch keine Veranstaltungen besucht"; else echo"Sie sind noch nicht zu einer Veranstaltung angemeldet";?>
    </div>
</div>