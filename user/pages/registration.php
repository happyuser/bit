<script src="js/registration.js" type="text/javascript"></script>
<?
if(!get_cookie('id_guest') && (get_get('step')!='0' && get_get('step')!='5'))exit;
if(get_get('step')=='0'){
    $_SESSION['step']=0;
    @$select=select_DB('guests', array('hash'=>get_cookie('hash'),'id'=>get_cookie('id_guest')),null);
    @$guest=$select[0];
?>
<div class="span6 offset5 active_hash active_hash_email_bestatigung" id="main_center">
	<div class="block_reg_index block_reg_index_email_bestatigung">
		<div class="title_block" style="font-size: 1.5em;">Email-Bestätigung</div>
        <div class="form_reg_index">
            <div class="row-fluid">
                <div class="span4">
                    Geschlecht
                </div>
                <div class="span8">
                    <select name="anrede" class="anrede_guest_first_select" required>
                        <option value="männlich" <?if($guest[anrede]=='männlich')echo"selected";?>>Männlich</option>
                        <option value="weiblich" <?if($guest[anrede]=='weiblich')echo"selected";?>>Weiblich</option>
                    </select>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Name
                </div>
                <div class="span8">
                    <input type="text" class="index" name="name" placeholder="Name" value="<?=$guest[vorname]?>"/>
                </div>
            </div>
			<div class="row-fluid">
                <div class="span4">
                    Vorname
                </div>
                <div class="span8">
                    <input type="text" class="index" name="vorname" placeholder="Vorname" value="<?=$guest[name]?>"/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    Email
                </div>
                <div class="span8">
                    <input type="text" class="index" name="email" placeholder="Email" value="<?=$guest[email]?>"/>
                </div>
            </div>
            <button id="reg_guest">BEWERBUNG</button>
		</div>
	</div>
</div>

<!--Licence terms box-->
    <div class="inactive_background">
        <div class="license_wrapper span5">
            <div class="license_terms">
                <h3>Allgemeine Geschäftsbedingungen - Sören Bauer Events GmbH</h3>

                <p>1) Allgemein<br/>Nachstehende "Allgemeine Geschäftsbedingungen" (AGB) gelten für alle Verträge,
                    Lieferungen und sonstige Leistungen. Abweichenden Vorschriften des Vertragspartners widerspricht
                    Sören
                    Bauer Events
                    GmbH hiermit ausdrücklich. Alle Nebenabreden bedürfen der schriftlichen Form von Sören Bauer Events
                    GmbH.
                    Sören Bauer Events GmbH ist jederzeit berechtigt, diese Geschäftsbedingungen einschliechβlich aller
                    eventuellen Anlagen
                    mit einer angemessenen Kündigungsfrist zu ändern oder zu ergänzen. Vorher eingehende Auftrage werden
                    nach den dann noch
                    gültigen alten Geschäftsbedingungen bearbeitet.
                </p>

                <h3>Allgemeine Geschäftsbedingungen - Sören Bauer Events GmbH</h3>

                <p>2) Allgemein<br/>Nachstehende "Allgemeine Geschäftsbedingungen" (AGB) gelten für alle Verträge,
                    Lieferungen und sonstige Leistungen. Abweichenden Vorschriften des Vertragspartners widerspricht
                    Sören
                    Bauer Events
                    GmbH hiermit ausdrücklich. Alle Nebenabreden bedürfen der schriftlichen Form von Sören Bauer Events
                    GmbH.
                    Sören Bauer Events GmbH ist jederzeit berechtigt, diese Geschäftsbedingungen einschliechβlich aller
                    eventuellen Anlagen
                    mit einer angemessenen Kündigungsfrist zu ändern oder zu ergänzen. Vorher eingehende Auftrage werden
                    nach den dann noch
                    gültigen alten Geschäftsbedingungen bearbeitet.
                </p>

                <h3>Allgemeine Geschäftsbedingungen - Sören Bauer Events GmbH</h3>

                <p>3) Allgemein<br/>Nachstehende "Allgemeine Geschäftsbedingungen" (AGB) gelten für alle Verträge,
                    Lieferungen und sonstige Leistungen. Abweichenden Vorschriften des Vertragspartners widerspricht
                    Sören
                    Bauer Events
                    GmbH hiermit ausdrücklich. Alle Nebenabreden bedürfen der schriftlichen Form von Sören Bauer Events
                    GmbH.
                    Sören Bauer Events GmbH ist jederzeit berechtigt, diese Geschäftsbedingungen einschliechβlich aller
                    eventuellen Anlagen
                    mit einer angemessenen Kündigungsfrist zu ändern oder zu ergänzen. Vorher eingehende Auftrage werden
                    nach den dann noch
                    gültigen alten Geschäftsbedingungen bearbeitet.
                </p>

                <h3>Allgemeine Geschäftsbedingungen - Sören Bauer Events GmbH</h3>

                <p>4) Allgemein<br/>Nachstehende "Allgemeine Geschäftsbedingungen" (AGB) gelten für alle Verträge,
                    Lieferungen und sonstige Leistungen. Abweichenden Vorschriften des Vertragspartners widerspricht
                    Sören
                    Bauer Events
                    GmbH hiermit ausdrücklich. Alle Nebenabreden bedürfen der schriftlichen Form von Sören Bauer Events
                    GmbH.
                    Sören Bauer Events GmbH ist jederzeit berechtigt, diese Geschäftsbedingungen einschliechβlich aller
                    eventuellen Anlagen
                    mit einer angemessenen Kündigungsfrist zu ändern oder zu ergänzen. Vorher eingehende Auftrage werden
                    nach den dann noch
                    gültigen alten Geschäftsbedingungen bearbeitet.
                </p>
            </div>

            <div class="license_form">
                <label class="checkbox">
                    <input type="checkbox" name="check_true"/> Ich akzeptiere Datenschutzbestimmungen und AGB
                </label>
                <button type="submit" id="licenseAccepted" class="btn" title="Weiter" disabled="false">BEWERBUNG
                </button>
                <button type="submit" id="unregister" class="btn" title="Zurück zur Hauptseite">Zurück</button>
            </div>
        </div>
    </div>

<?
}elseif(get_get('step')=='1'){?>
<div class="span3 offset4 active_hash active_hash_bestatigung" id="main_center">
	<div class="block_reg_index block_reg_index_bestatigung">
		<div class="title_block" style="font-size: 1.5em;">Email-Bestätigung</div>
		<div class="form_reg_index">
			<span style="display: block;">An Ihre Email-Adresse haben Sie einen Bestätigungsnummer bekommen. Uberprufen Sie bitte Ihre Post und Tragen Sie den Kode ein:</span>
			<br />
			<input type="text" name="hash" />
			<br />
			<a class="valera" style="display: block; margin-bottom: 20px;" href="/user/?page=registration&step=2">Keine Email erhalten?</a>
			<button class="btn btn_activ">Weiter</button>
		</div>
	</div>
</div>
<?
}elseif(get_get('step')=='3'){
?>

<div class="span3 offset4 update_mail_window" id="main_center">
	<div class="block_reg_index">
		<div class="title_block" style="font-size: 1.5em;">New email</div>
		<div class="form_reg_index">
			<input type="text" name="new_email" />
			<button class="btn btn_new_email">Send</button>
		</div>
	</div>
</div>
<?
}elseif(get_get('step')=='2'){
?>

<div class="span3 offset4 problem_window" id="main_center">
	<div class="block_reg_index" style="width: 320px;margin-top: 290px">
		<div class="title_block" style="font-size: 1.1em;">Keine Email-Bestätigung erhalten?</div>
		<div class="form_reg_index">
			<span style="display: block;">Warten Sie bitte ein bisschen – manchmal kann die Zustellung etwas dauern. Uberprufen Sie Ihren Spam-Ordner.</span>
			<br />
			<div class="row-fluid" style="margin-top: 20px;">
			<button style="width: 250px;"class="btn send_mail_hash" style="display: inline;">Email erneut senden</button>
			<br />
			<a class="valera" href="/user/?page=registration&step=3"><button style="width: 250px;">Email-Adresse andern</button></a>
		</div>
		</div>
	</div>
</div>

<?
}elseif(get_get('step')=='4'){
?>

<div class="span3 offset4 select_type_account" id="main_center">
	<div class="block_reg_index block_reg_index_bewerber_and_sponsoren">
		<div class="form_reg_index">
		<button class="btn btn_new_bewerber btn-block" style="width: 250px;">Bewerber-Profil erstellen</button>
		<!--
        <br />
		<button class="btn btn_new_sponsoren btn-block" style="width: 250px;">Sponsoren-Profil erstellen</button>-->
		</div>
	</div>
</div>

<?}elseif(get_get('step')=='5'){?>

<div id="main_center">
	<div class="block_reg_index">
		<div class="form_reg_index">
        Ihre Bewerbunganfrage wird bearbeitet. Sie bekommen die Antwort per Email zugeschickt.
		<a class="valera" style="display: block; text-align: center;" href="/user/?page=events">Zur Eventsseite</a>
		</div>
	</div>
</div>
<?}?>

<script type="text/javascript">
    $(".block_reg_index").alignCenterScreen();
</script>