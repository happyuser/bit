<?
if(!get_cookie('id_guest'))exit;
$select=select_DB('guests', array('hash'=>get_cookie('hash'),'id'=>get_cookie('id_guest')),null);
$guest=$select[0];
if(!$guest){
    delCookie('hash');
    delCookie('id_guest');
}
if(($_SESSION['step'] && $_SESSION['step']<get_get('step')) || !get_get('step')==1)$_GET[step]=$_SESSION['step'];
?>
<style>
/*.t_body {
    height: 100%;
}*/

.block_reg_index.form_reg {
    width: inherit;
}
</style>

<script src="js/reg_guest.js" type="text/javascript"></script>
<script src="js/registration.js" type="text/javascript"></script>
<div class="row-fluid row-fluid_sehr_geehrter_bewerber">
    <div class="form_page span8 offset4 form_page_sehr_geehrter_bewerber">
    <?if(get_get('step')=='1'){?>
    <form class="form_reg" id="1">
        <div class="block_reg_index form_reg block_reg_index_form_reg_first" id="1">
            <b style="display: block; margin-bottom: 10px; font-size: 1.15em;">Sehr geehrter Bewerber, herzlich willkommen bei FaceClub!</b>
            <span>Stellen Sie sich kurz vor!</span>
            <div class="form_reg_index">
                <div class="row-fluid">
                    <div class="img_profil text-center">
                    <p><img class="upload_image" src="<?if($guest[foto])echo"/uploads/".$guest[foto]; else echo"/uploads/useremptylogo.png";?>" width="100%"/></p>
                    <input type="hidden" name="foto" value="<?=$guest[foto]?>" />
                    <input type="file" id="imgFile" style="margin-top: -130px; height: 130px"/>
                    <div class="new_photo_upload">Ein neues Bild</div>
                </div>
                <div class="row-fluid box_geburtsdatum">
                    <div class="span4">
                        * Geburtsdatum
                    </div>
                    <div class="span8 birthdate_reg_box">
                        <input type="text" class="step1_date_birthdate" name="birthdate" value="<?=$guest[birthdate]?>" required/>
                    </div>
                </div>
                <div class="row-fluid kontaktdaten_box_submit_buttons">
                    <button class="btn back_reg" style="display: inline-block;">Zurück</button>
                    <button class="btn next pull-right" disabled />Weiter</button>
                </div>
            </div>
        </div>
    </form>
    <?
    }elseif(get_get('step')=='2'){
    ?>
    <form class="form_reg form_reg_three_step_first" id="2">
    <div class="block_reg_index form_reg form_reg_step_three" id="2">
        <b class="kontaktdaten_b">Kontaktdaten</b>
        <div class="form_reg_index form_reg_index_step_three">
            <div class="row-fluid">
                <div class="span12">
                    Kurzbeschreibung zu Ihrer Person:
                </div>
                <div class="span12">
                    <textarea name="send_for" class="kontaktdaten_uber_mich"><?=$guest[send_for]?></textarea>
                </div>
            </div>
            <div class="private_kontaktdaten_head">Private Kontaktdaten:</div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                     &nbsp Unternehmen:
                </div>
                <div class="span8">
                    <input type="text" name="unternehmen" value="<?=$guest[unternehmen]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    * Straße / Hausnr:
                </div>
                <div class="span8">
                    <input type="text" name="strabe" value="<?=$guest[strabe]?>" required/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    * PLZ:
                </div>
                <div class="span8">
                    <input type="text" name="plz" value="<?=$guest[plz]?>" required/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    * Ort:
                </div>
                <div class="span8">
                    <input type="text" name="ort" value="<?=$guest[ort]?>" required/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    * Land:
                </div>
                <div class="span8">
                    <select name="land" class="kontaktdaten_land">
                        <?
                        $LAND=select_DB('countries');
                        foreach($LAND as $key=>$val){
                            echo"<option value='$val[id]'";
                            if($guest[land]==$val[id])echo" selected";
                            echo">$val[de]</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    * Telefonnummer:
                </div>
                <div class="span8">
                    <input type="text" name="telefon" value="<?=$guest[telefon]?>" required/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    &nbsp Handynummer:
                </div>
                <div class="span8">
                    <input type="text" name="mobiltelefon" value="<?=$guest[mobiltelefon]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_submit_buttons">
                <button class="btn back_reg" style="display: inline-block;">Zurück</button>
                <button class="btn next pull-right next_kontaktdaten_submit" disabled />Weiter</button>
            </div>
        </div>
    </div>
    </form>
    <?
    }elseif(get_get('step')=='3'){
    ?>
    <form class="form_reg form_reg_kontaktdaten_step_3" id="3">
    <div class="block_reg_index form_reg form_reg_step_3" id="2">
        <b class="kontaktdaten_b">Kontaktdaten</b>
        <div class="form_reg_index form_reg_index_kontaktdaten_step_2">
            <div class="geschaftliche_head">Geschäftliche Kontaktdaten:</div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    Unternehmen:
                </div>
                <div class="span8">
                    <input type="text" name="unternehmen_bus" value="<?=$guest[unternehmen_bus]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    * Beruf / Funktion:
                </div>
                <div class="span8">
                    <input type="text" name="funktion_bus" value="<?=$guest[funktion_bus]?>" required/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    Straße / Hausnr:
                </div>
                <div class="span8">
                    <input type="text" name="strabe_bus" value="<?=$guest[strabe_bus]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    PLZ:
                </div>
                <div class="span8 ">
                    <input type="text" name="plz_bus" value="<?=$guest[plz_bus]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    Ort:
                </div>
                <div class="span8">
                    <input type="text" name="ort_bus" value="<?=$guest[ort_bus]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    Land:
                </div>
                <div class="span8">
                    <select name="land_bus" class="land_bus_step_three">
                        <?
                        $LAND=select_DB('countries');
                        foreach($LAND as $key=>$val){
                            echo"<option value='$val[id]'";
                            if($guest[land_bus]==$val[id])echo" selected";
                            echo">$val[de]</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    Telefonnummer:
                </div>
                <div class="span8">
                    <input type="text" name="telefon_bus" value="<?=$guest[telefon_bus]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    Handynummer:
                </div>
                <div class="span8">
                    <input type="text" name="mobil_bus" value="<?=$guest[mobil_bus]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    Branche:
                </div>
                <div class="span8">
                    <input type="text" name="branche" value="<?=$guest[branche]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    Im Unternehmen tatig seit:
                </div>
                <div class="span8 unternehmen_tatig_box">
                    <input type="text" name="unternehmen_tatig" value="<?=$guest[unternehmen_tatig]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_label_input">
                <div class="span4">
                    In der Branche tatig seit:
                </div>
                <div class="span8 branche_tatig_seit_box">
                    <input type="text" name="branche_tatig" value="<?=$guest[branche_tatig]?>"/>
                </div>
            </div>
            <div class="row-fluid kontaktdaten_box_submit_buttons">
                <button class="btn back_reg" style="display: inline-block;">Zurück</button>
                <button class="btn next pull-right" disabled />Weiter</button>
            </div>
        </div>
    </div>
    </form>
    <?
    }elseif(get_get('step')=='4'){
        $events=select_DB('events',array('active'=>1));
    ?>
    <form class="form_reg" id="4">
    <div class="block_reg_index form_reg form_reg_spezialfelder" id="3">
        <b style="display: block; margin-bottom: 10px; font-size: 1.15em;">Spezialfelder</b>
        <div class="form_reg_index">
            <div class="row-fluid">
                <div class="span7">
                    Für welche Veranstaltung bewerben Sie sich?
                </div>
                <div class="span5 spezialfelder_select_box">
                    <select name="event">
                    <?foreach($events as $value){?>
                        <option value="<?=$value[id]?>" <?if($value[id]==$guest[event])echo 'selected';?>><?=$value[name]?></option>
                    <?}?>
                    </select>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span7">
                    An wem möchten Sie eine Anfrage stellen?
                </div>
                <div class="span5 spezialfelder_select_box">
                    <select name="anfrage" required>
                        <option value="admin" <?if($guest[anfrage]=='admin')echo"selected";?>>an Soren Bauer Events</option>
                        <option value="sponsor" <?if($guest[anfrage]=='sponsor')echo"selected";?>>an die Sponsoren des Events</option>
                        <option value="netzwerk" <?if($guest[anfrage]=='netzwerk')echo"selected";?>>an das eingeladene Netzwerk des Events</option>
                    </select>
                </div>
            </div>
            <div class="pflichtfelder">* Pflichtfelder</div>
            <div class="row-fluid">
                <button class="btn back_reg" style="display: inline-block;">Zurück</button>
                <button class="btn next pull-right " style="margin-top: 20px; float: right;" disabled />Weiter</button>
            </div>
        </div>
    </div>
    </form>
    <?
    }elseif(get_get('step')=='5'){
    ?>
    <form class="form_reg" id="5">
    <div class="block_reg_index form_reg" id="8">
        <b style="display: block; margin-bottom: 10px; font-size: 1.15em;">Fragen</b>
        <div class="form_reg_index">

            <div class="row-fluid">
                <div class="span12">
                    Wie haben Sie von unserer Veranstaltung erfahren?
                </div>
                <div class="fragen_field">
                    <input type="text" name="erfahren" value="<?=$guest[erfahren]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    Warum mochten Sie zu der von Ihnen gewahlten Veranstaltung eingeladen sein?
                </div>
                <div class="fragen_field">
                    <input class="fragen_why" type="text" name="bewerbe" value="<?=$guest[bewerbe]?>" required/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span8">
                    Waren Sie schon einmal auf einer unserer Veranstaltungen?
                </div>
                <div class="fragen_field">
                    <select name="besucht" style="width: 100%;">
                        <option value="JA"<?if($guest[besucht]=='JA')echo"selected";?>>JA</option>
                        <option value="NEIN"<?if($guest[besucht]=='NEIN')echo"selected";?>>NEIN</option>
                    </select>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span8">
                    Mochten Sie Zusendung von Newsletter erhalten?
                </div>
                <div class="fragen_field">
                    <select name="newsletter" class="fragen_subscription" style="width: 100%">
                        <option value="email" <?if($guest[newsletter]=='email')echo"selected";?>>ja, per Email</option>
                        <option value="post" <?if($guest[newsletter]=='post')echo"selected";?>>ja, per Post</option>
                        <option value="no" <?if($guest[newsletter]=='no')echo"selected";?>>nein</option>
                    </select>
                </div>
            </div>
            <div class="pflichtfelder">* Pflichtfelder</div>
            <div class="row-fluid">
                <button class="btn back_reg" style="display: inline-block;">Zurück</button>
                <button class="btn next pull-right" style="margin-top: 20px; float: right;" disabled />Weiter</button>
            </div>
        </div>
    </div>
    <?}?>
    </form>
    </div>
<?if(get_get('step')=='9'){?>
<div class="pop_window activ_guest" style="top: 5%; margin-left: 150px;">
    <b>FaceClub-Team wird Ihre Anfrage bearbeiten und die Antwort auf Ihre Email-Adresse schicken.</b>
</div>
    
    <?}?>
</div>

<script type="text/javascript">
   $(".block_reg_index").alignCenterScreen();
</script>