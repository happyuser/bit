<div class="footer_content">
    <h4>Hilfebereich Face Club</h4>
    <ul class="faq-list">
        <li>Was ist Face Club</li>
        <li>
            <ul>
                <li>Welche Vorteile habe ich mit Face Club?</li>
                <li>Wer ist alles bei Face Club?</li>
                <li>Was kostet Face Club?  </li>
            </ul>
        </li>
        <li>Anmelden und Passwort</li>
        <li>
            <ul>
                <li>Wie melde ich mich an?</li>
                <li>Ich kann mich nicht anmelden – Hilfe!</li>
            </ul>
        </li>
        <li>Mein Face Club Konto</li>
        <li>
            <ul>
                <li>Erste Schritte bei Face Club</li>
                <li>Neuigkeiten</li>
                <li>Privatsphärebereich</li>
                <ul>
                    <li type="square">Wie mache ich bestimmte Informationen unkenntlich?</li>
                    <li type="square">Wie lange und wo werden meine Daten usw. gespeichert?</li>
                    <li type="square">Wer hat Zugriff auf meine Daten?</li>
                    <li type="square">Kann ich mein Face Club Konto löschen?</li>
                </ul>
                <li>Verwaltung des Kontos</li>
                <li>Veranstaltungen</li>
                <ul>
                    <li type="square">Wie erhalte ich meine Einladungen?
                        <ul>
                            <li type="disc">Zusagen</li>
                            <ul>
                                <li type="circle">Wie sage ich zu einer Veranstaltung zu?</li>
                                <li type="circle">Kann ich wieder absagen, nachdem ich zugesagt habe?</li>
                                <li type="circle">Wie schnell muss ich die Einladung beantworten?</li>
                                <li type="circle">Was muss ich auf der Veranstaltung vorzeigen, um Einlass gewährt zu bekommen?</li>
                            </ul>
                            <li type="disc">Absagen</li>
                            <ul>
                                <li type="circle">Wie sage ich zu einer Veranstaltung zu?</li>
                            </ul>
                            <li type="disc">Begleitperson</li>
                            <ul>
                                <li type="circle">anmelden</li>
                                <li type="circle">erfragen</li>
                                <li type="circle">ändern</li>
                                <li type="circle">nachträglich</li>
                            </ul>
                        </ul>
                    </li>
                    <li type="square">Verlinkung zu angenommenen Veranstaltungen</li>
                </ul>
            </ul>
        </li>
        <li>Häufige Fragen</li>
        <li>Neuigkeiten auf Face Club</li>
    </ul>
</div>