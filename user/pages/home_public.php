<body>
<div class="no_activ_body"></div>
<div class="t_wrapper" id="guest">
    <div class="container">
        <div class="row-fluid main_header_guest" id="main_header">
            <div class="span12 t_header">
                <div class="t_header-logo span5">
                    <a href="/user/" style="text-decoration: none;">
                        <span class="t_logo">Face</span>
                        <span>Club</span>
                    </a>
                </div>
                <?if(!$ID_USER){?>
                    <div class="span2 pull-right zuruck_btn">
                        <a class="" href="/">Zurück</a>
                    </div>
                <?}?>
<?
$ARRAY_SUCCESS=array('info_site','registration','events','event','reg_guest','reg_sponsor');
if(array_search($CONTENT,$ARRAY_SUCCESS)===false){ ?>
                <div class="form_login_header pull-right">
                    <form style="margin: 0;" method="post" action="/user/">
                        <ul>
                            <li>
                                <input id="email" type="text" name="mail" placeholder="Email:"/>
                            </li>
                            <li>
                                <input type="checkbox" name="save" id="merken" value="Passwort merken"/>
                                <span for="merken">Passwort merken</span>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <input id="password" type="password" name="user_password" placeholder="Passwort:"/>
                            </li>
                            <li>
                    			<span>
                                    <a href="#">Passwort vergessen?</a>
                                </span>
                            </li>
                        </ul>
                        <ul class="submit_btn">
                            <li>
                                <input class="t_login_header-submit" id="submit" type="submit" value="ANMELDEN"
                                       name="submit"/>
                            </li>
                        </ul>
                    </form>
                    <div id="err" class=""></div>
                </div>
<?}elseif($CONTENT=='registration'){
    ?>
    <ul class="step_registration">
        <li class="step_1 <?echo(get_get('step')=='0')?'active':'';?>"><img src="/images/bew1.png" width="50"></li>
        <li class="step_2 <?echo(get_get('step')=='1')?'active':'';?>"><img src="/images/bew4.png" width="50"></li>
        <li class="step_3"><img src="/images/bew2.png" width="50" /></li>
        <li class="step_5 <?echo(get_get('step')=='5')?'active':'';?>"><img src="/images/bew5.png" width="50" /></li>
    </ul>
    <?
}elseif($CONTENT=='reg_guest'){
    ?>
    <ul class="step_registration">
        <li class="step_1"><img src="/images/bew1.png" width="50" /></li>
        <li class="step_2"><img src="/images/bew4.png" width="50" /></li>
        <li class="step_3 <?echo(get_get('step')>=1 && get_get('step')<=5)?'active':'';?>"><img src="/images/bew2.png" width="50"></li>
        <li class="step_5"><img src="/images/bew5.png" width="50" /></li>
    </ul>
    <?
}?>
            </div>
        </div>
    </div>

    <a href="/" class="prev_page_uber_before_login" title="Zurück" style="display: inline;"></a>
        <div class="t_body">
            <div class="<?=($CONTENT=='events' || $CONTENT=='event')? 'container':'row-fluid'?>">
                <div class="
                <?if($CONTENT=='events' || $CONTENT=='event'){
//                    echo'span9 t_user_body';
                    if(!$ID_USER){
                        echo'span12 t_user_body';
                    }
                    else{
                        echo'span9 t_user_body';
                    }

                }else{
                    echo 'span12 ';
//                    if($CONTENT!='info_site')
//                        echo 't_body';
                }
                ?> t_body_email_bestatigung">
                    <div class="<?=($CONTENT=='events' || $CONTENT=='event')? '':'span12 '?>">
                        <?include( 'pages/'.$CONTENT. '.php')?>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="t_footer">
    <div class="container" id="footer_container" style="text-align: center">
        <ul class="footer_text">
            <li>
                <a href="/user/?page=events" class="no_active_func">Veranstaltungen</a>
            </li>
            <li>
                <a href="/user/?page=info_site&part=uber_fc" id="btn_uber_fc" class="no_active_func">Über FaceClub.de</a>
            </li>
            <li>
                <a href="/user/?page=info_site&part=impressum" id="btn_impressum" class="no_active_func">Impressum</a>
            </li>
        </ul>
    </div>
</div>
</body>
</html>