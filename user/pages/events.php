<?
$type=1;
if(get_get('type')=='close'){
    $type=0;
}
$EVENTS=select_DB('events', array('active'=>$type), array('dates'=>'asc'));
?>
<div class="gray_bcg"></div>
<div class="row-fluid event_conteiner">
    <?if(!$ID_USER){?><a href="/user/?page=main_profil"><span class="prev-page-guest prev-page-guest-events" title="Zur Startseite"></span></a><?}?>
    <ul class="nav nav-pills row-fluid ul_kontakten_top" style="text-align: center;">
        <li class="active"><a href="/user/?page=events&type=open">Kommende Veranstaltungen</a></li>
        <li><a href="/user/?page=events&type=close">Vergangene Veranstaltungen</a></li>
    </ul>
    <div class="list_events">
        <a href="/" class="prev_page_veranstaltungen_before_login" title="Zurück" style="display: inline;"></a>
        <?foreach($EVENTS as $key=>$val){
            $val[images]=json_decode($val[images]);
            ?>
        <a href="/user/?page=event&id=<?=$val[id]?>">
            <div class="li_event row-fluid">
                <?if($val[images][0] && file_exists('../uploads/'.$val[images][0])){?>
                    <div class="span<?if(!$ID_USER and !$ID_SPONSOR){?>3<?} else{?>4<?};?>"><img src="../uploads/<?=$val[images][0]?>"/></div>
                <?}else{?>
                    <div class="span<?if(!$ID_USER and !$ID_SPONSOR){?>3<?} else{?>4<?};?>"><img src="images/noFotoEvent_min.jpg" /></div>
                <?}?>
                <div class="info_event span<?if(!$ID_USER and !$ID_SPONSOR){?>9<?} else{?>8<?};?>">
                    <div class="title_event"><?=$val[name]?></div>
                    <div class="date_event"><?=print_date($val[dates]).' '.$val[city]?></div>
                    <div class="text_event"><p><?=$val[paragraph]?></p></div>
                </div>
            </div>
        </a>
        <?}?>
    </div>
</div>
<script src="js/events.js"></script>