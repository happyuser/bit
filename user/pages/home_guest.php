<body>
<div class="no_activ_body"></div>
<div class="t_wrapper" id="guest">
    <div id="white_bcg"></div>
    <div class="container container_guest_header">
        <div class="row-fluid" id="main_header">
            <div class="t_header-logo t_header_logo_guest span5">
                <span class="t_logo">Face</span>
                <span>Club</span>
            </div>

            <div class="form_login_header pull-right form_login_header_guest">
                <div class="t_btn_back hide" id="btn_back"></div>
                <form style="margin: 0;" class="form_login_guest" method="post" action="/user/">
                    <ul>
                        <li>
                            <input id="email" type="text" name="mail" placeholder="Email:"/>
                        </li>
                        <li>
                            <label class="control-label" for="merken">
                                <input type="checkbox" name="save" id="merken" value="Passwort merken"/>
                                Passwort merken
                            </label>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <input id="password" type="password" name="user_password" placeholder="Passwort:"/>
                        </li>
                        <li>
                            <span>
                                <a href="#" class="send_pass">Passwort vergessen?</a>
                            </span>
                        </li>
                    </ul>
                    <ul class="submit_btn">
                        <li>
                            <input class="t_login_header-submit" id="submit" type="submit" value="ANMELDEN"name="submit"/>
                        </li>
                    </ul>
                </form>
                <div id="err" class="span7">
                    <!--<i id="close-err-window" class="icon-remove-circle"></i>-->
                    <i id="close-err-window">x</i>

                    <p class="text-error"></p>
                </div>
            </div>
        </div>
    </div>

    <div class="t_body">
        <div class="container">
            <div class="row-fluid">
                <div id="main_center">
                    <div class="block_reg_index block_reg_index_einmalige_bewerburg">
                        <div class="title_block">EINMALIGE BEWERBUNG</div>
                        <a href="/user/?page=registration&step=0" class="link-guest-hier">Hier</a>
                    </div>
                    <div class='fc_logo_center'></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="t_footer">
    <div class="container" id="footer_container" style="text-align: center">
        <ul class="footer_text">
            <li>
                <a href="/user/?page=events" class="no_active_func">Veranstaltungen</a>
            </li>
            <li>
                <a href="/user/?page=info_site&part=uber_fc" id="btn_uber_fc" class="no_active_func">Über FaceClub.de</a>
            </li>
            <li>
                <a href="/user/?page=info_site&part=impressum" id="btn_impressum" class="no_active_func">Impressum</a>
            </li>
        </ul>
    </div>
</div>
<!--<div class="wrong-credentials"></div>-->


<div class="modal_edit_p pop_window send_password block_absolute_center block_reg_index" style="top: 12%; margin:0; width: 410px;">
 <div class="edit_profile">
  <div class="edit_profile_title" style="width: 100%; text-align: center;">Tragen Sie bitte Ihre E-mail adresse ein. Sie bekommen dann Ihren Passwort per E-mail zugeschickt</div>
      <div class="form_edit_p">
       <input type="text" name="email_by_pass" placeholder=""/>
       <div class="buttons_block">
            <button id="send_password" style="margin: 0 auto; display: block;">SPEICHERN</button>
       </div>
      </div>
  </form>
 </div>
</div>
    <script type="text/javascript">
        /*$(".t_body").height(function(i,val){
            return val-1;
        });*/

        $(".block_reg_index").alignCenterScreen();
        $(".block_absolute_center").alignCenterScreen();
        $(document).ready(function(){
            var wHeight = $(document).height();
            $('.t_body').css('height',(wHeight - 119) + 'px');

        });
    </script>
</body>