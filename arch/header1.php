<?
session_start();
include_once ('../controllers/functions.php');

?>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>BIT admin</title>
<!-- css --->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/jquery-ui.css" rel="stylesheet" />
        <style>
        img.load_ajax {
          position: absolute;
          top: 160px;
          left: 400px;
          z-index: 999;
          display: none;
        }
        img.load_ajax2 {
          position: absolute;
          top: 40%;
          left: 45%;
          z-index: 999;
          display: none;
        }
        </style>
<!-- script --->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="js/jquery-2.0.0.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="js/datepicker-de.js" type="text/javascript"></script>
        <script src="js/bootstrap-carousel.js" type="text/javascript"></script>
        <script src="js/index.js" type="text/javascript"></script>
		<script src="js/tables.js" type="text/javascript"></script>
		<script src="js/filter.js" type="text/javascript"></script>
        <script src="js/jquery.textchange.min.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="no_activ_body"></div>
    <div class="container-fluid container_fluid_registration">
        <div class="row-fluid menu" style="height: 40px;">
			<? $s = (preg_match('/admin\/(.*\.php)/', $_SERVER['REQUEST_URI'], $a) ? $a[1] : '');?>
            <ul class="menu_header navbar-fixed-top">
                <li class="<?php print ($s == 'mitglieder_registered.php' or $s == 'mitglieder_kandidate.php') ? active : ''; ?>"><a href="mitglieder_registered.php">Mitglieder</a></li>
                <li class="<?php print ($s == 'event_list.php') ? active : ''; ?>"><a href="./event_list.php">Veranstaltung</a></li>
                <li class="<?php print ($s == 'guest_list_eingeladene.php' or $s == 'guest_list_angemeldete.php' or $s == 'guest_list_bestatigte.php' or $s == 'guest_list_kandidaten.php') ? active : ''; ?>"><a href="./guest_list_eingeladene.php">Gästeliste</a></li>
                <li class="<?php print ($s == 'sponsoren.php') ? active : ''; ?>"><a href="./sponsoren.php">Sponsoren</a></li>
				<li class="<?php print ($s=='administration.php') ? active : ''; ?>"><a href="./administration.php">Administration</a></li>
                <li class="archivieren_btn"><div><a href="../admin">Admin</a></div></li>
            </ul>
        </div>
<img class="load_ajax" src="../images/load.gif"/>
<img class="load_ajax2" src="../images/load2.gif" width="90px"/>