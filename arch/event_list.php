<?
include_once ('header1.php');
include_once('../controllers/func_creat_event_list.php');
?>
<!-- css --->
<link href="css/wysiwyg.css" rel="stylesheet" />
<link href="js/external/google-code-prettify/prettify.css" rel="stylesheet" />
<link href="css/jquery-te-1.4.0.css" rel="stylesheet" />

<!-- script --->
<script src="js/event_list.js" type="text/javascript"></script>
<script src="js/registration.js" type="text/javascript"></script>
<script src="js/bootstrap-wysiwyg.js"></script>
<script src="js/external/jquery.hotkeys.js"></script>
<script src="js/external/google-code-prettify/prettify.js"></script>
<script src="js/jquery.textchange.min.js" type="text/javascript"></script>
<script src="js/jquery-te-1.4.0.min.js" type="text/javascript"></script>
    <!--Змінити ПОДІЮ-->
<div class="pop_window pop_window_list_event_modal" id="change_event">
    <form class="form_change_event">
		<div class="control-group">
			<label class="control-label space-right" for="editor">Name der Veranstaltung</label>
			<div class="controls">
				<input type="text" name="name" class="event_list_input">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="city">Stadt</label>
			<div class="controls">
				<input type="text" class="event_list_input" name="city" placeholder="" />
                <div class="list_cities offset2"></div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right type_of_event_title">Type</label>
			<div class="controls">
				<select id="type_of_event" class="type_of_event_red" name="type_of_event">
					<? if(isset($data['select_options'])){echo $data['select_options'];}?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="date_event">Veranstaltungstermine</label>
			<div class="controls myCalendar">
				<input class="event_list_input" name="dates" id="date_event myCalendar" placeholder="" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="description_event">Beschreibung der Veranstaltung</label>
			<div class="controls controls-event_list-modal">
				<textarea name="background"></textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="paragraph">Kurzinformation</label>
			<div class="controls">
				<textarea id="paragraph" class="kurzinformation-textaria" name="paragraph" rows="5" maxlength="300"></textarea>
				<span class="counter counter-kurzinformation"><span class="count">0</span> / 300</span>
			</div>
		</div>
		<div class="control-group control-group-time-event">
			<label class="control-label space-right control-label-time-event" id="label_event_points" for="event_points">Programmpunkte mit Uhrzeiten:</label>
			<div class="point controls" id="points_time">
				<input type="text" class="point_txt" name="points[txt][]" id="description_event_points" placeholder="" />
				<input type="time" class="point_time" name="points[time][]" id="event_points" placeholder="" />
				<button class="close" id="remove_event_point" data-dismiss="alert">&times;</button>
			</div>
			<div class="start_point"></div>
			<div class="controls">
				<button class="btn btn-primary add_new_event_point_first" id="form_change_event"><i class="icon-plus icon-white"></i> Hinzufugen</button>
			</div>
		</div>
		<div class="photos"></div>
		<div class="control-group fotos_container">
			<label class="control-label space-right title_fotos" for="photos">Fotos</label>
			<div class="controls">
				<div class="photo_part" id="photos">
					<div class="controls">
                        <ul class="sortable">
                                
                        </ul>
						<label for="add_new_photo" class="btn btn-primary pull-right">
							<i class="icon icon-plus"></i> Hinzfungen
						</label>
						<input id="add_new_photo" type="file" multiple="true"/>
					</div>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right link-youtube event_list_input" for="youtube_link">Link zur Youtube-Seite</label>
			<div class="controls">
				<input type="text" class="event_list_input" name="youtube" id="youtube_link" placeholder="" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="deadline">Anmeldeschluss</label>
			<div class="controls myCalendar">
				<input type="datetime-local" class="event_list_input" name="deadline" id="deadline" placeholder="" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="main_sposor">Gastgeber</label>
			<div class="controls">
				<select class="event_list_input" id="main_sposor" name="main_sponsor">
				</select>
                <input type="hidden" name="last_main_sponsor"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="sub_sposor">Ausstatter-Partner</label>
			<div class="controls">
				<select class="event_list_input" id="sub_sposor" name="sub_sponsor">
				</select>
                <input type="hidden" name="last_sub_sponsor"/>
			</div>
		</div>
		<div class="control-group control-group-weitere-sponsoren">
			<label class="control-label space-right" id="label_rest_sponsor" for="rest_sponsor">Weitere Sponsoren:</label>
			<div class="controls">
				<select class="event_list_input" id="rest_sponsor" name="rest_sponsor[]">
				</select>

				<button class="btn btn-primary add_new_event_point_red" id="add_new_sponsor"><i class="icon-plus icon-white"></i> Hinzufugen</button>
                <input type="hidden" name="last_rest_sponsor"/>
			</div>
		</div>
		<div class="row-fluid event_submit_box">
			<div class="bottom-box-btn">
				<button class="btn" title="Abbrechen" id="close_pop_window">Abbrechen</button>
				<button class="btn" title="Speichern" id="btn_change_event">Speichern</button>
                <span class="label label-success status_message_ok">Gespeichert <i class="icon-ok icon-white"></i></span>
			</div>
		</div>
	</form>
</div>

    <!--НОВА ПОДІЯ-->
<div class="pop_window pop_window_list_event_modal_new" id="new_member" for="events">
	<form class="new_event">
		<div class="control-group">
			<label class="control-label space-right"  for="name_even">Name der Veranstaltung</label>
			<div class="controls">
				<input type="text" class="event_list_input" id="name_event" name="name" placeholder="" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="city">Stadt</label>
			<div class="controls">
				<input type="text" class="event_list_input" id="city" name="city" placeholder="" />
                <div class="list_cities offset2"></div>
			</div>
		</div>
        <div class="control-group new_event_type">
			<label class="control-label space-right" for="city">Type</label>
			<div class="controls">
				<select id="type_of_event" name="type_of_event">
					<? if(isset($data['select_options'])){echo $data['select_options'];}?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="date_event">Veranstaltungstermine</label>
			<div class="controls myCalendar">
				<input class="event_list_input" id="date_event myCalendar" name="dates" placeholder="" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="description_event">Beschreibung der Veranstaltung</label>
			<div class="controls controls-event_list-modal">
				<textarea name="background" id="description_event"></textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="paragraph">Kurzinformation</label>
			<div class="controls">
				<textarea id="paragraph" class="kurzinformation-textaria" name="paragraph" rows="4" maxlength="300"></textarea>
				<div class="counter counter-kurzinformation"><span class="count">0</span> / 300</div>
			</div>
		</div>
		<div class="control-group">
			<div>
				<label class="control-label space-right" id="label_event_points" for="event_points">Programmpunkte mit Uhrzeiten</label>
				<div class="controls point" id="points_time">
					<input type="text" class="point_txt event_list_input" name="points[txt][]" id="description_event_points" placeholder="" />
					<input type="time" class="point_time event_list_input" name="points[time][]" id="event_points" placeholder="" />
				</div>
			</div>
			<div class="controls add_new_event_point_first_box">
				<button class="btn btn-primary hinzufugen-event_list-btn add_new_event_point_first" title="Hinzufugen" id="new_event"><i class="icon-plus icon-white"></i> Hinzufugen</button>
			</div>
			<div class="start_point"></div>
		</div>
		<div class="control-group fotos_box">
			<label class="control-label space-right title_fotos" for="photos">Fotos</label>
			<div class="controls">
				<div class="photo_part" id="photos">
					<div class="controls">
						<ul class="sortable">
                            
                        </ul>
						<label for="new_event_add_new_photo" title="Hinzufugen" class="btn btn-primary pull-right new_event_add_new_photo">
							<i class="icon icon-plus"></i> Hinzfungen
						</label>
						<input id="new_event_add_new_photo" type="file" multiple/>
					</div>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="youtube_link">Link zur Youtube-Seite</label>
			<div class="controls">
				<input type="text" class="event_list_input" name="youtube" id="youtube_link" placeholder="" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="deadline">Anmeldeschluss</label>
			<div class="controls myCalendar">
				<input type="datetime-local" class="event_list_input" id="deadline" name="deadline" placeholder="" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="main_sponsor">Gastgeber</label>
			<div class="controls">
				<select class="gastgeber_new_events" id="main_sponsor" name="main_sponsor">
					<? if(isset($data['pre_sponsor_name'])){echo $data['pre_sponsor_name'];}?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label space-right" for="sub_sponsor">Ausstatter-Partner</label>
			<div class="controls">
				<select class="ausstatter_partner_new_events" id="sub_sponsor" name="sub_sponsor">
					<? if(isset($data['pre_sponsor_name'])){echo $data['pre_sponsor_name'];}?>
				</select>
			</div>
		</div>
		<div class="control-group control-group-weitere-sponsoren_new_event">
			<label class="control-label space-right" id="label_rest_sponsor1" for="rest_sponsor">Weitere Sponsoren</label>
			<div class="controls new_sponsor" id="new_sponsor">
				<select id="rest_sponsor" name="rest_sponsor[]">
					<? if(isset($data['pre_sponsor_name'])){echo $data['pre_sponsor_name'];}?>
				</select>
				<button class="close" id="remove_spansor" data-dismiss="alert">&times;</button>
			</div>
			<div class="controls">
				<button class="btn btn-primary hinzufugen-event_list-btn hinzufugen_event_list_btn_second" title="Hinzufugen" id="add_new_sponsor1"><i class="icon-plus icon-white"></i> Hinzufugen</button>
			</div>
		</div>
		<div class="row-fluid">
			<div class="bottom-box-btn">
				<button class="btn" title="Abbrechen" id="close_pop_window">Abbrechen</button>
				<button class="btn" title="Speichern" id="btn_new_event">Speichern</button>
                <span class="label label-success status_message_ok">Gespeichert <i class="icon-ok icon-white"></i></span>
			</div>
		</div>
	</form>
</div>

    <div class="pop_window" id="overflow">

        <button class="close_window pull-right">
            <i class="icon-remove icon-white"></i>
        </button>

        <h6 id="overtext"></h6>
    </div>

    <div class="pop_window" id="programpoints">

    <button class="close_window pull-right">
        <i class="icon-remove icon-white"></i>
    </button>

    <table class="table table-bordered table_bordered_event_list">
        <thead>
            <tr>
                <th>Programmpuntk</th>
                <th>Uhrzeit</th>
            </tr>
        </thead>
        <tbody id="program">

        </tbody>
    </table>

    </div>

    <div class="pop_window" id="carousel">

        <button class="close_window pull-right">
            <i class="icon-remove icon-white"></i>
        </button>

        <div id="myCarousel" class="carousel slide" data-interval="false" data-ride="carousel">
            <!-- Carousel indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">


            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>

    </div>


<div class="container-fluid">

    <div class="input-append set_input_without_margin input_append_event_list">
        <input type="text" class="search-query" value="<?=$_GET[search]?>"/>
        <button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
        <div class="search_clear"><i class="icon-remove"></i></div>
    </div>

    <div class="pagination pagination_event_list">
        <ul>
            <?if(isset($data['pages_number'])) echo $data['pages_number'];?>
        </ul>
    </div>

    <div class="row-fluid header">
        <div class="span8">
            <button class="btn" id="new" title="Neue Veranstaltung anlegen" for="events"><i class="icon-file"></i></button>
            <button class="btn change_event" title="Veranstaltung bearbeiten"><i class="icon-pencil"></i></button>
            <?if($_SESSION['status']=='superadmin'){?>
                <button class="btn" id="drop" title="Veranstaltung löschen" for="events"><i class="icon-remove"></i></button>
            <?}?>
            <button class="btn" id="without_filters" title="Aktivieren den filter zu deaktivieren"><i class="icon-filter no_filters"></i></button>
        </div>
    </div>
   
     <?if(!isset($data['pages_number'])){echo $data['empty'];}
            else{?>

	<div class="row-fluid body view_table">
		<form class="table table-event_list_admin">
			<div class="no_sroll">
				<table class="table table-bordered">
					<thead>
						<? if(isset($data['header'])){echo $data['header'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table'])){echo $data['table'];}?>	
					</tbody>
				</table>
			</div>
			<div class="scroll scroll_verantaltung">
				<table class="table table-bordered">
					<thead>
						<? if(isset($data['header_slider'])){echo $data['header_slider'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table_slider'])){echo $data['table_slider'];}?>	
					</tbody>
				</table>
			</div>
		</form>
	</div>
<?php } ?>
</div>
