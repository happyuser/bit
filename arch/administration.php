<?
include_once('header1.php');
include_once('../controllers/func_creat_administration.php');
?>
<script src="js/administration.js" type="text/javascript"></script>
<script src="js/tables.js" type="text/javascript"></script>

    <div class="pop_window pop_window_administration" id="new_member" for="editor">
    <form class="new_member" for="editor">
        <div class="row-fluid">
            <div class="row-fluid">
                <div class="row-fluid">
                    <div class="span5"></div>  
                </div>
                <div class="row-fluid">
                    <div class="span4 offset2">Status</div>
                    <div class="span5">
                        <select class="select_status" name="status">
                            <option value="superadmin">Superadmin</option>
                            <option value="team">Team</option>
                            <option value="praktikanten">Praktikanten</option>
                            <option value="hostess">Hostess</option>
                        </select>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span4 offset2">Email</div>
                    <div class="span5">
                        <input class="input_admin_administrator" type="text" name="email" value=""/>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span4 offset2">Passwort</div>
                    <div class="span5">
                        <input class="input_admin_administrator" type="text" name="sha_password" value=""/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 offset2">Vorname</div>
                    <div class="span5">
                        <input class="input_admin_administrator" type="text" name="last_name" value=""/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 offset2">Name</div>
                    <div class="span5">
                        <input class="input_admin_administrator" type="text" name="first_name" value=""/>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span5 offset6 administrator_submit_box">
                    <button class="btn" title="Abbrechen" id="close_pop_window">Abbrechen</button>
                    <button class="btn" title="Speichern" id="btn_new_member" for="editor">Speichern</button>
                </div>
            </div>
        </div>
    </form>
    </div>


<div class="container-fluid">
	<div class="input-append set_input_without_margin input_append_event_list">
		<input type="text" class="search-query" value="<?=$_GET[search]?>"/>
		<button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
		<div class="search_clear"><i class="icon-remove"></i></div>
	</div>

	<div class="pagination pagination_event_list">
		<ul>
			<?if(isset($data['pages_number'])) echo $data['pages_number'];?>
		</ul>
	</div>
	<div class="row-fluid header">
		<div class="span8">
			<button class="btn btn_administration_php" title="Neues Mitglied anlegen" id="new" for="editor"><i class="icon-file"></i></button>
			<button class="btn save_admin btn_administration_php" title="Behalten"><i class="icon-ok"></i></button>
			<button class="btn btn_administration_php" title="Entfernen" for="editor" id="drop"><i class="icon-minus"></i></button>
			<button class="btn btn_administration_php" id="without_filters" title="Aktivieren den filter zu deaktivieren"><i class="icon-filter no_filters"></i></button>
			<span class="label label-success status_message_ok btn_administration_php">Gespeichert <i class="icon-ok icon-white"></i></span>
		</div>
	</div>    
<br />
            
    <?php  if(!isset($data['pages_number'])){echo $data['empty'];}
            else{    ?>

	<div class="row-fluid body view_table">
		<form class="table">
			<div class="no_sroll no-scroll-mitglieder">
				<table class="table table-bordered">
					<thead>
						<? if(isset($data['header'])){echo $data['header'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table'])){echo $data['table'];}?>	
					</tbody>
				</table>
			</div>
			<div class="scroll scroll_administrator">
				<table class="table table-bordered table_bordered_administration">
					<thead>
						<? if(isset($data['header_slider'])){echo $data['header_slider'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table_slider'])){echo $data['table_slider'];}?>	
					</tbody>
				</table>
			</div>
		</form>
	</div>
<?php } ?>
</div>