<?
include ('header1.php');
include_once('../controllers/func_creat_gastelist.php');
?>
<script src="js/guest.js" type="text/javascript"></script>

<div class="pop_window pop_window_list_user" id="list_user">
    <div class="row-fluid" style="float: left;width: 163px;margin-left: 20px;">
		<input class="btn" id="close_pop_window" type="reset" value="Schliessen"/>
        <button class="btn" id="check_member_ok">OK</button>
    </div>
    Check All <input type="checkbox" class="check_all" style="margin-top: -2px;"/>
    
    <div class="input-append">
		<input type="text" class="search-query" value="<? if(isset($_GET['search'])){echo $_GET['search'];}?>"/>
		<button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
		<div class="search_clear"><i class="icon-remove"></i></div>
    </div>            
	<div class="pagination" style="float: right;margin: 0 0 0 15px;">
		<ul>
			<?if(isset($data['pages_modal'])) echo $data['pages_modal'];?>
		</ul>
	</div>          
	<div class="row-fluid body view_table view_table_list">
		<form class="table">
			<div class="no_sroll">
				<table class="table table-bordered no_sroll_bordered">
					<thead>
						<? if(isset($data['header_modal'])){echo $data['header_modal'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table_modal'])){echo $data['table_modal'];}?>	
					</tbody>
				</table>
			</div>
			<div class="scroll">
				<table class="table table-bordered">
					<thead>
						<? if(isset($data['header_slider_modal'])){echo $data['header_slider_modal'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table_slider_modal'])){echo $data['table_slider_modal'];}?>	
					</tbody>
				</table>
			</div>
		</form>
    </div>
</div>

<div class="pop_window" id="overflow" style="display: none">
    <button class="close_window pull-right" style="margin-right: -10px;margin-top: -10px">
        <i class="icon-remove icon-white"></i>
    </button>
    <h6 id="overtext" style="text-align:center "></h6>
</div>

<div class="pop_window" id="photo_window" style="display: none">
    <button class="close_window pull-right" style="margin-right: -10px;margin-top: -10px;margin-bottom: 2%">
        <i class="icon-remove icon-white"></i>
    </button>
    <h6 style="text-align:center ">FOTO</h6>
    <img id="pop_foto" title="your_photo" style=" display: block; margin: 0 auto; max-width: 600px ">
</div>

<div>
    <div style="margin:20px 20px; display: inline-block;">
        <span style="margin:0 20px 5px 0">Veranstaltung</span>
        <select class="event" id="event_select" name="events">
			<?if(isset($data['select'])) echo $data['select'];?>
        </select>
    </div>
	<div class="input-append set_input">
		<input type="text" class="search-query" value="<? if(isset($_GET['search'])){echo $_GET['search'];}?>"/>
		<button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
		<div class="search_clear"><i class="icon-remove"></i></div>
	</div>
</div>
<div class="pagination" style="float: right;margin: 0 0 0 15px;">
	<ul>
		<?if(isset($data['pages_number'])) echo $data['pages_number'];?>
	</ul>
</div>
<div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="/admin/guest_list_eingeladene.php">Eingeladene</a></li>
		<li><a href="/admin/guest_list_angemeldete.php">Angemeldete</a></li>
		<li><a href="/admin/guest_list_bestatigte.php">Bestätigte</a></li>
		<li><a href="/admin/guest_list_kandidaten.php">Kandidaten</a></li>
	</ul> 
	<div class="tab-content">
		<div class="row-fluid header" style="margin-top: 10px">
	        <div class="span8">
	            <button class="btn" id="without_filters" title="Filter an/ausschalten" style="margin: 2px; float: left;"><i class="icon-filter no_filters"></i></button>
	        </div>
	    </div>
        <div class="tab-pane active" id="tab1">
			<div class="row-fluid body view_table">
				<form class="table">
					<div class="no_sroll">
						<table class="table table-bordered">
							<thead>
								<? if(isset($data['header'])){echo $data['header'];}?>		
							</thead>
							<tbody>
								<? if(isset($data['table'])){echo $data['table'];}?>	
							</tbody>
						</table>
					</div>
					<div class="scroll scroll_gasteliste">
						<table class="table table-bordered">
							<thead>
								<? if(isset($data['header_slider'])){echo $data['header_slider'];}?>		
							</thead>
							<tbody>
								<? if(isset($data['table_slider'])){echo $data['table_slider'];}?>	
							</tbody>
						</table>
					</div>
                    <div class="hinzufuegen_entfernen_box">
                        <button class="btn" title="Hinzufuegen" id="view_list" style="background: #ddd;">Hinzufügen</button>
                        <?if($_SESSION['status']=='superadmin'){?>
                            <button class="btn" title="Entfernen" id="drop" for="guest_list" style="margin: 0; float: initial;">Entfernen</button>
                        <?}?>
                    </div>
				</form>
			</div>
        </div>
    </div>
</div>

