<?
include_once('header1.php');
include_once('../controllers/func_creat_mitglieder.php');
?>

<script src="js/calendar.js" type="text/javascript"></script>
<script src="js/registration.js" type="text/javascript"></script>

<div class="row-fluid menu">
    <div class="input-append set_input_without_margin input_append_mitglieder_registered">
        <div class="input-append"></div>
        <input type="text" class="search-query" value="<? if(isset($_GET['search'])){echo $_GET['search'];}?>"/>
        <button type="submit" class="btn btn_search" id="appendedInputButtons">Suche</button>
        <div class="search_clear"><i class="icon-remove"></i></div>
    </div>
    <div class="pagination pagination_mitglieder_registered">
        <ul>
            <?if(isset($data['pages_number'])) echo $data['pages_number'];?>
        </ul>
    </div>
	<ul class="sub_menu_header nav-tabs">
		<li class="<?php print ($s == 'mitglieder_registered.php') ? active : ''; ?>"><a href="./mitglieder_registered.php">Mitglieder</a></li>
		<li class="<?php print ($s == 'mitglieder_kandidate.php') ? active : ''; ?>"><a href="./mitglieder_kandidate.php">Bewerber</a></li>
		<li class="<?php print ($s == 'mitglieder_approve.php') ? active : ''; ?>"><a href="./mitglieder_approve.php">Geänderte Mitglieder</a></li>
	</ul>
</div>
<div class="pop_window" id="vita" style="display: none">
    <button class="close_window pull-right" style="margin-right: -10px;margin-top: -10px">
        <i class="icon-remove icon-white"></i>
    </button>
    <h6 style="text-align: center">Berusvita</h6>
    <table class="table table-bordered" id="vita" style="background: #ffffff">
        <thead>
			<tr>
				<th>Seit/Bis</th>
				<th>Enternehmen</th>
				<th>Position</th>
				<th>Funktion</th>
			</tr>
        </thead>
        <tbody id="vita_table_body">
        </tbody>
    </table>
</div>

<div class="pop_window" id="overflow" style="display: none">
	<button class="close_window pull-right" style="margin-right: -10px;margin-top: -10px">
		<i class="icon-remove icon-white"></i>
	</button>
	<h6 id="overtext" style="text-align:center "></h6>
</div>

<div class="pop_window" id="photo_window" style="display: none">
	<button class="close_window pull-right" style="margin-right: -10px;margin-top: -10px;margin-bottom: 2%">
		<i class="icon-remove icon-white"></i>
	</button>
	<h6 style="text-align:center ">FOTO</h6>
	<img id="pop_foto" title="your_photo" style="display: block; margin: 0 auto; max-width: 600px;" />
</div>

<div class="pop_window pop_window_mitglieder_registered_change_member" id="change_member" style="display: none;">
    <form class="form_change_member" enctype="multipart/form-data" action="ajax/update_member.php" method="post">
		<div class="row-fluid form_change_member_modal" style="overflow: auto;height: 400px;">
			<? if(isset($data['change_member'])){echo $data['change_member'];}?>	
		</div>
        <div class="row-fluid mit_registered_submit_box">
			<input class="btn" id="close_pop_window" type="reset" value="Abbrechen"/>
            <input class="btn" id="member" type="submit" id="btn_change_member" value="Speichern"/>
        </div>
	</form>
</div>

<div class="pop_window pop_window_mitglieder_registered" id="new_member" for="member" style="display: none;">
    <form class="new_member" for="member" enctype="multipart/form-data" action="ajax/new_member.php" method="post">
        <div class="row-fluid" style="overflow: auto;height: 400px;">
			<? if(isset($data['change_member'])){echo $data['change_member'];}?>	
        </div>
        <div class="row-fluid mit_registered_submit_box">
			<input class="btn" title="Abbrechen" id="close_pop_window" type="reset" value="Abbrechen"/>
            <input class="btn" title="Speichern" id="member" type="submit" id="btn_change_member" value="Speichern"/>
        </div>

    </form>
</div>

<div class="pop_window" id="history_user" style="display: none;">
	<table class="table table-bordered table_bordered_history_user">
		<thead>
			<th>
                Typ
                <div></div>
            </th>
			<th>
                Alter Wert
                <div></div>
            </th>
			<th>
                Neuer Wert
                <div></div>
            </th>
			<th>
                Datum
                <div></div>
            </th>
			<th>
                Messadge
                <div></div>
            </th>
			<th>
                Status
                <div></div>
            </th>
			<th>
                Feld
                <div></div>
            </th>
		</thead>
		<tbody>
			<tr class="start_history"></tr>
		</tbody>
	</table>
	<div class="row-fluid">
			<input class="btn" id="close_pop_window" type="reset" value="Abbrechen"/>
	</div>
</div>

<div class="container-fluid">
    <div class="row-fluid header">
        <div class="span8">
            <button class="btn" for="member" title="Neues Mitglied anlegen" id="new" style="margin: 2px; float: left;"><i class="icon-file"></i></button>
            <button class="btn change_member change_member_mitglied" title="Mitglied bearbeiten" id="change_member" alt="change member" style="margin: 2px; float: left;"><i class="icon-pencil"></i></button>
            <?if($_SESSION['status']=='superadmin'){?>
                <button class="btn" title="Mitlied löschen" id="drop_user" style="margin: 2px; float: left;"><i class="icon-remove"></i></button>
<!---                <button class="btn" title="Entfernen" id="drop_user" style="margin: 2px; float: left;"><i class="icon-minus"></i></button> -->
            <?}?>
            <button class="btn" id="exportxls" title="Export in die Excel Datei" type="submit" ><i class="icon-share-alt"></i></button>
            <label class="btn" title="Datei hochladen" for="excel_import" style="margin: 2px; float: left;"><i class="icon-share"></i></label>
            <input type="file" id="excel_import" style="display: none;"/>
            <button class="btn" id="history" title="Änderungsgeschichte" style="margin: 2px; float: left;"><i class="icon-list-alt"></i></button>
            <button class="btn" id="without_filters" title="Filter an/ausschalten" style="margin: 2px; float: left;"><i class="icon-filter no_filters"></i></button>
            <div class="link_excel_file"></div>
            <a class="exportxls" href="full_table" style="display: none;">Die ganze Tabelle</a>
            <a class="exportxls" href="selected" style="display: none;">Nur ausgewählte Mitglieder</a>
        </div>
    </div>

     <?php  if(!isset($data['pages_number'])){echo $data['empty'];}
            else{    ?>

	<div class="row-fluid body view_table">
		<form class="table">
			<div class="no_sroll no-scroll-mitglieder">
				<table class="table table-bordered">
					<thead>
						<? if(isset($data['header'])){echo $data['header'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table'])){echo $data['table'];}?>	
					</tbody>
				</table>
			</div>
			<div class="scroll scroll-mitglieder">
				<table class="table table-bordered table_width_cell">
					<thead>
						<? if(isset($data['header_slider'])){echo $data['header_slider'];}?>		
					</thead>
					<tbody>
						<? if(isset($data['table_slider'])){echo $data['table_slider'];}?>	
					</tbody>
				</table>
			</div>
		</form>
	</div>
<?php } ?>
</div>
</body>
</html>