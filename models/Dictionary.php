<?php 

/*
 * Dictorionary class
 * 
 * Клас для доступу до словників, які знаходяться в папці dictionary
 * напр., dictionary/de.dict dictionary/en.dict dictionary/ua.dict
 * 
 * доступ до кожної фрази в коді php здійснюється за ключем масива, напр.,
 * $dict['submit']
 *
 * 
 * методи:
 * __construct($lang_name,$secondary_langs=array());
 * конструктор класу
 * 	$lang_name - основна мова
 *  $secondary_langs - масив "заміняючих" мов - якщо фрази немає в словнику
 *  основної мови, то пошук здійснюється в словниках інших мов
 *  якщо ж фрази немає в жодному зі словників, то здійснюється вивід ключа з 
 *  префіксом dict:
 *  
 *  модифікація словника програмними засобами не підтримується
 * 
 * 
 */

if (!defined('DICT_DIR')) define('DICT_DIR',dirname(__FILE__).'/../dictionary');

class Dictionary implements ArrayAccess
{
	
	private $_lang_name;
	private $_secondary_langs=array();
	
	private $_dict;
	
	public function __construct($lang_name,$secondary_langs=array())
	{
		$filename = DICT_DIR.'/'.$lang_name.'.dict';
		if (file_exists($filename)) {
			$dict0 = file($filename);
			foreach ($dict0 as $line) {
				list($key,$value) = preg_split('/\s*=\s*/',$line);
				$this->_dict[$key] = $value;
			}
		}
		$this->_lang_name = $lang_name;
		$this->_secondary_langs = $secondary_langs;
	}
	
	public function offsetExists ( $offset )
	{
		return true;
	}
	
	public function offsetGet ( $offset )
	{
		if (!empty($this->_dict[$offset])) {
			return $this->_dict[$offset];
		}
		foreach ($this->_secondary_langs as $sec_lang) {
			$filename = DICT_DIR.'/'.$sec_lang.'.dict';
			if (!file_exists($filename)) continue;
			$dict0 = file($filename);
			foreach ($dict0 as $line) {
				list($key,$value) = preg_split('/\s*=\s*/',$line);
				if ($key!=$offset) continue;
				return $value;
			}
		}
		return 'dict:'.$offset;
	}
	
	public function offsetSet ( $offset , $value )
	{
		throw new Exception("Setting dictionary value is not allowed");
	}
	
	public function offsetUnset ( $offset )
	{
		throw new Exception("Unsetting dictionary value is not allowed");
	}
}

?>