<?php

/*
 * Модель User_Member
 * Таблиця member
 * Клас для маніпулювання даними зареєстрованого користувача
 *
 * Конструктор 
 * User::User() - без параметрів
 *
 * функція обмеження вибірки WHERE
 * User::filter(array $ids) - додати фільтр для рядків
 * де $ids - асоціативний масив $key=>$value, де $key - назва колонки, $value - значення колонки
 * або
 * User::filter($ids)
 * де $ids - значення колонки 'id' через кому
 *
 * метод clearFilters() - зняти всі фільтри
 *
 *
 * властивість $paginationCount - об'єктів на сторінку
 * властивість $pagesCount - тільки для читання - кількість сторінок при заданому $paginationCount 
 * властивість $pageNumber - виставити номер сторінки для SELECT
 *
 * колонки доступні як властивості як для читання, так і для зберігання
 * метод update() оновлює значення колонок у відфільтрованих рядках
 * метод insert() вставляє новий рядок у таблицю
 * 
 * 
 *
 * метод getAll() - повернути масив рядків (обмежений фільтром та пагінацією)
 *
 * специфічні методи User:
 * filterChangedToApprove() - фільтрувати лише рядки, в яких відбулися зміни
 * getAllNewValues() - повернути масив рядків з неапрувнутими значеннями
 *   - рядки співпадають зі значеннями, повернутими getAll
 *   - там, де значення не змінилися - повертаються null value
 * approve() - апрувнити зміни для відфільтрованих рядків (при відсутності фільтрації виникає помилка)
 * decline() - відмінити зміни для відфільтрованих рядків (при відсутності фільтрації виникає помилка)
 *
 *
 *
 *
 */

require_once(dirname(__FILE__).'/User.php');

class User_Member extends User
{
	protected $name = 'member';
	
	public function filterChangedToApprove()
	{
		$dbConn = self::getDbConn();
		$query = 'SELECT member_id as id FROM member_history WHERE status="new"';
		
		$ids = $dbConn->query($query)->fetchAll();
		$n = count($ids);
		for ($i=0;$i<$n;$i++) {
			if ($i>0) $res .= ' OR ';
			$res .= 'id='.$ids[$i]['id'];
		}
		if (!empty($res)) $res = ' AND ('.$res.')';
		if (empty($this->where))
			$this->where = ' WHERE 1';
		$this->where .= $res;
	}
	
	public function getAllNewValues()
	{
	}
	
	public function approve()
	{
	}
	
	public function decline()
	{
	}	
	
}

// TESTING

//if (__FILE__)

$um = new User_Member();
$um->filter('5,10,12');
$um->setPaginationCount(5);
$um->setPageNumber(1);
//$um->filterChangedToApprove();
$data = $um->getAll();
?><pre><?php
var_dump($data);


?>