<?php 

// захищаємо файл від прямого доступу
// а взагалі весь код перенесений в процедурну версію роботи із сесіями
// session.php
if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
	ini_set('display_errors','0');
	require_once '../exception/ESecurity.php';
	throw new ESecurity();
}

require_once dirname(__FILE__).'/DBModel.php';

class MySessionHandler implements SessionHandlerInterface
{
	
	public function close()
	{
		// To realize	
	}
	
	public function destroy($session_id)
	{
		// To realize
	}
	
	public function gc($maxlifetime)
	{
		// To realize
	}
	
	public function open($save_path, $name)
	{
		// To realize
	}
	
	public function read($id)
	{
		$dbConn = DBModel::getDbConn();
		$pdoQ = $dbConn->prepare('SELECT * FROM session_editor WHERE session_id=:session_id
		AND ip_address=:ip_address AND user_agent_hash=SHA1(:user_agent)');
		$row = $pdoQ->execute(array(
				'session_id'=>$id,
				':ip_address'=>!empty($_SERVER['HTTP_X_REAL_IP']) 
					? $_SERVER['HTTP_X_REAL_IP']
					: $_SERVER['REMOTE_ADDR'],
				'user_agent'=>$_SERVER['HTTP_USER_AGENT']
			))->fetchRow(0);
		if (empty($row)) {
			return serialize(array(
				'editor_id'=>NULL
				));		
		}
		return serialize(array(
			'editor_id'=>$row['editor_id']
		) + unserialize($data) );
		
	}	
	
	public function write($id,$data)
	{
		$data = unserialize($data);
		if (!is_array($data)) {
			throw new Exception();
		}
		if (!empty($data['editor_id'])) {
			$editor_id = $data['editor_id'];
			unset($data['editor_id']);
		} else {
			$editor_id = NULL;
		}
		unset($data['editor']);
		$data = serialize($data);
		
		$dbConn = DBModel::getDbConn();
		
		$pdoQ = $dbConn->prepare('SELECT COUNT(*) FROM session_editor WHERE session_id=:session_id
		AND ip_address=:ip_address AND user_agent_hash=SHA1(:user_agent)');
		$count = $pdoQ->execute(array(
				'session_id'=>$id,
				':ip_address'=>!empty($_SERVER['HTTP_X_REAL_IP']) 
					? $_SERVER['HTTP_X_REAL_IP']
					: $_SERVER['REMOTE_ADDR'],
				'user_agent'=>$_SERVER['HTTP_USER_AGENT']
			))->fetchColumn(0);
		if ($count==1) {
			$pdoQ = $dbConn->prepare('UPDATE session_editor SET session_id=:session_id,  data=:data WHERE session_id=:session_id');
			$res = $pdoQ->execute(array(':session_id'=>$id,':data'=>$data));
		} elseif (count==0) {
			$pdoQ = $dbConn->prepare(
					'INSERT INTO session_editor SET 
					session_id=:session_id,
					ip_address=:ip_address,
					user_agent=:user_agent,
					user_agent_hash=SHA1(:user_agent),
					editor_id=:editor_id,
					data=:data,
					time_first = NOW(); 
					'
					);
			session_regenerate_id();			
			$new_sessionid = session_id();
			
		} else {
			throw new Exception("Session getting error");
		}
	}	


}


?>