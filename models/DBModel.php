<?php


/*
 * базовий клас моделі доступу до таблиці в базі даних
 * Клас для маніпулювання даними
 *
 * Конструктор 
 * DBModel::DBModel() - без параметрів
 *
 * функція обмеження вибірки WHERE
 * DBModel::filter(array $ids) - додати фільтр для рядків
 * де $ids - асоціативний масив $key=>$value, де $key - назва колонки, $value - значення колонки
 * або
 * DBModel::filter($ids)
 * де $ids - значення колонки 'id' через кому
 *
 * метод clearFilters() - зняти всі фільтри
 *
 *
 * властивість $paginationCount - об'єктів на сторінку
 * властивість $pagesCount - тільки для читання - кількість сторінок при заданому $paginationCount 
 * властивість $pageNumber - виставити номер сторінки для SELECT
 *
 * колонки доступні як властивості як для читання, так і для зберігання
 * метод update() оновлює значення колонок у відфільтрованих рядках
 * метод insert() вставляє новий рядок у таблицю
 * 
 * 
 *
 * метод getAll() - повернути масив рядків (обмежений фільтром та пагінацією)
 *
 *
 *
 *
 *
 */

//require_once(dirname(__FILE__).'/TAccessor.php');
require_once dirname(__FILE__).'/../conf.php' ;

// працює лише коли є первинний чифровий ключ id
abstract class DBModel
{
	// статична змінна - доступ до бази	
	static protected $dbConn;
	// назва таблиці
	protected $name;
	// масив ідентифікаторів, який використовється при sql-пошуку в таблиці
	protected $whereIds = array();
	// додаткові WHERE-умови для SELECT`у
	protected $where = '';
	// LIMIT-секція запиту
	protected $limit = '';
	// значення колонок рядка
	protected $properties = array();
	
	//use TAccessor;
	
	// отримати доступ - якщо ще немає з'єднання то встановити його
	static public function getDbConn()
	{
		if (!empty(self::$dbConn)) return self::$dbConn;
		self::$dbConn = new PDO($GLOBALS['pdo_conf']['addr'], $GLOBALS['pdo_conf']['user'], $GLOBALS['pdo_conf']['pass']);
		self::$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if (!empty(self::$dbConn)) return self::$dbConn;
	}
	
	// фільтрувати (вибрати) лише ті рядки які мають задані властивості колонок - $ids
	public function filter($ids=array())
	{
		// тест на помилку		
		if (!is_array($ids) and !is_int($ids) and !preg_match('/\d+(,\d+)+/',$ids)) {
			throw new Exception("Incorrect id`s for DBModel::filter method");		
		}
		// якщо $ids - ціле число, то поправити
		if (is_int($ids)) $ids = array('id'=>$ids);
		// якщо $ids - вираз виду 14,16,299 - то розпарсити цей вираз і додати у WHERE частину
		// вираз виду (id=14 OR id=16 OR id=299)
		if (is_string($ids) and preg_match('/\d+(,\d+)+/',$ids)) {
			$ids_tmp = preg_split('/,/',$ids);
			$where_add = '(id=' . implode(' OR id=',$ids_tmp) .')';
			if (empty($this->where)) {
				$this->where = 'WHERE '.$where_add;
			} else {
				$this->where .= ' AND '.$where_add;
			}
			// очистити $ids
			$ids = array();
		}
		// обчилити WHERE частину
		foreach ($ids as $key=>$value) {
			if (empty($this->where)) {
				$this->where = 'WHERE ';
			} else {
				$this->where .= ' AND ';
			}
			$this->where .= '`'.$key.'`="'.addslashes($value).'"';		
		}
		$this->whereIds += $ids;
	}
	
	// пагінація запитів
	// кількість апитів на сторінку
	private $_paginationCount = null;
	// кількість сторінок - обчислюється
	private $_pagesCount = null;	
	public function getPaginationCount()
	{
		return $this->_paginationCount;
	}
	// встановлення кількості об'єктів на сторінку	
	public function setPaginationCount($value)
	{
		if (!is_int($value) || $value<=0)
			throw new Exception('Incorrect pagination value: '.$value);
		$this->_paginationCount = $value;
		$this->_pagesCount = ceil($this->getCount() / $this->_paginationCount);
	}
	// отримати кількість сторінок
	// слід викликати тільки, якщо встановлено paginationCount
	public function getPagesCount()
	{
		return $this->_pagesCount;	
	}	
	
	// номер сторінки
	private $_pageNumber = null;
	// отримати номер сторінки
	public function getPageNumber()
	{
		return $this->_pageNumber;
	}
	// встановити номер сторінки, тобто згенерувати LIMIT-частину запиту
	public function setPageNumber($value)
	{
		if (!is_int($value) || $value<=0)
			throw new Exception('Incorrect pagination value: '.$value);
		$this->_pageNumber = $value;
		$this->limit = ' LIMIT '.($value-1)*$this->_paginationCount.','.$this->_paginationCount;
	}
	
	// кількість (відфільтрованих) об'єктів
	public function getCount()
	{
		$dbConn = self::getDbConn();
		$row = $dbConn->query('SELECT COUNT(*) as count FROM `'.$this->name.'` '.$this->where)->fetch();
		return (int)$row['count'];
	}

	// магічний метод (див. документацію) 
	// якщо є метод для даної властивості ($count - getCount), то викликаємо його
	// в іншому випадку викликаємо __get2
	public function __get($propertyName)
	{
		$method = 'get'.ucfirst($propertyName);
		if (method_exists($this,$method)) {
			return $this->$method();
		} else {
			return $this->__get2($propertyName);
		}
	}

	// магічний метод (див. документацію) 
	// якщо є метод для даної властивості ($count - setCount), то викликаємо його
	// в іншому випадку викликаємо __set2
	public function __set($propertyName,$propertyValue)
	{
		$method = 'set'.ucfirst($propertyName);
		if (method_exists($this,$method)) {
			return $this->$method($propertyValue);
		} else {
			return $this->__set2($propertyName,$propertyValue);
		}
	}
	
	// тут ми зчитуємо для propertyName відповідне значення з першого відфільтрованого рядка,
	// якщо цього значення ще немає
	// слід доробити ситуацію, коли йде оновлення таблиці
	public function __get2($propertyName)
	{
		if (!isset($this->properties[$propertyName])) {		
			$dbConn = self::getDbConn();
			$row = $dbConn->query('SELECT `'.$propertyName.'` FROM `'.$this->name.'` '.$this->where)->fetch();
			$this->properties[$propertyName] = $row[$propertyName];
		}
		return $this->properties[$propertyName];
	}
	
	// встановлення властивості
	public function __set2($propertyName,$propertyValue)
	{
		$this->properties[$propertyName] = $propertyValue;
	}
	
	// отримати двовимірний масив всіх відфільтрованих даних
	public function getAll()
	{
		$dbConn = self::getDbConn();
		echo 'SELECT * FROM `'.$this->name.'` '.$this->where.$this->limit;
		return $dbConn->query('SELECT * FROM `'.$this->name.'` '.$this->where.$this->limit)->fetchAll();
	}
	
	// вставити новий рядок із заданими раніше властивостями
	public function insert()
	{
		$sql = '';		
		foreach ($this->properties as $key=>$value) {
			if (!empty($sql)) $sql .= ', ';
			$sql .= '`'.$key.'`="'.addslashes($value).'"';		
		}		
		$sql = 'INSERT INTO `'.$this->name.'` SET '.$sql;
		$dbConn = self::getDbConn();
		if ($dbConn->query($sql)==false)
			return false;
		// отримати id
		$properties = $dbConn->query('SELECT MAX(id) as id FROM `'.$this->name.'`')->fetch();
		$this->properties['id'] = $properties['id'];
		$this->whereIds = array('id'=>$this->properties['id']);
		$this->where = 'id='.$this->properties['id'];
		return true;
	}
	
	// апдейтнути дані
	public function update()
	{
		$sql = '';		
		foreach ($this->properties as $key=>$value) {
			if (!empty($sql)) $sql .= ', ';
			$sql .= '`'.$key.'`="'.addslashes($value).'"';		
		}		
		$sql = 'UPDATE `'.$this->name.'` SET '.$sql.' '.$this->where;
		$dbConn = self::getDbConn();
		if ($dbConn->query($sql)==false)
			return false;
	}

}

?>