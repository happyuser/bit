<?php
include_once('functions.php');
connect();

foreach ($_POST as $key => $val) {
    //$val=get_post($key);
    if ($key != 'name_func') $params[$key] = $val;
}

if ($_POST['name_func'] == 'select_DB') {
    foreach ($params as $key => $val) {
        if ($key == 'table') { $table = $val; continue; }
        $where[$key] = $val;
    }
    $out_result = select_DB($table, $where);
} else $out_result = call_user_func($_POST['name_func'], $params);

print json_encode($out_result);
?>