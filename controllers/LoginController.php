<?php

/*
 * контролер логіну
 *
 */

class LoginController extends BaseController 
{
	
// метод, що використовується конструктор BaseController`a
// якщо є залогований едітор (адміністратор або інтерн)
// то метод використовується метод виводу echooLogged 
public function actionStart()
{
	if (!empty($_SESSION['editor']))
			$this->outputMethod = 'echooLogged';
}

// actionPost - метод, який повинен викликатися з конструктора BaseController'а 
// але скорше за все не викликається, бо зміни десь поділися
// це метод, який викливається, якщо є дані $_POST
public function actionPost($postValues)
{
	// форму логування слід виводити	
	$login_form = true;

	// завантажити editor`а із вказаними емейлом і зашифрованим паролем
	$editor = new MEditor();
	$editor->filter(array('email'=>$postValues['email'],'sha_password'=>sha1($postValues['password'])));
	
	// в залежності від того, скільки едіторів знайдено
	switch ($editor->count) {
		// один - тоді завантажуєом в сесію айді едітора та самого едітора (він не має зберігатися
		// за тим, як це було запрограмовано в SessionController`і)
		case 1:
			//$editor->getFirst();
			$_SESSION['editor_id'] = $editor->id;
			$_SESSION['editor'] = $editor;
			// задаємо метод виводу даних в браузер
			$this->outputMethod = 'echooLogged';
			break;
		case 0:
			// якщо не знайдено едітора, то помилка у введених даних
			// сповіщаємо про це повідомленням зі словника
			$this->views['login']->error = $GLOBALS['dict']['incorrect_email_or_password'];
			break;
		default:
			// якщо раптом є більше одного такого едітора, то щось не комільфо - генеруємо помилку 
			throw new EDbConstructionErrror();
	}
}

// ядро виводу - виводимо вид login
public function coreEcho()
{
	$this->views['login']->echoo();
}

// якщо залоговано, то перенаправляємо на одну зі сторінок адмінки
public function echooLogged()
{
	//$this->views['message']->text =  'Hello, '.$_SESSION['editor']->first_name.' '.$_SESSION['editor']->last_name.'<br /> Your status: '.$_SESSION['editor']->status
	//	. '<br /><a href="/logout/">' . $GLOBALS['dict']['logout'] . '</a>'; 	
	//;
	//$this->echooMessage();
	header('Location: /mitglieder_registered.php');
}

}

?>