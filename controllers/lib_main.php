<?php
//счет элементов в массиве
function count_array_element($massive, $sSearch)
{
/*
$massive - array - массив с искомыми значениями
$sSearch - string - искомое значние
*/
	$rgResult = array_intersect_key($massive, array_flip(array_filter(array_keys($massive), function($sKey) use ($sSearch)
	{
		return preg_match('/^'.preg_quote($sSearch).'/', $sKey);
	})));
	$counter=count($rgResult);
	return $counter;
}
//функция по обработке фильтра
function get_filter($table_index=null)
{
/*
$table_index - array - возможный индекс таблицы (например: id=>g.,all=>m. где ключ это имя поля а значение будет применено к этому полю, в случае указания ключа all значение будет применено ко всем полям)
*/
	foreach($_GET as $key=>$val){
		if(strripos($key, 'filter~')!==false){
			$filter=explode('~', $key);
			$filters[$filter[1]]=$val;
		}
	}
	if(!empty($table_index)){
		foreach($table_index as $keys=>$value){
			if($keys=='all'){
				$all=$value;
			}
			else{
				$index[$keys]=$value;
			}
		}		
	}
	if ($filters){
		$counter_filter=count($filters);
		$where=null;
		for($a=0;$a<$counter_filter;$a++){
			$filter=current($filters);
			$keys=key($filters);
			if($a==0){
				if(!empty($table_index)){
					if(isset($index)){
						$c=0;
						foreach($index as $keys_index=>$value_index){
							if($keys==$keys_index){
								$where.=$value_index;
								$c++;
							}
						}
						if($c==0){
							$where.=$all;
						}
					}
					else{
						$where.=$all;
					}
				}
				$where.="$keys='$filter'";
			}
			else{
				$where.=" and ";
				if(!empty($table_index)){
					if(isset($index)){
						$c=0;
						foreach($index as $keys_index=>$value_index){
							if($keys==$keys_index){
								$where.=$value_index;
								$c++;
							}
						}
						if($c==0){
							$where.=$all;
						}
					}
					else{
						$where.=$all;
					}
				}
				$where.="$keys='$filter'";
			}
			next($filters);
		}
		return $where;
	}
}
//функция по обработке сортировки
function get_sort($table_index=null)
{
/*
$table_index - array - возможный индекс таблицы (например: id=>g.,all=>m. где ключ это имя поля а значение будет применено к этому полю, в случае указания ключа all значение будет применено ко всем полям)
*/
	if(!empty($table_index)){
		foreach($table_index as $keys=>$value){
			if($keys=='all'){
				$all=$value;
			}
			else{
				$index[$keys]=$value;
			}
		}		
	}
    $name_sort=explode('~',$_GET['sort']);
	$sort=null;
	if(!empty($table_index)){
		if(isset($index)){
			$c=0;
			foreach($index as $keys_index=>$value_index){
				if($name_sort[0]==$keys_index){
					$where.=$value_index;
					$c++;
				}
			}
			if($c==0){
				$where.=$all;
			}
		}
		else{
			$where.=$all;
		}
	}
	$sort.=$name_sort[0]." ".$name_sort[1];
	return $sort;
}
//функция по обработке поиска
function get_search($table=null,$table_index=null,$table_column=null)
{
/*
$table - string - возможный перечень таблиц
$table_index - string - возможный индекс таблицы (например: m.)
$table_column - string - возможный набор полей
*/
	if(!empty($table_column)){
		$row=explode(",",$table_column);
		foreach ($row as $key => $value){
			$result[]['COLUMN_NAME'] = $value;
		}		
//var_dump($result);
	}
	else{
		$tables_name=explode(",",$table);
		$count_names=count($tables_name);
		$name_for_bd=" AND ";
		if($count_names>1){
			$name_for_bd.="(";
		}
		for($i=0;$i<$count_names;$i++){
			if($i==0){
				$name_for_bd.="table_name='$tables_name[$i]'";
			}
			else{
				$name_for_bd.=" OR table_name='$tables_name[$i]'";
			}
		}
		if($count_names>1){
			$name_for_bd.=")";
		}
		$res=mysql_query('SELECT DATABASE()');
		$db_name=mysql_result($res,0);
		$where_user="table_schema='$db_name' $name_for_bd";
		$result=db_select('INFORMATION_SCHEMA.COLUMNS','COLUMN_NAME',$where_user,$order);
	}
	$rows=count($result);
	$a=0;
	if(!empty($table_index)){
		foreach($table_index as $keys=>$value){
			if($keys=='all'){
				$all=$value;
			}
			else{
				$index[$keys]=$value;
			}
		}
	}
	for($i=0;$i<$rows;$i++){
		if($i==0){
			$where="(";
			if(!empty($table_index)){
				if(isset($index)){
					$c=0;
					foreach($index as $keys_index=>$value_index){
						if($result[$i]['COLUMN_NAME']==$keys_index){
							$where.=$value_index;
							$c++;
						}
					}
					if($c==0){
						$where.=$all;
					}
				}
				else{
					$where.=$all;
				}
			}
			$where.=$result[$i]['COLUMN_NAME']." like '%".$_GET['search']."%'";
		}
		else{
			$where.=" or ";
			if(!empty($table_index)){
				if(isset($index)){
					$c=0;
					foreach($index as $keys_index=>$value_index){
						if($result[$i]['COLUMN_NAME']==$keys_index){
							$where.=$value_index;
							$c++;
						}
					}
					if($c==0){
						$where.=$all;
					}
				}
				else{
					$where.=$all;
				}
			}
		$where.=$result[$i]['COLUMN_NAME']." like '%".$_GET['search']."%'";
		}
	}
	if(!empty($where)){
		$where.=")";
	}
	return $where;
}
//функция фозвращает отображение строк
function get_limit()
{
	$start=$_GET['page']*10-9;
	$finish=$_GET['page']*10;
	return "$start, $finish";
}
//формирование номеров страниц и переключателей
function creat_number_page($row_num)
{
/*
$row_num - int - колличество строк в формирующейся таблице
*/	
	if(isset($_GET['page'])){
		$number_pages=$_GET['page'];
	}
	else{
		$number_pages=1;
	}
	$get_counter=count($_GET);
	if(count($get_counter)>0){
		$gets=null;
		for($i=0;$i<$get_counter;$i++){
			$value=current($_GET);
			$keys=key($_GET);
			if($keys!=='page'){
				$gets.="&$keys=$value";
			}
			next($_GET);
		}
	}
	$data="<li><a href='?page=1$gets' style='height: 23px;margin-top: -1px;'> << </a></li>";
	if($number_pages>1){
		$trow_left=$number_pages-1;
		$data.="<li><a href='?page=$trow_left$gets' style='height: 23px;margin-top: -1px;'> < </a></li>";
	}
	$num_page=ceil($row_num/10);
	if($num_page<6){
		$start_row=1;
		$end_row=$num_page;
	}
	else{
		if($number_pages>2){
			$start_row=$number_pages-2;
		}
		if($number_pages<=2){
			$start_row=1;
		}
		if($number_pages<$num_page-5){
			$end_row=$start_row+5;
		}
		else{
			$temp=$num_page-$number_pages;
			$start_row=$number_pages-(5-$temp);
			$end_row=$num_page;
		}
	}
	for($i=$start_row;$i<=$end_row;$i++){
		if($i==$number_pages){
			$data.="<li class='active'>";
		}
		else{
			$data.="<li>";
		}
		$data.="<a href='?page=$i$gets' style='height: 23px;margin-top: -1px;'> $i </a></li>";
	}
	if($number_pages<$num_page){
		$trow_right=$number_pages+1;
		$data.="<li><a href='?page=$trow_right$gets' style='height: 23px;margin-top: -1px;'> > </a></li>";
	}
	$data.="<li><a href='?page=$num_page$gets' style='height: 23px;margin-top: -1px;'> >> </a></li>";
	return $data;
}
//фукнция по созданию опций произвольных селектов
function creat_select($datas,$selected=null)
{
/*
$datas - array - массив формируемых значений
$selectad - string - возможное выбранное значение в селекте
*/
	$keys=array_keys($datas[0]);
	$data=null;
	foreach($datas as $key=>$val){
		$data.="<option value='".$val[$keys[0]]."'";
		if($selected==$val[$keys[0]]){
			$data.="selected";
		}
		$data.=">".$val[$keys[1]]."</option>";
	}
	return $data;
}
//функция по созданию опций фильтров для таблиц
function creat_select_filter($table,$column_name=null,$where=null,$order=null)
{
/*
$column_name - string - имя поля по которому делать вобрку
$table - string - имя таблицы из которой получать данные
$order - string - сортировка полученной выборки
$where_user - string - возможные дополнительные условия
*/
	$datas=db_select($table,$column_name,$where,$order,null,1);
	if(!empty($datas)){
		$rows=mysql_num_rows($datas);
		if($rows>0){
			$row=key(mysql_fetch_array($datas, MYSQL_ASSOC));
			$filter_name="filter~$row";
			if(isset($_GET[$filter_name])){
				$selected=$_GET[$filter_name];
			}
			for($i=0;$i<$rows;$i++){
				$val=mysql_result($datas,$i);
				$data.="<option value='$val'";
				if(isset($selected) AND $selected==$val){
					$data.="selected";
				}
				$data.=">$val</option>";
			}
			$select="<option value='*all'>*all</option>".$data;
		}
	}
	else{
		$select="<option value='*all'>*all</option>";
	}
	return $select;	
}
	function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}
//функция возвращает значения селектов для mitglieder register
function get_option_for_select(){

$OPTIONS_MEMBER=array(
    'kart_ammount'=>array(
        0,
        1,
        2,
        3,
        '2 au?er bei MmM Berlin & Munchen, da 1',
        '2 au?er bei DC Berlin & MmM Berlin, da 1',
        '1 au?er GSA, da 2',
        '1 au?er Hamburg, da 2',
        '1 au?er MumM & GSA, da 2',
        '1 au?er MumM, da 2',
        '2 au?er Berlin, da 1',
        '1 au?er MmM Hamburg, da 2',
        '2 au?er MmM, da 1',
        '2 au?er MmM Berlin, da 1',
        '2 au?er DC, dort nur 1',
        '2 au?er MmM Munchen, da 1',
        '2 au?er HERBERT, da 1',
        '2 au?er MmM Berlin & Munchen, da 1',
        '2 au?er Hamburg, da 1',
        '2 au?er Munchen, da 1',
        'Entscheidet die PR-Agentur'
    ),
    'function'=>array(
        'e&m',
        'FdH',
        'WP',
        'SP',
        'VIP1',
        'VIP2',
        'Kein1',
        'FP',
        'P-VIP',
        'P',
        'S',
        'A',
        'R',
        'Z',
        'KL',
        'Musiker',
        'SportVip1',
        'SportVip2'
    ),
    'invitation'=> array(
        'E/P',
        'E-Mail',
        'Post'
    ),
    'travelcosts'=> array(
        'NEIN',
        'Hotel',
        'Hotel beim HERBERT',
        'Hotel Hamburg & MumM',
        'JA',
        'JA Hotel HH',
        'JA, bei MumM Hotel',
        'JA, Berlin & Munchen MmM',
        'JA, fur HH',
        'JA, HERBERT',
        'JA, HH',
        'JA, HH & MumM',
        'JA, HH Hotel',
        'Ja, HH Hotel & MumM',
        'JA, HH MmM',
        'JA, Hotel beim HERBERT',
        'JA, Hotel HH',
        'JA, Hotel MmM HH',
        'JA, HOTEL MumM',
        'JA, HoTEL MumM & MmM HH',
        'JA, MmM HH',
        'JA, MmM HH & HERBERT',
        'JA, MmM-HH',
        'JA, MumM',
        'JA, Munchen & HH',
        'MmM-HH Reise inkl. Hotel',
        'MumM Reise inkl. Hotel',
        'Hotel bei GSA'
    ),
    'time_invited'=> array(
        'ab 21 Uhr',
        'ab 22 Uhr ',
        'ab 23 Uhr',
        'BEGINN'
    ),
    'vip_zugang'=> array(
        'NEIN',
        'JA'
    ),
    'centurion'=> array(
        'NEIN',
        'JA'
    ),
    'soap'=>array(
        'NEIN',
        'Als JURY Mitglied anfragen',
        'Als Laudator anfragen',
        'JA',
        'JA, ab 21 Uhr',
        'JA, ab Beginn',
        'JA, Berlin',
        'JA, Hamburg',
        'JA, Munchen',
        'JA, wenn nicht Sponsor',
        'KAUFKARTEN',
        'nach Bedarf',
        'Uber PR-AGENTUR'
    ),
    'herbert'=>array(
        'NEIN',
        'Flanier',
        'JA',
        'JA, ab Beginn',
        'JA, ohne Begleitung',
        'JA, sofern nicht Sponsor',
        'KAUFKARTE',
        'nach Bedarf',
        'JA, wenn nicht Sponsor',
        'JA, wenn Sky nicht Sponsor ist',
        'UBER PR-AGENTUR',
        'JA, wenn BILD nicht Sponsor ist'
    ),
    'cnn'=>array(
        'NEIN',
        'JA',
        'Uber PR-AGENTUR'
    ),
    'yearcalendar'=>array(
        'NEIN',
        'JA'
    ),
    'proposition'=>array(
        'Barterdeals',
        'BERLIN EVENTS',
        'CNN, HERBERT',
        'CNN, MmM, HERBERT',
        'DC',
        'DC, MumM',
        'DC, CNN',
        'DC, CNN, HERBERT',
        'DC, MmM',
        'DC, MumM, Soap',
        'DC, SOAP',
        'DC, Soap, MumM, HERBERT',
        'FB Postings',
        'GSA',
        'GSA & MumM',
        'HERBERT',
        'HERBERT & MmM',
        'Hamburg Events',
        'KAUFKARTE',
        'MmM',
        'Kartenkauf; Herbert',
        'Kaufkarten & MumM, Herbert',
        'Mediadeals',
        'Mediavolumen',
        'MmM',
        'MmM & MumM',
        'MmM Muc',
        'MmM, CNN',
        'MumM',
        'MumM, GSA',
        'PR-leistungen',
        'Promivermittlung',
        'Shuttle-Sponsoring',
        'GSA',
        'GSA, DC, MumM'
    ),
    'personal'=>array(
        'NEIN',
        'JA'
    ),
    'directors'=>array(
        'NEIN',
        'JA'
    ),
    'branchenevents'=>array(
        'NEIN',
        'JA'
    ),
    'films'=>array(
        'NEIN',
        'JA'
    ),
    'charity'=>array(
        'NEIN',
        'JA'
    ),
    'accompaniment'=>array(
        'NEIN',
        'JA'
    ),
    'salutation'=>array(
        'Männlich',
        'Weiblich'
    ),
    'before_title'=>array(
        'Sehr geehrter',
        'Sehr geehrte'
    ),
    'du_or_sie'=>array(
        'SIE',
        'DU'
    ),
    'mmm_b'=> array(
        'NEIN',
        'JA'
    ),
    'mmm_b_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'mmm_b_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'mmm_muc'=>array(
        'NEIN',
        'JA'
    ),
	'mmm_muc_begleitung'=>array(
        'NEIN',
        'JA'
    ),
	'mmm_muc_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'mmm_hh'=>array(
        'NEIN',
        'JA'
    ),
    'mmm_hh_begleitung'=>array(
        'NEIN',
        'JA'
    ),
	'mmm_hh_karten'=>array(
        'NEIN',
        'JA'
    ),
	'mmm_hh_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'mumm_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'mumm_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'mumm'=>array(
        'NEIN',
        'JA'
    ),
    'mumm_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'dc'=> array(
        'NEIN',
        'JA'
    ),
    'dc_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'dc_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'sport'=>array(
        'NEIN',
        'JA'
    ),
    'sport_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'sport_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'sonstige'=>array(
        'NEIN',
        'JA'
    ),
    'sonstige_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'sonstige_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'newsletter'=>array(
        'NEIN',
        'JA'
    )
);
$select=select_DB('type_account');
foreach($select as $value){
    $distribution[$value[english]]=$value[german]; 
}
$OPTIONS_MEMBER[distribution]=$distribution;

$select=select_DB('countries');
foreach($select as $value){
    $OPTIONS_MEMBER[bus_country][$value[id]]=$OPTIONS_MEMBER[country][$value[id]]=$value[de]; 
}

return $OPTIONS_MEMBER;
}
?>