<?php
session_start();
if($_SESSION['status']=='hostess') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");
//получение данных из гет строки
	if(!isset($_GET['id_event'])){
		$id_event=db_select('events','id','active=1','id desc');
		if(!empty($id_event)){
			$_GET['id_event']=$id_event[0]['id'];
		}
	}
	if(count_array_element($_GET, 'filter~')>0){
		$where=get_filter(array('id'=>'gu.','all'=>'g.'));
	}
	if(isset($_GET['search'])){
		if(isset($where)){
			$where.=" AND ".get_search('member',array('id'=>'gu.','all'=>'g.'));
		}
		else{
			$where=get_search('member',array('id'=>'gu.','all'=>'g.'));
		}
	}
	if(isset($_GET['sort'])){
		$order=get_sort(array('id'=>'gu.','all'=>'g.'));
	}
	else{
		$order="id";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=db_select('guest_column_names',null,null,'id');
//получение данных для таблиц
	if(isset($where)){
		$where_main=$where." AND gu.id_user = g.id AND gu.id_event='".$_GET['id_event']."' AND gu.type='guests' AND gu.status <> 'ablehnen_k'";
		$where_modal=$where." AND gu.id_user = g.id AND gu.id_event='".$_GET['id_event']."' AND gu.type='guests' AND gu.status = 'ablehnen_k'";
	}
	else{
		$where_main="gu.id_user = g.id AND gu.id_event='".$_GET['id_event']."' AND gu.type='guests' AND gu.status <> 'ablehnen_k'";
		$where_modal="gu.id_user = g.id AND gu.id_event='".$_GET['id_event']."' AND gu.type='guests' AND gu.status = 'ablehnen_k'";	
	}
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			if($i==0){
				$columns="gu.id";
				$columns_for_filter="gu.id";
			}
			else{
				$columns.=", g.".$header_name[$i]['english'];
				$columns_for_filter="g.".$header_name[$i]['english'];
			}
			$select_opton_main[]=creat_select_filter('guests g,guest_list gu',"DISTINCT $columns_for_filter",$where_main,$columns_for_filter);
			$select_opton_modal[]=creat_select_filter('guests g,guest_list gu',"DISTINCT $columns_for_filter",$where_modal,$columns_for_filter);
		}
		$tables_data=db_select('guests g,guest_list gu',$columns,$where_main,$order,$limit);
		$tables_modal=db_select('guests g,guest_list gu',$columns,$where_modal,$order,$limit);
		$data['pages_number']=creat_number_page(count(db_select('guests g,guest_list gu','gu.id',$where_main,$order)));
		$data['pages_modal']=creat_number_page(count(db_select('guests g,guest_list gu','gu.id',$where_modal,$order)));
		$data['select']=creat_select(db_select('events','id,name','active=1'),$_GET['id_event']);
		$data['header']=header_tables($header_name,$select_opton_main,0,4);
		$data['header_slider']=header_tables($header_name,$select_opton_main,4);
		$data['header_modal']=header_tables($header_name,$select_opton_modal,0,4);
		$data['header_slider_modal']=header_tables($header_name,$select_opton_modal,4);
		if(count($tables_data)>0){
			$data['table']=tables($tables_data,0,4);
			$data['table_slider']=tables($tables_data,4);
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
		if(count($tables_modal)>0){
			$data['table_modal']=tables($tables_modal,0,4);
			$data['table_slider_modal']=tables($tables_modal,4);
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}
//получение данных из таблицы для модального окна
    return $data;
?>