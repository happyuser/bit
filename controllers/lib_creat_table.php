<?php
require_once("lib_db_method.php");
//функция по формированию заголовка таблицы
function header_tables($header_data,$selected_option=null,$start=null,$end=null)
{
/*
$header_data - array - массив заголовков данных
$selected_option - array - возможный массив данных о фильтрах
$start - int - возможное значение начала отсчета в массивах
$end - int - возможное значение конца перебора массивов
*/
	$data="<th class='id_th_check'";
	if($start==0){
		$data.=" id='id_th_check'";
	}
	else{
		$data.=" style='display:none'";
	}
	$data.=">ID</th>";
	if(empty($end)){
		$end=count($header_data);
	}
	for($i=$start;$i<$end;$i++){
		$data.="<th class='".$header_data[$i]['english']."_th' id='".$header_data[$i]['english']."_th'>".$header_data[$i]['german']."<div style='";
		if($header_data[$i]['english']!='last_name' && $header_data[$i]['english']!='first_name'){
			$data.="margin-top:15px;";
		}
		$data.="margin-top:15px;overflow:hidden;width:100%;height:20px;'><a class='sort' id='".$header_data[$i]['english']."~";
		if($_GET['sort']==($header_data[$i]['english'].'~desc')){
			$data.="desc";
		}
		else{
			$data.="asc";
		}
		$data.="' style='cursor: pointer;'><i class='";
		if($_GET['sort']==($header_data[$i]['english'].'~desc')){
			$data.="icon-chevron-down";
		}
		else{
			$data.="icon-chevron-up";
		}
		$data.="'></i></a>";
		$name_filter="filter~".$header_data[$i]['english'];
		$data.="<select class='";
		if(isset($_GET[$name_filter])){
			$data.="icon_blue_filter";
		}
		else{
			$data.="icon-filter";
		}
		$data.=" filter' style='background-color: white' name='$name_filter'>";
		if(!empty($selected_option)){
			$data.=$selected_option[$i];
		}
		else{
			$data.="<option value='*all'>*all</option>";
		}
		$data.="</select></div></th>";
	}
	return $data;
}
//функция формирования таблицы
function tables($datas,$start=null,$end=null,$pages=null){
//получение номера отображаемой страницы
	if(isset($_GET['page'])){
		$number_pages=$_GET['page'];
	}
	else{
		$number_pages=1;
	}
//определение конца отображения если его не определили
	if(empty($end)){
		$end=count($datas[0])+1;
	}
//формирование таблицы согласно включенной страницы
	$data=null;
	for($i=0;$i<10;$i++){
		if(!empty($datas[$i])){
//получаем массив рабочих мест		
			if($pages=='m_reg'){
				$jobs=db_select('users_career',null,"id_user='".$datas[$i]['id']."'");
			}
//формируем строку
			$data.="<tr";
			if($pages=='m_reg' AND $datas[$i]['approve']==3){
				$data.=" class='edit_val'";
			}
			if($pages=='g_ein' AND $datas[$i]['status']=='no'){
				$data.=" style='background:grey'";
			}
			$data.=">";
//определяем айди записи
			if($start==0){
				$data.="<td class='id_td_it' id='id_td_it'><input type='checkbox' class='one_checkbox_active' value='".$datas[$i]['id']."' name='id[]'";
				if($pages=='g_ein' AND $datas[$i]['status']=='no'){
					$data.=" disabled";
				}
				$data.="/>";
				if($datas[$i]['id']<10){
					$data.="00".$datas[$i]['id'];
				}
				elseif($datas[$i]['id']>9 AND $datas[$i]['id']<100){
					$data.="0".$datas[$i]['id'];
				}
				else{
					$data.=$datas[$i]['id'];
				}
				$data.="</td>";
			}
			else{
				$data.="<td class='id_td_it' style='display:none'>".$datas[$i]['id']."</td>";
			}
//формируем строку согластно необходимому колличеству столбцов
			if($start==0){
				$cur_mas=array_slice($datas[$i],$start,$end);
			}
			else{
				$cur_mas=array_merge(array_slice($datas[$i],0,1),array_slice($datas[$i],$start,$end));
			}
			next($cur_mas);
			for($a=$start+1;$a<$end;$a++){
				$value=current($cur_mas);
				$keys=key($cur_mas);
//сверяем ключ массива с именем заголовка
				if($pages=='m_reg' AND ($keys!='active' or $keys!='approve')){
					$data.=creat_m_reg($keys,$value,$jobs,$datas[$i]['active']);
				}
				elseif($pages=='g_ein'){
					if($keys!=='status'){
						$data.=creat_g_ein($keys,$value);
					}
				}
				elseif($pages=='m_kan' and $keys=='event'){
					$data.=creat_m_kan($keys,$value);
				}
				elseif($pages=='spon' and $keys=='land'){
					$data.=creat_spon($keys,$value);
				}
				elseif($pages=='m_app' and $keys=='id_admin'){
					$data.=creat_m_app($keys,$value);
				}
				else{
					$data.="<td id='".$keys."_td' class='".$keys."_td'><p id='overflow'>";
					$data.=strip_tags($value);
					$data.="</p></td>";				
				}
				next($cur_mas);
			}
			$data.="</tr>";
		}			
		else{
			break;
		}
	}
	return $data;
}

//Создание таблицы в cтранице Mitglieder Kandidaten
function creat_m_reg($keys,$value,$jobs,$active){
	$data="<td id='".$keys."_td' class='".$keys."_td'";
	if($active==0){ 
		$data.='no_activ_user';
	}
	$data.="><p id='overflow'>";
	if($keys=='country'||$keys=='bus_country'){
		$country=db_select('countries',null,"id='$value'");
		$data.=$country[0]['country'];
	}
	else if($keys=='photo'){
		$data.="<a rel='../uploads/$value'></a>foto";
	}
	else if($keys=='professional_vita'){
		$data.="<span>Show</span><div class='berufsvita' style='display: none'>";
		if(!empty($jobs)){
			while ($value=current($jobs)){
				$data.="<span job='".key($jobs)."'><div>";
				while($val=current($value)){
					$data.="<p id='".key($value)."'>$val</p>";
					next($value);
				}
			$data.="</div></span>";
			next($jobs);
			}
		}
		$data.="</div>";
	}
	else if($keys=='last_activ'){
		$data.="<span>";
		if(!empty($value)){
			$data.=date('d M Y H:i',$value);
		}
		$data.="</span>";
	}
	else if($keys=='letzter'){
		$data.="<span>";
		$name_admin=db_select('editor','last_name',"id='".$value."'");
		if(!empty($name_admin[0]['last_name'])){
			$data.=$name_admin[0]['last_name'];
		}
		$data.="</span>";
	}
	else {
		$data.=strip_tags($value);
	}
	$data.="</p></td>";
	return $data;
}
//Создание таблицы в cтранице Gasteliste Eingeladene
function creat_g_ein($keys,$value){
	$data="<td id='".$keys."_td' class='".$keys."_td'><p id='overflow'>";
	$data.=strip_tags($value);
	$data.="</p></td>";
	return $data;
}	
//Создание таблицы в cтранице Mitglieder Bewerber
function creat_m_kan($keys,$id){
	$data.="<td id='".$keys."_td' class='".$keys."_td'><p id='overflow'>";
	$event=db_select('events',null,"id='$id'");
	$data.="<a href='/user/?page=event&id=$id' target='_blank'>".$event[0]['name']."</a>";
	$data.="</p></td>";
	return $data;
}
//Создание таблицы в cтранице Sponsoren
function creat_spon($keys,$value){
	$name_country=db_select('countries','de',"id='$value'");
	$data="<td id='".$keys."_td' class='".$keys."_td'><p id='overflow'>".$name_country[0]['de']."</p></td>";
	return $data;
}
//Создание таблицы в cтранице Mitglieder Geänderte Mitglieder
function creat_m_app($keys,$value){
	$name_admin=db_select('editor','last_name',"id='$value'");
	$data="<td id='".$keys."_td' class='".$keys."_td'><p id='overflow'>";
	if(!empty($name_admin[0]['last_name'])){
		$data.=$name_admin[0]['last_name'];
	}
	else{
		$data.="Geändert von dem Mitglied selbst";
	}
	$data.="</p></td>";
	return $data;
}
?>