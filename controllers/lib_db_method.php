<?
//функция возращает сортированный массив данных согласно переданным параметрам
function db_select($tables,$columns=null,$where=null,$order=null,$limit=null,$sort=null){
/*
$tables - string - имя таблицы для получения данных (например: member)
$columsn - string - перечень полей и условий записанных с соблюдением синтаксиса MySQL (например: DISTINCT first_name)
$where - string - условий для получения выборки записанных с соблюдением синтаксиса MySQL (например: id='10')
$order - string - параметры сортировки данных записанных с соблюдением синтаксиса MySQL (например: id DESK)
$limit - string - перечень полей и условий записанных с соблюдением синтаксиса MySQL (например: 0,20)
$sort - int - 0,1 при значении больше нуля возвращается не обработанный результат запроса
*/
//$start=microtime();
	if(empty($columns)){
		$columns='*';
	}
	$query="SELECT $columns FROM $tables";
	if($where!==null){
		$query.=" where ".$where;
	}
	if($order!==null){
		$query.=" order by ".$order;
	}
	if($limit!==null){
		$query.=" limit ".$limit;
	}
//var_dump("<pre>".$query."</pre>");
    $result=mysql_query($query);
	if($sort>0){
		return $result;
	}
	else{
		if($result){
			$rows=mysql_num_rows($result);
			if($rows>0){
				for($i=0;$i<$rows;$i++){
					$row=mysql_fetch_array($result, MYSQL_ASSOC);
					foreach ($row as $key => $value){
						$db_content[$i][$key] = $value;
					}
				}
			}
			else{
				$db_content=null;
			}
		}
		else{
			$db_content=null;
		}
	}
//$finish=microtime();
//var_dump($finish-$start);
    return $db_content;
}
//добавление значений в таблтицу
function db_insert($tables,$columns,$values){
/*
$tables - string - имя таблицы для получения данных (например: member)
$columsn - string - перечень полей и условий записанных с соблюдением синтаксиса MySQL (например: first_name)
$values - string - перечень значений записанных с соблюдением синтаксиса MySQL (например: 'value1', 'value2')
*/
	$values=convert_string($values);
	return mysql_query("INSERT INTO $tables($columns) VALUES ($values)");
}
//удаление значений из таблицы
function db_delete($tables,$where=null){
/*
$tables - string - имя таблицы для получения данных (например: member)
$where - string - возможные условий для получения выборки записанных с соблюдением синтаксиса MySQL (например: id='10')
*/
	$query="DELETE FROM $tables";
	if(isset($where)){
		$query.=" WHERE ".$where;
	}
	return mysql_query($query);
}
//обновление записей
function db_update($tables,$set,$where){
/*
$tables - string - имя таблицы для получения данных (например: member)
$set - string - данные для записи в таблицу, (например id='1',english='id',german='ID')
$where - string - возможные условий для получения выборки записанных с соблюдением синтаксиса MySQL (например: id='10')
*/
	$set=convert_string($set);
    
    //echo "UPDATE $tables SET $set WHERE $where";
    return mysql_query("UPDATE $tables SET $set WHERE $where");
}

//получение последнего ИД с таблицы
function last_id($table){
    $query = "SELECT * FROM $table WHERE active=1 order by id desc";
    $result = mysql_query($query);
    $row = mysql_fetch_assoc($result);
    return $row[id];
}

//тащит всю табдицу с БД
function _DB_views($table)
{
    $query = "SELECT * FROM $table";
    $result = mysql_query($query);
    $rows = mysql_num_rows($result);

    for ($i = 0; $i <= $rows; $i++) {
        $row = mysql_fetch_assoc($result);
        $id = $row[id];
        if (!is_array($row) || count($row) == 0) {
            continue;
        }

        foreach ($row as $key => $value) {
            $_DB_events[$id][$key] = $value;
        }
    }
    return $_DB_events;
}

//вибірка з БД
//$where - массив типу : array('id'=>3);
//$order - массив типу : array('id'=>'desc');
//$key_id - якщо true, то ключі масиву (на вивід) будуть відповідати ІД запису в таблиці БД
//$limit - наприклад "0,10"
function select_DB($table, $where=null, $order=null, $key_id=false, $limit=null, $column=null, $view_sql=false){
    $query = "SELECT ";
    if($column){
        if(is_string($column))$column=explode(',',$column);
        foreach($column as $val){
            $query.="`$val`";
        }
        $query = str_replace("``", "`, `", $query);
    }else
        $query.='*';
        
    $query.=" FROM $table";
    if($where){
        $query = $query.' WHERE ';
        foreach($where as $key=>$val){
            $query = $query."`$key`='$val'";
        }
        $query = str_replace("'`", "' AND `", $query);
    }
    if($order){
        foreach($order as $key=>$val){
            $query = $query." order by $key $val";
        }
    }
    if($limit){
        $query = $query.' '.$limit;
    }
    if($view_sql)echo$query;
    $result = mysql_query($query);
if($result)
    while($row = mysql_fetch_assoc($result)){

        if (count($row) == 0) {
            continue;
        }

        if($key_id)
            $_select_DB[$row[id]] = $row;
        else
            $_select_DB[] = $row;
    }
    
    return $_select_DB;
}

//зміна запису в таблиці БД
//$table - назва таблички в БД
//$set - массив зі змінами: array('column'=>'new_value');
//$where - массив для ідентифікації запису в БД: array('id'=>3);
//Якщо $where містить кілька параметрів, то між ними стоїть по замовчуванню AND
function update_DB_and ($table, $set, $where, $echo_sql=false) {
    unset($set[name_table]);
    foreach($where as $key=>$val){
        $str_where=$str_where."`".$key."`='".$val."'";
    }
        $str_where = str_replace("'`", "' AND `", $str_where);

    foreach($set as $key=>$val){
        $str_set=$str_set."`".$key."`='".$val."'";
    }
        $str_set = str_replace("'`", "', `", $str_set);
    $query="UPDATE `$table` SET $str_set WHERE $str_where";
if($echo_sql)echo $query;
    $result=mysql_query($query);
    if($result)
$out_result[ok]='true';
else
$out_result[ok]='false';
return $out_result;
}

//створення нового запису в таблиці БД
//на вхід приходить масив з параметрами які потрібно для заповнення таблиці + назва таблиці:
//array('name_table'=>'member','name'=>'vasja','last_name'=>'pupkin');
//якщо функція викликається для створення нової події, то викликається ще одна функція "new_event"
function new_member($arr){
    $str=$strVal='';
	if($arr['name_table']=='editor'){
		$message=$arr['first_name']." ".$arr['last_name']." Ihr Passwort um das Admin-Panel zugreifen - ".$arr['sha_password'].".";
		send_mail($arr['email'],'hello',$message);
	}
    $query="INSERT INTO `$arr[name_table]` (";
    foreach($arr as $key=>$val){
        if($key=='sha_password')$val=sha1($val);
        if($key=='name_table' || $key=='view_sql')continue;
        if($val){
          $str=$str.'`'.$key.'`';
          $strVal=$strVal."'".addslashes($val)."'";
        }
    }
    $str = str_replace('``', '`, `', $str);
    $strVal = str_replace("''", "', '", $strVal);
    $query=$query.$str.") VALUES ($strVal)";
    if($arr[view_sql])echo$query;
    $result=mysql_query($query);

if($result){
    $out_result[ok]='true';
    $out_result[id]=mysql_insert_id();
    if($arr[name_table]=='events'){
        new_event(array('id'=>$out_result[id],'city'=>$arr[city]));
    }
}
else
$out_result[ok]='false';
return $out_result;
}

function countries_users(){
    $query="SELECT DISTINCT(`country`) FROM `member`";
    $result = mysql_query($query);
    $countries=array();
    while($row = mysql_fetch_assoc($result)){
        $info=select_DB('countries',array('id'=>$row[country]));
        $info=$info[0];
        $countries[]=array('name'=>$info[de],'id'=>$row[country]);
    }
    foreach ($countries as $key => $row) {
        $country[$key]  = $row['name'];
        $id[$key] = $row['id'];
    }
    
    array_multisort($country, SORT_ASC, $countries);
    return $countries;
}

function view_cities_for_country($arr){
    $query="SELECT DISTINCT(`postaddress`) FROM `member` WHERE `country`='$arr[id]'";
    $result = mysql_query($query);
    $cities=array();
    while($row = mysql_fetch_assoc($result)){
        $cities[]=$row[postaddress];
    }
    sort($cities);
    return $cities;
}

//экранирование входной информации
function convert_string($text){
    $order=array('«','»','“','”',"`");
    $replace=array('"','"','"','"',"'");
	return str_replace($order,$replace,$text);	
	}
?>