<?
//ADMIN
//зміна профілю юзера
//передається масив з даними які треба замінити + ІД юзера:
//array('id'=>3,'name'=>'new_name','lsat_name'=>'new_value');
//дані міняються + записуються зміни в таблиці логу змін (logs_users_edit)
function update_user($arr){
    $query = "SELECT * FROM member WHERE id=$arr[id]";
    $result = mysql_query($query);
    $rows = mysql_num_rows($result);
    $row = mysql_fetch_assoc($result);
    $approve=$row[approve];
    $arr_new_val=array();
    foreach($arr as $key=>$val){
        if($key!='no_write'){
            if($key=='photo' && $val=='')continue;
            if($key=='photo_user')$key='photo';
            if($val!=$row[$key] || $key=='msg')$arr_new_val[$key]=$val;
        }
    }
    if(count($arr_new_val)==1 && $arr_new_val[msg]){
        $out_result[ok]='true';
        return $out_result;
    }
    if(!$arr[no_write]){
            if($approve!=3){//якщо НЕМАЄ відкритих змін
                $chars = '8bm9e19th2lqqk6i41if083b94';
                $numChars = strlen($chars);
                $string = '';
                for ($i = 0; $i < 10; $i++) {
                    $string .= substr($chars, rand(1, $numChars) - 1, 1);
                }
                $gen_key=$string;
            }else{
                $q = "SELECT * FROM logs_users_edit WHERE id_user=$arr[id] order by id desc limit 0,1";
                $r = mysql_query($q);
                $r = mysql_fetch_assoc($r);
                $gen_key=$r[gen_key];
            }
    
        if($arr_new_val)
        foreach($arr_new_val as $key=>$val){
            if($key=='approve' || $key=='msg')continue;
            $date=date('Y-m-d',time());
            $query="INSERT INTO `logs_users_edit`(`id`, `id_user`, `type`, `last_val`, `new_val`, `date`, `gen_key`, `msg`) VALUES (NULL,$arr[id],'$key','$row[$key]','$val','$date','$gen_key','$arr[msg]')";
            $result=mysql_query($query);
        }
    }

    if($arr_new_val)
    {
        $str='';
        $query="UPDATE `member` SET ";
        foreach($arr_new_val as $key=>$val){
            if($val!==false && $key!='id' && $key!='no_write' && $key!='msg')$str=$str."`".$key."`='".$val."'";
        }
        $str = str_replace("'`", "', `", $str);
        $query=$query.$str." WHERE id=$arr[id]";
        $result=mysql_query($query);
    }

if($result)
$out_result[ok]='true';
else
$out_result[ok]='false';
return $out_result;
}


//зміна налаштувань доступу до інформації, для гостей сторінки користувача
function edit_access_users_info($arr){
    $_USER_INFO[member][id]=get_cookie('id_user');
    $arr[id_user]=$_USER_INFO[member][id];

    return update_DB_and ('access_users_info', array($arr[type]=>$arr[status]), array('id_user'=>$arr[id_user]));
}

//редагування конкретного запису в кар`єрі користувача
//на вхід іде масив із новими значеннями, ІД користувача і ІД місця роботи (ід в табл. БД)
function update_userJob_for_user($arr){
    $arr[id_user]=get_cookie('id_user');
    foreach($arr as $key=>$val){
        if (strripos($key, 'where')!==false && $val){
            $key=explode('-',$key);
            if(($key[1]=='time_start' || $key[1]=='time_end') && $val!='now'){
                $val=explode('-',$val);
                $val=$val[0].'-'.$val[1].'-01';
            }
            $where[$key[1]]=$arr_result[$key[1]]=$val;
        }
    }
    $arr_result[id]=$arr[id];
    $arr_result[id_user]=$arr[id_user];
    if($arr_result[time_end]=='now')$arr_result[time_end]='bis jetzt';
    $arr_result[result]=update_DB_and('users_career',$where,array('id'=>$arr[id],'id_user'=>$arr[id_user]));
    
    if($arr_result[time_end]!='bis jetzt')$end=explode('-',$arr_result[time_end]); else {$end[0]=date('Y',time()); $end[1]=date('m',time());}
    $start=explode('-',$arr_result[time_start]);
        $years=$end[0]-$start[0];
        $months=$end[1]-$start[1];
        if($months<0){$years-=1; $months=12+$months;}
    $arr_result['years']=$years;
    $arr_result['months']=$months;
    
    if($arr_result[time_end]!='bis jetzt')
    $arr_result[time_end]=print_date($arr_result[time_end]);
    $arr_result[time_start]=print_date($arr_result[time_start],'strtotime');
    
    return $arr_result;
}

//редагування профілю спонсора
//на вхід приходить масив із змінами array('поле'=>'нове значення');
function update_sponsor_for_sponsor($arr){
    $id=get_cookie('id_sponsor');
    return update_DB_and ('reg_sponsor', $arr, array('id'=>$id,'activ'=>1));
}

//Зміна свого профілю користувачем. Статус юзера стає як РЕДАГУЄТЬСЯ (в таблиці approve = 3)
//на вхід масив із змінами array('поле'=>'нове значення');
function update_user_for_user($arr){
    $arr[id]=get_cookie('id_user');
    $arr[approve]='3';
    $out_result=update_user($arr);
return $out_result;
}


//ADMIN
//зміна даних події
//на вхід масив із змінами array('поле'=>'нове значення');
//також функція редагує список подій на які зареєстровані конкретні спонсори (що зареєстровані на цю подію, чи були зареєстровані)
//переписано в новой концепции Шнуровой В. 15.06.15
function update_event($arr){
	$id=$arr['id'];
	unset($arr['id']);
	unset($arr['name_func']);
	unset($arr['last_main_sponsor']);
	unset($arr['last_sub_sponsor']);
	unset($arr['last_rest_sponsor']);
	if($arr['points']['txt']){
		foreach($arr['points']['txt'] as $key=>$val){
			if($val!='')$arr_new_points[$key]=$val.';'.$arr['points']['time'][$key];
		}
	}
	if($arr['images']){
		foreach($arr['images'] as $key=>$val){
			if($val!='')$arr_new_images[$key]=$val;
		}
	}
	$arr['points']=json_encode($arr_new_points);
	$arr['images']=json_encode($arr_new_images);
    /*
    echo $arr[background];
    echo"\n\n\n";
    */
    $arr['background']= htmlspecialchars($arr['background'],ENT_QUOTES);
    $arr['paragraph']= htmlspecialchars($arr['paragraph'],ENT_QUOTES);
	if(!empty($arr['main_sponsor'])){
		$sponsor[]="'".$arr['main_sponsor']."','$id','main_sponsor'";
	}
	unset($arr['main_sponsor']);
	if(!empty($arr['sub_sponsor'])){
		$sponsor[]="'".$arr['sub_sponsor']."','$id','sub_sponsor'";
	}
    unset($arr['sub_sponsor']);
	if($arr['rest_sponsor']){
		foreach($arr['rest_sponsor'] as $val){
			if($val!='')$sponsor[]="'$val','$id','rest_sponsor'";
		}
	}
	unset($arr['rest_sponsor']);
	$i=0;
	foreach($arr as $keys=>$value){
		if($i==0){
			$set="$keys='$value'";
		}
		else{
			$set.=",$keys='$value'";
		}
		$i++;
	}
	$result=db_update('events',$set,"id='$id'");
	if($result!=false){
		$result=db_delete('sponsor_in_event',"id_event='$id'");
		if($result!=false){
			$counter=count($sponsor);
			for($i=0;$i<$counter;$i++){
				$result=db_insert('sponsor_in_event','id_sponsor,id_event,type_sponsor',$sponsor[$i]);
				if($result==false)break;
			}
			if($result!=false){
				$out_result['ok']='true';
			}
			else{
				$out_result['ok']='false';
			}
		}
		else{
			$out_result['ok']='false';
		}
	}
	else{
		$out_result['ok']='false';
	}
	return $out_result;
}

//інформація з БД про подію
//на вхід массив з параметром ІД запису в БД
function get_event($arr){
    $out_result=select_DB('events',array('id'=>$arr[id]));
    $out_result=$out_result[0];
    $out_result[background]=htmlspecialchars_decode($out_result[background]);
    $points=json_decode($out_result[points]);
    $out_result[points]=array();
    if($points)
    foreach($points as $key=>$val){
        $val=explode(';',$val);
        $out_result[points][]=array('txt'=>$val[0],'time'=>$val[1]);
    }
    $out_result[images]=json_decode($out_result[images]);
    return $out_result;
}


//тяне інфу з БД по ІД запису
//передається масив із назвою таблиці і ІД запису
function get_info_DB_id($arr){
	$query = "SELECT * FROM $arr[name_table] WHERE id=".$arr['id'];
	$result = mysql_query($query);
	$rows = mysql_num_rows($result);
	$row = mysql_fetch_assoc($result);
	if(!empty($row['last_activ'])){
		$data=date('d M Y H:i',$row['last_activ']);
		$row['last_activ']=$data;
	}
	$name_admin=db_select('editor','last_name',"id='".$row['letzter']."'");
	if(!empty($name_admin[0]['last_name'])){
		$row['letzter']=$name_admin[0]['last_name'];
	}
	else{
		$row['letzter']='';
	}
	if($result){
		$out_result['ok']='true';
		$out_result['DB_member']=$row;
	}
    else
        $out_result['ok']='false';
return $out_result;
}


//створює нове місце роботи в кар`єрі користувача
//на вхід приходять масив зі всіма даними для БД
function new_job($arr){
    $_USER_INFO[member][id]=get_cookie('id_user');
    $VALUES[id_user]=$_USER_INFO[member][id];
    foreach($arr as $key=>$val){
        if (strripos($key, 'where')!==false && $val){
            $key=explode('-',$key);
            $VALUES[$key[1]]=$val;
        }
    }
    $VALUES[name_table]='users_career';
    $arr['new_']=new_member($VALUES);
    $id_job=$arr['new_'][id];
        $start=explode('-',$arr['where-time_start']);
        if($arr['where-time_end']!='now' && $arr['where-time_end']!='')$end=explode('-',$arr['where-time_end']); else {$end[0]=date('Y',time()); $end[1]=date('m',time()); $arr['where-time_end']='bis jetzt';}
        $years=$end[0]-$start[0];
        $months=$end[1]-$start[1];
        if($months<0){$years-=1; $months=12+$months;}
    $arr['id_job']=$id_job;
    $arr['years']=$years;
    $arr['months']=$months;
    $arr['time_start']=$arr['where-time_start'];
    $arr['time_end']=$arr['where-time_end'];
    $arr['time_start_format']=print_date($arr['where-time_start']);
    if($arr['where-time_end']!='bis jetzt')
        $arr['time_end_format']=print_date($arr['where-time_end']);
    else
        $arr['time_end_format']='bis jetzt';
    $arr['firmenname']=$arr['where-firmenname'];
    $arr['position']=$arr['where-position'];
    $arr['aufgaben']=$arr['where-aufgaben'];

    return $arr;
}

//ADMIN
//вертає історію редагування профілю користувача
//на вхід масив із параметром ІД
function get_history_member($arr){
    $id=$arr[id];
	$query=db_select('logs_users_edit',"last_val,new_val,date,type,id_admin","id_user=$id");
	if($query!=false){
		$counter=count($query);
		for($i=0;$i<$counter;$i++){
			$name=db_select('member_column_names','german',"english='".$query[$i]['type']."'");
			if($name!=false){
				$name_admin=db_select('editor','last_name',"id='".$query[$i]['id_admin']."'");
				if(!empty($name_admin[0]['last_name'])){
					$query[$i]['id_admin']=$name_admin[0]['last_name'];
				}
				else{
					$query[$i]['id_admin']='Geändert von dem Mitglied selbst';
				}
				unset($query[$i]['type']);
				$out_result['history'][$i]=array_merge($name[0],$query[$i]);
			}
			else{
				$out_result['ok']='false';
				return $out_result;
			}
		}
		$out_result['ok']='true';
	}
	else{
		$out_result['ok']='false';
	}
	return $out_result;
}

//ADMIN
//вертає історію редагування профілю користувача
//на вхід масив із параметром ІД
function get_event_sponsor($arr){
    $id=$arr[id];
	$result=db_select('sponsor_in_event',null,"id_sponsor=$id");
	if($result==false){
		$out_result[ok]='false';
	}
	else{
		$rows=count($result);
		for($i=0;$i<$rows;$i++){
			foreach($result as $key=>$val){
				$row[$key]=$val;
				$row['type_en']=$key;
				$out_result['history'][$i]=$row;
			}
		}
		$out_result[ok]='true';
	}
	return $out_result;
}

//видалення з БД місця роботи в кар`єрі
//на вхід массив з параметром ІД запису в табл. БД
function drop_job($arr){
    $arr[id_user]=get_cookie('id_user');
        $query="DELETE FROM `users_career` WHERE id='$arr[id]' AND id_user='".$arr[id_user]."'";
        $result=mysql_query($query);
if($result==false)
    $out_result[ok]='false';
else
    $out_result[ok]='true';
return $out_result;
}

//ADMIN
//видалення юзера з БД
//також видаляються дані зі всіх таблиць які мають відношення до юзера 
function delete_user($arr){
    foreach($arr[id] as $key=>$val){
        $query="DELETE FROM `member` WHERE id=$val";
        $result=mysql_query($query);
        $query="DELETE FROM `access_users_info` WHERE id_user=$val";
        $result=mysql_query($query);
        $query="DELETE FROM `friendes` WHERE id=$val";
        $result=mysql_query($query);
        $query="DELETE FROM `member_login` WHERE id=$val";
        $result=mysql_query($query);
    }
if($result==false)
    $out_result[ok]='false';
else
    $out_result[ok]='true';
return $out_result;
}

//ADMIN
//видалення запису з БД
//також можна прислати масив із кількома ІД / array('id'=>array('1','2','40')) /, видалення буде йти по циклу
function delete_by_id_basket($arr){
    $name_table=$arr[name_table];
    if(is_array($arr[id]))
        foreach($arr[id] as $key=>$val){
            $query="DELETE FROM `$name_table` WHERE id=$val";
            $result=mysql_query($query);
        }
    elseif(is_numeric($arr[id]))
        {
        $query="DELETE FROM `$name_table` WHERE id=$arr[id]";
        $result=mysql_query($query);
        }
if($result==false)
    $out_result[ok]='false';
else
    $out_result[ok]='true';
return $out_result;
}

//пошук юзерів по БД
//на вхід масив з параметрами пошуку
//друзів пошук відкидає
function kontakten_finden($arr){
    if(get_cookie('id_user'))$type_acc='user'; else $type_acc='sponsor'; 
    if($arr[gender]!='beide')$arr[salutation]=$arr[gender];
    
    $page=$arr[page];
    unset($arr[gender]);
    unset($arr[page]);
    if($type_acc=='user'){
        $ID_USER_coo=get_cookie('id_user');
        $FRIENDS=select_DB('friendes',array('id'=>$ID_USER_coo));
        $INITIAL=json_decode($FRIENDS[0][initial],true);//для того щоб потім показувати іконки потрібні (ПОДАВ ЗАЯВКУ В ДРУЗІ чи НІ)
        $FRIENDS=json_decode($FRIENDS[0][id_friends],true);
    }
    $colums='`first_name`,`last_name`,`firma`,`functions`,`id`,`photo`,`salutation`';
    $query = "SELECT %colums% FROM member";
    
        foreach($arr as $key=>$val){
            if($key=='search_text' || strpos($val,'all')!==false || $key=='page')continue;
            $str.= "LOWER(`$key`)='".strtolower($val)."'";
        }
        if($str) $str=str_replace("'LOWER", "' AND LOWER", $str);
    if($arr[search_text]){
        $arr[search_text]=strtolower($arr[search_text]); 
        if($str)$str.=" AND ";
        $str.= " (LOWER(`last_name`) LIKE '%$arr[search_text]%' OR LOWER(`first_name`) LIKE '%$arr[search_text]%')";
    }
    
    if($FRIENDS){
        foreach($FRIENDS as $value){
            $str_fr.="`id`!='".$value."'";
        }
        $str_fr = '('.str_replace("'`", "' AND `", $str_fr).')';
    }
    if($str && $str_fr)$str_fr=' AND '.$str_fr;
    
    if($type_acc=='user'){
        $query.=" WHERE `id`!='".$ID_USER_coo."'";
        if($str || $str_fr)$query.=' AND ';
    }elseif($str || $str_fr)
        $query.=" WHERE ";
    
    if($str)$query.=$str;
    if($str_fr)$query.=$str_fr;

    @$query_count = mysql_fetch_array(mysql_query(str_replace("%colums%", 'COUNT(*)', $query)));
    $query_count=$query_count[0];
    
    $query = str_replace("%colums%", $colums, $query);
    $page=$page*10-10;
    //echo $query." limit $page,10";
    
    $result = mysql_query($query." limit $page,10"); 
    
    while(@$row = mysql_fetch_assoc($result)){
        if(!file_exists('../uploads/'.$row[photo])){
            $row[photo]='useremptylogo.png';
        }

        if($type_acc=='user' && false!==array_search($row[id],$INITIAL))$row[initial]=true; else $row[initial]=false;  
        $kontakten['list'][]=$row;
    }
    $kontakten[count]=$query_count;
    $kontakten[type_acc]=$type_acc;
    return $kontakten;
}

//робить заявку користувача неактивною
function ablehnen($arr){
    foreach($arr[id] as $value){
        update_DB_and('application_for_event', array('visible'=>0), array('id'=>$value));
    }
    return true;
}

//видаляє гостя зі списку запрошений (точніше робить його заявку неактивною, дальше можна буде відновити)
//передається сюди ІД запису в табл. БД
function delete_kandidate($arr){
    foreach($arr[id] as $value){
        update_DB_and('guest_list', array('status'=>'ablehnen_k'), array('id'=>$value));
    }
    return true;
}

function ablehnen_k_remove($arr){
    foreach($arr[id] as $value){
        update_DB_and('guest_list', array('status'=>'null'), array('id'=>$value));
    }
    return true;
}

function update_sponsor_by_sponsor($arr){
    return update_DB_and('reg_sponsor',$arr,array('id'=>get_cookie('id_sponsor')));
}

//відновлення паролю
//приходить емейл
//відправляється на мило користувачу новий пароль і після того як він клікне по лінку з хешом то пароль зміниться на новий
function send_my_pass($arr){
    $select=select_DB('member_login',array('email'=>$arr[email]));
    if($select){
        $new_password=rand_string();
        $hash=hash("md5",rand_string());
        $hash_password = hash("md5",$new_password);
        if(new_member(array('name_table'=>'change_pass','hash'=>$hash,'member_id'=>$select[0][id],'new_pass'=>$hash_password))){
            $row=select_DB('member',array('id'=>$select[0][id]));
            $row=$row[0];
            $before = $row[before_title];
            $anrede = $row[salutation];
            $anrede=($anrede=='Weiblich')?'Frau':'Herr';
            $name = $row[first_name];
            $html="{USER} ! Sie bekommen einen neuen Ihren Passwort. Folgen Sie bitte dem Link <a href='http://".$_SERVER['SERVER_NAME'].'/user/?change_pass='.$hash."'>http://".$_SERVER['SERVER_NAME'].'/user/?change_pass='.$hash."</a> und tragen Sie den neuen Ihren Passwort $new_password ein. Später haben Sie die Möglichkein ihren passwort in Einstellungen zu verändern.\n Mit freundlichen Grüssen Face Club Team";
            $html = str_replace('{USER}',$before.' '.$anrede.' '.$name,$html);
            return send_mail($arr[email],'Neues Passwort',$html);
        }
    }
}

function send_msg($arr){
    $files='[]';
    $files=json_encode($arr[files]);
    return new_member(array('name_table'=>'messages','u_from'=>get_cookie('id_user'),'u_to'=>$arr[to],'message'=>$arr[text],'attached_file'=>$files));
}

function well_admin_post($arr){
    return new_member(array('attached_file'=>json_encode($arr[files]),'text'=>$arr[text],'type'=>$arr[type],'addiction_time'=>time(),'name_table'=>'news'));
}

function delete_my_photo(){
    return update_DB_and('member',array('photo'=>'useremptylogo.png'),array('id'=>get_cookie('id_user')));
}

function set_time_zone($arr){
    
    $date_str=explode('GMT',$arr[date]);
    $date_str=explode(' ',$date_str[1]);
    return saveCookie('timezone',(float)$date_str[0]/100,1);
}
?>