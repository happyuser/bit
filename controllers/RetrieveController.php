<?php

/*
 * Контролер вкладення іншого довільного скрипта
 * щоб його виконати
 * (використовується для того, щоб цей скрипт можна було викликати
 * у випадку залогування користувача)
 *
 */

class RetrieveController extends BaseController 
{
	
public function echoo()
{
 	// якщо немає залогованого користувача, то основний метод
 	// цього класу echoo перенаправляє бразуер на сторінку /
	if (empty($_SESSION['editor_id'])) {
		header('Location: /');
		die();
	}
	// збиваємо доступ до бази даних
	$dbConn = DBModel::getDbConn();
	if (!empty($dbConn)) {
		$dbConn = null;
	}
	// обчислюємо файл, який слід включити за допомогою змінної $GLOBALS['original_url']
	$file_to_include = preg_replace('@^/@','',$GLOBALS['original_url']);
	if (file_exists($file_to_include)) {
		// зберігаємо поточну робочу диреторію
		$dirname = getcwd();
		// змінюємо робочу директорію на ту, в якій є файл
		chdir(dirname($dirname.'/'.$file_to_include));
		// включаємо цей файл
		include basename($file_to_include);
		// змінюємо робочу директорію, на ту, що була до того
		chdir($dirname);
	}
}

}


?>