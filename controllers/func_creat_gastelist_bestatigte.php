<?php
session_start();
if($_SESSION['status']=='hostess') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");
//получение данных из гет строки
	if(!isset($_GET['id_event'])){
		$id_event=db_select('events','id','active=1','id desc');
		if(!empty($id_event)){
			$_GET['id_event']=$id_event[0]['id'];
		}
	}
	if(count_array_element($_GET, 'filter~')>0){
		$where=get_filter(array('id'=>'b.','all'=>'m.'));
	}
	if(isset($_GET['search'])){
		if(isset($where)){
			$where.=" AND ".get_search(array('id'=>'b.','all'=>'m.'));
		}
		else{
			$where=get_search(array('id'=>'b.','all'=>'m.'));
		}
	}
	if(isset($_GET['sort'])){
		$order=get_sort(array('id'=>'b.','all'=>'m.'));
	}
	else{
		$order="id";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=db_select('member_column_names',null,null,'id');
//получение данных для таблиц
	if(isset($where)){
		$where_main=$where." AND b.id_user = m.id AND b.id_event='".$_GET['id_event']."'";
	}
	else{
		$where_main="b.id_user = m.id AND b.id_event='".$_GET['id_event']."'";
	}
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			if($i==0){
				$columns="b.id";
				$columns_for_filter="b.id";
			}
			else{
				$columns.=", m.".$header_name[$i]['english'];
				$columns_for_filter="m.".$header_name[$i]['english'];
			}
			$select_opton_main[]=creat_select_filter('member m,bestatigte b',"DISTINCT $columns_for_filter",$where_main,$columns_for_filter);
		}
		$tables_data=db_select('member m,bestatigte b',$columns,$where_main,$order,$limit);
		$data['pages_number']=creat_number_page(count(db_select('member m,bestatigte b','b.id',$where_main,$order)));
		$data['select']=creat_select(db_select('events','id,name','active=1'),$_GET['id_event']);
		$data['header']=header_tables($header_name,$select_opton_main,0,4);
		$data['header_slider']=header_tables($header_name,$select_opton_main,5);
		if(count($tables_data)>0){
			$data['table']=tables($tables_data,0,4);
			$data['table_slider']=tables($tables_data,5,null);
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}
//получение данных из таблицы для модального окна
    return $data;
?>