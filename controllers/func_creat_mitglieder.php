<?php
session_start();
if($_SESSION['status']=='hostess') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");
//получение данных из таблицы
	if(count_array_element($_GET, 'filter~')>0){
		$where=get_filter();
	}
	if(isset($_GET['search'])){
		if(isset($where)){
			$where.=' AND '.get_search('member');
		}
		else{
			$where=get_search('member');
		}
	}
	if(isset($_GET['sort'])){
		$order=get_sort();
	}
	else{
		$order="id";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=db_select('member_column_names',null,null,'id');
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			if($i==0){
				$columns.=$header_name[$i]['english'];
			}
			else{
				$columns.=", ".$header_name[$i]['english'];
			}
			$select_opton_main[]=creat_select_filter('member',"DISTINCT ".$header_name[$i]['english'],$where,$header_name[$i]['english']);
		}
		$tables_data=db_select('member',$columns,$where,$order,$limit);
		$data['change_member']=creat_change_member($header_name);
		$data['pages_number']=creat_number_page(count(db_select('member','id',$where,$order)));
		$data['header']=header_tables($header_name,$select_opton_main,0,3);
		$data['header_slider']=header_tables($header_name,$select_opton_main,3);
		if(count($tables_data)>0){
			$data['table']=tables($tables_data,0,3,'m_reg');
			$data['table_slider']=tables($tables_data,3,null,'m_reg');
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}
    return $data;

function creat_change_member($header_name){
	$data=null;
	$_OPTIONS_MEMBER=get_option_for_select();
	foreach($header_name as $key => $val){
		if ($val['english']!='active' && $val['english']!='approve' && $val['english']!='profession' && $val['english']!='type_account'){
			$data.="<div class='row-fluid'>";
			if($val['english']!='id'){
				$data.="<div class='span4 offset2'>".$val['german']."</div>";
			}
			if($val['english']=='about_me' || $val['english']=='i_search_for' || $val['english']=='i_offer_field'){
				$data.="<div class='span4'><textarea style='width: 100%' name='".$val['english']."'></textarea></div>";
			}
			elseif(array_key_exists($val['english'],$_OPTIONS_MEMBER)){
				$data.="<div class='span4'><select style='width: 100%;' name='".$val['english']."'>";
					foreach($_OPTIONS_MEMBER[$val['english']] as $k=>$v){
						$data.="<option value='";
						if($val['english']!='country' && $val['english']!='bus_country' && $val['english']!='distribution'){
							$data.=$v; 
						}
						else{
							$data.=$k;
						}
						$data.="'>$v</option>";
					}
				$data.="</select></div>";
			}
			elseif($val['english']=='photo'){
				$data.="<div class='span4'><input type='file' name='photo' id='photoimg' /></div>";
			}
			elseif($val['english']=='birthdate'){
				$data.="<div class='span4 myCalendar'><input style='height: 25px; width: 100%' id='myCalendar' name='birthdate'/></div>";
			}
			elseif(strpos($val['english'], 'uhrzeit')!=false){
				$data.="<div class='span4'><select style='width: 100%' name='".$val['english']."'>";
					for($i=1;$i<=24;$i++){
						$data.="<option value='$i'>$i Uhr</option>";
					}
				$data.="</select></div>";
			}
			elseif($val['english']=='art_der_events'){
				$data.="<div class='span4'><select style='width: 100%' name='art_der_events'>".
                creat_select(db_select('event_type','event_type,event_type_name',null,null,null,'event_type_name')).
                "</select></div>";
			}
            elseif($val['english']=='id'){
				$data.="<div class='span4'><input type='hidden' name='id' value=''/></div>";
			}
            elseif($val['english']=='letzter' or $val['english']=='last_activ'){
				$data.="<div class='span4' name='".$val['english']."'></div>";
			}
			else{
				$data.="<div class='span4'><input style='height: 25px; width: 100%' maxlength='30' type='text' name='".$val['english']."' value=''/></div>";
			}
			$data.="</div>";
		}
	}
	return $data;
}	

?>