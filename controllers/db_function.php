<?php
require_once("db_method.php");
require_once("taras/filter.php");
//получение данных из таблицы
	$where=null;
	if(count_array_element($_GET, 'filter~')>0){
		$where.=get_filter();
	}
	if(isset($_GET['search'])){
		$where.=get_search($table);
	}
	if(isset($_GET['sort'])){
		$order=get_sort();
	}
	else{
		$order="order by id";
	}
	$tables_data=db_select('member',null,$where,$order);
	$counter=count($tables_data);
	if($counter>0){
		$data['pages_number']=creat_number_page($counter);
		$data['header']=header_tables(0,4);
		$data['header_slider']=header_tables(4);
		$data['table']=tables($tables_data,0,4);
		$data['table_slider']=tables($tables_data,4);
	}
	else{
		$data['header']=header_tables(0,4);
		$data['header_slider']=header_tables(4);
		$data['empty']="<div class='empty_tab'>Table is empty</div>";
	}
    return $data;

//функция по обработке фильтра
function get_filter()
{
	foreach($_GET as $key=>$val){
		if(strripos($key, 'filter~')!==false){
			$filter=explode('~', $key);
			$filters[$filter[1]]=$val;
		}
	}
	if ($filters){
		$counter_filter=count($filters);
		for($a=0;$a<$counter_filter;$a++){
			$filter=current($filters);
			$keys=key($filters);
			if($a==0){
				$where="$keys='$filter'";
			}
			else{
				$where.=" and $keys='$filter'";
			}
			$a++;
			next($filters);
		}
		return $where;
	}
}
//функция по обработке сортировки
function get_sort()
{
    $name_sort=explode('~',$_GET['sort']);
	return "order by ".$name_sort[0]." ".$name_sort[1];
}
//функция по обработке поиска
function get_search($table)
{
	$result=mysql_query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS	WHERE table_schema='u586914022_bit' AND table_name='$table'");
	$rows=mysql_num_rows($result);
	$a=0;
	for($i=0;$i<$rows;$i++){
		if($a==0){
			$where=mysql_result($result,$i)." like '%".$_GET['search']."%'";
		}
		else{
			$where.=" or ".mysql_result($result,$i)." like '%".$_GET['search']."%'";
		}
		$a++;
	}
	return $where;
}
//счет элементов в массиве
function count_array_element($massive, $sSearch)
{
	$rgResult = array_intersect_key($massive, array_flip(array_filter(array_keys($massive), function($sKey) use ($sSearch)
	{
		return preg_match('/^'.preg_quote($sSearch).'/', $sKey);
	})));
	$counter=count($rgResult);
	return $counter;
}
//формирование номеров страниц и переключателей
function creat_number_page($row_num){
	if(isset($_GET['page'])){
		$number_pages=$_GET['page'];
	}
	else{
		$number_pages=1;
	}
	$data="<li><a href='?page=1' style='height: 23px;margin-top: -1px;'> << </a></li>";
	if($number_pages>1){
		$trow_left=$number_pages-1;
		$data.="<li><a href='?page=$trow_left' style='height: 23px;margin-top: -1px;'> < </a></li>";
	}
	$num_page=ceil($row_num/10);
	if($num_page<6){
		$start_row=1;
		$end_row=$num_page;
	}
	else{
		if($number_pages>2){
			$start_row=$number_pages-2;
		}
		if($number_pages<=2){
			$start_row=1;
		}
		$end_row=$start_row+5;
	}
	for($i=$start_row;$i<=$end_row;$i++){
		if($i==$number_pages){
			$data.="<li class='active'>";
		}
		else{
			$data.="<li>";
		}
		$data.="<a href='?page=$i' style='height: 23px;margin-top: -1px;'> $i </a></li>";
	}
	if($number_pages<$num_page){
		$trow_right=$number_pages+1;
		$data.="<li><a href='?page=$trow_right' style='height: 23px;margin-top: -1px;'> > </a></li>";
	}
	$data.="<li><a href='?page=$num_page' style='height: 23px;margin-top: -1px;'> >> </a></li>";
	return $data;
}
//функция по формированию заголовка таблицы
function header_tables($start=null,$end=null)
{
	$header_data=db_select('member_column_names',null,null,'order by id');
	$data="<th class='id_th_check'";
	if($start==0){
		$data.=" id='id_th_check'";
	}
	else{
		$data.=" style='display:none'";
	}
	$data.=">ID</th>";
	if(empty($end)){
		$end=count($header_data);
	}
	for($i=$start;$i<$end;$i++){
		$data.="<th class='".$header_data[$i]['english']."_th' id='".$header_data[$i]['english']."_th'>".$header_data[$i]['german']."<div style='";
		if($header_data[$i]['english']!='last_name' && $header_data[$i]['english']!='first_name'){
			$data.="margin-top:15px;";
		}
		$data.="overflow:hidden;width:100%;height:20px;'><a class='sort' id='".$header_data[$i]['english']."~";
		if($_GET['sort']==($header_data[$i]['english'].'~asc')){
			$data.="desc";
		}
		else{
			$data.="asc";
		}
		$data.="' style='cursor: pointer;'><i class='";
		if($_GET['sort']==($header_data[$i]['english'].'~asc')){
			$data.="icon-chevron-down";
		}
		else{
			$data.="icon-chevron-up";
		}
		$data.="'></i></a>";
		$data.="<select class='icon-filter filter' style='background-color: white' name='filter~".$header_data[$i]['english']."'>
				<option value='*all'>*all</option>
				</select></div></th>";
	}
	return $data;
}
//функция формирования таблицы
function tables($datas,$start=null,$end=null){
//получение номера отображаемой страницы
	if(isset($_GET['page'])){
		$number_pages=$_GET['page'];
	}
	else{
		$number_pages=1;
	}
//получение заголовка
	$header_data=db_select('member_column_names',null,null,'order by id');
//определение конца отображения если его не определили
	if(empty($end)){
		$end=count($header_data);
	}
//формирование таблицы согласно включенной страницы
	$data=null;
	for($i=$number_pages*10-10;$i<$number_pages*10;$i++){
		if(!empty($datas[$i])){
//получаем массив рабочих мест			
			$jobs=db_select('users_career',null,"id_user='".$datas[$i]['id']."'");
//формируем строку
			$data.="<tr";
			if($datas[$i]['approve']==3){
				$data.=" class='edit_val'";
			}
			$data.=">";
//определяем айди записи
			if($start==0){
				$data.="<td class='id_td_it' id='id_td_it'><input type='checkbox' class='one_checkbox_active' value='".$datas[$i]['id']."' name='id'/>".$datas[$i]['id']."</td>";
			}
			else{
				$data.="<td class='id_td_it' style='display:none'>".$datas[$i]['id']."</td>";
			}
			$key_count=count($datas[$i]);
//формируем строку согластно необходимому колличеству столбцов
			for($a=$start;$a<$end;$a++){
				$cur_mas=$datas[$i];
				for($c=0;$c<$key_count;$c++){
					$value=current($cur_mas);
					$keys=key($cur_mas);
//сверяем ключ массива с именем заголовка
					if($header_data[$a]['english']==$keys){
						if($keys!='active' or $keys!='approve'){
							$data.="<td id='".$keys."_td' class='".$keys."_td'";
							if($datas[$i]['active']==0){ $data.='no_activ_user';}
							$data.="><p id='overflow'>";
							if($keys=='country'||$keys=='bus_country'){
								$country=db_select('countries',null,'id="$value"');
								$data.=$country[0]['country'];
							}
							else if($keys=='photo'){
								$data.="<a rel='../uploads/$value'></a>foto";
							}
							else if($keys=='professional_vita'){
								$data.="<span>Show</span><div class='berufsvita' style='display: none'>";
								if(!empty($jobs)){
									while ($value=current($jobs)){
										$data.="<span job='".key($jobs)."'><div>";
										while($val=current($value)){
											$data.="<p id='".key($value)."'>$val</p>";
											next($value);
										}
										$data.="</div></span>";
										next($jobs);
									}
								}
								$data.="</div>";
							}
							else {$data.=$value;}
							$data.="</p>";
							$data.="</td>";
							break;
						}
					}
					next($cur_mas);
				}
			}
			$data.="</tr>";
		}			
		else{
			break;
		}
	}
	return $data;
}
?>