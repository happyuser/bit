<?php
session_start();
if($_SESSION['status']=='hostess' or $_SESSION['status']=='praktikanten') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");
//получение данных из таблицы
	$index=array('id'=>'g.','type'=>'g.','last_val'=>'g.','new_val'=>'g.','id_admin'=>'g.','all'=>'m.');
	if(count_array_element($_GET, 'filter~')>0){
		$where=get_filter($index);
	}
	if(isset($_GET['search'])){
		if(isset($where)){
			$where.=" AND ".get_search('member,logs_users_edit',$index,"id,first_name,last_name,type,last_val,new_val,id_admin");
		}
		else{
			$where=get_search('member,logs_users_edit',$index,"id,first_name,last_name,type,last_val,new_val,id_admin");
		}
	}
	if(isset($_GET['sort'])){
		$order=get_sort($index);
	}
	else{
		$order="id";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=array_merge(db_select('member_column_names',null,'english="id" OR english="first_name" OR english="last_name"'),db_select('logs_users_edit_column_name',null,'english="type" OR english="last_val" OR english="new_val" OR english="id_admin"'));
//получение данных для таблиц
	if(isset($where)){
		$where_main=$where." AND g.id_user = m.id AND g.status = '0'";
	}
	else{
		$where_main="g.id_user = m.id AND g.status = '0'";
	}
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			if($i==0){
				$columns="g.id";
				$columns_for_filter="g.id";
			}
			elseif($i>0 AND $i<3){
				$columns.=", m.".$header_name[$i]['english'];
				$columns_for_filter="m.".$header_name[$i]['english'];
			}
			else{
				$columns.=", g.".$header_name[$i]['english'];
				$columns_for_filter="g.".$header_name[$i]['english'];
			}
			$select_opton_main[]=creat_select_filter('member m,logs_users_edit g',"DISTINCT $columns_for_filter",$where_main,$columns_for_filter);
		}
		$tables_data=db_select('member m,logs_users_edit g',$columns,$where_main,$order,$limit);
		$data['pages_number']=creat_number_page(count(db_select('member m,logs_users_edit g','g.id',$where_main,$order)));
		$data['header']=header_tables($header_name,$select_opton_main,0,3);
		$data['header_slider']=header_tables($header_name,$select_opton_main,3);
		if(count($tables_data)>0){
			$data['table']=tables($tables_data,0,3,"m_app");
			$data['table_slider']=tables($tables_data,3,null,"m_app");
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}
    return $data;

?>