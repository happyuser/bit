<?php
session_start();
if($_SESSION['status']=='hostess') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");
//получение данных из таблицы
	if(count_array_element($_GET, 'filter~')>0){
		$where=get_filter();
	}
	if(isset($_GET['search'])){
		if(isset($where)){
			$where.=' AND '.get_search('events');
		}
		else{
			$where=get_search('events');
		}
	}
	if(isset($_GET['sort'])){
		$order=get_sort();
	}
	else{
		$order="id";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=db_select('event_column_names',null,null,'id');
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			if($i==0){
				$columns=$header_name[$i]['english'];
			}
			else{
				$columns.=", ".$header_name[$i]['english'];
			}
			$select_opton_main[]=creat_select_filter('events',"DISTINCT ".$header_name[$i]['english'],$where,$header_name[$i]['english']);
		}
		$tables_data=db_select('events',$columns,$where,$order,$limit);
		$data['select_options']=creat_select(db_select('event_type','event_type,event_type_name',null,null,null,'event_type_name'));
		$t=db_select('event_type','event_type',null,null,'0,1','event_type_name');
		$temp['select']=$t[0]['event_type'];
		$data['pre_sponsor_name']=event_new_member($temp);
		$data['pages_number']=creat_number_page(count(db_select('events','id',$where,$order)));
		$data['header']=header_tables($header_name,$select_opton_main,0,4);
		$data['header_slider']=header_tables($header_name,$select_opton_main,4);
		if(count($tables_data)>0){
			$data['table']=tables($tables_data,0,4);
			$data['table_slider']=tables($tables_data,4);
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}

    return $data;
?>