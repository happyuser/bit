<?php

// контролер забутого пароля
class ForgotpasswordController extends BaseController
{

	// час, за який користувач повинен скористатися посиланням, присланим йому по емейлу
	// для того, щоб змінити пароль
	const SECONDS_TO_WAIT = 600;

	// метод, який завжди викликається конструктором
	// якщо є код на зміну пароля,
	// то спочатку задати метод виведення (повідомлення буде визначене)
	// потім запустити actionFormtochange
	public function actionStart()
	{
		if (!empty($_GET['code'])) {
			$this->outputMethod = 'echooMessage';
			$this->actionFormtochange();
		}	
	}
	
	// метод, який викликається для обробки
	// форми зміни пароля
	public function actionFormtochange()
	{
		// вибираємо едітор'а, для якого створений відповідний код зміни пароля		
		$this->models['editor']->filter(array('reset_pass_code'=>$_GET['code']));
		// якщо такого немає, то меседжем буде помилка
		// і завершуємо метод
		if ($this->models['editor']->count!=1) {
			$this->views['message']->text = $GLOBALS['dict']['incorrect_reset_code'];
			return;
		}
		// якщо термін використання коду витік
		// то меседжем буде теж відповідна помилка
		// завершуємо функцію
		if (time() > strtotime($this->models['editor']->reset_pass_finishtime)) {
			$this->views['message']->text = $GLOBALS['dict']['expired_reset_code'];
			return;
		}
		$postValues = $_POST;
		// якщо заданий новий пароль
		if (!empty($postValues['new_password'])) {
			if (!preg_match('//',$postValues['new_password'])) {
			}
			// якщо підтвердження пароля не співпадає з паролем
			// ставимо меседжем текст відповідної помилка
			// завершуємо виклик функції
			if ($postValues['new_password']!=$postValues['confirm_password']) {
				$this->views['newpass_by_code']->error = $GLOBALS['dict']['passwords_do_not_match'];
				$this->views['message']->text = $GLOBALS['dict']['password_do_not_match'];
				return;
			}
			// апдейтимо рядок таблиці новим зашифрованим паролем
			$this->models['editor']->sha_password = sha1($postValues['new_password']);
			$this->models['editor']->update();
			// меседж - успішна зміна пароля
			// завершуємо виклик методу
			$this->views['message']->text = $GLOBALS['dict']['password_successfully_changed'];
			return;
		}
		// якщо новго пароля не задано
		// проставляємо код у вид
		// проставляємо метод виводу
		$this->views['newpass_by_code']->code = $_GET['code'];
		$this->outputMethod = 'echooFormToChange';
	}	
	
	
	// метод при присутніх даних $_POST 
	// тут використовується, якщо емейл для відновлення 
	public function actionPost($postValues)
	{
		// якщо введений код, то це не до цієї функції
		// а до тої, що вище
		if (!empty($_GET['code'])) return;		
		
		// за замовчуванням вважаємо, що лист з кодом ми змогли надіслати
		$isActivationEmailSent = true;
		// взяти едітора з конкретним емейлом
		$this->models['editor']->filter(array('email'=>$postValues['email']));
		// якщо такий є в таблиці, то генеруємо код
		// проставляємо час, до якого можна змінити пароль
		// і заливаємо ці дані в таблицю
		if ($this->models['editor']->count==1) {
			require_once LIB_DIR . '/generate32code.php';
			$this->models['editor']->reset_pass_code = $reset_code = rand_chars();
			$this->models['editor']->reset_pass_finishtime = date('Y-m-d H:i:s', time() + self::SECONDS_TO_WAIT);
			$this->models['editor']->update();
			// вдалося або не вдалося надіслати емейл
			$isActivationEmailSent = mail(
				$this->models['editor']->email,
				$GLOBALS['dict']['reset_your_password'],
				$GLOBALS['dict']['please_reset_your_password_by_clicking'].
				$_SERVER['SERVER_NAME'].'/passwort-vergessen/?code='.$reset_code
			);			
		}
		// повідомлення на вивід про успішне чи неуспішне надсилання коду
		if ($isActivationEmailSent) {
			$this->views['message']->text = $GLOBALS['dict']['reset_pass_link_is_sended'];
		} else {
			$this->views['message']->text = $GLOBALS['dict']['error_mail_sending'];
		}
		// просталвяємо метод виводу
		$this->outputMethod = 'echooMessage';
	}

	// ядро виводу форми на запит зміни пароля за емейлом
	public function coreEcho()
	{
		$this->views['forgotpassword']->echoo();
	}

	// вивід форми на зміну пароля
	public function echooFormToChange()
	{
		$this->echooStart();
		$this->views['newpass_by_code']->echoo();
		$this->echooFinish();
	}

}

?>