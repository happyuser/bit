<?php
date_default_timezone_set('UTC'); 
require("connect_db.php");
require_once('lib_db_method.php');
require_once('lib_ajax_func.php');
function get_get($var)
{
    $var = sanitizeString($_GET[$var]);
    return $var;
}

function get_post($var)
{
    $var = sanitizeString($_POST[$var]);
    return $var;
}

function sanitizeString($var)
{
    $var = mysql_real_escape_string($var);
    $var = stripslashes($var);
    //$var = htmlentities($var, ENT_COMPAT, "cp1251");
    $var = strip_tags($var);
    return $var;
}

function DB_member_filterM($filter_approve = null, $active = null)
{
    global $_DB_member;
    $_DB_member_for_MA = array();
    $id = 1;
    if($_DB_member)
    foreach ($_DB_member as $key => $value) {

        if($active===null || $value[active] == $active){
            if (($filter_approve === null or $value[approve] == $filter_approve)) {
                $_DB_member_for_MA[$id][id] = $key;
                foreach ($value as $k => $v) {
                    $_DB_member_for_MA[$id][$k] = $v;
                }
                $id++;
            }
        }
    }
    return $_DB_member_for_MA;
}

function DB_events_filter()
{
    global $_DB_member;
    $_DB_member_for_MA = array();
    $id = 1;
    foreach ($_DB_member as $key => $value) {
        $_DB_member_for_MA[$id][id] = $key;
        foreach ($value as $k => $v) {
            $_DB_member_for_MA[$id][$k] = $v;
        }
        $id++;
    }
    return $_DB_member_for_MA;
}


function DB_guest_filter($id_event){
    $query="SELECT * FROM `guest_list` WHERE `id_event`=$id_event AND (`status`='ok' OR `status`='was' OR `status`='send')";
    $result = mysql_query($query);
    while($row = mysql_fetch_assoc($result))
        $_DB_member[]=$row;
    
    $EVENT=select_DB('events',array('id'=>$id_event));
    $EVENT=$EVENT[0];
    $cities=array(
        'München'=>'muc',
        'Berlin'=>'b',
        'Hamburg'=>'hh'
    );
    if($EVENT[type_of_event]=='mmm')
        $limit='mmm_'.$cities[$EVENT[city]].'_uhrzeit';
    else
        $limit=$EVENT[type_of_event].'_uhrzeit';
                    
        foreach ($_DB_member as $key => $value) {
            $array=array();
            if($value[type]=='guests'){
                $where[id]=$value[id_user];
                $info=select_DB('guests',$where,null);
                if($info){
                    $info[0][first_name]=$info[0][name];
                    $info[0][last_name]=$info[0][vorname];
                    unset($info[0][name],$info[0][vorname]);
                    $array[info_guest]=$value;
                    $array[info_guest][type_user]='guests';
                    $array[info_user]=$info[0];
                }
            }else{
                $where[id]=$value[id_user];
                $info=select_DB('member',$where,null);
                if($info){
                    $array[info_guest]=$value;
                    $array[info_guest][type_user]='users';
                    $array[info_user]=$info[0];
                    $array[info_user][limit]=$info[0][$limit];
                }
            }
            $_DB_guest_filter[]=$array;
            unset($array);
        }
    return $_DB_guest_filter;
}

function GET_str($str)
{
    $str = explode('=', $str);
    $_GET[$str[0]] = $str[1];
    $i = 0;
    foreach ($_GET as $key => $val) {
        $GET_str = $GET_str . $key . '=' . $val;
        $i++;
        if ($i < count($_GET))
            $GET_str = $GET_str . '& ';
    }
    return $GET_str;
}

function get_info_guests_for_event($arr){
    foreach ($arr[id] as $key => $val) $str_types .= "`member`.`distribution`='$val'";
    
    $str_types = str_replace("'`", "' OR `", $str_types);
    
$q = "SELECT * FROM `member`, `guest_list` WHERE (
(`guest_list`.`id_user` = `member`.`id`)
AND (`guest_list`.`id_event` = '$arr[for]')
AND ($str_types))";
    $result = mysql_query($q);
    while (($row = mysql_fetch_assoc($result)) !== false)
		$a[] = $row;
        return $a;
}


function sending_emails($arr) {
    // Fixed by Vovk. //Valera
    foreach ($arr[id] as $key => $val){
        if(!select_DB('status_message_type',array('id_type'=>$val,'id_event'=>$arr['for'],'type_user'=>$arr['type'])))
            new_member(array('name_table'=>'status_message_type','id_event'=>$arr['for'],'id_type'=>$val,'status'=>'send','type_user'=>$arr['type']));
        else
            update_DB_and ('status_message_type', array('status'=>'send'), array('id_event'=>$arr['for'],'id_type'=>$val,'type_user'=>$arr['type']));
                        
        if($arr[type]!='kandidaten'){
            $str_types.="`member`.`distribution`='$val'";
        }else{
            $list_kand.="`id_user`='$val'";
        }
            
        $types .= "`id`='$val'";
        if($arr['type']=='kandidaten'){
            if(select_DB('message_type_account',array('id'=>$val,'type_user'=>'kandidaten','id_event'=>$arr['for'])))
                update_DB_and('message_type_account',array('message'=>$arr[html]),array('id'=>$val,'type_user'=>'kandidaten','id_event'=>$arr['for']));
            else
                new_member(array('name_table'=>'message_type_account','message'=>$arr[html],'id'=>$val,'type_user'=>'kandidaten','id_event'=>$arr['for']));
        }
    }
    
    @$str_types = str_replace("'`", "' OR `", $str_types);
    $types = str_replace("'`", "' OR `", $types);
    if($list_kand)
        $list_kand = str_replace("'`", "' OR `", $list_kand);
    if($arr['type']!='kandidaten'){
        $name = "UPDATE `message_type_account` SET message='{$arr['html']}' WHERE ($types) AND `type_user`='{$arr['type']}' AND id_event={$arr['for']}";
        $result = mysql_query($name);
    }
    $html=$arr[html];
    if($arr['type']!='eingeladene'){//тут вибираємо уже тих хто на вєру йде на подію і шлемо їм приглас на мило
        if($arr['type']=='kandidaten'){
            $table_user='guests'; $table_list='guest_list';
            update_DB_and ('guest_list', array('status'=>'send'), array('id_event'=>$arr['for'],'type'=>'guests'));
        } else{
            $table_user='member'; $table_list='bestatigte';
        }
        //SQL ПОПРАВИТИ
        //видає копії стрічок
        if($list_kand)$types=$list_kand;
        $q = "SELECT * FROM `$table_user` WHERE `id` IN (SELECT DISTINCT(`id_user`) FROM `$table_list` WHERE ";
        if($arr['type']=='kandidaten')
            $q.=" ($types) AND ";
        $q.="`$table_list`.`id_event` = '$arr[for]' AND `$table_list`.`type`=";
        $q.=($arr['type']=='kandidaten')?"'guests')":"'eingeladene')";
        if($arr['type']=='kandidaten')
            $q.="AND `$table_user`.`activ`=1";
        else
            $q.=" AND (".$str_types.")";
        $result = mysql_query($q);
    
        $a = array();
    	while (($row = mysql_fetch_assoc($result)) !== false){
    		if($arr['type']!='kandidaten'){
                $email = $row[email_privat];
                $before = $row[before_title];
                $anrede = $row[salutation];
                if($anrede=='Weiblich')$anrede='Frau';else$anrede='Herr';
                $name = $row[last_name];
            }else{
                $email = $row[email];
                $before = ($row[anrede]=='weiblich')?'Sehr geehrte':'Sehr geehrter';
                $anrede = $row[anrede];
                if($anrede=='weiblich')$anrede='Frau';else$anrede='Herr';
                $name = $row[last_name];
            }

                $message = str_replace('Voranrede Anrede Name',$before.' '.$anrede.' '.$name,$html);
                //echo'func_1:<br />'.$email.'<br />hello<br />'.$message.'<br />';
                send_mail($email,'hello',$message);
        }
    }else{
        if($arr['type']='eingeladene'){
            $info_event=select_DB('events',array('id'=>$arr['for']));
            $info_event=$info_event[0];
            $subject = "title";
            $html="Voranrede Anrede Name,
            <br /> 
            wir freuen uns Ihnen Ihre persönliche Einladung zum/zur $info_event[last_name] zukommen zu lassen.
            <br />
            Alle weiteren Details finden Sie auf Ihrem persönlichen FaceClub-Profil.
            <br />
            Einfach <a href='http://fc.happyuser.info/'>hier</a> einloggen und zusagen!
            <br />
            Freundliche Grüße,
            <br />
            Ihr Sören Bauer Events-Team
            <br />
             
            Geschäftsführer: Sören Bauer<br /> 
            HRB 83724/ ST-Nr. 42/759/00597 Finanzamt Hamburg-Am Tierpark<br /> 
            Deutsche Bank AG, Konto Nr. 979 00 49, BLZ 200 700 00<br />
            www.soerenbauer.de;www.MovieMeetsMedia.de";
        }
        //SQL поправити
        //тут замість селекту зробити апдейт по цьому селекту, щоб РНР цим не займався
        //а може і не потрібно, так як всерівно в циклі прийдеться перебирати юзерів для розсилки на емейл
        $q = "SELECT `id`,`before_title`, `salutation`, `last_name`, `email_privat` FROM `member` WHERE `id` IN (SELECT DISTINCT(`member`.`id`) FROM `member`, `guest_list` WHERE ((`guest_list`.`id_user` = `member`.`id`) AND (`guest_list`.`id_event` = '{$arr['for']}' AND ($str_types))))";
        $result = mysql_query($q);
        while (($row = mysql_fetch_assoc($result)) !== false){
            update_DB_and ('guest_list', array('status'=>'view'), array('id_event'=>$arr['for'],'id_user'=>$row[id],'type'=>'users'));
            $anrede=($row[salutation]=='Weiblich')?'Frau':'Herr';
            $message = str_replace('Voranrede Anrede Name',$row[before_title].' '.$anrede.' '.$row[last_name],$html);
            $subject = "title";
            send_mail($row['email_privat'],$subject,$message);
        }
    }
    return true;
}
function message_type_account($arr){
    $select=select_DB('message_type_account',array('id'=>$arr[id],'type_user'=>$arr[type],'id_event'=>$arr['for']));
    return $select[0];
}

function save_emails($arr){
        foreach($arr[id] as $value){
            $str_types=$str_types."`id`='$value'";
            
            if($arr['type']=='kandidaten'){
                if(select_DB('message_type_account',array('id'=>$val,'type_user'=>'kandidaten','id_event'=>$arr['for'])))
                    update_DB_and('message_type_account',array('message'=>$arr[html]),array('id'=>$value,'type_user'=>'kandidaten','id_event'=>$arr['for']));
                else
                    new_member(array('name_table'=>'message_type_account','message'=>$arr[html],'id'=>$value,'type_user'=>'kandidaten','id_event'=>$arr['for']));
            }

        }
        
    $str_types = str_replace("'`", "' OR `", $str_types);
    $arr[html] = str_replace("'", '"', $arr[html]);
    if($arr['type']!='kandidaten'){
        $name = "UPDATE `message_type_account` SET message='$arr[html]' WHERE ($str_types) AND `type_user`='$arr[type]' AND id_event={$arr['for']}";
        $result = mysql_query($name);
    }
        foreach($arr[id] as $value){
            if(!select_DB('status_message_type',array('id_type'=>$value,'id_event'=>$arr['for'],'type_user'=>$arr['type'])))
                new_member(array('name_table'=>'status_message_type','id_event'=>$arr['for'],'id_type'=>$value,'status'=>'save','type_user'=>$arr['type']));
        }
        
    $out_result[ok]='true';
    return $out_result;
}

function change_user_for_event($arr){//добавляє юзера на подію (в адмінці) з віконця "Hinzufuegen"
    foreach($arr[id] as $key=>$val){
        $query="SELECT * FROM `guest_list` WHERE id_event=$arr[id_event] AND id_user=$val AND type='users'";
        $result=mysql_query($query);
        $rows = mysql_num_rows($result);

        if(!$rows){
            $query="INSERT INTO `guest_list`(`id`, `id_event`, `id_user`, `type`) VALUES (NULL,$arr[id_event],$val,'users')";
            $result=mysql_query($query);
            if($result){
                $info[mysql_insert_id()]=get_info_DB_id(array("id"=>$val, "name_table"=>'member'));
            }
        }
    }

if($result){
    $out_result[ok]='true';
    $out_result[list_new_user]=$info;
}
else
    $out_result[ok]='false';
return $out_result;
}

function edit_user_for_event($arr){
    //print_r($arr);
    $str='';
    foreach($arr as $key=>$val){
        $query="UPDATE `guest_list` SET ";
            if($key!='id')
              $str=$str.'`'.$key.'`="'.$val.'"';
        }
        $str = str_replace('"`', '", `', $str);
        $query=$query."$str WHERE `id`=$arr[id]";
        $result=mysql_query($query);

if($result){
    $out_result[ok]='true';
}
else
    $out_result[ok]='false';
return $out_result;
}

function edit_admin($arr){
    foreach($arr as $key=>$val){
        $query="UPDATE `editor` SET `status`='$val' WHERE `id`='$key'";
        $result=mysql_query($query);
    }

if($result)
    $out_result[ok]='true';
else
    $out_result[ok]='false';
return $out_result;
}

function get_option_select(){

$OPTIONS_MEMBER=array(
    'kart_ammount'=>array(
        0,
        1,
        2,
        3,
        '2 au?er bei MmM Berlin & Munchen, da 1',
        '2 au?er bei DC Berlin & MmM Berlin, da 1',
        '1 au?er GSA, da 2',
        '1 au?er Hamburg, da 2',
        '1 au?er MumM & GSA, da 2',
        '1 au?er MumM, da 2',
        '2 au?er Berlin, da 1',
        '1 au?er MmM Hamburg, da 2',
        '2 au?er MmM, da 1',
        '2 au?er MmM Berlin, da 1',
        '2 au?er DC, dort nur 1',
        '2 au?er MmM Munchen, da 1',
        '2 au?er HERBERT, da 1',
        '2 au?er MmM Berlin & Munchen, da 1',
        '2 au?er Hamburg, da 1',
        '2 au?er Munchen, da 1',
        'Entscheidet die PR-Agentur'
    ),
    'function'=>array(
        'e&m',
        'FdH',
        'WP',
        'SP',
        'VIP1',
        'VIP2',
        'Kein1',
        'FP',
        'P-VIP',
        'P',
        'S',
        'A',
        'R',
        'Z',
        'KL',
        'Musiker',
        'SportVip1',
        'SportVip2'
    ),
    'invitation'=> array(
        'E/P',
        'E-Mail',
        'Post'
    ),
    'travelcosts'=> array(
        'NEIN',
        'Hotel',
        'Hotel beim HERBERT',
        'Hotel Hamburg & MumM',
        'JA',
        'JA Hotel HH',
        'JA, bei MumM Hotel',
        'JA, Berlin & Munchen MmM',
        'JA, fur HH',
        'JA, HERBERT',
        'JA, HH',
        'JA, HH & MumM',
        'JA, HH Hotel',
        'Ja, HH Hotel & MumM',
        'JA, HH MmM',
        'JA, Hotel beim HERBERT',
        'JA, Hotel HH',
        'JA, Hotel MmM HH',
        'JA, HOTEL MumM',
        'JA, HoTEL MumM & MmM HH',
        'JA, MmM HH',
        'JA, MmM HH & HERBERT',
        'JA, MmM-HH',
        'JA, MumM',
        'JA, Munchen & HH',
        'MmM-HH Reise inkl. Hotel',
        'MumM Reise inkl. Hotel',
        'Hotel bei GSA'
    ),
    'time_invited'=> array(
        'ab 21 Uhr',
        'ab 22 Uhr ',
        'ab 23 Uhr',
        'BEGINN'
    ),
    'vip_zugang'=> array(
        'NEIN',
        'JA'
    ),
    'centurion'=> array(
        'NEIN',
        'JA'
    ),
    'soap'=>array(
        'NEIN',
        'Als JURY Mitglied anfragen',
        'Als Laudator anfragen',
        'JA',
        'JA, ab 21 Uhr',
        'JA, ab Beginn',
        'JA, Berlin',
        'JA, Hamburg',
        'JA, Munchen',
        'JA, wenn nicht Sponsor',
        'KAUFKARTEN',
        'nach Bedarf',
        'Uber PR-AGENTUR'
    ),
    'herbert'=>array(
        'NEIN',
        'Flanier',
        'JA',
        'JA, ab Beginn',
        'JA, ohne Begleitung',
        'JA, sofern nicht Sponsor',
        'KAUFKARTE',
        'nach Bedarf',
        'JA, wenn nicht Sponsor',
        'JA, wenn Sky nicht Sponsor ist',
        'UBER PR-AGENTUR',
        'JA, wenn BILD nicht Sponsor ist'
    ),
    'cnn'=>array(
        'NEIN',
        'JA',
        'Uber PR-AGENTUR'
    ),
    'yearcalendar'=>array(
        'NEIN',
        'JA'
    ),
    'proposition'=>array(
        'Barterdeals',
        'BERLIN EVENTS',
        'CNN, HERBERT',
        'CNN, MmM, HERBERT',
        'DC',
        'DC, MumM',
        'DC, CNN',
        'DC, CNN, HERBERT',
        'DC, MmM',
        'DC, MumM, Soap',
        'DC, SOAP',
        'DC, Soap, MumM, HERBERT',
        'FB Postings',
        'GSA',
        'GSA & MumM',
        'HERBERT',
        'HERBERT & MmM',
        'Hamburg Events',
        'KAUFKARTE',
        'MmM',
        'Kartenkauf; Herbert',
        'Kaufkarten & MumM, Herbert',
        'Mediadeals',
        'Mediavolumen',
        'MmM',
        'MmM & MumM',
        'MmM Muc',
        'MmM, CNN',
        'MumM',
        'MumM, GSA',
        'PR-leistungen',
        'Promivermittlung',
        'Shuttle-Sponsoring',
        'GSA',
        'GSA, DC, MumM'
    ),
    'personal'=>array(
        'NEIN',
        'JA'
    ),
    'directors'=>array(
        'NEIN',
        'JA'
    ),
    'branchenevents'=>array(
        'NEIN',
        'JA'
    ),
    'films'=>array(
        'NEIN',
        'JA'
    ),
    'charity'=>array(
        'NEIN',
        'JA'
    ),
    'accompaniment'=>array(
        'NEIN',
        'JA'
    ),
    'salutation'=>array(
        'Männlich',
        'Weiblich'
    ),
    'before_title'=>array(
        'Sehr geehrter',
        'Sehr geehrte'
    ),
    'du_or_sie'=>array(
        'SIE',
        'DU'
    ),
    'mmm_b'=> array(
        'NEIN',
        'JA'
    ),
    'mmm_b_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'mmm_b_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'mmm_muc'=>array(
        'NEIN',
        'JA'
    ),
	'mmm_muc_begleitung'=>array(
        'NEIN',
        'JA'
    ),
	'mmm_muc_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'mmm_hh'=>array(
        'NEIN',
        'JA'
    ),
    'mmm_hh_begleitung'=>array(
        'NEIN',
        'JA'
    ),
	'mmm_hh_karten'=>array(
        'NEIN',
        'JA'
    ),
	'mmm_hh_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'mumm_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'mumm_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'mumm'=>array(
        'NEIN',
        'JA'
    ),
    'mumm_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'dc'=> array(
        'NEIN',
        'JA'
    ),
    'dc_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'dc_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'sport'=>array(
        'NEIN',
        'JA'
    ),
    'sport_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'sport_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'sonstige'=>array(
        'NEIN',
        'JA'
    ),
    'sonstige_begleitung'=>array(
        'NEIN',
        'JA'
    ),
    'sonstige_reisek'=>array(
        'NEIN',
        'JA'
    ),
    'newsletter'=>array(
        'NEIN',
        'JA'
    )
);
$select=select_DB('type_account');
foreach($select as $value){
    $distribution[$value[english]]=$value[german]; 
}
$OPTIONS_MEMBER[distribution]=$distribution;

$select=select_DB('countries');
foreach($select as $value){
    $OPTIONS_MEMBER[bus_country][$value[id]]=$OPTIONS_MEMBER[country][$value[id]]=$value[de]; 
}

return $OPTIONS_MEMBER;
}

function new_event($arr){
    $city=select_DB('cities',array('name'=>$arr[city]));
    if(!$city)new_member(array('name_table'=>'cities','name'=>$arr[city]));
    
    $query="SELECT * FROM `events` WHERE  `id` =$arr[id]";
    $result = mysql_query($query);
    $row = mysql_fetch_assoc($result);
    $type_event=$row[type_of_event];

    $query="SELECT * FROM `member` WHERE active=1 AND `$type_event` LIKE '%JA%'";
    $result = mysql_query($query);
    @$rows = mysql_num_rows($result);
    while($row = mysql_fetch_assoc($result)){
        $query2="INSERT INTO `guest_list`(`id`, `id_event`, `id_user`, `type`,`accompany`,`time_at_event`,`status`) VALUES (NULL,$arr[id],$row[id],'users','','','null')";
        $result2 = mysql_query($query2);
    }
    $array_ids=select_DB('type_account');
    $array_types=array('eingeladene','bestatigte');
    foreach($array_ids as $value){
        foreach($array_types as $val)
        new_member(array('name_table'=>'message_type_account','id'=>$value[english],'type_user'=>$val,'id_event'=>$arr[id]));
    }
        
	if ($result) $out_result[ok]='true';
	return $out_result;
}

function edit_registration($arr){
    $time=time();
    $query = "UPDATE `guest_list` SET `time_at_event`='$time',`accompany`='$arr[accompany]', `status`='was' WHERE `id_user`='$arr[id_user]' AND `id_event`='$arr[id_event]' AND `type`='$arr[type_user]'";
    $result = mysql_query($query);
    $out_result['time']=print_date($time,'hour');
    $out_result[ok]='true';
    return $out_result;
}

function update_admin_status($arr){
    $query="UPDATE `editor` SET status='$arr[status]' WHERE id=$arr[id]";
    $result=mysql_query($query);

if($result)
$out_result[ok]='true';
else
$out_result[ok]='false';
return $out_result;
}
//функция Шнурового В. - работа с подтверждением в изменения данных пользователей
function edit_member_approve($arr){
    foreach($arr['id'] as $key=>$val){
        $pasted=db_select('logs_users_edit',null,"id='$val'");
        $status=db_update('member',"approve='0'","id='".$pasted[0]['id_user']."'");
		if($status!=false){
			if($arr['type']=='true'){
				$status=db_update('logs_users_edit','status="1"',"id='$val'");
			}else{
				$status=db_update('logs_users_edit','status="2"',"id='$val'");
				if($status!=false){
					if($pasted){
						$status=db_update('member',$pasted[0]['type']."='".$pasted[0]['last_val']."'","id='".$pasted[0]['id_user']."'");
					}
					else{
						$status='false';
					}
				}
			}
		}
    }
	if($status!=false){
		$out_result[ok]='true';
	}
	else{
		$out_result[ok]='false';
	}
	return $out_result;
}

function if_access_info($type){
    global $_USER_INFO;
    if($_USER_INFO[access_users_info][0][$type]==='0'){
        $class='no_access';
        $i='';
        $title="Fur andere Mitglieder sichtbar machen";
    }else{
        $class='';
        $i='icon-white';
        $title="Fur andere Mitglieder unsichtbar machen";
    }
    $str="
    <a href='#' class=\"block_info $class\" for=\"$type\" title=\"$title\">&nbsp;</a>";
    return $str;
}

// Created by Vovk.
function change_user_events($arr) {
    if (!get_cookie('id_user')) return false;
    $array=array('mmm_b','mmm_muc','mmm_hh','mumm','dc','sport','sonstige');
    $arr_events=explode(',', $arr['events']);
    foreach($array as $value){
        if(array_search($value,$arr_events)!==false)$result[$value]='JA'; else $result[$value]='NEIN';
    }
        $result[id]=get_cookie('id_user');
        $result[approve]='3';
        $result[msg]='Benutzerprofil bearbeiten';
        return update_user($result);
}

// Created by Vovk.
function change_user_data($arr) {
if (get_cookie('id_user'))
    return update_user_for_user($arr);
elseif(get_cookie('id_sponsor')){
    return update_DB_and('reg_sponsor',array('first_person'=>$arr[first_name]),array('id'=>get_cookie('id_sponsor')));
}

/*
	$ID_USER=get_cookie('id_user');
    $rand=rand_string();
	$q = "SELECT * FROM member WHERE id=$ID_USER";
    $result=mysql_query($q);
    $row = mysql_fetch_assoc($result);
	$q = "UPDATE member SET ";
	foreach ($arr as $k => $v) {
 	  if($k=='msg')continue;
        $date=date('Y-m-d',time());
        $query="INSERT INTO `logs_users_edit`(`id_user`, `type`, `last_val`, `new_val`, `date`, `gen_key`, `msg`) VALUES ($ID_USER,'$k','$row[$k]','$v','$date','$rand','$arr[msg]')";
        mysql_query($query);
		if (strpos($q, "=") !== false) $q .= ", ";
		$q .= ("$k='" . mysql_real_escape_string($v) . "'");
	}
	$q .= " WHERE id=$ID_USER";
    mysql_query($q);
    if($arr[first_name] || $arr[last_name]){
        $q = "UPDATE member SET approve=3 WHERE id=$ID_USER";
        mysql_query($q);
    }
	return array('ok' => 'true');
 */
}


// Created by Vovk.//Valera
// $arr - ����� �������� �� ������� ������.
function change_user_login_data($arr) {
    if(get_cookie('id_user')){$table='member_login';$id=get_cookie('id_user');} elseif(get_cookie('id_sponsor')) {$table='reg_sponsor';$id=get_cookie('id_sponsor');}
	$q = "UPDATE $table SET ";
	foreach ($arr as $k => $v) {
		$value = mysql_real_escape_string($v);
		if (strpos($q, "=") !== false) $q .= ", ";
		$q .= ("$k=" . (($k == 'password') ? "PASSWORD('$value')" : "'$value'"));
	}
	$q .= " WHERE id=$id";
	return mysql_query($q) ? array('ok' => 'true') : array('ok' => 'false');


}


// Created by Vovk.
// FindEvents ���� ��䳿 �� ����. ���� ��������� ���� < 4 �� ���� ������������ �� ���.
/*function FindEvents($name, $city, $id) {
	$result = array();
	$q = "SELECT * FROM events WHERE name='" . mysql_real_escape_string($name) . "'";
	if (($r = mysql_query($q)) !== FALSE) {
		while (($a = mysql_fetch_assoc($r)) !== FALSE && $a[id]!=$id) $result[] = $a;
		mysql_free_result($r);
		if (count($result) < 4) {
			$events=select_DB('events', array('city'=>$city), array('dates'=>'asc'));
            if($events)
            foreach ($events as $key => $value) {
                if($value[id]!=$id)
				$result[]=$value;
			}
		}
	} else print "Error in: $q<br />";
	return $result;
}
*/
/* deprecated - not used now
// Created by Vovk.
// check_pass �������� �� ���������� ������.
// ���� ������ ����������, $access ���� ���� OK. ������ $access ���� ���� ERROR.
*/


function change_user_password($arr) {
    if(strlen($arr[new_password])<=0)return array('ok' => 'false');
	if(get_cookie('id_user')){$table='member_login';$id=get_cookie('id_user');} elseif(get_cookie('id_sponsor')) {$table='reg_sponsor';$id=get_cookie('id_sponsor');}
    return update_DB_and($table,array('password'=>hash('md5',$arr['new_password'])),array('id'=>$id));
}

function check_pass($arr) {
    if(get_cookie('id_user')){$table='member_login';$id=get_cookie('id_user');} elseif(get_cookie('id_sponsor')) {$table='reg_sponsor';$id=get_cookie('id_sponsor');}
    $result=select_DB($table,array('id'=>$id,'password'=>hash("md5",$arr['password'])));
    if ($result)
		$access = 'OK';
	else
		$access = 'ERROR';
        
    return $access;
}


function user_login($arr) {
    session_start();
    $username=$arr['email'];
	$password=$arr['password'];
	$hashPassword = hash("md5", $password);
	$query = "SELECT id FROM member_login WHERE email='".$arr['email']."' AND password='$hashPassword'";
	$result = mysql_query($query);
	$count = mysql_num_rows($result);
	$result = mysql_fetch_array($result);
	if($count == 1){
	    if(isset($arr[save])){
            saveCookie("id_user",$result['id'],true);// echo '<br />1';
            //echo get_cookie("id_user");
        }else{
            saveCookie("id_user",$result['id']);// echo '<br />2';
        }
        $_SESSION[hash_login_rand]=md5(rand(1,100).time());
        $_SESSION[hash_login_check]=md5(md5($result['id']).$_SESSION[hash_login_rand]);
       delCookie("id_sponsor",get_cookie('id_sponsor'));
       update_DB_and('member', array('last_activ'=>time()), array('id'=>$result['id']));
       $access[ok] = 'true';
       
    }else{
        $select=select_DB('reg_sponsor', array('email'=>$username,'password'=>$hashPassword,'activ'=>1), null);
        if($select[0]){
            if(isset($arr[save]))
                saveCookie("id_sponsor",$select[0][id],true);
            else
                saveCookie("id_sponsor",$select[0][id]);
                
            delCookie("id_user",get_cookie('id_user'));
            $access[ok] = 'true';
            
            $_SESSION[hash_login_rand]=md5(rand(1,100).time());
            $_SESSION[hash_login_check]=md5(md5($select[0][id]).$_SESSION[hash_login_rand]);
        }
        else
            $access[ok] = 'false';
	}
    
//    if($access[ok]==true)unset($_SESSION['admin_id']);
    return $access;
}

// Created by Vovk.
function GetNewsComments($arr) {
    if(get_cookie('id_sponsor')){
        $TYPE_ACCOUNT='sponsor';
        $my_id=get_cookie('id_sponsor');
    } else {
        $TYPE_ACCOUNT='user';
        $my_id=get_cookie('id_user');
    }
 $q = "SELECT * from news_comments WHERE news_id='${arr['id']}'";
 if($arr['last_id']) $q.= " AND id > '${arr['last_id']}'";
 //echo $q;
 if (($r = mysql_query($q)) !== FALSE) {
  $comments = array();
  while (($a = mysql_fetch_assoc($r)) !== FALSE){
    $a[youtube]=json_decode($a[youtube]);
    $a[time]=print_date($a[time],'hour');
    $a[attached_file]=json_decode($a[attached_file],true);
    if($a[attached_file])
    foreach($a[attached_file] as $key=>$val){
        if(!file_exists('../uploads/'.$val))unset($a[attached_file][$key]);
    }
    $a[liked_members]=json_decode($a[liked_members],true);
    if(count($a[liked_members])){
        foreach($a[liked_members] as $index=>$type){
            if($index=='sponsors')
                $table='reg_sponsor';
            else
                $table='member';
            foreach($type as $idx=>$id_user){
                $info_user=select_DB($table,array('id'=>$id_user),0,0,0,array('photo'));
                if(!file_exists('../uploads/crop_img/'.$info_user[0][photo]) || !$info_user)
                    $type[$idx]='../uploads/crop_img/useremptylogo.png';
                else
                    $type[$idx]='../uploads/'.$info_user[0][photo];
            }
            $a[liked_members][$index]=$type;
        }
    }else unset($a[liked_members]);
    
    $a[my_comment]=(($TYPE_ACCOUNT==$a[type] && $a[member_id]===$my_id)) ? true : false;
    
    if($a['type']=='sponsor'){
        $table='reg_sponsor';
        $column='photo,id,firmenname';
    }else{
        $table='member';
        $column='photo,id,first_name,last_name';
    }
    $select=mysql_query("SELECT $column FROM $table WHERE `id`=$a[member_id]");
    if($select)
    $member=mysql_fetch_assoc($select);
    if(!$member){
        $member[photo]='../uploads/crop_img/useremptylogo.png';
        $member[first_name]='DELETED';
        $deleted_user=true;
    }else{
        $member[photo]='../uploads/crop_img/'.$member[photo];
    }
    if($a['type']=='sponsor' && !$deleted_user){
        $member[first_name]=$member[firmenname];
        unset($member[firmenname]);
    }//якщо це дані від спонсора
	if(!file_exists($member[photo]) || !$member[photo])$member[photo]='../uploads/crop_img/useremptylogo.png';
    $a[info_user]=$member;
    array_push($comments,$a);
  }
  mysql_free_result($r);
  return array('ok' => 'true', 'result' => $comments);
 } else return array('ok' => 'false', 'message' => ("Error in: $q\n" . mysql_error()));
}

// Created by Vovk.
function FindNews($arr) {
 $q = "SELECT * from news WHERE type='${arr['type']}'";
 if (($r = mysql_query($q)) !== FALSE) {
  $news = array();
  while (($a = mysql_fetch_assoc($r)) !== FALSE) {
   $news[$a['id']] = $a;
   unset($news[$a['id']]['id']);
   $comments = GetNewsComments(array('id' => $a['id']));
   if ($comments['ok'] == 'true') $news[$a['id']]['comments'] = $comments['result'];
  }
  mysql_free_result($r);
  return array('ok' => 'true', 'result' => $news);
 } else return array('ok' => 'false', 'message' => ("Error in: $q\n" . mysql_error()));
}

// Created by Vovk.
function get_new_posts_and_comments($arr) {
    if(get_cookie('id_sponsor')){
        $TYPE_ACCOUNT='sponsor';
        $my_id=get_cookie('id_sponsor');
    } else {
        $TYPE_ACCOUNT='user';
        $my_id=get_cookie('id_user');
    }
	$max_id=0; $new_comments = array();
    $min_id=false;
    //$arr['posts']='187=77,188=,189=,190=78,191=,192=,193=';
	if (!empty($arr['posts'])) {
		foreach (explode(',', $arr['posts']) as $v) {
			// $a[0] - post id. $a[1] - last comment id.
			$a = explode('=', $v);
			if    (count($a) != 2)
				return array('ok' => 'false', 'message' => "Error in: '$v' of posts argument.");
			if ($a[0] > $max_id) $max_id = $a[0];
            if (!$min_id) $min_id=$a[0];
            if ($a[0] < $min_id) $min_id = $a[0];
			$comments = GetNewsComments(array('id' => $a[0],'last_id'=>$a[1]));
            
			if ($comments['ok'] == 'true') {
				if (count($comments['result'])) $new_comments[$a[0]] = $comments['result'];
			}
		}
	}
    if(!$arr['type'] && !$arr['post_id_user'])$arr['type']='alle';
    
    if(($arr['type']=='friends' || $arr['type']=='alle') && $TYPE_ACCOUNT!='sponsor'){
    $my_friends=select_DB('friendes', array('id'=>$my_id));
    $my_friends=json_decode($my_friends[0]['id_friends'],true);
        if($my_friends)
            foreach($my_friends as $value){
                $str.="`member_id`='".$value."'";
            }
        else
            $str="`member_id`='0'";//якщо у тіпа немає друзів
    }elseif($arr['type']=='sponsors'){
        $str.="`type`='sponsor'";
    }elseif($arr['type']=='sbe'){
        $str.="`type`='sbe'";
    }elseif($arr['type']=='club'){
        $str.="`type`='club'";
    }
    $str = str_replace("'`", "' OR `", $str);
    if($arr['post_id_user']){
        if($str)
        $str="(".$str.") AND ";
        $str.="(`member_id`='".$arr['post_id_user']."')";
    }
    
    if($arr['type']=='alle'){
        $str.=($str)?"OR":""; $str.=" `type`='sbe' OR `type`='sponsor' OR `type`='club' OR `member_id`=".$my_id;
    }
		if(!$arr[start]){
            $q = "SELECT * from news WHERE ($str) and (id > $max_id) order by id desc limit 0,5";//.(($arr['type'] != 'all') ? "id > $max_id" : "(id > $max_id) and (type='$arr[type]')");
        }else{
            $q = "SELECT * from news WHERE ($str) and (id < $min_id) order by id desc limit 0,5";//.(($arr['type'] != 'all') ? "id < $min_id" : "(type='$arr[type]')")." order by id desc limit 0,5";
        }
        //echo $q;
	if (($r = mysql_query($q)) !== FALSE) {
		$new_posts = array();
		while (($a = mysql_fetch_assoc($r)) !== FALSE) {
            $a[youtube]=json_decode($a[youtube]);
            $a[attached_file]=json_decode($a[attached_file],true);
            if($a[attached_file])
            foreach($a[attached_file] as $key=>$val){
                if(!file_exists('../uploads/'.$val))unset($a[attached_file][$key]);
            }
            $a[liked_members]=json_decode($a[liked_members],true);
            $a[addiction_time]=print_date($a[addiction_time],'hour');
            if(count($a[liked_members])){
                foreach($a[liked_members] as $index=>$type){
                    if($index=='sponsors')
                        $table='reg_sponsor';
                    else
                        $table='member';
                    foreach($type as $idx=>$id_user){
                        $info_user=select_DB($table,array('id'=>$id_user),0,0,0,array('photo'));
                        if(!file_exists('../uploads/crop_img/'.$info_user[0][photo]) || !$info_user)
                            $type[$idx]='../uploads/crop_img/useremptylogo.png';
                        else
                            $type[$idx]='../uploads/crop_img/'.$info_user[0][photo];
                    }
                    $a[liked_members][$index]=$type;
                }
            }else unset($a[liked_members]);
            
            $new_posts[$a['id']] = $a;
            $images_folder=false;
            unset($table);
            if($a['type']=='sponsor'){
                $table='reg_sponsor';
                $column='photo,id,firmenname';
            }elseif($a['type']=='user'){
                $table='member';
                $column='photo,id,first_name,last_name';
            }
            unset($member);
            if($table){
                $select=mysql_query("SELECT $column FROM $table WHERE `id`=$a[member_id]");
                if($select)
                $member=mysql_fetch_assoc($select);
                if(!$member){
                    $member[photo]='../uploads/crop_img/useremptylogo.png';
                    $member[first_name]='DELETED';
                    $deleted_user=true;
                }
            }else{
                $member[first_name]='SBE';
                $member[photo]='../images/admin_ava.jpg';
                $images_folder=true;
            }
            if($a['type']=='sponsor' && !$deleted_user){
                $member[first_name]=$member[firmenname];
                unset($member[firmenname]);
            }//якщо це дані від спонсора
            
			if(!$images_folder){
			    if(!file_exists('../uploads/crop_img/'.$member[photo]) || !$member[photo])
                    $member[photo]='useremptylogo.png';
                    
                $member[photo]='../uploads/crop_img/'.$member[photo];
            }
            $new_posts[$a['id']][info_user]=$member;
            //$new_posts[$a['id']][my_post]=($new_posts[$a['id']][member_id]==get_cookie('id_user'))? true : false;

            $new_posts[$a['id']][my_post]=(($TYPE_ACCOUNT==$a[type] && $a[member_id]===$my_id)) ? true : false;
			
            unset($new_posts[$a['id']]['id']);
			$comments = GetNewsComments(array('id' => $a['id']));
			if ($comments['ok'] == 'true') $new_comments[$a['id']] = $comments['result'];
            //print_r($new_comments);
		}
		mysql_free_result($r);
		return array('ok' => 'true', 'new_posts' => $new_posts, 'new_comments' => $new_comments);
	} else return array('ok' => 'false', 'message' => ("Error in: $q\n" . mysql_error()));
}

function rand_string($chars = '8bm9e19th2lqqk6i41if083b94',$lenght=10){
    $numChars = strlen($chars);
    $string = '';
    for ($i = 0; $i < $lenght; $i++) {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
    }
    return $gen_key=$string;
}
                
function new_guest($arr){
    if(!get_cookie('hash') && !get_cookie('id_guest')){
        $arr[name_table]='guests';
        $arr[hash]=rand_string();
        foreach($arr as $key=>$val){
            if(!$val){return false;exit;}
        }
        $arr[activ]=0;
        
        if($arr[anrede]=='weiblich'){$accost='Frau'; $sex='geehrte';} else {$accost='Herr'; $sex='geehrter';} 
        $message=
            '<table style="width: 100%; border-top: 1px; border-bottom: 1px; border-left: 0px; border-right: 0px; border-style: solid; border-color: #000000;">
    			<thead></thead>
    			<tbody>
    				<tr><td><a href="">Sehr '.$sex.' '.$accost.' '.$arr[name].',</a></td></tr>
    				<tr style="height: 10px;"></tr>
    				<tr><td>Vielen Dank für Ihre Bewerbung bei Face Club!</td></tr>
    				<tr style="height: 10px;"></tr>
    				<tr><td>Ihr einmaliger Zugangscode lautet: '.$arr[hash].'</td></tr>
    				<tr style="height: 10px;"></tr>
    				<tr><td>Nachdem Sie sich mit Ihrem einmaligen Zugangscode angemeldet haben und alle relevanten Daten angegeben haben, werden wir diese prüfen und uns umgehend bei Ihnen melden. </td></tr>
    				<tr style="height: 10px;"></tr>
    				<tr><td>Freundliche Grüße,</td></tr>
    				<tr style="height: 10px;"></tr>
    				<tr><td>das Face Club - Team</td></tr>
    			</tbody>
    		</table>';
        send_mail($arr[email],'Bewerbung Code',$message);
        $new_guest=new_member($arr);
        saveCookie('id_guest',$new_guest[id]);
        return $new_guest;
    }else
        return false;
}

function saveCookie($name,$val,$long=false)
{
    if(!$long)$time = time() + 60 * 60 * 24 * 30; else $time = time()+60*60*24*360;
    setcookie($name, $val, $time, '/');
}

function delCookie($name)
{
    $time = time() - 60 * 60 * 24 * 30;
    setcookie($name, $val, $time, '/');
}

function get_cookie($var)
{
    $var = sanitizeString($_COOKIE[$var]);
    return $var;
}

function new_post_well($arr){
    if(get_cookie('id_user')){
        $arr[member_id]=get_cookie('id_user');
        $arr[type]='user';
    }else{
        $arr[member_id]=get_cookie('id_sponsor');
        $arr[type]='sponsor';
    }
    $arr[addiction_time]=time();
    $arr[name_table]='news';
    $arr[text]=htmlspecialchars($arr[text]);
    $arr[youtube]=json_encode($arr[youtube]);
    if(count($arr[images])>10)
    $count=count($arr[images]);
        for($i=10;$i<$count;$i++){
            unset($arr[images][$i]);
        }
        $arr[attached_file]=json_encode($arr[images]);
    unset($arr[images]);
    return new_member($arr);
}

function cropStr($string, $size){
    $str=explode(' ',$string);
    if(count($str)<=$size)
        return implode(' ',$str);
    else 
        $str[$size].='<div class="view_crop">mehr anzeigen...</div><div class="crop_str">';
        return implode(' ',$str).'</div>';
}

function new_comment_post($arr){
    if(get_cookie('id_user')){
        $arr[member_id]=get_cookie('id_user');
        $arr[type]='user';
    }elseif(get_cookie('id_sponsor')){
        $arr[member_id]=get_cookie('id_sponsor');
        $arr[type]='sponsor';
    }        
    $arr[time]=time();
    $arr[text]=htmlspecialchars($arr[text]);
    $arr[name_table]='news_comments';
    $arr[youtube]=json_encode($arr[youtube]);
    if(count($arr[images])>10)
    $count=count($arr[images]);
        for($i=10;$i<$count;$i++){
            unset($arr[images][$i]);
        }
        $arr[attached_file]=json_encode($arr[images]);
    unset($arr[images]);
    return new_member($arr);
}

function check_hash_guest($arr){
    session_start();
    $id_user=get_cookie('id_guest');
    $hash=get_cookie('hash');
    $_SESSION['step']=1;
    $id_guest=get_cookie('id_guest');
    $guest=select_DB('guests', array('id'=>$id_guest, 'hash'=>$arr[hash]),null);
    if($guest){
        saveCookie('hash',$arr[hash]);
        return true;
    }else{
        return false;
    }
}

function save_guest_step($arr){
    session_start();
    $_SESSION['step']=$arr[id_step]+1;
    $id_guest=get_cookie('id_guest');
    $hash=get_cookie('hash');
    if($arr[id_step]==4){
        $info=select_DB('guests',array('id'=>$id_guest,'hash'=>$hash));
        $info=$info[0];
        if($info[email] && select_DB('guests',array('email'=>$info[email],'event'=>$arr[event])))
            return false;
    }
    if($arr[id_step]==5){
        $arr[activ]=1;
        delCookie('hash');
        delCookie('id_guest');
        unset($_SESSION['step']);
    }
    unset($arr[id_step]);
    
    //print_r($_SESSION);
    return update_DB_and ('guests', $arr, array('id'=>$id_guest,'hash'=>$hash));
}

function exit_site(){
    //unset($_SESSION[my_info]);
    if (get_cookie('id_user'))return delCookie('id_user'); else return delCookie('id_sponsor'); 
}

function send_mail($to,$title,$message,$subject=null){
    if(is_array($to))$to=implode(', ',$to);
    $headers = "Content-type: text/html; charset=utf-8\r\n" .
                   "From: FaceClub.de <fc@happyuser.info>\r\n" .
//             "Bcc: uptofly73@gmail.com\r\n" .
                   "X-Mailer: PHP/" . phpversion();
                   //echo $to.'<br />'.$title.'<br />'.$message.'<br />'.$headers;
    return mail($to,$title,$message,$headers);
}

function send_hash_guest(){
    $id_guest=get_cookie('id_guest');
    $guest=select_DB('guests', array('id'=>$id_guest),null);
    return send_mail($guest[0][email],'hash',$guest[0][hash]);
}

function update_mail_guest($arr){
    update_DB_and ('guests', array('email'=>$arr[new_email]), array('id'=>get_cookie('id_guest'),'activ'=>0));
    $guest=select_DB('guests', array('id'=>get_cookie('id_guest'),'activ'=>0),null);
    return send_mail($arr[new_email],'new email',$guest[0][hash]);
}

//добавление новых событий
function new_event_list($arr){
    foreach($arr[points][txt] as $key=>$val){
        if($val)$points[]=$val.';'.$arr[points][time][$key];
    }
	$main_sponsor=$arr['main_sponsor'];
	unset($arr['main_sponsor']);
	$sub_sponsor=$arr['sub_sponsor'];
	unset($arr['sub_sponsor']);
	$temp=$arr['rest_sponsor'];
	unset($arr['rest_sponsor']);
    $arr[points]=json_encode($points);
    $arr[images]=json_encode($arr[images]);
    //$arr[rest_sponsor]=json_encode($arr[rest_sponsor]);
    $arr[name_table]='events';
    $arr[background]=htmlspecialchars($arr[background],ENT_QUOTES);
    $new_event=new_member($arr);
	if(!empty($main_sponsor)){
	   new_member(array('name_table'=>'sponsor_in_event','id_sponsor'=>$main_sponsor,'id_event'=>$new_event[id],'type_sponsor'=>'main_sponsor'));
	}
	if(!empty($sub_sponsor)){
	   new_member(array('name_table'=>'sponsor_in_event','id_sponsor'=>$sub_sponsor,'id_event'=>$new_event[id],'type_sponsor'=>'sub_sponsor'));
	}
    if($temp)
    foreach($temp as $value){
		if(!empty($value)){
            new_member(array('name_table'=>'sponsor_in_event','id_sponsor'=>$value,'id_event'=>$new_event[id],'type_sponsor'=>'rest_sponsor'));
		}
	}
    return array('ok'=>'true','id'=>$new_event[id]);
}

function delete_comm_post($arr){//type=table; id_post/comment
    if($arr[type]=='post')$arr_dlt[name_table]='news'; else $arr_dlt[name_table]='news_comments';
    if(get_cookie('id_user'))
        $member_id=get_cookie('id_user');
    else
        $member_id=get_cookie('id_sponsor');
    
    $arr_dlt[id]=$arr[id];
    if(select_DB($arr_dlt[name_table], array('id'=>$arr_dlt[id], 'member_id'=>$member_id),null))
        delete_by_id_basket($arr_dlt);
        
        if(is_array($arr_dlt[id]))
            foreach($arr_dlt[id] as $key=>$val){
                $query="DELETE FROM `news_comments` WHERE news_id=$val";
                $result=mysql_query($query);
            }
        elseif(is_numeric($arr_dlt[id])){
            $query="DELETE FROM `news_comments` WHERE news_id=$arr_dlt[id]";
            $result=mysql_query($query);
        }

        
}

function like_comm_post($arr){//type=table; id_post/comment
    if($arr[type]=='post')$arr[name_table]='news'; else $arr[name_table]='news_comments';
    $POST=select_DB($arr[name_table], array('id'=>$arr[id]));
    $POST[0][liked_members]=json_decode($POST[0][liked_members],true);
    
    if(get_cookie('id_user')){
        $type_user='users';
        $ID_coo=get_cookie('id_user');
    }else{
        $type_user='sponsors';
        $ID_coo=get_cookie('id_sponsor');
    }
    
    $index_=false;
    if($POST[0][liked_members][$type_user])
        $index_=array_search($ID_coo,$POST[0][liked_members][$type_user]);
    else
        $POST[0][liked_members][$type_user]=array();
    if($index_!==false)
        unset($POST[0][liked_members][$type_user][$index_]);
    else
        array_push($POST[0][liked_members][$type_user],$ID_coo);
        
    if(count($POST[0][liked_members][$type_user])==0)
        unset($POST[0][liked_members][$type_user]);
    $liked_members=json_encode($POST[0][liked_members]);
    return update_DB_and ($arr[name_table], array('liked_members'=>$liked_members), array('id'=>$arr[id]));
}

function get_info_post_comment($arr){
    session_start();
    if(get_cookie('id_user')){
        $member_id=get_cookie('id_user');
    }elseif(get_cookie('id_sponsor')){
        $member_id=get_cookie('id_sponsor');
    }elseif($_SESSION['admin_id']){
        $member_id=0;
    }
    if($arr[type]=='post')$arr[name_table]='news'; else $arr[name_table]='news_comments';
    $POST=select_DB($arr[name_table], array('id'=>$arr[id],'member_id'=>$member_id));
    $POST[0][youtube]=json_decode($POST[0][youtube],true);
    $POST[0][attached_file]=json_decode($POST[0][attached_file],true);
    $POST[0][addiction_time]=print_date($POST[0][addiction_time],'hour');
    return $POST;
}

function edit_post($arr){
    if($arr[type]=='post'){
        $table='news';
        $set[addiction_time]=time();
    } else {
        $table='news_comments';
        $set[time]=time();
    }
    
    session_start();
    if(get_cookie('id_user')){
        $member_id=get_cookie('id_user');
    }elseif(get_cookie('id_sponsor')){
        $member_id=get_cookie('id_sponsor');
    }elseif($_SESSION['admin_id']){
        $member_id=0;
    }
    
    if(!$arr[images] && !$arr[youtube] && !$arr[text])
    return array('problem'=>'empty_form');
    if($arr[images])$set[attached_file]=json_encode($arr[images]);
    else $set[attached_file]='';
    if($arr[youtube])$set[youtube]=json_encode($arr[youtube]);
    else $set[youtube]='';
    if($arr[text])$set[text]=$arr[text];
    else $set[text]='';
    if($arr[type_post])$set[type]=$arr[type_post];
    else{
        $select_post=select_DB($table,array('id'=>$arr[id],'member_id'=>$member_id));
        $set[type]=$select_post[0][type];
    }
    $data=update_DB_and($table,$set,array('id'=>$arr[id],'member_id'=>$member_id));
    if($arr[youtube])
        foreach($arr[youtube] as $key=>$value){
            $data[youtubes].='<div class="sub_attached_photo" style="margin: 0;">
                            <iframe width="100%" height="200" src="https://www.youtube.com/embed/'.$value.'" frameborder="0" allowfullscreen=""></iframe>
                        </div>';
        }
    else
        $data[youtubes]=' ';
        
    if($arr[images])
        foreach($arr[images] as $key=>$value){
            $data[files].='<div class="sub_attached_photo">
                        <img src="/uploads/'.$value.'">                        
                    </div>';
        }
    else
        $data[files]=' ';
    $data[text]=$arr[text];
    return $data; 
}

function move_guest_to_sponsor($arr){
    $select=select_DB('guests', array('hash'=>get_cookie('hash'),'id'=>get_cookie('id_guest')),null);
    $new_sponsor=new_member(array('hash'=>$select[0][hash],'name_table'=>'reg_sponsor','email'=>$select[0][email]));
    saveCookie('id_sponsor',$new_sponsor[id]);
    return delete_by_id_basket(array('id'=>$select[0][id],'name_table'=>'guests'));
}

function save_sponsor_step($arr){
    if($arr[event_type]){
        $arr[activ]=1;
    }
    if($arr[password])$arr[password] = hash("md5", $arr[password]);
    return update_DB_and ('reg_sponsor', $arr, array('id'=>get_cookie('id_sponsor'),'hash'=>get_cookie('hash')));
}

function access_edit_profil($id,$style=null){
    if(MY_PAGE){
        if($style)$style='style="'.$style.'"';
        return '<a href="#" title="Bearbeiten" class="edit_icon" id="'.$id.'">&nbsp;</a>';
    }
}

function access_info_block($type){
    $ID_USER=get_get('id');
    $ID_USER_coo=get_cookie('id_user');
    if(!$ID_USER)$ID_USER=$ID_USER_coo;
    if($ID_USER==$ID_USER_coo)
        return true;
    else{
        $select=select_DB('access_users_info', array('id_user'=>$ID_USER),null);
        if($select[0][$type])return true; else return false;
    }
}

function update_friend($arr){
    $arr[name_table]='friendes';
    $ID_USER=get_cookie('id_user');
    if($arr[id]==$ID_USER)exit;
    $DATA_MY=select_DB($arr[name_table], array('id'=>$ID_USER),null);
    $DATA_MY=$DATA_MY[0];
    $DATA_USER=select_DB($arr[name_table], array('id'=>$arr[id]),null);
    $DATA_USER=$DATA_USER[0];
    
    $DATA_MY[id_friends]=json_decode($DATA_MY[id_friends],true);
    $DATA_MY[initial]=json_decode($DATA_MY[initial],true);
    $DATA_MY[incoming]=json_decode($DATA_MY[incoming],true);
    
    $DATA_USER[id_friends]=json_decode($DATA_USER[id_friends],true);
    $DATA_USER[initial]=json_decode($DATA_USER[initial],true);
    $DATA_USER[incoming]=json_decode($DATA_USER[incoming],true);
    if(!isset($arr[status]))
        if(($index_=array_search($arr[id],$DATA_MY[id_friends]))!==false && $DATA_MY[id_friends]){//видалити друга
            unset($DATA_MY[id_friends][$index_]);
            $status='del_friend';
        }elseif(($index_=array_search($arr[id],$DATA_MY[initial]))!==false && $DATA_MY[initial]){//забрати у себе з вихідних заявок І у нього із вхідних
            unset($DATA_MY[initial][$index_]);
            $status='del_initial';
        }elseif(($index_=array_search($arr[id],$DATA_MY[incoming]))!==false && $DATA_MY[incoming]){//добавити в друзі, забрати із моїх вихідних і його вхідних
            unset($DATA_MY[incoming][$index_]);
            $status='add_friend';
        }else//подати заявку на дружбу, добавити у себе до вихідних і у нього до вхідних
            $status='add_initial';
    else
        $status=$arr[status];
        
        //echo $status;
        
        if($status=='add_friend'){
            $DATA_MY[id_friends][]=$arr[id];
            $incoming=json_encode($DATA_MY[incoming]);
            $friends=json_encode($DATA_MY[id_friends]);
            update_DB_and ($arr[name_table], array('id_friends'=>$friends,'incoming'=>$incoming), array('id'=>$ID_USER)); // добавили собі в друзі
            
            $DATA_USER[id_friends][]=$ID_USER;
            $index_=array_search($ID_USER,$DATA_USER[initial]);
            unset($DATA_USER[initial][$index_]);
            $initial=json_encode($DATA_USER[initial]);
            $friends=json_encode($DATA_USER[id_friends]);
            update_DB_and ($arr[name_table], array('id_friends'=>$friends,'initial'=>$initial), array('id'=>$arr[id])); // добавили йому себе в друзі
        }elseif($status=='del_initial'){
            $initial=json_encode($DATA_MY[initial]);
            update_DB_and ($arr[name_table], array('initial'=>$initial), array('id'=>$ID_USER));
            
            $index_=array_search($ID_USER,$DATA_USER[incoming]);
            unset($DATA_USER[incoming][$index_]);
            $incoming=json_encode($DATA_USER[incoming]);
            update_DB_and ($arr[name_table], array('incoming'=>$incoming), array('id'=>$arr[id]));
        }elseif($status=='del_friend'){
            $friends=json_encode($DATA_MY[id_friends]);
            update_DB_and ($arr[name_table], array('id_friends'=>$friends), array('id'=>$ID_USER));
            
            $index_=array_search($ID_USER,$DATA_USER[id_friends]);
            unset($DATA_USER[id_friends][$index_]);
            $friends=json_encode($DATA_USER[id_friends]);
            update_DB_and ($arr[name_table], array('id_friends'=>$friends), array('id'=>$arr[id]));
        }elseif($status=='add_initial'){
            $DATA_MY[initial][]=$arr[id];
            $initial=json_encode($DATA_MY[initial]);
            update_DB_and ($arr[name_table], array('initial'=>$initial), array('id'=>$ID_USER));
            
            $DATA_USER[incoming][]=$ID_USER;
            $incoming=json_encode($DATA_USER[incoming]);
            update_DB_and ($arr[name_table], array('incoming'=>$incoming), array('id'=>$arr[id]));
        }elseif($status=='del_incoming'){
            $index_=array_search($arr[id],$DATA_MY[incoming]);
            unset($DATA_MY[incoming][$index_]);
            $incoming=json_encode($DATA_MY[incoming]);
            update_DB_and ($arr[name_table], array('incoming'=>$incoming), array('id'=>$ID_USER));
            
            $index_=array_search($ID_USER,$DATA_USER[initial]);
            unset($DATA_USER[initial][$index_]);
            $initial=json_encode($DATA_USER[initial]);
            update_DB_and ($arr[name_table], array('initial'=>$initial), array('id'=>$arr[id]));
        }
    return array('status'=>$status,'ok'=>true);
}

function view_friends($arr){
    $arr[name_table]='friendes';
    $POST=select_DB($arr[name_table], array('id'=>$arr[id]),null);
    $friends=json_decode($POST[0][id_friends]);
    if($friends)
    foreach($friends as $key=>$val){
        $select=select_DB('member', array('id'=>$val), null);
        $result[]=$select[0];
    }
    return $result;
}

function check_friend($arr){
    $arr[name_table]='friendes';
    $POST=select_DB($arr[name_table], array('id'=>$arr[id]),null);
    $POST[0][id_friends]=json_decode($POST[0][id_friends],true);
    $ID_USER_coo=get_cookie('id_user');
    if(!$POST[0][id_friends])return false;
    $index_=array_search($ID_USER_coo,$POST[0][id_friends]);
    
    if($index_===false || !$POST[0][id_friends])
        return false;
    else
        return true;
}

function key_compare_func($key1, $key2)
{
    //echo$key1.', '.$key2.'<br />';
    if ($key1 == $key2)
        return 0;
    else if ($key1!=$key2)
        return 1;
    else
        return 1;
}

function send_news($arr){
    return update_DB_and ('member_login', array('news'=>$arr[type]), array('id'=>get_cookie('id_user')));
}

function search_user($arr){
    $arr[text]=trim(strtolower($arr[text]));
    foreach(explode(' ',$arr[text]) as $val){//якщо 2 слова відіслали чи більше, то шукаємо по всіх словах, що правда шукає все по новій з кожним словом
        $query="SELECT id, last_name, first_name, photo FROM  `member` WHERE  LOWER(last_name) LIKE '$val%' OR LOWER(first_name) LIKE  '$val%' LIMIT 0 , 5";
        $result = mysql_query($query);
        if($result)
        while($row = mysql_fetch_assoc($result)){
            $row[type_search]='user';
            if(!file_exists('../uploads/crop_img/'.$row[photo]))$row[photo]='useremptylogo.png';
            $return[$row[id]]=$row;
        }
        $query="SELECT id, photo, firmenname, first_person FROM  `reg_sponsor` WHERE  LOWER(firmenname) LIKE '$val%' OR LOWER(first_person) LIKE '$val%' LIMIT 0 , 5";
        $result = mysql_query($query);
        if($result)
        while($row = mysql_fetch_assoc($result)){
            $row[type_search]='sponsor';
            if(!file_exists('../uploads/crop_img/'.$row[photo]))$row[photo]='useremptylogo.png';
            $return[$row[id]]=$row;
        }
        $query="SELECT id, name, city, images FROM  `events` WHERE  LOWER(name) LIKE '$val%' OR LOWER(city) LIKE '$val%' AND active=1 ORDER BY id DESC LIMIT 0 , 5";
        $result = mysql_query($query);
        if($result)
        while($row = mysql_fetch_assoc($result)){
            $row[type_search]='event';
            $row[images]=json_decode($row[images],true);
            if(!file_exists('../uploads/'.$row[images][0])){
                $row[images][0]='images/noFotoEvent.jpg';
                $row[folder_photo]='images';
            }
            $return[$row[id]]=$row;
        }
    }
    return $return;
}

function search_friends($arr){
    $friends=view_friends(array('id'=>get_cookie('id_user')));
    $arr[text]=trim(strtolower($arr[text]));
    foreach($friends as $value){
        $str_ids.="`id`='$value[id]'";
    }
    $str_ids = str_replace("'`", "' OR `", $str_ids);
    foreach(explode(' ',$arr[text]) as $val){//якщо 2 слова відіслали чи більше, то шукаємо по всіх словах, що правда шукає все по новій з кожним словом
        $query="SELECT id, last_name, first_name, photo FROM  `member` WHERE  (LOWER(last_name) LIKE '$val%' OR LOWER(first_name) LIKE '$val%') AND ($str_ids) LIMIT 0,10";
        $result = mysql_query($query);
        while($row = mysql_fetch_assoc($result)){
            $return[$row[id]]=$row;
        }
    }
    return $return;
}

function search_event($arr) {
    $arr[text]=trim(strtolower($arr[text]));
    foreach(explode(' ',$arr[text]) as $val){//якщо 2 слова відіслали чи більше, то шукаємо по всіх словах, що правда шукає все по новій з кожним словом
        $query="SELECT id, name, dates, city, images FROM  `events` WHERE  (LOWER(name) LIKE '$val%' OR LOWER(city) LIKE '$val%') AND active=1 LIMIT 0 , 10";
        $result = mysql_query($query);
        while($row = mysql_fetch_assoc($result)){
            $images=json_decode($row[images],true);
            $row[img]=$images[0];
            unset($row[images]);
            $return[$row[id]]=$row;
        }
    }
    return $return;
}

function youtube_video($arr){
/*$arr[comment] = str_replace("watch?v=", "embed/", $arr[comment]);
$arr[comment] = preg_replace("#(https?|ftp)://\S+[^\s.,> )\];'\"!?]#",'<iframe width="560" height="315" src="\\0" frameborder="0" allowfullscreen>\\0</iframe>',$arr[comment]);
preg_match_all();*/
preg_match_all("!v\=([A-z|0-9|\-]*)!", $arr[comment], $url_parts);

foreach($url_parts[1] as $key=>$val){
    $arr[ids][]=$val;
}
return $arr;
}


function empty_block($title,$_USER_INFO){
    switch ($title) {
        case 'profildaten':
        $arr_out[functions]=true;
        $arr_out[firma]=true;
        $arr_out[i_search_for]=true;
        $arr_out[i_offer_field]=true;
        $arr_out[profildaten]=false;
            if(!$_USER_INFO[member][functions])
                $arr_out[functions]=false; else $arr_out[profildaten]=true;
            if(!$_USER_INFO[member][firma])
                $arr_out[firma]=false; else $arr_out[profildaten]=true;
            if(!$_USER_INFO[member][i_search_for])
                $arr_out[i_search_for]=false; else $arr_out[profildaten]=true;
            if(!$_USER_INFO[member][i_offer_field])
                $arr_out[i_offer_field]=false; else $arr_out[profildaten]=true;
            
            return $arr_out;
            break;
        case "textarea":
            if(!$_USER_INFO[member][about_me])
            return true; else return false;
            break;
        case "contacts":
            if(empty_contacts($_USER_INFO))
            return false; else return true;
            break;
        case "berufsvita":
            if(!$_USER_INFO[users_career])
            return true; else return false;
            break;
    }
}
function empty_contacts($_USER_INFO){
    $names=array(
        'adress_privat','postaddress','plz_privat','telephone_privat','fax_privat','phone_privat','email_privat','adress_bus','postadress_bus','plz_bus','telephone_bus','fax_bus','phone_bus','personal_homepage_url','email_bus','facebook_url','xing_url','linked_in_url','twitter_url','instagram','youtube','myvideo','imdb','skype','other_link'
    );
    foreach($names as $value){
        if($_USER_INFO[member][$value])return true;
    }
    return false;
}
//прийняти гостя на подію чи ні із таблиці mitglieder_kandidate
function kandidaten_decision($arr){
    if($arr[status]=='true')
        foreach($arr[id] as $val){
            $select=select_DB('guests',array('id'=>$val));
            $guest_list=select_DB('guest_list',array('id_user'=>$val,'id_event'=>$select[0][event],'type'=>'guests'));
            if($guest_list) continue;
            if($select[0][anrede]=='weiblich'){
                $accost='Frau';
                $sex='geehrte';
            } else {
                $accost='Herr';
                $sex='geehrter';
            }
            $event=select_DB('events',array('id'=>$select[0][event]));
            $message="Sehr $sex $accost ".$select[0][vorname].' '.$select[0][name].", 
            sie sind in die Gästeliste der '".$event[0][name]."' eingertagen. Zusätzliche Informationen bekommen Sie bald.
            Mit freundlichen Grüssen
            Face Club Team";
            $new[id_user]=$val;
            $new[name_table]='guest_list';
            $new[type]='guests';
            $new[id_event]=$select[0][event];
            $return=new_member($new);
            send_mail($select[0][email],'Bewerbung Entscheidung',$message);
            if($return['ok']=='true') $return['ok']=update_DB_and('guests',array('invited'=>1),array('id'=>$val));
            if($return['ok']=='false') {return false; exit;}
        }
    elseif($arr[status]=='false'){
        foreach($arr[id] as $val){
            $select=select_DB('guests',array('id'=>$val));
            if($select[0][anrede]=='weiblich'){
                $accost='Frau';
                $sex='geehrte';
            } else {
                $accost='Herr';
                $sex='geehrter';
            }
            $event=select_DB('events',array('id'=>$select[0][event]));
            $message="Sehr $sex $accost ".$select[0][vorname].' '.$select[0][name].",
            sie sind diesmal leider nicht in die Gästeliste der '".$event[0][name]."' eingetragen.
            Mit freundlichen Grüssen
            Face Club Team";
            send_mail($select[0][email],'Bewerbung Entscheidung',$message);
        }
            $return['ok']=delete_by_id_basket(array('name_table'=>'guests','id'=>$arr[id]));
            if($return['ok']=='false'){
                return $return['ok'];
            }
	}
    return $return['ok'];
}

function cron() {
    $events=select_DB('events',array('active'=>'1'));
    if($events)
    foreach ($events as $value){
        $time=strtotime($value[dates]);
        if(time()>$time)update_DB_and('events',array('active'=>0),array('id'=>$value[id]));
    }
}

function get_posts($arr){
    return select_DB('news', $where, array('id'=>'desc'),false,'limit '.$arr[start].', 5');
}

function friends_finden($arr) {
    if($arr[gender]!='beide')$arr[salutation]=$arr[gender];
    
    $page=$arr[page];
    unset($arr[gender]);
    unset($arr[page]);
    $ID_USER_coo=get_cookie('id_user');
    $FRIENDS=select_DB('friendes',array('id'=>$ID_USER_coo));
    $FRIENDS=json_decode($FRIENDS[0][id_friends],true);
if(!$FRIENDS)return false;
    
    $colums='`first_name`,`last_name`,`firma`,`functions`,`id`,`photo`,`salutation`';
    $query = "SELECT %colums% FROM member";
        foreach($arr as $key=>$val){
            if($key=='search_text' || strpos($val,'all')!==false || $key=='page')continue;
            $str.= "LOWER(`$key`)='".strtolower($val)."'";
        }
        if($str) $str=str_replace("'LOWER", "' AND LOWER", $str);
    if($arr[search_text]){
        $arr[search_text]=strtolower($arr[search_text]); 
        if($str)$str.=" AND ";
        $str.= " (LOWER(`last_name`) LIKE '$arr[search_text]%' OR LOWER(`first_name`) LIKE '$arr[search_text]%')";
    }
    
    
    foreach($FRIENDS as $value){
        $str_fr.="`id`='".$value."'";
    }
    $str_fr = '('.str_replace("'`", "' OR `", $str_fr).')';

    if($str && $str_fr)$str_fr=' AND '.$str_fr;
    
    if($type_acc=='user'){
        $query.=" WHERE `id`!='".$ID_USER_coo."'";
        if($str || $str_fr)$query.=' AND ';
    }elseif($str || $str_fr)
        $query.=" WHERE ";
    
    if($str)$query.=$str;
    if($str_fr)$query.=$str_fr;
    
    @$query_count = mysql_fetch_array(mysql_query(str_replace("%colums%", 'COUNT(*)', $query)));
    $query_count=$query_count[0];
    
    $query = str_replace("%colums%", $colums, $query);
    $page=$page*10-10;
    //echo $query." limit $page,10";
    
    $result = mysql_query($query." limit $page,10"); 
    
    while(@$row = mysql_fetch_assoc($result)){
        if(!file_exists('../uploads/'.$row[photo])){
            $row[photo]='useremptylogo.png';
        }
  
        $kontakten['list'][]=$row;
    }
    $kontakten[count]=$query_count;
    return $kontakten;
}

function crop_img_profile($arr){//мініатюри
session_start();
    if($_SESSION['admin_id'] && $arr[id_user])
        $id_user=$arr[id_user];
    else
    if(get_cookie('id_user'))
        $id_user=get_cookie('id_user');
    elseif(get_cookie('id_sponsor'))
        $id_user=get_cookie('id_sponsor');
        
    if(!$arr[type]){
        if(get_cookie('id_user'))
            $table='member';
        elseif(get_cookie('id_sponsor'))
            $table='reg_sponsor';
    }else{
        if($arr[type]=='user')
            $table='member';
        elseif($arr[type]=='sponsor')
            $table='reg_sponsor';
    }
    
        
    if($arr[img_user]){//явно вказується фото з соціалки при першому обрізанні :)
        $link='../uploads/'.$arr[img_user];
        $link_crop='../uploads/crop_img/'.$arr[img_user];
        if(get_cookie('id_user'))
            update_user_for_user(array('photo_user'=>$arr[img_user]));//така назва тому що в адмінці також є редагування юзера (там копати походу)
        elseif(get_cookie('id_sponsor'))
            update_sponsor_for_sponsor(array('photo'=>$arr[img_user]));
    }else{
        $info_user=select_DB($table,array('id'=>$id_user));
        if($info_user[0][photo]=='useremptylogo.png')return false;
        
        /*з адмінки ці параметри вказуються явно, функція викликається з: admin/ajax/new_member.php, admin/ajax/update_member.php*/
        if($arr[img])$link=$arr[img]; else $link='../uploads/'.$info_user[0][photo];
        if($arr[img_crop])$link_crop=$arr[img_crop]; else $link_crop='../uploads/crop_img/'.$info_user[0][photo];
    }
    
    $INFO=getimagesize($link);
    
    if(empty($arr[img_w])){//якщо налаштування обрізки не передано, то ставимо по дефолту
        $arr[img_w]=$arr[w]=$INFO[0];
        $arr[img_h]=$arr[h]=$INFO[1];
        $arr[x]=$arr[y]=0;
    }
    
    $width=$arr[w]*($INFO[0]/$arr[img_w]);
    $height=$arr[h]*($INFO[1]/$arr[img_h]);
    
    $x=$arr[x]*($INFO[0]/$arr[img_w]);
    $y=$arr[y]*($INFO[1]/$arr[img_h]);
    
    if($arr[img_user])
        crop($link,'big_small',$x,$y,$width,$height);//записуємо мініатюрку і оригінал зображення
    else
	   crop($link,$link_crop,$x,$y,$width,$height);
}

function crop($image,$image_out, $x_o, $y_o, $w_o, $h_o) {//$image_out=big_small - зберегти зміни для оригіналу і мініатюри
    if($image_out=='big_small'){
        $image_arr=explode('/',$image);

        $img=array_pop($image_arr);
        $link=str_replace($img,'',$image);
    }
    list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
    $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
    if ($ext) {
      $func = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
      $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
    } else {
      echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
      return false;
    }
    if ($x_o + $w_o > $w_i) $w_o = $w_i - $x_o; // Если ширина выходного изображения больше исходного (с учётом x_o), то уменьшаем её
    if ($y_o + $h_o > $h_i) $h_o = $h_i - $y_o; // Если высота выходного изображения больше исходного (с учётом y_o), то уменьшаем её
    $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения
    imagecopy($img_o, $img_i, 0, 0, $x_o, $y_o, $w_o, $h_o); // Переносим часть изображения из исходного в выходное
    $func = 'image'.$ext; // Получаем функция для сохранению результата
    if($image_out=='big_small'){
        $func($img_o, $link.'/'.$img);
        $image_out=$link.'/crop_img/'.$img;
    }
        
    return $func($img_o, $image_out); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
}

function register_for_event($arr){
    $array=array('name_table'=>'guest_list','id_event'=>$arr[id],'type'=>'users','id_user'=>get_cookie('id_user'));
    if($arr[accompany])$array[accompany]=$arr[accompany];
    return new_member($array);
}

function application_for_event($arr){
    $info_event=select_DB('events',array('id'=>$arr[id],'active'=>1));
    $info_event=$info_event[0];
    
    if(strtotime($info_event[deadline])<time()){
        exit;
    }
    
    $select_1=select_DB('application_for_event',array('id_event'=>$arr[id],'id_user'=>get_cookie('id_user')));
    $select_2=select_DB('bestatigte',array('id_event'=>$arr[id],'id_user'=>get_cookie('id_user')));
    if(!$select_1 && !$select_2)
        $select_3=select_DB('guest_list',array('id_event'=>$arr[id],'id_user'=>get_cookie('id_user'),'type'=>'users'));
    else return false;
    
    if(!$select_3){
        $array=array('name_table'=>'application_for_event','id_event'=>$arr[id],'id_user'=>get_cookie('id_user'));
        if($arr[accompany])$array[accompany]=$arr[accompany];
        return new_member($array);
    }elseif($select_3[0][status]=='no'){
        //якщо юзер якого запрочили на подію відмовився а потім передумав, то він подає заявку уже як НЕ запрошений на подію
        //якщо потрібно буде його сприймати все ще як запрошеного, то просто глянути функцію, що це робить
        //$result=mysql_query("DELETE FROM `guest_list` WHERE id_event=$arr[id] AND id_user=".get_cookie('id_user')." AND type='users'");
        
        $array=array('name_table'=>'application_for_event','id_event'=>$arr[id],'id_user'=>get_cookie('id_user'));
        if($arr[accompany])$array[accompany]=$arr[accompany];
        return new_member($array);
    }else return false;
}

function bestatigte_for_event($arr){
    $success=select_DB('guest_list',array('id_user'=>get_cookie('id_user'),'id_event'=>$arr[id],'status'=>'view'));
    if(!$success){return false; exit;}
    update_DB_and('guest_list',array('status'=>'ok'),array('id_user'=>get_cookie('id_user'),'id_event'=>$arr[id],'type'=>'users'));
    $select=select_DB('bestatigte',array('id_event'=>$arr[id],'id_user'=>get_cookie('id_user')));
    if(!$select){
        $array=array('name_table'=>'bestatigte','id_event'=>$arr[id],'id_user'=>get_cookie('id_user'));
        if($arr[accompany])$array[accompany]=$arr[accompany];
        if($arr[type])$array[type]=$arr[type];
        return new_member($array);
    }else return false;
}

function guest_list_solution_no($arr){
    update_DB_and('guest_list',array('status'=>'no'),array('id_user'=>get_cookie('id_user'),'id_event'=>$arr[id],'type'=>'users'));
    return true;
}
//Функция Савчука В. - создание новго пользователя из зарегистрированного гостя
function guest_create_user($arr){
    foreach($arr[id] as $value){
        $guest=select_DB('guests',array('id'=>$value));
        $guest=$guest[0];
        $array=array(
            'name_table'=>'member',        
            'first_name'=>$guest[name],
            'last_name'=>$guest[vorname],
            'plz_privat'=>$guest[plz],
            'email_privat'=>$guest[email],
            'telephone_privat'=>$guest[telefon],
            'phone_privat'=>$guest[mobiltelefon],
            'personal_homepage_url'=>$guest[internet_links],
            'photo'=>'useremptylogo.png',
            'birthdate'=>$guest[birthdate],
            'country'=>$guest[land]
        );
        $new_user=new_member($array);
        $ID=$new_user[id];
        
        $pass=rand_string();
        $hash_password = hash("md5",$pass);
        $array=array(
            'name_table'=>'member_login',
            
            'id'=>$ID,
            'email'=>$guest[email],
            'password'=>$hash_password,
            'newsletter_email_addresses'=>1,
            'newsletter_post_addresses'=>1,
            'news'=>1
        );
        new_member($array);
        
        $array=array(
            'name_table'=>'friendes',
            
            'id'=>$ID,
            'id_friends'=>"[]",
            'initial'=>"[]",
            'incoming'=>"[]"
        );
        new_member($array);
        
        $array=array(
            'name_table'=>'access_users_info',
            'id_user'=>$ID,
            'profildaten'=>1,
            'textarea'=>1,
            'privatkontakten'=>1,
            'geschaftskontakten'=>1,
            'netzprofilen'=>1,
            'berufsvita'=>1
        );
        new_member($array);
        
        delete_by_id_basket(array('name_table'=>'guests','id'=>$arr[id]));
        
        send_mail($guest[email],'hello', 'your_password:'.$pass);
    }
    return true;
}

function get_to_sess($arr){
    session_start();
    if(isset($_SESSION[get_str])){
        $str= $_SESSION[get_str];
        unset($_SESSION[get_str]);
        return array('str'=>$str);
    }
    
    if($arr)
    foreach($arr as $key=>$value){
        $_SESSION[get_str].=$key.'='.$value.'&';
    }
    //if($_SESSION)print_r($_SESSION);
    return true;
}


function bestatigte($arr){
    foreach($arr[id] as $value){
        $select=select_DB('application_for_event',array('id'=>$value));
        $select=$select[0];
        //delete_by_id_basket(array('name_table'=>'application_for_event','id'=>$value));
        update_DB_and('application_for_event',array('visible'=>0),array('id'=>$value));
        new_member(array('name_table'=>'bestatigte','id_event'=>$select[id_event],'id_user'=>$select[id_user],'type'=>'angemeldete','accompany'=>$select[accompany]));
    }
    return true;
}

function like_events($id){
    $query = "SELECT `type_of_event`,`city` FROM `events` WHERE `id`=$id";
    $result = mysql_query($query);
    $row = mysql_fetch_assoc($result);
    $type_of_event = $row['type_of_event'];
    $city = $row['city'];
    $q_type_city = "SELECT * FROM `events` WHERE `type_of_event`= '$type_of_event' AND `city` = '$city' AND `id` != $id LIMIT 4";
    $q_type = "SELECT * FROM `events` WHERE `type_of_event`= '$type_of_event' AND `id` != $id LIMIT 4";
    $q_city = "SELECT * FROM `events` WHERE `city` = '$city' AND `id` != $id LIMIT 4";
    $q_all = "SELECT * FROM `events` WHERE `id` != $id LIMIT 4";
    if(check($q_type_city))
        $result = mysql_query($q_type_city);
    elseif(check($q_type))
        $result = mysql_query($q_type);
    elseif(check($q_city))
        $result = mysql_query($q_city);
    else{
        $result = mysql_query($q_all);
    }
    $events = array();
    while($row = mysql_fetch_assoc($result)){
        $events[] = $row;
    }
    return $events;
}

function check($query){
    $result = mysql_query($query);
    $count = 0;
    while($row = mysql_fetch_assoc($result)){
        $count++;
    }
    if($count == 4)
        return $result;
    else
        return false;
}

function check_begleitung($EVENT){

$cities=array(
    'München'=>'muc',
    'Berlin'=>'b',
    'Hamburg'=>'hh'
);
    if($EVENT[type_of_event]=='mmm')
        $success='mmm_'.$cities[$EVENT[city]].'_begleitung';
    else
        $success=$EVENT[type_of_event].'_begleitung';

    $select_user=select_DB('member', array('id'=>get_cookie('id_user')));
    $success=$select_user[0][$success];
    if($success=='JA')return true; else return false;
}

function print_date($date,$type=null){
    
    $format="d.m.Y";
    if($type=='hour'){
        $format="d.m.Y H:i";
        $type=false;
    }
    
    if(!$type){
        $type='unix';
        $dot = array ('.', '/', '-');
        foreach($dot as $val){
            $pos=strpos($date, $val);
            if(!empty($pos)){
                $type='strtotime'; break;
            }
        }
    }
        
    if($type=='strtotime')$date=strtotime($date);
    $date += get_cookie('timezone') * 3600;
    return date($format,$date);
}

function select_guest_einlass($arr){
    if($arr[type]=='users')
        return select_DB('member',array('id'=>$arr[id]));
    else
        return select_DB('guests',array('id'=>$arr[id]));
}

function select_cities($arr){
    $query="SELECT * FROM `cities` WHERE name LIKE '%$arr[city_name]%'";
    $result = mysql_query($query);
    while($row = mysql_fetch_assoc($result))$return[]=$row;
    return $return;
}
//функция Шнурового В.(переписанная из функции Савчука В.) - внесение данных о новом спонсоре
function new_sponsor($arr){
    $success=select_DB('member',array('email_privat'=>$arr['email']));
    if(!$success){
		$success=select_DB('reg_sponsor',array('email'=>$arr['email']));
	}
    if($success){
        return $out_result['ok']='false';
    }
	else{
		$activ=1;
		$password=rand_string();
		$messadge="Guten Tag";
		if(!empty($arr['first_person'])){
			$messadge.=" ,".$arr['first_person'];
		}
		$messadge.="\n Sie sind zum geschlossen sozialen Netzwerk FaceClub hinzugefugt.
		Auf der Seite http://fc.happyuser.info/ konnen Sie sich jetzt anmelden. \n
		password:\n
		$password\n
		Wir wunschen Ihnen angenehme Treffen in unserem Netzwerk!";
		send_mail($arr['email'],'password for sponsor',$messadge);
		$password = hash("md5", $password);
		$columns='activ,password';
		$values="'$activ','$password'";
                
		foreach($arr as $key=>$val){
			if($key=='event_type'){
				$event_type=$arr['event_type'];
			}
			else{
				$columns.=",$key";
				$values.=",'$val'";
			}
		}
		$return=db_insert('reg_sponsor',$columns,$values);
        
		if($return==false){
			$out_result['ok']='false';
		}
		else{
			$id=mysql_insert_id();
            crop_img_profile(array('id_user'=>$id,'type'=>'sponsor'));
			foreach($event_type as $keys=>$value){
				if(!empty($value)){
					db_insert('sponsor_event_type','id_sponsor,event_type',"'$id','$value'");
				}
			}
			$out_result['ok']='true';
		}
        return $out_result;
	}
}

//функция Шнурового В. - удаление данных при удалении спонсора
function delete_by_sponsor($arr){
	$counter=count($arr['id']);
	for($i=0;$i<$counter;$i++){
		if($i==0){
			$where="id='".$arr["id"][$i]."'";
		}
		else{
			$where.=" OR id='".$arr["id"][$i]."'";
		}
	}
	$return=db_delete('reg_sponsor',$where);
	if($return==false){
		$out_result['ok']='false';
	}
	else{
		for($i=0;$i<$counter;$i++){
			if($i==0){
				$where="id_sponsor='".$arr["id"][$i]."'";
			}
			else{
				$where.=" OR id_sponsor='".$arr["id"][$i]."'";
			}
		}
		$return=db_delete('sponsor_event_type',$where);
		if($return==false){
			$out_result['ok']='false';
		}
		else{
			$out_result['ok']='true';
		}
	}
	return $out_result;
}
//функция Шнурового В.(переписанная из функции Савчука В.) - изменение данных о спонсоре
function change_sponsor($arr){
	$i=0;
	foreach($arr as $key=>$val){
		if($key=='event_type'){
			$event_type=$arr['event_type'];
		}
		else{
			if($i==0){
				$set="$key='$val'";
			}
			else{
				$set.=",$key='$val'";
			}
			$i++;
		}
	}
	$id=$arr['id'];
	$return=db_update('reg_sponsor',$set,"id='$id'");
	if($return==false){
		$out_result['ok']='false';
	}
	else{
		$return=db_delete('sponsor_event_type',"id_sponsor='$id'");
		if($return==false){
			$out_result['ok']='false';
		}
		else{		
			foreach($event_type as $keys=>$value){
				if(!empty($value)){
					db_insert('sponsor_event_type','id_sponsor,event_type',"'$id','$value'");
				}
			}
			$out_result['ok']='true';
		}
	}
    crop_img_profile(array('id_user'=>$id,'type'=>'sponsor'));
	return $out_result;
}
//функция Шнурового В. - передача данных о спонсорых в событии
function event_sponsors($arr){
	$id=$arr['id'];
	$event_name=db_select('events','name',"id='$id'");
	$sponsor_in_event=db_select('sponsor_in_event','id_sponsor,type_sponsor',"id_event='$id'");
	$counter=count($sponsor_in_event);
	for($i=0;$i<$counter;$i++){
		$data[$i]['id']=$id;
		$sponsor_name=db_select('reg_sponsor','firmenname',"id='".$sponsor_in_event[$i]['id_sponsor']."'");
		$data[$i]['event']=$event_name[0]['name'];
		$data[$i]['firmenname']=$sponsor_name[0]['firmenname'];
		$data[$i]['type_sponsor']=$sponsor_in_event[$i]['type_sponsor'];
	}
	return $data;
}
//функция Шнурового В. - удаление данных из рекламы
function delete_werbung(){
	db_delete("werbung","id='".$_POST['param1']."'");
}
//функция Шнурового В. - передача данных о типах событий спонсора при редактировании спонсора
function sponsor_event_type_to_paste($arr){
	$id=$arr['id'];
	$sponsor_in_event=db_select('sponsor_event_type','event_type',"id_sponsor='$id'");
    if($sponsor_in_event)
	foreach($sponsor_in_event as $value){
		$data[]=$value;
	}
	$data=array_merge(array('Sponsor Select'),$data);
    return $data;
}
//функция Шнурового В. - передача данных о типах событий спонсора
function sponsor_event_type($arr){
	$id=$arr['id'];
	$sponsor_name=db_select('reg_sponsor','firmenname',"id='$id'");
	$sponsor_in_event=db_select('sponsor_in_event','id_event,type_sponsor',"id_sponsor='$id'");
	$counter=count($sponsor_in_event);
	for($i=0;$i<$counter;$i++){
		$data[$i]['id']=$id;
		$data[$i]['firmenname']=$sponsor_name[0]['firmenname'];
		$event_name=db_select('events','name',"id='".$sponsor_in_event[$i]['id_event']."'");
		$data[$i]['event']=$event_name[0]['name'];
		$data[$i]['type_sponsor']=$sponsor_in_event[$i]['type_sponsor'];
	}
	return $data;
}
//функция Шнурового В. - передача данных о типах событий спонсора
function get_event_type_sponsor($arr){
	$id=$arr['id'];
	$event=db_select('sponsor_event_type','event_type',"id_sponsor='$id'",'event_type');
	$sponsor=db_select('reg_sponsor','firmenname',"id='$id'");
	$counter=count($event);
	for($i=0;$i<$counter;$i++){
		$data[$i]['id']=$id;
		$data[$i]['sponsor']=$sponsor[0]['firmenname'];
		$event_name=db_select('event_type','event_type_name',"event_type='".$event[$i]['event_type']."'");
		$data[$i]['event_name']=$event_name[0]['event_type_name'];
	}	
	return $data;
}
//функция Шнурового В. - создание опций для селектов по выбору спонсоров в event_list при создании события
function event_new_member($arr){
	$id_sponsor=db_select('sponsor_event_type','id_sponsor',"event_type='".$arr['select']."'");
	$counter=count($id_sponsor);
	$option="<option value=''>Sponsor Select</option>";
	for($i=0;$i<$counter;$i++){
		$data=db_select('reg_sponsor','id,firmenname',"id='".$id_sponsor[$i]['id_sponsor']."'");
		$option.="<option value='".$data[0]['id']."'>".$data[0]['firmenname']."</option>";
	}
	return $option;
}
//функция Шнурового В. - создание опций для селектов по выбору спонсоров в event_list при редактировании события
function event_change_event($arr){
	$type_sponsor=array('main_sponsor','sub_sponsor','rest_sponsor');
	$id_sponsor=db_select('sponsor_event_type','id_sponsor',"event_type='".$arr['select']."'");
	$id_sponsor_in_event=db_select('sponsor_in_event','id_sponsor,type_sponsor',"id_event='".$arr['id_event']."'",'type_sponsor');
	$counter=count($id_sponsor);
	$counter_type=count($id_sponsor_in_event);
	if($counter_type<3){
		if($counter_type==1){
			if($id_sponsor_in_event[0]['type_sponsor']!='main_sponsor'){
				if($id_sponsor_in_event[0]['type_sponsor']!='sub_sponsor'){	
					$id_sponsor_in_event[2]=$id_sponsor_in_event[0];
					$id_sponsor_in_event[0]=null;
					$id_sponsor_in_event[1]=null;
                }else{
					$id_sponsor_in_event[1]=$id_sponsor_in_event[0];
					$id_sponsor_in_event[0]=null;
					$id_sponsor_in_event[2]=null;
                }
			}
		}
		else{
			if($id_sponsor_in_event[0]['type_sponsor']!='main_sponsor'){
				$id_sponsor_in_event[2]=$id_sponsor_in_event[1];
				$id_sponsor_in_event[1]=$id_sponsor_in_event[0];
				$id_sponsor_in_event[0]=null;				
			}
			else if($id_sponsor_in_event[1]['type_sponsor']!='sub_sponsor'){
				$id_sponsor_in_event[2]=$id_sponsor_in_event[1];
				$id_sponsor_in_event[1]=null;					
			}
			else{
				$id_sponsor_in_event[2]=null;
			}
		}
		$counter_type=3;
	}
	for($a=0;$a<$counter_type;$a++){
		for($i=0;$i<$counter;$i++){
			$data=db_select('reg_sponsor','id,firmenname',"id='".$id_sponsor[$i]['id_sponsor']."'");
			$b=$a;
			if($a>2){
				$b=2;
			}
			@$option[$a][$type_sponsor[$b]].="<option value='".$data[0]['id']."'";
			if(!empty($id_sponsor_in_event[$a])){
				if($data[0]['id']==$id_sponsor_in_event[$a]['id_sponsor']){
					$option[$a][$type_sponsor[$b]].=" selected";
				}
			}
			$option[$a][$type_sponsor[$b]].=">".$data[0]['firmenname']."</option>";				
		}
        $option[$a][$type_sponsor[$b]]='<option value="">Sponsor Select</option>'.$option[$a][$type_sponsor[$b]];
	}
	return $option;
}

//функція заміни пароля на новий
function change_password($arr){
    $select=select_DB('change_pass',array('hash'=>$arr['hash']));
    if($select)update_DB_and('member_login',array('password'=>$select[0][new_pass]),array('id'=>$select[0][member_id]));
    $query="DELETE FROM `change_pass` WHERE `hash`=".$select[0]['hash'];
    $result = mysql_query($query);
}

function search_guest_list($arr){
    $query="SELECT `member`.`id`,first_name, last_name, firma, functions, accompany FROM `member`,`guest_list` WHERE `member`.`id`=`guest_list`.`id_user` and `id_event`='$arr[event]' AND `type`='users' AND (`status`='ok' OR `status`='was') AND (LOWER (`first_name`) LIKE LOWER('$arr[text]%') OR LOWER (`last_name`) LIKE LOWER('$arr[text]%'))";
    $result = mysql_query($query);
    while($row = mysql_fetch_assoc($result)){
        $row[type_user]='user';
        $return[]=$row;
    }
    $query="SELECT `member`.`id`,first_name, last_name, firma, functions, accompany FROM `member`,`bestatigte` WHERE `member`.`id`=`bestatigte`.`id_user` and `id_event`='$arr[event]' AND (LOWER (`first_name`) LIKE LOWER('$arr[text]%') OR LOWER (`last_name`) LIKE LOWER('$arr[text]%'))";
    $result = mysql_query($query);
    while($row = mysql_fetch_assoc($result)){
        $row[type_user]='user';
        $return[]=$row;
    }
    $query="SELECT `guests`.`id`,vorname, name, funktion_bus, accompany FROM `guests`,`guest_list` WHERE `guests`.`id`=`guest_list`.`id_user` and `id_event`='$arr[event]' AND `type`='guests' AND (`status`='ok' OR `status`='was') AND (LOWER (`vorname`) LIKE LOWER('$arr[text]%') OR LOWER (`name`) LIKE LOWER('$arr[text]%'))";
    $result = mysql_query($query);
    while($row = mysql_fetch_assoc($result)){
        $row[type_user]='guest';
        $return[]=$row;
    }

    return $return;
}

function del_guest(){
    delCookie('hash');
    delCookie('id_guest');    
}
//редактирование админа
function change_admin($arr){
	$id=$arr['id'];
	$i=0;
	$result=db_select('editor',null,"id<>$id and email='".$arr['email']."'");
	if($result){
		$out_result='mail';
	}
	else{
		foreach($arr as $key=>$val){
			if($key!='name_table' and $key!='id'){
				if($key=='sha_password'){
					$result=db_select('editor',null,"id=$id and sha_password<>'$val'");
					if($result){
						$message=$arr['first_name']." ".$arr['last_name']." Ihr Passwort um das Admin-Panel zugreifen - ".$arr['sha_password'].".";
						send_mail($arr['email'],'hello',$message);
						$val=sha1($val);
					}
				}
				if($val){
					if($i==0){
						$set="$key='$val'";
					}
					else{
						$set.=",$key='$val'";
					}
					$i++;
				}
			}
		}
		$out_result=db_update('editor',$set,"id='$id'");
	}
	return $out_result;
}

function check_login(){
    if(get_cookie('id_user') || get_cookie('id_sponsor')){
        session_start();
        if(get_cookie('id_user'))
            $ID=get_cookie('id_user');
        elseif(get_cookie('id_sponsor'))
            $ID=get_cookie('id_sponsor'); 
        if($_SESSION[hash_login_check]!=md5(md5($ID).$_SESSION[hash_login_rand])){
            exit_site();
            header('Location: /user/');
            exit;
        }
    }
}
?>