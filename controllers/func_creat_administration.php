<?php
session_start();
if($_SESSION['status']!='superadmin') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");

	$columns=('id,first_name,last_name,email');
//получение данных из гет строки
	if(count_array_element($_GET, 'filter~')>0){
		$where=get_filter();
	}
	if(isset($_GET['search'])){
		if(isset($where)){
			$where.=" AND ".get_search(null,null,$columns);
		}
		else{
			$where=get_search(null,null,$columns);
		}
	}
	if(isset($_GET['sort'])){
		$order=get_sort();
	}
	else{
		$order="id";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=array('0'=> array(
							'id'=>'0',
							'english'=>'id',
							'german'=>'ID'),
						'1'=> array(
							'id'=>'1',
							'english'=>'first_name',
							'german'=>'Name'),
						'2'=> array(
							'id'=>'2',
							'english'=>'last_name',
							'german'=>'Vorname'),							
						'3'=> array(
							'id'=>'3',
							'english'=>'email',
							'german'=>'Email')
						);
//получение данных для таблиц
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			$columns_for_filter=$header_name[$i]['english'];
			$select_opton_main[]=creat_select_filter('editor',"DISTINCT $columns_for_filter",$where,$columns_for_filter);
		}
		$tables_data=db_select('editor',$columns,$where,$order,$limit);
		$data['pages_number']=creat_number_page(count(db_select('editor','id',$where,$order)));
		$data['header']=header_tables($header_name,$select_opton_main,0,4);
		$data['header_slider']="<th>Superadmin</th><th>Team</th><th>Praktikanten</th><th>Hostess</th>";
		if(count($tables_data)>0){
			$data['table']=tables($tables_data,0,4);
			$data['table_slider']=admin_tables($where,$order,$limit);
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}
//получение данных из таблицы для модального окна
    return $data;

	function admin_tables($where,$order,$limit){
		$id=db_select('editor','id, status',$where,$order,$limit);
		$counter=count($id);
		for($i=0;$i<$counter;$i++){
			@$datas.="<tr class='change' id='".$id[$i]['id']."'>";
			for($a=0;$a<4;$a++){
				$datas.="<td><input type='radio'";
				if($a==0){
					$datas.=" value='superadmin'";
					if($id[$i]['status']=='superadmin'){
						$datas.=" checked";
					}
				}
				else if($a==1){
					$datas.=" value='team'";
					if($id[$i]['status']=='team'){
						$datas.=" checked";
					}
				}
				else if($a==2){
					$datas.=" value='praktikanten'";
					if($id[$i]['status']=='praktikanten'){
						$datas.=" checked";
					}
				}
				else{
					$datas.=" value='hostess'";
					if($id[$i]['status']=='hostess'){
						$datas.=" checked";
					}
				}
				$datas.=" name='status_".$id[$i]['id']."'></td>";
			}
			$datas.="</tr>";
		}
		return $datas;
		
	}
?>