<?php
session_start();
if($_SESSION['status']=='hostess') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");
//получение данных из гет строки
	if(!isset($_GET['id_event'])){
		$id_event=db_select('events','id','active=1','id desc');
		if(!empty($id_event)){
			$_GET['id_event']=$id_event[0]['id'];
		}
	}
	if(count_array_element($_GET, 'filter~')>0){
		$where=get_filter(array('id'=>'g.','all'=>'m.'));
	}
	if(isset($_GET['search'])){
		if(isset($where)){
			$where.=" AND ".get_search('member',array('id'=>'g.','all'=>'m.'));
		}
		else{
			$where=get_search('member',array('id'=>'g.','all'=>'m.'));
		}
	}
	if(isset($_GET['sort'])){
		$order=get_sort(array('id'=>'g.','all'=>'m.'));
	}
	else{
		$order="id";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=db_select('member_column_names',null,null,'id');
//получение данных для таблиц
	if(isset($where)){
		$where_main=$where." AND g.id_user = m.id AND g.id_event='".$_GET['id_event']."' AND g.type='users'";
		$where_modal=$where." AND g.id_event='".$_GET['id_event']."' AND g.type='users'";	
	}
	else{
		$where_main="g.id_user = m.id AND g.id_event='".$_GET['id_event']."' AND g.type='users'";
		$where_modal=" g.id_event='".$_GET['id_event']."' AND g.type='users'";
	}
//$start_all=microtime_float();
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			if($i==0){
				$columns="g.id, g.status";
				$columns_for_filter="g.id";
				$columns_for_filter_modal="m.id";
				$columns_modal="m.id";
			}
			else{
				$columns.=", m.".$header_name[$i]['english'];
				$columns_for_filter="m.".$header_name[$i]['english'];
				$columns_for_filter_modal="m.".$header_name[$i]['english'];
				$columns_modal.=", m.".$header_name[$i]['english'];
			}
			$select_opton_main[]=creat_select_filter('member m,guest_list g',"DISTINCT $columns_for_filter",$where_main,$columns_for_filter);
			$select_opton_modal[]=creat_select_filter("member m LEFT JOIN guest_list g ON m.id=g.id_user AND $where_modal","DISTINCT $columns_for_filter_modal",'g.id_user IS NULL');	
		}
/*
$start=microtime_float();*/
		$tables_data=db_select('member m,guest_list g',$columns,$where_main,$order,$limit);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('tables_data '.$temp);

$start=microtime_float();*/
		$tables_modal=db_select("member m LEFT JOIN guest_list g ON m.id=g.id_user AND $where_modal",$columns_modal,'g.id_user IS NULL',$order,$limit);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('tables_modal '.$temp);

$start=microtime_float();*/
//		$select_opton_main=creat_select_filter('member m,guest_list g',$columns_for_filter,$where_main);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('select_opton_main'.$temp);

$start=microtime_float();*/
//		$select_opton_modal=creat_select_filter('member m',$columns_modal,$where_modal);	
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('select_opton_modal '.$temp);

$start=microtime_float();*/
		$data['pages_number']=creat_number_page(count(db_select('member m,guest_list g','g.id',$where_main,$order)));
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('pages_number '.$temp);

$start=microtime_float();*/
		$data['pages_modal']=creat_number_page(count(db_select('member m','m.id',$where_modal,$order)));
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('pages_modal '.$temp);

$start=microtime_float();*/
		$data['select']=creat_select(db_select('events','id,name','active=1'),$_GET['id_event']);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('select '.$temp);

$start=microtime_float();*/
		$data['header']=header_tables($header_name,$select_opton_main,0,4);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('header '.$temp);

$start=microtime_float();*/
		$data['header_slider']=header_tables($header_name,$select_opton_main,4);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('header_slider '.$temp);

$start=microtime_float();*/
		$data['header_modal']=header_tables($header_name,$select_opton_modal,0,4);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('header_modal '.$temp);

$start=microtime_float();*/
		$data['header_slider_modal']=header_tables($header_name,$select_opton_modal,4);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('header_slider_modal '.$temp);*/
		if(count($tables_data)>0){
//$start=microtime_float();
			$data['table']=tables($tables_data,0,5,'g_ein');
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('table '.$temp);
			
$start=microtime_float();*/
			$data['table_slider']=tables($tables_data,5,null,'g_ein');
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('table_slider '.$temp);*/
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
		if(count($tables_modal)>0){
//$start=microtime_float();
			$data['table_modal']=tables($tables_modal,0,4);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('table_modal '.$temp);
			
$start=microtime_float();*/
			$data['table_slider_modal']=tables($tables_modal,4);
/*$finish=microtime_float();
$temp=$finish-$start;
var_dump('table_slider_modal '.$temp);*/
			}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}
/*$finish_all=microtime_float();
$temp=$finish_all-$start_all;
var_dump('all '.$temp);*/

//получение данных из таблицы для модального окна
    return $data;
?>