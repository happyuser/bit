<?php

// базовий для всіх інших контролер
// абстраткий бо базовий 
abstract class BaseController
{
	
	// заголовок в браузері
	static protected $title = 'FaceBIT';	
	
	// моделі і види будуть підвантажуватися так, як це є прописано в конфігураційному файлі conf.php
	protected $models = array();
	protected $views = array();
	
	// метод, який використовується для виводу в браузер
	protected $outputMethod = 'echoo';
	
	// конструктор - задаються моделі і види
	// якщо в об'єкті є метод actionStart, то він викликається
	// потім з'єднуються два заголовки - той що прописаний тут - в базовому контролері
	// і той, що прописаний в безпосередньому класі об'єкта 	
	public function __construct($modelsArr=array(),$viewsArr=array())
	{
		$this->models = $modelsArr;
		$this->views = $viewsArr;
		if (method_exists($this,'actionStart')) $this->actionStart();
		
		$class = get_called_class();
		if ($class::$title != self::$title)
			self::$title = $class::$title . ' :: ' . self::$title;
	}
	
	// функція перенаправлення 
	public function reloc($newpath)
	{
		Header('Location: '.$newpath);
		die();	
	}
	
	public function actionDefault()
	{
	}
	
	// шапка виводу
	// кожен вид може додавати свої, наприклад, файли стилів і джаваскрипта у заголовок
	protected function echooStart()
	{
		echo '<!DOCTYPE html><html><head><meta charset="utf-8" /><link rel="stylesheet" href="/css/main.css" /><title>'.self::$title.'</title>';
		foreach ($this->views as $view) {
			if (!is_object($view)) continue;
			$view->dict = $GLOBALS['dict'];
			$view->echooHtmlHeadSection();
		}
		echo '</head><body>';
	}
	
	// кінець виводу
	protected function echooFinish()
	{
		echo '</body></html>';	
	}
	
	// стандартний вивід - початку шапка, потім ядро, потім кінець виводу
	public function echoo()
	{
		$this->echooStart();
		if (method_exists($this, 'coreEcho')) $this->coreEcho();
		$this->echooFinish();
	}
	
	// вивід як меседж
	public function echooMessage()
	{
		$this->echooStart();
		$this->views['message']->echoo();
		$this->echooFinish();
	}

	// базова функція виводу
	// вивористовує метод $methodname - див. вище
	public function outputs()
	{
		$methodname = $this->outputMethod;
		$this->$methodname();	
	}
	
	// функція, що повертає вивід
	// завдяки його буферизації 
	public function getOutput()
	{
		ob_start();
		$this->outputs();
		return ob_get_clean();
	}

}

?>