<?php
session_start();
if($_SESSION['status']=='hostess') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");
//получение данных из таблицы
	$where="activ='1' AND invited='0'";
	if(count_array_element($_GET, 'filter~')>0){
		$where.=' AND '.get_filter();
	}
	if(isset($_GET['search'])){
		$where.=' AND '.get_search('member');
	}
	if(isset($_GET['sort'])){
		$order=get_sort();
	}
	else{
		$order="id desc";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=db_select('guest_column_names',null,null,'id');
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			if($i==0){
				$columns.=$header_name[$i]['english'];
			}
			else{
				$columns.=", ".$header_name[$i]['english'];
			}
			$select_opton_main[]=creat_select_filter('guests',"DISTINCT ".$header_name[$i]['english'],$columns,$where,$header_name[$i]['english']);
		}
		$tables_data=db_select('guests',$columns,$where,$order,$limit);
		$data['pages_number']=creat_number_page(count(db_select('guests','id',$where,$order)));
		$data['header']=header_tables($header_name,$select_opton_main,0,4);
		$data['header_slider']=header_tables($header_name,$select_opton_main,4);
		if(count($tables_data)>0){
			$data['table']=tables($tables_data,0,4,'m_kan');
			$data['table_slider']=tables($tables_data,4,null,'m_kan');
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Angaben</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}
    return $data;
?>