<?php
session_start();
if($_SESSION['status']=='hostess') header("Location: login.php");
require_once("lib_db_method.php");
require_once("lib_creat_table.php");
require_once("lib_main.php");
//получение данных из гет строки
	if(count_array_element($_GET, 'filter~')>0){
		$where=get_filter();
	}
	if(isset($_GET['search'])){
		if(isset($where)){
			$where.=" AND ".get_search('reg_sponsor');
		}
		else{
			$where=get_search('reg_sponsor');
		}
	}
	if(isset($_GET['sort'])){
		$order=get_sort();
	}
	else{
		$order="id";
	}
	if(isset($_GET['page'])){
		$limit=get_limit();
	}
	else{
		$limit="0, 10";
	}
	$header_name=db_select('sponsor_column_names',null,null,'id');
//получение данных для таблиц
	if(isset($where)){
		$where_main=$where." AND activ='1'";
	}
	else{
		$where_main="activ='1'";
	}
	if($header_name){
		$count_head=count($header_name);
		for($i=0;$i<$count_head;$i++){
			if($i==0){
				$columns="id";
				$columns_for_filter="id";
			}
			else{
				$columns.=",".$header_name[$i]['english'];
				$columns_for_filter=$header_name[$i]['english'];
			}
			$select_opton_main[]=creat_select_filter('reg_sponsor',"DISTINCT $columns_for_filter",$where_main,$columns_for_filter);
		}
		$tables_data=db_select('reg_sponsor',$columns,$where_main,$order,$limit);
		$data['select_options']=creat_select(db_select('event_type','event_type,event_type_name'));
		$data['select_country']=creat_select(db_select('countries','id,de'));
		$data['pages_number']=creat_number_page(count(db_select('reg_sponsor','id',$where_main,$order)));
		$data['header']=header_tables($header_name,$select_opton_main,0,4);
		$data['header_slider']=header_tables($header_name,$select_opton_main,5);
		if(count($tables_data)>0){
			$data['table']=tables($tables_data,0,4,'spon');
			$data['table_slider']=tables($tables_data,5,null,'spon');
		}
		else{
			$data['empty']="<div class='empty_tab'>Derzeit keine Informationen</div>";
		}
	}
	else{
		$data['empty']="<div class='empty_tab'>Error!</div>"; 
	}
    return $data;
?>