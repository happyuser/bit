<?php

/*
 *
 *
 */

class LoginController extends BaseController 
{
	
public function actionStart()
{
	if (!empty($_SESSION['editor']))
			$this->outputMethod = 'echooLogged';
}

public function actionPost($postValues)
{
	$login_form = true;

	$editor = new MEditor();
	$editor->filter(array('email'=>$postValues['email'],'sha_password'=>sha1($postValues['password'])));
	
	switch ($editor->count) {
		case 1:
			//$editor->getFirst();
			$_SESSION['editor_id'] = $editor->id;
			$_SESSION['editor'] = $editor;
			$this->outputMethod = 'echooLogged';
			break;
		case 0:
			$this->views['login']->error = $GLOBALS['dict']['incorrect_email_or_password'];
			break;
		default:
			throw new EDbConstructionErrror();
	}
}


public function coreEcho()
{
	$this->views['login']->echoo();
}

public function echooLogged()
{
	//$this->views['message']->text =  'Hello, '.$_SESSION['editor']->first_name.' '.$_SESSION['editor']->last_name.'<br /> Your status: '.$_SESSION['editor']->status
	//	. '<br /><a href="/logout/">' . $GLOBALS['dict']['logout'] . '</a>'; 	
	//;
	//$this->echooMessage();
	header('Location: /mitglieder_registered.php');
}

}

?>