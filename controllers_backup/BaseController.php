<?php

abstract class BaseController
{
	
	static protected $title = 'FaceBIT';	
	
	protected $models = array();
	protected $views = array();
	
	protected $outputMethod = 'echoo';
	
	public function __construct($modelsArr=array(),$viewsArr=array())
	{
		$this->models = $modelsArr;
		$this->views = $viewsArr;
		if (method_exists($this,'actionStart')) $this->actionStart();
		
		$class = get_called_class();
		if ($class::$title != self::$title)
			self::$title = $class::$title . ' :: ' . self::$title;
	}
	
	public function reloc($newpath)
	{
		Header('Location: '.$newpath);
		die();	
	}
	
	public function actionDefault()
	{
	}
	
	protected function echooStart()
	{
		echo '<!DOCTYPE html><html><head><meta charset="utf-8" /><link rel="stylesheet" href="/css/main.css" /><title>'.self::$title.'</title>';
		foreach ($this->views as $view) {
			if (!is_object($view)) continue;
			$view->dict = $GLOBALS['dict'];
			$view->echooHtmlHeadSection();
		}
		echo '</head><body>';
	}
	
	protected function echooFinish()
	{
		echo '</body></html>';	
	}
	
	public function echoo()
	{
		$this->echooStart();
		if (method_exists($this, 'coreEcho')) $this->coreEcho();
		$this->echooFinish();
	}
		
	public function echooMessage()
	{
		$this->echooStart();
		$this->views['message']->echoo();
		$this->echooFinish();
	}

	public function outputs()
	{
		$methodname = $this->outputMethod;
		$this->$methodname();	
	}
	
	public function getOutput()
	{
		ob_start();
		$this->outputs();
		return ob_get_clean();
	}

}

?>