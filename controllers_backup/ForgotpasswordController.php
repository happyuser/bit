<?php

class ForgotpasswordController extends BaseController
{

	const SECONDS_TO_WAIT = 600;

	public function actionStart()
	{
		if (!empty($_GET['code'])) {
			$this->outputMethod = 'echooMessage';
			$this->actionFormtochange();
		}	
	}
	
	public function actionFormtochange()
	{
		$this->models['editor']->filter(array('reset_pass_code'=>$_GET['code']));
		if ($this->models['editor']->count!=1) {
			$this->views['message']->text = $GLOBALS['dict']['incorrect_reset_code'];
			return;
		}
		if (time() > strtotime($this->models['editor']->reset_pass_finishtime)) {
			$this->views['message']->text = $GLOBALS['dict']['expired_reset_code'];
			return;
		}
		$postValues = $_POST;
		if (!empty($postValues['new_password'])) {
			if (!preg_match('//',$postValues['new_password'])) {
			}
			if ($postValues['new_password']!=$postValues['confirm_password']) {
				$this->views['newpass_by_code']->error = $GLOBALS['dict']['passwords_do_not_match'];
				$this->views['message']->text = $GLOBALS['dict']['password_do_not_match'];
				return;
			}
			$this->models['editor']->sha_password = sha1($postValues['new_password']);
			$this->models['editor']->update();
			$this->views['message']->text = $GLOBALS['dict']['password_successfully_changed'];
			return;
		}
		$this->views['newpass_by_code']->code = $_GET['code'];
		$this->outputMethod = 'echooFormToChange';
	}	
	
	public function actionPost($postValues)
	{
		
		if (!empty($_GET['code'])) return;		
		
		$isActivationEmailSent = true;		
		$this->models['editor']->filter(array('email'=>$postValues['email']));
		if ($this->models['editor']->count==1) {
			require_once LIB_DIR . '/generate32code.php';
			$this->models['editor']->reset_pass_code = $reset_code = rand_chars();
			$this->models['editor']->reset_pass_finishtime = date('Y-m-d H:i:s', time() + self::SECONDS_TO_WAIT);
			$this->models['editor']->update();
			$isActivationEmailSent = mail(
				$this->models['editor']->email,
				$GLOBALS['dict']['reset_your_password'],
				$GLOBALS['dict']['please_reset_your_password_by_clicking'].
				$_SERVER['SERVER_NAME'].'/passwort-vergessen/?code='.$reset_code
			);			
		}
		if ($isActivationEmailSent) {
			$this->views['message']->text = $GLOBALS['dict']['reset_pass_link_is_sended'];
		} else {
			$this->views['message']->text = $GLOBALS['dict']['error_mail_sending'];
		}
		$this->outputMethod = 'echooMessage';
	}

	public function coreEcho()
	{
		$this->views['forgotpassword']->echoo();
	}

	public function echooFormToChange()
	{
		$this->echooStart();
		$this->views['newpass_by_code']->echoo();
		$this->echooFinish();
	}

}

?>