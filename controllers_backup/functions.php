<?php
require("connect_db.php");

function last_id($table){
    $query = "SELECT * FROM $table WHERE active=1 order by id desc";
    $result = mysql_query($query);
    $row = mysql_fetch_assoc($result);
    return $row[id];
}

function get_get($var)
{
    $var = sanitizeString($_GET[$var]);
    return $var;
}

function get_post($var)
{
    $var = sanitizeString($_POST[$var]);
    return $var;
}

function sanitizeString($var)
{
    $var = mysql_real_escape_string($var);
    $var = stripslashes($var);
    //$var = htmlentities($var, ENT_COMPAT, "cp1251");
    $var = strip_tags($var);
    return $var;
}


function _DB_views($table)
{
    $query = "SELECT * FROM $table";
    $result = mysql_query($query);
    $rows = mysql_num_rows($result);

    for ($i = 0; $i <= $rows; $i++) {
        $row = mysql_fetch_assoc($result);
        $id = $row[id];
        if (!is_array($row) || count($row) == 0) {
            continue;
        }

        foreach ($row as $key => $value) {
            $_DB_events[$id][$key] = $value;
        }
    }
    return $_DB_events;
}

function select_DB($table, $where=null, $order=null){
    $query = "SELECT * FROM $table";
    if($where){
        $query = $query.' WHERE ';
        foreach($where as $key=>$val){
            $query = $query."`$key`='$val'";
        }
        $query = str_replace("'`", "' AND `", $query);
    }
    if($order){
        foreach($order as $key=>$val){
            $query = $query." order by $key $val";
        }
    }
    //echo$query;
    $result = mysql_query($query);

    while($row = mysql_fetch_assoc($result)){

        if (count($row) == 0) {
            continue;
        }

        $_select_DB[] = $row;
    }

    return $_select_DB;
}

function DB_member_filterM($filter_approve = null, $active = null)
    //це фільтр який видає таблицю з АКТИВНИМИ КОРИСТУВАЧАМИ і за ЗАДАНИМ "approve"
{

    global $_DB_member;
    $_DB_member_for_MA = array();
    $id = 1;
    foreach ($_DB_member as $key => $value) {

        if($active===null || $value[active] == $active){
            if (($filter_approve === null or $value[approve] == $filter_approve)) {
                $_DB_member_for_MA[$id][id] = $key;
                foreach ($value as $k => $v) {
                    $_DB_member_for_MA[$id][$k] = $v;
                }
                $id++;
            }
        }
    }
    return $_DB_member_for_MA;
}

function DB_events_filter()
{
    global $_DB_member;
    $_DB_member_for_MA = array();
    $id = 1;
    foreach ($_DB_member as $key => $value) {
        if ($value[active] == 1) {
            $_DB_member_for_MA[$id][id] = $key;
            foreach ($value as $k => $v) {
                $_DB_member_for_MA[$id][$k] = $v;
            }
            $id++;
        }
    }
    return $_DB_member_for_MA;
}


function DB_guest_filter($id_event,$active=null){

    $_DB_member=select_DB('guest_list',array('id_event'=>$id_event));
    if($active!==null){
        $where[active]=$active;
    }
    //print_r($_DB_member);
    //print_r($where)
    $i=0;
        foreach ($_DB_member as $key => $value) {
            $where[id]=$value[id_user];
            $info=select_DB('member',$where);
            if($info){
                $_DB_guest_filter[$i][info_guest]=$value;
                $_DB_guest_filter[$i][info_user]=$info[1];
                $i++;
            }
        }
    return $_DB_guest_filter;
}

function GET_str($str)
{
    $str = explode('=', $str);
    $_GET[$str[0]] = $str[1];
    $i = 0;
    foreach ($_GET as $key => $val) {
        $GET_str = $GET_str . $key . '=' . $val;
        $i++;
        if ($i < count($_GET))
            $GET_str = $GET_str . '& ';
    }
    return $GET_str;
}


function update_user($arr){

$query = "SELECT * FROM member WHERE id=$arr[id]";
$result = mysql_query($query);
$rows = mysql_num_rows($result);
$row = mysql_fetch_assoc($result);
$arr_new_val=array();
foreach($arr as $key=>$val){
    if($key!='no_write'){
        if($key=='photo' && $val=='')continue;
        if($key=='photo_user')$key='photo';
        if($val!=$row[$key])$arr_new_val[$key]=$val;
    }
}

if(!$arr[no_write]){
                $chars = '8bm9e19th2lqqk6i41if083b94';
                $numChars = strlen($chars);
                $string = '';
                for ($i = 0; $i < 10; $i++) {
                    $string .= substr($chars, rand(1, $numChars) - 1, 1);
                }
                $gen_key=$string;

    if($arr_new_val)
    foreach($arr_new_val as $key=>$val){
        if($key=='approve')continue;
        $date=date('Y-m-d',time());
        $query="INSERT INTO `logs_users_edit`(`id`, `id_user`, `type`, `last_val`, `new_val`, `date`, `gen_key`) VALUES (NULL,$arr[id],'$key','$row[$key]','$val','$date','$gen_key')";
        $result=mysql_query($query);
    }
}

    if($arr_new_val)
    {
        $str='';
        $query="UPDATE `member` SET ";
        foreach($arr_new_val as $key=>$val){
            if($val!==false && $key!='id' && $key!='no_write')$str=$str."`".$key."`='".$val."'";
        }
        $str = str_replace("'`", "', `", $str);
        $query=$query.$str." WHERE id=$arr[id]";
        $result=mysql_query($query);
    }

if($result)
$out_result[ok]='true';
else
$out_result[ok]='false';
return $out_result;
}

function edit_access_users_info($arr){
    $_USER_INFO[member][id]=88;//********************************************МАЄ БУТИ провірка ЮЗЕРА, витягування його ІД
    $arr[id_user]=$_USER_INFO[member][id];

    return update_DB_and ('access_users_info', array($arr[type]=>$arr[status]), array('id_user'=>$arr[id_user]));
}

function update_DB_and ($table, $set, $where) {
    foreach($where as $key=>$val){
        $str_where=$str_where."`".$key."`='".$val."'";
    }
        $str_where = str_replace("'`", "' AND `", $str_where);

    foreach($set as $key=>$val){
        $str_set=$str_set."`".$key."`='".$val."'";
    }
        $str_set = str_replace("'`", "', `", $str_set);

    $query="UPDATE `$table` SET $str_set WHERE $str_where";
    $result=mysql_query($query);
    if($result)
$out_result[ok]='true';
else
$out_result[ok]='false';
return $out_result;
}

function update_userJob_for_user($arr){
    $_USER_INFO[member][id]=88;//********************************************МАЄ БУТИ провірка ЮЗЕРА, витягування його ІД
    $arr[id_user]=$_USER_INFO[member][id];

    foreach($arr as $key=>$val){
        if (strripos($key, 'where')!==false && $val){
            $key=explode('-',$key);
            $where[$key[1]]=$arr_result[$key[1]]=$val;
        }
    }
    $arr_result[id]=$arr[id];
    $arr_result[id_user]=$arr[id_user];
    if($arr_result[time_end]=='now')$arr_result[time_end]='bis jetzt';
    $arr_result[result]=update_DB_and('users_career',$where,array('id'=>$arr[id],'id_user'=>$arr[id_user]));
    return $arr_result;
}

function update_user_for_user($arr){

    $_USER_INFO[member][id]=88;//********************************************МАЄ БУТИ провірка ЮЗЕРА, витягування його ІД

    $arr[approve]='0';
    $arr[id]=$_USER_INFO[member][id];
    $out_result=update_user($arr);

return $out_result;
}

function update_event($arr){
$query = "SELECT * FROM events WHERE id=$arr[id]";
$result = mysql_query($query);
$rows = mysql_num_rows($result);
$row = mysql_fetch_assoc($result);

$arr_new_val=array();
foreach($arr as $key=>$val){
    if($val!=$row[$key])$arr_new_val[$key]=$val;
}
    foreach($arr[points] as $key=>$val){
        if($val!='')$arr_new_points[$key]=$val;
    }
    $arr[points]=$arr_new_points;

    $arr[points]=json_encode($arr[points]);
    $str='';
    $query="UPDATE `events` SET ";
    foreach($arr as $key=>$val){
        if($val && $key!='id')$str=$str."`".$key."`='".$val."'";
    }
    $str = str_replace("'`", "', `", $str);
    $query=$query.$str." WHERE id=".$arr[id];

    $result=mysql_query($query);

if($result)
$out_result[ok]='true';
else
$out_result[ok]='false';
return $out_result;
}

function get_event($arr){
    $arr['name_table']='events';
    //print_r($arr);
    $out_result=get_info_DB_id($arr);
    $out_result[DB_member][points]=json_decode($out_result[DB_member][points]);
    return $out_result;
}

function get_info_DB_id($arr){
    $id=$arr['id'];
$query = "SELECT * FROM $arr[name_table] WHERE id=$id";
$result = mysql_query($query);
$rows = mysql_num_rows($result);
$row = mysql_fetch_assoc($result);
if($result){
    $out_result[ok]='true';
    $out_result[DB_member]=$row;
}
else
$out_result[ok]='false';
return $out_result;
}

function new_job($arr){

    $_USER_INFO[member][id]=88;//********************************************МАЄ БУТИ провірка ЮЗЕРА, витягування його ІД
    $VALUES[id_user]=$_USER_INFO[member][id];

    foreach($arr as $key=>$val){
        if (strripos($key, 'where')!==false && $val){
            $key=explode('-',$key);
            $VALUES[$key[1]]=$val;
        }
    }
    $VALUES[name_table]='users_career';
    $arr['new_']=new_member($VALUES);

        $start=explode('-',$arr['where-time_start']);
        if($arr['where-time_end']!='now' && $arr['where-time_end']!='')$end=explode('-',$arr['where-time_end']); else {$end[0]=date('Y',time()); $end[1]=date('m',time()); $arr['where-time_end']='bis jetzt';}
        $years=$end[0]-$start[0];
        $months=$end[1]-$start[1];
        if($months<0){$years-=1; $months=12+$months;}

    $arr['years']=$years;
    $arr['months']=$months;
    $arr['time_start']=$arr['where-time_start'];
    $arr['time_end']=$arr['where-time_end'];
    $arr['firmenname']=$arr['where-firmenname'];
    $arr['position']=$arr['where-position'];
    $arr['aufgaben']=$arr['where-aufgaben'];

    return $arr;
}

function new_member($arr){
    $str=$strVal='';
    $query="INSERT INTO `$arr[name_table]` (";
    foreach($arr as $key=>$val){
        if($key=='sha_password')$val=sha1($val);
        if($key=='name_table')continue;
        if($key=='points'){
            foreach($val as $k=>$v){
                if($v!='')$val_new[$k]=$v;
            }
            $val=json_encode($val_new);
        }
        if($val){
          $str=$str.'`'.$key.'`';
          $strVal=$strVal."'".$val."'";
        }
    }
    $str = str_replace('``', '`, `', $str);
    $strVal = str_replace("''", "', '", $strVal);
    $query=$query.$str.")VALUES ($strVal)";
    $result=mysql_query($query);

if($result){
    $out_result[ok]='true';
    $out_result[id]=mysql_insert_id();
    if($arr[name_table]=='events'){
        new_event(array('id'=>$out_result[id]));
    }
}
else
$out_result[ok]='false';
return $out_result;
}

function get_history_member($arr){
    $id=$arr[id];
$query = "SELECT * FROM logs_users_edit WHERE id_user=$id";
$result = mysql_query($query);
$rows = mysql_num_rows($result);

for($i=0; $i<$rows; $i++){
    $row=mysql_fetch_assoc($result);
        $name = "SELECT * FROM member_column_names WHERE english='$row[type]'";
        $name = mysql_query($name);
        $name = mysql_fetch_assoc($name);
        $row[type]=$name[german];
        $row[type_en]=$name[english];
    $out_result[history][$i]=$row;
}
if($result){
    $out_result[ok]='true';
}
else
    $out_result[ok]='false';
return $out_result;
}

function drop_job($arr){
    $arr[id_user]=$_USER_INFO[member][id]=88;//********************************************МАЄ БУТИ провірка ЮЗЕРА, витягування його ІД

        $query="DELETE FROM `users_career` WHERE id='$arr[id]' AND id_user='".$arr[id_user]."'";
        $result=mysql_query($query);
if($result)
    $out_result[ok]='true';
else
    $out_result[ok]='false';
return $out_result;
}

function delete_by_id_basket($arr){
    $name_table=$arr[name_table];
    foreach($arr[id] as $key=>$val){
        $query="DELETE FROM `$name_table` WHERE id=$val";
        $result=mysql_query($query);
    }

if($result)
    $out_result[ok]='true';
else
    $out_result[ok]='false';
return $out_result;
}

function delete_by_id($arr){
    $data=array();
    $name_table=$arr[name_table];

    foreach($arr[id] as $k=>$v){
        $data[$v][active]=0;
    }


    foreach($data as $key=>$val){
        $str='';
        $query="UPDATE `$name_table` SET ";
        foreach($val as $k=>$v){
            if($k==active)
              $str=$str.'`'.$k.'`="'.$v.'"';
        }
        $str = str_replace('"`', '", `', $str);
        $query=$query."$str WHERE `id`=$key";

        $result=mysql_query($query);
    }
if($result)
    $out_result[ok]='true';
else
    $out_result[ok]='false';
return $out_result;
}

function sending_emails($arr) {
	// Fixed by Vovk.
    /*
    echo "we are here!";
    print_r($arr);
    */
    foreach ($arr[id] as $key => $val) $str_types .= "`member`.`distribution`='$val'";
    $str_types = str_replace("'`", "' OR `", $str_types);
    //$name = "SELECT * FROM member WHERE $str_types";
    $q = "SELECT * FROM `member`, `guest_list` WHERE (
(`guest_list`.`id_user` = `member`.`id`)
AND (`guest_list`.`id_event` = '$arr[for]')
AND ($str_types))";
    $result = mysql_query($q);
/*
    $rows = mysql_num_rows($result);
    for ($i = 1; $i <= $rows; $i++) {
        $row = mysql_fetch_assoc($result);
        $emails .= $emails ."|$row[last_name] <$row[private_email]>|";
    }
    $emails = str_replace("||", ", ", $emails);
    echo $emails = str_replace("|", "", $emails);
*/
	$a = array();
	while (($row = mysql_fetch_assoc($result)) !== false)
		$a[] = "$row[last_name] <$row[email_privat]>";
    $emails = implode(', ', $a);
    $subject = "тема";
    $message = $arr[html];
    $headers  = "Content-type: text/html; charset=utf-8 \r\n";
    $headers .= "From: Birthday Reminder <vaylon@yandex.ru>\r\n";
    $headers .= "Bcc: uptofly73@gmail.com\r\n";
    mail($emails, $subject, $message, $headers) . "\n"; // тут вертає 1, значить відправка успішна
}

function save_emails($arr){
    foreach($arr[id] as $key=>$val){
        $str_types=$str_types."`id`='$val'";
    }
    $str_types = str_replace("'`", "' OR `", $str_types);
    $arr[html] = str_replace("'", '"', $arr[html]);
    $name = "UPDATE `message_type_account` SET message='$arr[html]' WHERE $str_types";
    $result = mysql_query($name);
    if($result){
        $out_result[ok]='true';
    }
    return $out_result;
}

function change_user_for_event($arr){
    foreach($arr[id] as $key=>$val){
        $query="SELECT * FROM `guest_list` WHERE id_event=$arr[id_event] AND id_user=$val";
        $result=mysql_query($query);
        $rows = mysql_num_rows($result);

        if(!$rows){
            $query="INSERT INTO `guest_list`(`id`, `id_event`, `id_user`) VALUES (NULL,$arr[id_event],$val)";
            $result=mysql_query($query);
            if($result){
                $info[mysql_insert_id()]=get_info_DB_id(array("id"=>$val, "name_table"=>'member'));
            }
        }
    }

if($result){
    $out_result[ok]='true';
    $out_result[list_new_user]=$info;
}
else
    $out_result[ok]='false';
return $out_result;
}

function edit_user_for_event($arr){
    //print_r($arr);
    $str='';
    foreach($arr as $key=>$val){
        $query="UPDATE `guest_list` SET ";
            if($key!='id')
              $str=$str.'`'.$key.'`="'.$val.'"';
        }
        $str = str_replace('"`', '", `', $str);
        $query=$query."$str WHERE `id`=$arr[id]";
        $result=mysql_query($query);

if($result){
    $out_result[ok]='true';
}
else
    $out_result[ok]='false';
return $out_result;
}

function edit_admin($arr){
    //print_r($arr);
    $str='';
    foreach($arr as $key=>$val){
        $query="UPDATE `editor` SET ";
            if($key=='id')continue;

            if(strripos($key, 'status')!==false)$key='status';
              $str=$str.'`'.$key.'`="'.$val.'"';
        }
        $str = str_replace('"`', '", `', $str);
        $query=$query."$str WHERE `id`=$arr[id]";
        $result=mysql_query($query);

if($result){
    $out_result[ok]='true';
}
else
    $out_result[ok]='false';
return $out_result;
}

function get_option_select(){

$OPTIONS_MEMBER=array(
    'kart_ammount'=>array(
        0,
        1,
        2,
        3,
        '2 außer bei MmM Berlin & München, da 1',
        '2 außer bei DC Berlin & MmM Berlin, da 1',
        '1 außer GSA, da 2',
        '1 außer Hamburg, da 2',
        '1 außer MumM & GSA, da 2',
        '1 außer MumM, da 2',
        '2 außer Berlin, da 1',
        '1 außer MmM Hamburg, da 2',
        '2 außer MmM, da 1',
        '2 außer MmM Berlin, da 1',
        '2 außer DC, dort nur 1',
        '2 außer MmM München, da 1',
        '2 außer HERBERT, da 1',
        '2 außer MmM Berlin & München, da 1',
        '2 außer Hamburg, da 1',
        '2 außer München, da 1',
        'Entscheidet die PR-Agentur'
    ),
    'function'=>array(
        'e&m',
        'FdH',
        'WP',
        'SP',
        'VIP1',
        'VIP2',
        'Kein1',
        'FP',
        'P-VIP',
        'P',
        'S',
        'A',
        'R',
        'Z',
        'KL',
        'Musiker',
        'SportVip1',
        'SportVip2'
    ),
    'distribution'=> array(
        'e&mkunde',
        'FDH',
        'FP Verteiler',
        'INTERNETINVESTOR',
        'Künstleragenten',
        'Mediaagenturen',
        'MUSIKER',
        'P- VIP',
        'Regisseure',
        'SOAP',
        'SONSTIGE',
        'SP Firmen',
        'SPORTVIP1',
        'SPORTVIP2',
        'VIP1',
        'VIP2',
        'WP Verteiler',
        'Z',
    ),
    'invitation'=> array(
        'E/P',
        'E-Mail',
        'Post'
    ),
    'travelcosts'=> array(
        'Hotel',
        'Hotel beim HERBERT',
        'Hotel Hamburg & MumM',
        'JA',
        'JA Hotel HH',
        'JA, bei MumM Hotel',
        'JA, Berlin & München MmM',
        'JA, für HH',
        'JA, HERBERT',
        'JA, HH',
        'JA, HH & MumM',
        'JA, HH Hotel',
        'Ja, HH Hotel & MumM',
        'JA, HH MmM',
        'JA, Hotel beim HERBERT',
        'JA, Hotel HH',
        'JA, Hotel MmM HH',
        'JA, HOTEL MumM',
        'JA, HoTEL MumM & MmM HH',
        'JA, MmM HH',
        'JA, MmM HH & HERBERT',
        'JA, MmM-HH',
        'JA, MumM',
        'JA, München & HH',
        'MmM-HH Reise inkl. Hotel',
        'MumM Reise inkl. Hotel',
        'NEIN',
        'Hotel bei GSA'
    ),
    'time_invited'=> array(
        'ab 21 Uhr',
        'ab 22 Uhr ',
        'ab 23 Uhr',
        'BEGINN'
    ),
    'mmm'=> array(
        'JA',
        'JA & zusätzliches Kaufkartenmailing',
        'JA, ab 21 Uhr',
        'JA, Berlin',
        'JA, Berlin & Hamburg',
        'JA, Berlin & München',
        'JA, Hamburg',
        'JA, Hamburg & München',
        'JA, Hamburg ab 21 Uhr',
        'JA, München',
        'JA, München & Hamburg ab 21 Uhr',
        'JA, München, wenn nicht Sponsor',
        'JA, wenn nicht Sponsor',
        'JA, wenn Pro7 NICHT Sponsor ist',
        'JA, wenn VW NICHT Sponsor ist',
        'KAUFKARTEN',
        'nach Bedarf',
        'NEIN',
        'NEIN, weil Sponsor',
        'Über PR-AGENTUR'
    ),
    'centurion'=> array(
        'JA',
        'NEIN'
    ),
    'dc'=> array(
        'JA',
        'JA, ab 21 Uhr',
        'JA, ab Beginn',
        'JA, BERLIN',
        'JA, BERLIN & MÜNCHEN',
        'JA, Hamburg',
        'JA, Hamburg & München',
        'JA, HH ab 21 Uhr',
        'JA, HH ab Beginn',
        'JA, München',
        'JA, München & Hamburg',
        'JA, München & HH ab Beginn',
        'JA, MÜNCHEN ab 21 Uhr',
        'JA, München ab Beginn',
        'JA, München sofern nicht Sponsor',
        'JA, sofern Bavaria nicht Sponsor ist',
        'JA, wenn nicht Sponsor',
        'KAUFKARTEN',
        'nach Bedarf',
        'NEIN',
        'nur bei Bedarf',
        'Über PR-AGENTUR',
        'JA & zusätzliches Kaufkartenmailing'
    ),
    'soap'=>array(
        'Als JURY Mitglied anfragen',
        'Als Laudator anfragen',
        'JA',
        'JA, ab 21 Uhr',
        'JA, ab Beginn',
        'JA, Berlin',
        'JA, Hamburg',
        'JA, München',
        'JA, wenn nicht Sponsor',
        'KAUFKARTEN',
        'nach Bedarf',
        'NEIN',
        'Über PR-AGENTUR'
    ),
    'mumm'=>array(
        'JA',
        'JA & zusätzlich Kaufkartenmailing schicken',
        'JA, ab Beginn',
        'JA, ab 21 Uhr',
        'Ja, wenn nicht Sponsor',
        'KAUFKARTEN',
        'NEIN',
        'Ja, wenn nicht von Pro7 eingeladen',
        'ÜBER PR-AGENTUR'
    ),
    'herbert'=>array(
        'Flanier',
        'JA',
        'JA, ab Beginn',
        'JA, ohne Begleitung',
        'JA, sofern nicht Sponsor',
        'KAUFKARTE',
        'nach Bedarf',
        'NEIN',
        'JA, wenn nicht Sponsor',
        'JA, wenn Sky nicht Sponsor ist',
        'ÜBER PR-AGENTUR',
        'JA, wenn BILD nicht Sponsor ist'
    ),
    'cnn'=>array(
        'JA',
        'NEIN',
        'Über PR-AGENTUR'
    ),
    'yearcalendar'=>array(
        'JA',
        'NEIN'
    ),
    'proposition'=>array(
        'Barterdeals',
        'BERLIN EVENTS',
        'CNN, HERBERT',
        'CNN, MmM, HERBERT',
        'DC',
        'DC, MumM',
        'DC, CNN',
        'DC, CNN, HERBERT',
        'DC, MmM',
        'DC, MumM, Soap',
        'DC, SOAP',
        'DC, Soap, MumM, HERBERT',
        'FB Postings',
        'GSA',
        'GSA & MumM',
        'HERBERT',
        'HERBERT & MmM',
        'Hamburg Events',
        'JA',
        'KAUFKARTE',
        'MmM',
        'Kartenkauf; Herbert',
        'Kaufkarten & MumM, Herbert',
        'Mediadeals',
        'Mediavolumen',
        'MmM',
        'MmM & MumM',
        'MmM Muc',
        'MmM, CNN',
        'MumM',
        'NEIN',
        'MumM, GSA',
        'PR-leistungen',
        'Promivermittlung',
        'Shuttle-Sponsoring',
        'GSA',
        'GSA, DC, MumM'
    ),
    'personal'=>array(
        'JA',
        'NEIN'
    ),
    'du_or_sie'=>array(
        'SIE',
        'DU'
    )
);
return $OPTIONS_MEMBER;
}

function new_event($arr){
    $query="SELECT * FROM  `events` WHERE  `id` =$arr[id]";
    $result = mysql_query($query);
    $row = mysql_fetch_assoc($result);
    $type_event=$row[type_of_event];

    $query="SELECT * FROM `member` WHERE active=1 AND `$type_event` LIKE '%JA%'";
    $result = mysql_query($query);
    $rows = mysql_num_rows($result);
    for($i=1; $i<=$rows; $i++){
        $row = mysql_fetch_assoc($result);
        $query2="INSERT INTO `guest_list`(`id`, `id_event`, `id_user`) VALUES (NULL,$arr[id],$row[id])";
        $result2 = mysql_query($query2);
    }
	if ($result) $out_result[ok]='true';
	return $out_result;
}

function edit_registration($arr){
    $time=time();

    $query = "UPDATE `guest_list` SET `time_at_event`='$time',`accompany`='$arr[accompany]' WHERE id_user=$arr[id_user] AND id_event=$arr[id_event]";
    $result = mysql_query($query);
    if($result){
        $out_result[ok]='true';
    }
    return $out_result;
}

function update_admin_status($arr){
    $query="UPDATE `editor` SET status='$arr[status]' WHERE id=$arr[id]";
    $result=mysql_query($query);

if($result)
$out_result[ok]='true';
else
$out_result[ok]='false';
return $out_result;
}


function edit_member_approve($arr){
    foreach($arr[id] as $key=>$val){
        $history=get_history_member(array('id'=>$val));
        $key=$history[history][count($history[history])-1][gen_key];
        foreach($history[history] as $k=>$v){
            if($key==$v[gen_key]){
                if($arr[type]=='true'){
                    $status=update_user(array('id'=>$v[id_user],'no_write'=>'true','approve'=>'0'));
                }else{
                    $status=update_user(array('id'=>$v[id_user],'no_write'=>'true',$v[type_en]=>$v[last_val],'approve'=>'0'));
                    if($status[ok]=='true')
                    $status=delete_by_id_basket(array('id'=>array($v[id]), 'name_table'=>'logs_users_edit'));
                }
            }
        }
    }

if($status[ok]=='true')
$out_result[ok]='true';
else
$out_result[ok]='false';
return $out_result;
}

function if_access_info($type){
    global $_USER_INFO;
    if($_USER_INFO[access_users_info][$type]==='0'){
        $class='no_access';
        $i='';
    }else{
        $class='';
        $i='icon-white';
    }
    $str="
    <div class=\"block_info $class\" for=\"$type\" style=\"margin:0;\">
        <i class=\"icon-lock $i\"></i>
    </div>";
    return $str;
}

// Created by Vovk.
// data - масив елементів які потрібно змінити.
function change_user_login_data($data) {
//	if (!isset($_COOKIES['user_id'])) return false;
	$id = 88; // $_COOKIES['user_id'];
	$q = "UPDATE member_login SET ";
	foreach ($data as $k => $v) {
		$value = mysql_real_escape_string($v);
		if (strpos($q, "=") !== false) $q .= ", ";
		$q .= ("$k=" . (($k == 'password') ? "ENCRYPT('$value')" : "'$value'"));
	}
	$q .= " WHERE id=$id";
	return mysql_query($q);
}

// Created by Vovk.
// FindEvents шукає події по назві. Якщо знайдених подій < 4 то вони фільтруються по даті.
function FindEvents($name, $date) {
	$result = array();
	$q = "SELECT * from events WHERE name='" . mysql_real_escape_string($name) . "'";
	if (($r = mysql_query($q)) !== FALSE) {
		while (($a = mysql_fetch_assoc($r)) !== FALSE) $result[] = $a;
		mysql_free_result($r);
		if (count($result) < 4) {
			foreach ($result as $key => $value) {
				if ($value['dates'] != $date) unset($result[$key]);
			}
		}
	} else print "Error in: $q<br />";
	return $result;
}

// Created by Vovk.
// check_pass перевіряє чи правильний пароль.
// Якщо пароль правильний, $access буде рівна OK. Інакше $access буде рівна ERROR.
function check_pass($email, $password, &$access) {
//	print "$email\n$password\n";
	$q = "SELECT id from member_login WHERE (email='" . mysql_real_escape_string($email) .
		"') and (password=PASSWORD('" . mysql_real_escape_string($password) . "'))";
	if (($r = mysql_query($q)) !== FALSE) {
//		print_r(mysql_fetch_assoc($r)) . "\n";
//		exit;
		$access = (mysql_num_rows($r) ? 'OK' : 'ERROR');
		mysql_free_result($r);
	} else {
		$access = 'ERROR';
		print "Error in: $q\n" . mysql_error() . "\n";
	}
}

function change_user_password($arr) {
//	if (!isset($_COOKIES['user_id'])) return false;
	$id = 88; // $_COOKIES['user_id'];
	if (mysql_query("UPDATE member_login SET password=PASSWORD('" .
		mysql_real_escape_string($arr['new_password']) . "') WHERE id=$id"))
		return array('ok' => 'true'); else return array('ok' => 'false');
}
?>